package jmn.services;

import java.util.Date;
import java.util.List;

import jmn.models.DnevnoStanje;
import jmn.models.Racun;

public interface DnevnoStanjeService {

	List<DnevnoStanje> findAll();

	DnevnoStanje saveDnevnoStanje(DnevnoStanje dns);

	DnevnoStanje findOne(Long id);

	void delete(Long id);

	void delete(DnevnoStanje dns);

	DnevnoStanje findByDatumIzvodaAndRacun(Date datumIzvoda, Racun racun);

	List<DnevnoStanje> findByDatumIzvoda(Date datumIzvoda);
	
	List<DnevnoStanje> findByRacun(Racun racun);
}
