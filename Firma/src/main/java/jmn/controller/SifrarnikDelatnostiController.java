package jmn.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;

import jmn.annotation.PermissionType;
import jmn.models.Drzava;
import jmn.models.SifrarnikDelatnosti;
import jmn.services.SifrarnikDelatnostiService;

@Controller
public class SifrarnikDelatnostiController {

	@Autowired
	private SifrarnikDelatnostiService sdsr;
	
	@PermissionType("SifrarnikDelatnosti:view")
	@RequestMapping(value="/delatnosti/", method=RequestMethod.GET)
	public ResponseEntity<List<SifrarnikDelatnosti>> fetchDelatnosti(HttpSession session){
		List<SifrarnikDelatnosti> delatnosti = sdsr.findAll();
		if(delatnosti.isEmpty())
			return new ResponseEntity<List<SifrarnikDelatnosti>>(HttpStatus.NO_CONTENT);
		return new ResponseEntity<List<SifrarnikDelatnosti>>(delatnosti, HttpStatus.OK);
	}
	
	@PermissionType("SifrarnikDelatnosti:add")
	@RequestMapping(value = "/delatnosti/", method = RequestMethod.POST)
	public ResponseEntity<Void> createSifrarnikDelatnosti(HttpSession session, @RequestBody SifrarnikDelatnosti sd, UriComponentsBuilder ucBuilder) {
	        System.out.println("Creating Sifrarnik Delatnosti " + sd.getNazivDelatnosti());
	  
	        SifrarnikDelatnosti d = sdsr.saveSifrarnikDelatnosti(sd);
	        try {
		        HttpHeaders headers = new HttpHeaders();
		        headers.setLocation(ucBuilder.path("/delatnosti/{id}").buildAndExpand(d.getId()).toUri());
		        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	        }
	        catch(Exception e){
	        	return new ResponseEntity<Void>(HttpStatus.CONFLICT);
	        }
	    }
	@PermissionType("SifrarnikDelatnosti:edit")
	 @RequestMapping(value = "/delatnosti/{id}", method = RequestMethod.POST)
	    public ResponseEntity<?> updateSifrarnikDelatnosti(HttpSession session, @PathVariable("id") long id, @RequestBody SifrarnikDelatnosti sd) {
	 
		SifrarnikDelatnosti exists = sdsr.findOne(id);
		 	if(exists == null){
		 		return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		 	}
		 	exists.setNazivDelatnosti(sd.getNazivDelatnosti());
		 	exists.setSifra(sd.getSifra());
		 	exists = sdsr.saveSifrarnikDelatnosti(exists);
		 	return new ResponseEntity<SifrarnikDelatnosti>(exists, HttpStatus.OK);
	    }
	
	@PermissionType("SifrarnikDelatnosti:delete")
	@RequestMapping(value = "/delatnosti/{id}", method = RequestMethod.DELETE)
	    public ResponseEntity<?> deleteSifrarnikDelatnosti(HttpSession session, @PathVariable("id") long id) {
		SifrarnikDelatnosti exists = sdsr.findOne(id);
		 if(exists == null)
			 return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		 
		 sdsr.delete(id);
	     return new ResponseEntity<SifrarnikDelatnosti>(HttpStatus.NO_CONTENT);
	    }
	
	@PermissionType("SifrarnikDelatnosti:search")
	@RequestMapping(value = "/delatnosti/search/", method = RequestMethod.POST)
	    public ResponseEntity<?> searchSifrarnikDelatnosti(HttpSession session, @RequestBody SifrarnikDelatnosti sd) {
		 if(sd.getNazivDelatnosti() == null)
			 sd.setNazivDelatnosti("");
		 if(sd.getSifra() == null)
			 sd.setSifra("");
		 List<SifrarnikDelatnosti> delatnosti = sdsr.findByNazivDelatnostiAndSifra(sd.getNazivDelatnosti(), sd.getSifra());
		 if(delatnosti == null)
			 return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);

	     return new ResponseEntity<List<SifrarnikDelatnosti>>(delatnosti, HttpStatus.OK);
	    }
}
