package jmn.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import jmn.models.Banka;
import jmn.models.Preduzece;
import jmn.models.Racun;

public interface RacunRepository extends CrudRepository<Racun, Long> {

	public List<Racun> findByPreduzeceAndBanka(Preduzece preduzece, Banka banka);

	public List<Racun> findByPreduzece(Preduzece preduzece);

	public Racun findByBrRacuna(String brRacuna);
}
