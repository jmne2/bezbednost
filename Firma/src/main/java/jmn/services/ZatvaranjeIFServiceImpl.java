package jmn.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jmn.models.Faktura;
import jmn.models.StavkaIzvoda;
import jmn.models.ZatvaranjeIF;
import jmn.repositories.ZatvaranjeIFRepository;

@Service
public class ZatvaranjeIFServiceImpl implements ZatvaranjeIFService{

	@Autowired
	private ZatvaranjeIFRepository zr;
	
	@Override
	public List<ZatvaranjeIF> findAll() {
		return (List<ZatvaranjeIF>)zr.findAll();
	}

	@Override
	public ZatvaranjeIF saveZatvaranjeIF(ZatvaranjeIF z) {
		return zr.save(z);
	}

	@Override
	public ZatvaranjeIF findOne(Long id) {
		return zr.findOne(id);
	}

	@Override
	public void delete(Long id) {
		zr.delete(id);
	}

	@Override
	public void delete(ZatvaranjeIF z) {
		zr.delete(z);
	}

	@Override
	public List<ZatvaranjeIF> findByIzlaznaFaktura(Faktura izlaznaFaktura) {
		return zr.findByIzlaznaFaktura(izlaznaFaktura);
	}

	@Override
	public List<ZatvaranjeIF> findByStavkaIzvoda(StavkaIzvoda stavkaIzvoda) {
		return zr.findByStavkaIzvoda(stavkaIzvoda);
	}

}
