
package izvod;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the izvod package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SetBankName_QNAME = new QName("http://izvod", "setBankName");
    private final static QName _ReceiveMT102AndMT910_QNAME = new QName("http://izvod", "receiveMT102AndMT910");
    private final static QName _Clearing_QNAME = new QName("http://izvod", "clearing");
    private final static QName _SendNalogResponse_QNAME = new QName("http://izvod", "sendNalogResponse2");
    private final static QName _SetBankCodeResponse_QNAME = new QName("http://izvod", "setBankCodeResponse");
    private final static QName _ClearingResponse_QNAME = new QName("http://izvod", "clearingResponse");
    private final static QName _GetIzvod_QNAME = new QName("http://izvod", "getIzvod");
    private final static QName _ReceiveNalogNbsResponse_QNAME = new QName("http://izvod", "receiveNalogNbsResponse");
    private final static QName _ReceiveNalogNbs_QNAME = new QName("http://izvod", "receiveNalogNbs");
    private final static QName _ReceiveMT102AndMT910Response_QNAME = new QName("http://izvod", "receiveMT102AndMT910Response");
    private final static QName _GetIzvodResponse_QNAME = new QName("http://izvod", "getIzvodResponse");
    private final static QName _SetBankCode_QNAME = new QName("http://izvod", "setBankCode");
    private final static QName _Clearing2_QNAME = new QName("http://izvod", "clearing2");
    private final static QName _Clearing2Response_QNAME = new QName("http://izvod", "clearing2Response");
    private final static QName _SetBankNameResponse_QNAME = new QName("http://izvod", "setBankNameResponse");
    private final static QName _SendNalog_QNAME = new QName("http://izvod", "sendNalog2");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: izvod
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SendNalogResponse }
     * 
     */
    public SendNalogResponse createSendNalogResponse() {
        return new SendNalogResponse();
    }

    /**
     * Create an instance of {@link SetBankCodeResponse }
     * 
     */
    public SetBankCodeResponse createSetBankCodeResponse() {
        return new SetBankCodeResponse();
    }

    /**
     * Create an instance of {@link ClearingResponse }
     * 
     */
    public ClearingResponse createClearingResponse() {
        return new ClearingResponse();
    }

    /**
     * Create an instance of {@link SetBankName }
     * 
     */
    public SetBankName createSetBankName() {
        return new SetBankName();
    }

    /**
     * Create an instance of {@link ReceiveMT102AndMT910 }
     * 
     */
    public ReceiveMT102AndMT910 createReceiveMT102AndMT910() {
        return new ReceiveMT102AndMT910();
    }

    /**
     * Create an instance of {@link Clearing }
     * 
     */
    public Clearing createClearing() {
        return new Clearing();
    }

    /**
     * Create an instance of {@link GetIzvodResponse }
     * 
     */
    public GetIzvodResponse createGetIzvodResponse() {
        return new GetIzvodResponse();
    }

    /**
     * Create an instance of {@link SetBankCode }
     * 
     */
    public SetBankCode createSetBankCode() {
        return new SetBankCode();
    }

    /**
     * Create an instance of {@link ReceiveMT102AndMT910Response }
     * 
     */
    public ReceiveMT102AndMT910Response createReceiveMT102AndMT910Response() {
        return new ReceiveMT102AndMT910Response();
    }

    /**
     * Create an instance of {@link Clearing2 }
     * 
     */
    public Clearing2 createClearing2() {
        return new Clearing2();
    }

    /**
     * Create an instance of {@link Clearing2Response }
     * 
     */
    public Clearing2Response createClearing2Response() {
        return new Clearing2Response();
    }

    /**
     * Create an instance of {@link SetBankNameResponse }
     * 
     */
    public SetBankNameResponse createSetBankNameResponse() {
        return new SetBankNameResponse();
    }

    /**
     * Create an instance of {@link SendNalog }
     * 
     */
    public SendNalog createSendNalog() {
        return new SendNalog();
    }

    /**
     * Create an instance of {@link GetIzvod }
     * 
     */
    public GetIzvod createGetIzvod() {
        return new GetIzvod();
    }

    /**
     * Create an instance of {@link ReceiveNalogNbsResponse }
     * 
     */
    public ReceiveNalogNbsResponse createReceiveNalogNbsResponse() {
        return new ReceiveNalogNbsResponse();
    }

    /**
     * Create an instance of {@link ReceiveNalogNbs }
     * 
     */
    public ReceiveNalogNbs createReceiveNalogNbs() {
        return new ReceiveNalogNbs();
    }

    /**
     * Create an instance of {@link Date }
     * 
     */
    public Date createDate() {
        return new Date();
    }

    /**
     * Create an instance of {@link Nalog }
     * 
     */
    public Nalog createNalog() {
        return new Nalog();
    }

    /**
     * Create an instance of {@link ArrayList }
     * 
     */
    public ArrayList createArrayList() {
        return new ArrayList();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetBankName }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://izvod", name = "setBankName")
    public JAXBElement<SetBankName> createSetBankName(SetBankName value) {
        return new JAXBElement<SetBankName>(_SetBankName_QNAME, SetBankName.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReceiveMT102AndMT910 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://izvod", name = "receiveMT102AndMT910")
    public JAXBElement<ReceiveMT102AndMT910> createReceiveMT102AndMT910(ReceiveMT102AndMT910 value) {
        return new JAXBElement<ReceiveMT102AndMT910>(_ReceiveMT102AndMT910_QNAME, ReceiveMT102AndMT910 .class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Clearing }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://izvod", name = "clearing")
    public JAXBElement<Clearing> createClearing(Clearing value) {
        return new JAXBElement<Clearing>(_Clearing_QNAME, Clearing.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendNalogResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://izvod", name = "sendNalogResponse")
    public JAXBElement<SendNalogResponse> createSendNalogResponse(SendNalogResponse value) {
        return new JAXBElement<SendNalogResponse>(_SendNalogResponse_QNAME, SendNalogResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetBankCodeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://izvod", name = "setBankCodeResponse")
    public JAXBElement<SetBankCodeResponse> createSetBankCodeResponse(SetBankCodeResponse value) {
        return new JAXBElement<SetBankCodeResponse>(_SetBankCodeResponse_QNAME, SetBankCodeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClearingResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://izvod", name = "clearingResponse")
    public JAXBElement<ClearingResponse> createClearingResponse(ClearingResponse value) {
        return new JAXBElement<ClearingResponse>(_ClearingResponse_QNAME, ClearingResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetIzvod }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://izvod", name = "getIzvod")
    public JAXBElement<GetIzvod> createGetIzvod(GetIzvod value) {
        return new JAXBElement<GetIzvod>(_GetIzvod_QNAME, GetIzvod.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReceiveNalogNbsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://izvod", name = "receiveNalogNbsResponse")
    public JAXBElement<ReceiveNalogNbsResponse> createReceiveNalogNbsResponse(ReceiveNalogNbsResponse value) {
        return new JAXBElement<ReceiveNalogNbsResponse>(_ReceiveNalogNbsResponse_QNAME, ReceiveNalogNbsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReceiveNalogNbs }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://izvod", name = "receiveNalogNbs")
    public JAXBElement<ReceiveNalogNbs> createReceiveNalogNbs(ReceiveNalogNbs value) {
        return new JAXBElement<ReceiveNalogNbs>(_ReceiveNalogNbs_QNAME, ReceiveNalogNbs.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReceiveMT102AndMT910Response }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://izvod", name = "receiveMT102AndMT910Response")
    public JAXBElement<ReceiveMT102AndMT910Response> createReceiveMT102AndMT910Response(ReceiveMT102AndMT910Response value) {
        return new JAXBElement<ReceiveMT102AndMT910Response>(_ReceiveMT102AndMT910Response_QNAME, ReceiveMT102AndMT910Response.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetIzvodResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://izvod", name = "getIzvodResponse")
    public JAXBElement<GetIzvodResponse> createGetIzvodResponse(GetIzvodResponse value) {
        return new JAXBElement<GetIzvodResponse>(_GetIzvodResponse_QNAME, GetIzvodResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetBankCode }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://izvod", name = "setBankCode")
    public JAXBElement<SetBankCode> createSetBankCode(SetBankCode value) {
        return new JAXBElement<SetBankCode>(_SetBankCode_QNAME, SetBankCode.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Clearing2 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://izvod", name = "clearing2")
    public JAXBElement<Clearing2> createClearing2(Clearing2 value) {
        return new JAXBElement<Clearing2>(_Clearing2_QNAME, Clearing2 .class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Clearing2Response }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://izvod", name = "clearing2Response")
    public JAXBElement<Clearing2Response> createClearing2Response(Clearing2Response value) {
        return new JAXBElement<Clearing2Response>(_Clearing2Response_QNAME, Clearing2Response.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetBankNameResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://izvod", name = "setBankNameResponse")
    public JAXBElement<SetBankNameResponse> createSetBankNameResponse(SetBankNameResponse value) {
        return new JAXBElement<SetBankNameResponse>(_SetBankNameResponse_QNAME, SetBankNameResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendNalog }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://izvod", name = "sendNalog")
    public JAXBElement<SendNalog> createSendNalog(SendNalog value) {
        return new JAXBElement<SendNalog>(_SendNalog_QNAME, SendNalog.class, null, value);
    }

}
