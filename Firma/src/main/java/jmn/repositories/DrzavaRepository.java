package jmn.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import jmn.models.Drzava;

public interface DrzavaRepository extends CrudRepository<Drzava, Long>{
	
	public void delete(Drzava drz);
	public List<Drzava> findByOznakaContainingAndNazivContaining(String oznaka, String naziv);
}
