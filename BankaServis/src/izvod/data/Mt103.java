package izvod.data;

import java.sql.Date;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import annotations.SqlDateAdapter;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "mt103",  
		propOrder = {"idPoruke",
					 "swiftBankeD",
					 "racunBankeD",
					 "swiftBankeP",
					 "racunBankeP",
					 "duznik",
					 "svrhaPlacanja",
					 "primalac",
					 "datumNaloga",
					 "datumValute",
					 "racunDuznika",
					 "modelZaduzenja",
					 "pozivNaBrojZaduzenja",
					 "racunPrimaoca",
					 "modelOdobrenja",
					 "pozivNaBrojOdobrenja",
					 "iznos", 
					 "sifraValute" })
public class Mt103 implements java.io.Serializable{

	@XmlElement(name="idPoruke", required = true)
	private String idPoruke;
	
	//swift kod banke duznika
	@XmlElement(name="swiftBankeD", required = true)
	private String swiftBankeD;
	
	//obracunski racun banke duznika
	@XmlElement(name="racunBankeD", required = true)
	private String racunBankeD;

	//swift kod banke poveriocaa
	@XmlElement(name="swiftBankeP", required = true)
	private String swiftBankeP;
	
	//obracunski racun banke poverioca
	@XmlElement(name="racunBankeP", required = true)
	private String racunBankeP;
	
	//duznik - nalogodavac
	@XmlElement(name="duznik", required = true)
	private String duznik;
	
	//svrha placanja
	@XmlElement(name="svrhaPlacanja", required = true)
	private String svrhaPlacanja;

	//primalac - poverilac
	@XmlElement(name="primalac", required = true)
	private String primalac;
	
	@XmlElement(name="datumNaloga", required = true)
    @XmlSchemaType(name = "dateTime")
	@XmlJavaTypeAdapter(SqlDateAdapter.class)
	private Date datumNaloga;
	
	@XmlElement(name="datumValute", required = true)
    @XmlSchemaType(name = "dateTime")
	@XmlJavaTypeAdapter(SqlDateAdapter.class)
	private Date datumValute;
	
	@XmlElement(name="racunDuznika", required = true)
	private String racunDuznika;
	
	@XmlElement(name="modelZaduzenja", required = true)			
	private int modelZaduzenja;
	
	@XmlElement(name="pozivNaBrojZaduzenja", required = true)
	private String pozivNaBrojZaduzenja;
	
	@XmlElement(name="racunPrimaoca", required = true)
	private String racunPrimaoca;

	@XmlElement(name="modelOdobrenja", required = true)	
	private int modelOdobrenja;
	
	@XmlElement(name="pozivNaBrojOdobrenja", required = true)
	private String pozivNaBrojOdobrenja;

	@XmlElement(name="iznos", required = true)
	private double iznos;

	@XmlElement(name="sifraValute", required = true)
	private String sifraValute;

	public Mt103(){
		
	}
	
	public Mt103(String idPoruke, String swiftBankeD, String racunBankeD, String swiftBankeP, String racunBankeP,
			String duznik, String svrhaPlacanja, String primalac, Date datumNaloga, Date datumValute,
			String racunDuznika, int modelZaduzenja, String pozivNaBrojZaduzenja, String racunPrimaoca,
			int modelOdobrenja, String pozivNaBrojOdobrenja, double iznos, String sifraValute) {
		super();
		this.idPoruke = idPoruke;
		this.swiftBankeD = swiftBankeD;
		this.racunBankeD = racunBankeD;
		this.swiftBankeP = swiftBankeP;
		this.racunBankeP = racunBankeP;
		this.duznik = duznik;
		this.svrhaPlacanja = svrhaPlacanja;
		this.primalac = primalac;
		this.datumNaloga = datumNaloga;
		this.datumValute = datumValute;
		this.racunDuznika = racunDuznika;
		this.modelZaduzenja = modelZaduzenja;
		this.pozivNaBrojZaduzenja = pozivNaBrojZaduzenja;
		this.racunPrimaoca = racunPrimaoca;
		this.modelOdobrenja = modelOdobrenja;
		this.pozivNaBrojOdobrenja = pozivNaBrojOdobrenja;
		this.iznos = iznos;
		this.sifraValute = sifraValute;
	}

	public String getIdPoruke() {
		return idPoruke;
	}

	public void setIdPoruke(String idPoruke) {
		this.idPoruke = idPoruke;
	}

	public String getSwiftBankeD() {
		return swiftBankeD;
	}

	public void setSwiftBankeD(String swiftBankeD) {
		this.swiftBankeD = swiftBankeD;
	}

	public String getRacunBankeD() {
		return racunBankeD;
	}

	public void setRacunBankeD(String racunBankeD) {
		this.racunBankeD = racunBankeD;
	}

	public String getSwiftBankeP() {
		return swiftBankeP;
	}

	public void setSwiftBankeP(String swiftBankeP) {
		this.swiftBankeP = swiftBankeP;
	}

	public String getRacunBankeP() {
		return racunBankeP;
	}

	public void setRacunBankeP(String racunBankeP) {
		this.racunBankeP = racunBankeP;
	}

	public String getDuznik() {
		return duznik;
	}

	public void setDuznik(String duznik) {
		this.duznik = duznik;
	}

	public String getSvrhaPlacanja() {
		return svrhaPlacanja;
	}

	public void setSvrhaPlacanja(String svrhaPlacanja) {
		this.svrhaPlacanja = svrhaPlacanja;
	}

	public String getPrimalac() {
		return primalac;
	}

	public void setPrimalac(String primalac) {
		this.primalac = primalac;
	}

	public Date getDatumNaloga() {
		return datumNaloga;
	}

	public void setDatumNaloga(Date datumNaloga) {
		this.datumNaloga = datumNaloga;
	}

	public Date getDatumValute() {
		return datumValute;
	}

	public void setDatumValute(Date datumValute) {
		this.datumValute = datumValute;
	}

	public String getRacunDuznika() {
		return racunDuznika;
	}

	public void setRacunDuznika(String racunDuznika) {
		this.racunDuznika = racunDuznika;
	}

	public int getModelZaduzenja() {
		return modelZaduzenja;
	}

	public void setModelZaduzenja(int modelZaduzenja) {
		this.modelZaduzenja = modelZaduzenja;
	}

	public String getPozivNaBrojZaduzenja() {
		return pozivNaBrojZaduzenja;
	}

	public void setPozivNaBrojZaduzenja(String pozivNaBrojZaduzenja) {
		this.pozivNaBrojZaduzenja = pozivNaBrojZaduzenja;
	}

	public String getRacunPrimaoca() {
		return racunPrimaoca;
	}

	public void setRacunPrimaoca(String racunPrimaoca) {
		this.racunPrimaoca = racunPrimaoca;
	}

	public int getModelOdobrenja() {
		return modelOdobrenja;
	}

	public void setModelOdobrenja(int modelOdobrenja) {
		this.modelOdobrenja = modelOdobrenja;
	}

	public String getPozivNaBrojOdobrenja() {
		return pozivNaBrojOdobrenja;
	}

	public void setPozivNaBrojOdobrenja(String pozivNaBrojOdobrenja) {
		this.pozivNaBrojOdobrenja = pozivNaBrojOdobrenja;
	}

	public double getIznos() {
		return iznos;
	}

	public void setIznos(double iznos) {
		this.iznos = iznos;
	}

	public String getSifraValute() {
		return sifraValute;
	}

	public void setSifraValute(String sifraValute) {
		this.sifraValute = sifraValute;
	}
	
}
