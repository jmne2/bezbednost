
'use strict';
 
angular.module('repo3App').factory('StanjeService', ['$http', '$q', 'urls', function($http, $q, urls){
 
    var factory = {
        fetchAllStanja: fetchAllStanja,
        createStanje: createStanje,
        searchStanja: searchStanja
    };
 
    return factory;
 
    function fetchAllStanja() {
        var deferred = $q.defer();
        $http.get(urls.STANJE_SERVICE)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while fetching Reciepts');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
    function createStanje(stanje) {
        var deferred = $q.defer();
        $http.post(urls.STANJE_SERVICE, stanje)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
    function searchStanja(stanje){
    	var deferred = $q.defer();
        $http.post(urls.STANJE_SERVICE+"search/", stanje)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
}]);