package jmn.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import jmn.models.NalogZaPlacanje;
import jmn.models.PredlogPlacanja;

public interface NalogZaPlacanjeRepository extends CrudRepository<NalogZaPlacanje, Long>{

	public List<NalogZaPlacanje> findByUplatilacAndPrimalac(String uplatilac, String primalac);
	public List<NalogZaPlacanje> findByUplatilacContainingAndSvrhaUplateContainingAndPrimalacContainingAndSifraPlacanjaContainingAndValutaContainingAndIznosAndBrModelaAndPozivNaBrojContaining(String uplatilac, String svrhaUplate, String primalac, String sifraPlacanja, String valuta, double iznos, int brModela, String pozivNaBroj);
	public List<NalogZaPlacanje> findByUplatilacContainingAndSvrhaUplateContainingAndPrimalacContainingAndSifraPlacanjaContainingAndValutaContainingAndIznosAndBrModelaAndPozivNaBrojContainingAndZaPredlog(String uplatilac, String svrhaUplate, String primalac, String sifraPlacanja, String valuta, double iznos, int brModela, String pozivNaBroj, PredlogPlacanja zaPredlog);
}
