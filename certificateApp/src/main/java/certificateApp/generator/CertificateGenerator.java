package certificateApp.generator;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.DERNull;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x509.AccessDescription;
import org.bouncycastle.asn1.x509.AuthorityInformationAccess;
import org.bouncycastle.asn1.x509.AuthorityKeyIdentifier;
import org.bouncycastle.asn1.x509.BasicConstraints;
import org.bouncycastle.asn1.x509.ExtendedKeyUsage;
import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.asn1.x509.GeneralNames;
import org.bouncycastle.asn1.x509.KeyPurposeId;
import org.bouncycastle.asn1.x509.SubjectKeyIdentifier;
import org.bouncycastle.cert.CertIOException;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cert.jcajce.JcaX509v3CertificateBuilder;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;

import certificateApp.models.IssuerData;
import certificateApp.models.SubjectData;

public class CertificateGenerator {
public CertificateGenerator() {}
	
	public X509Certificate generateCertificate(SubjectData subjectData, IssuerData issuerData, Boolean isCA, Boolean isIntermediate, X509Certificate parentCertificate) throws IOException {
		try {
			//Posto klasa za generisanje sertifiakta ne moze da primi direktno privatni kljuc pravi se builder za objekat
			//Ovaj objekat sadrzi privatni kljuc izdavaoca sertifikata i koristiti se za potpisivanje sertifikata
			//Parametar koji se prosledjuje je algoritam koji se koristi za potpisivanje sertifiakta
			JcaContentSignerBuilder builder = new JcaContentSignerBuilder("SHA256WithRSAEncryption");
			//Takodje se navodi koji provider se koristi, u ovom slucaju Bouncy Castle
			builder = builder.setProvider("BC");

			//Formira se objekat koji ce sadrzati privatni kljuc i koji ce se koristiti za potpisivanje sertifikata
			ContentSigner contentSigner = builder.build(issuerData.getPrivateKey());
			
			//Postavljaju se podaci za generisanje sertifiakta
			X509v3CertificateBuilder certGen = null;
				certGen = new JcaX509v3CertificateBuilder(issuerData.getX500name(),
					new BigInteger(subjectData.getSerialNumber(),16),
					subjectData.getStartDate(),
					subjectData.getEndDate(),
					subjectData.getX500name(),
					subjectData.getPublicKey());
			
			certGen.addExtension(new ASN1ObjectIdentifier("2.5.29.14"), false,
			        new SubjectKeyIdentifier(subjectData.getPublicKey().getEncoded()));
			if(!isCA){	
			//X509Extension extension = new X509Extension(false, new DEROctetString(new AuthorityKeyIdentifierStructure(parentCertificate)));
	//		certGen.addExtension(new ASN1ObjectIdentifier("2.5.29.35"), false, new AuthorityKeyIdentifier(parentCertificate.getExtensionValue("2.5.29.14")));
			certGen.addExtension(new ASN1ObjectIdentifier("2.5.29.35"), false, new AuthorityKeyIdentifier(parentCertificate.getExtensionValue("2.5.29.14"), new GeneralNames(new GeneralName(new X500Name(parentCertificate.getSubjectX500Principal().getName()))), parentCertificate.getSerialNumber()) );
			
			File f = new File("cerNBS/");
			File[] files = f.listFiles();
			File file = files[0];
			String location1 = "cerNBS/"+file.getName();
			String location = "cerBanks/Certificate"+parentCertificate.getSerialNumber()+".cer";
			AccessDescription des1 = new AccessDescription(AccessDescription.id_ad_ocsp, new GeneralName(GeneralName.uniformResourceIdentifier, location1));
			AccessDescription des2 = new AccessDescription(AccessDescription.id_ad_caIssuers, new GeneralName(GeneralName.uniformResourceIdentifier, location));
			AccessDescription[] descr = {des2, des1};
			AuthorityInformationAccess aia = new AuthorityInformationAccess(descr);
			
			certGen.addExtension(new ASN1ObjectIdentifier("1.3.6.1.5.5.7.1.1"), false, aia);
			//certGen.addExtension(new ASN1ObjectIdentifier("2.5.29.35"), false, new AuthorityKeyIdentifier(parentSubjectKeyIdentifier, new GeneralNames(new GeneralName(0,parentCertificate.getSubjectX500Principal().getName())),parentCertificate.getSerialNumber());
				//certGen.addExtension(new ASN1ObjectIdentifier("2.5.29.35"), false, new AuthorityKeyIdentifier(new SubjectPublicKeyInfo(new AlgorithmIdentifier(new ASN1ObjectIdentifier("2.5.29.14")),parentSubjectKeyIdentifier)));
			}
			if(isCA){
				
				try {
					certGen.addExtension(new ASN1ObjectIdentifier("2.5.29.19"), false,
					        new BasicConstraints(true));
				} catch (CertIOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				if(isIntermediate){	
				//	AuthorityKeyIdentifier aki = new AuthorityKeyIdentifier(parentCertificate.getn, parentCertificate.getSerialNumber())
	//				certGen.addExtension(new ASN1ObjectIdentifier("2.5.29.35"), false, new AuthorityKeyIdentifier(parentCertificate.getExtensionValue("2.5.29.14")));
					certGen.addExtension(new ASN1ObjectIdentifier("2.5.29.35"), false, new AuthorityKeyIdentifier(parentCertificate.getExtensionValue("2.5.29.14"), new GeneralNames(new GeneralName(new X500Name(parentCertificate.getSubjectX500Principal().getName()))), parentCertificate.getSerialNumber()) );

					File f = new File("cerNBS/");
					File[] files = f.listFiles();
					File file = files[0];
					String location1 = "cerNBS/"+file.getName();
					String location = "cerNBS/Certificate"+parentCertificate.getSerialNumber()+".cer";
					AccessDescription des1 = new AccessDescription(AccessDescription.id_ad_ocsp, new GeneralName(GeneralName.uniformResourceIdentifier, location1));
					AccessDescription des2 = new AccessDescription(AccessDescription.id_ad_caIssuers, new GeneralName(GeneralName.uniformResourceIdentifier, location));
					AccessDescription[] descr = {des2, des1};
					AuthorityInformationAccess aia = new AuthorityInformationAccess(descr);
					
					certGen.addExtension(new ASN1ObjectIdentifier("1.3.6.1.5.5.7.1.1"), false, aia);
					GeneralName[] genNames = new GeneralName[1];
					genNames[0] = new GeneralName(GeneralName.dNSName, "localhost");
					 
					certGen.addExtension(new ASN1ObjectIdentifier("2.5.29.17"), false, new GeneralNames(genNames));
					
				}
				else{
					// OCSP Signing
					KeyPurposeId kpid = KeyPurposeId.id_kp_OCSPSigning;
					ExtendedKeyUsage eku = new ExtendedKeyUsage(kpid);
					certGen.addExtension(new ASN1ObjectIdentifier("2.5.29.37"), false, eku);
					// OCSP nocheck extension
					certGen.addExtension(new ASN1ObjectIdentifier("1.3.6.1.5.5.7.48.1.5"), false, new DERNull());
					//SubjectAlternativeNameExtension san = new SubjectAlternativeNameExtension(new GeneralNames(new GeneralName(GeneralName.dNSName, "localhost")));
					GeneralName[] genNames = new GeneralName[1];
					genNames[0] = new GeneralName(GeneralName.dNSName, "localhost");
					 
					certGen.addExtension(new ASN1ObjectIdentifier("2.5.29.17"), false, new GeneralNames(genNames));
					 
				}
	 
			}
			//firma
			else{
				try {
					certGen.addExtension(new ASN1ObjectIdentifier("2.5.29.19"), false,
					        new BasicConstraints(false));
					//SubjectAlternativeNameExtension san = new SubjectAlternativeNameExtension(new GeneralNames(new GeneralName(GeneralName.dNSName, "localhost")));
					GeneralName[] genNames = new GeneralName[1];
					genNames[0] = new GeneralName(GeneralName.dNSName, "localhost");
					 
					certGen.addExtension(new ASN1ObjectIdentifier("2.5.29.17"), false, new GeneralNames(genNames));
					 
					
					} catch (CertIOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}	
			
			
			//Generise se sertifikat
			X509CertificateHolder certHolder = certGen.build(contentSigner);

			//Builder generise sertifikat kao objekat klase X509CertificateHolder
			//Nakon toga je potrebno certHolder konvertovati u sertifikat, za sta se koristi certConverter
			JcaX509CertificateConverter certConverter = new JcaX509CertificateConverter();
			certConverter = certConverter.setProvider("BC");

			//Konvertuje objekat u sertifikat
			return certConverter.getCertificate(certHolder);
		} catch (CertificateEncodingException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (OperatorCreationException e) {
			e.printStackTrace();
		} catch (CertificateException e) {
			e.printStackTrace();
		}
		return null;
	}
}
