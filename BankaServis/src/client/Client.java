package client;

import java.sql.Date;

import izvod.IzvodWSImpl;
import izvod.data.NalogBanka;

public class Client {
	
	public static void main(String[] args){
		IzvodWSImpl izvod = new IzvodWSImpl();
		izvod.setBankCode("123");
		
		NalogBanka nb = new NalogBanka();
		java.util.Date date = new java.util.Date();
		Date datum = new Date(date.getTime());
		System.out.println("Datum je " + datum.toString());

		nb.setDatumNaloga(datum);
		nb.setDatumValute(datum);
		nb.setHitno(false);
		nb.setIdPoruke("bla");
		nb.setIznos(300.00);
		nb.setModelOdobrenja(2);
		nb.setModelZaduzenja(2);
		nb.setOznakaValute("DIN");
		nb.setPozivNaBrojOdobrenja("222");
		nb.setPozivNaBrojZaduzenja("333");
		nb.setPrimalac("Pera Peric Novi Sad");
		nb.setRacunPrimaoca("456456789111111111");
		nb.setUplatilac("Mika Mikic Bg");
		nb.setRacunUplatioca("123456789222222222");
		
		// set bank code naspram racuna uplatioca
		
		nb.setSvrhaUplate("ISPIT");
		izvod.sendNalog(nb);
		
		izvod.clearing("456");
	}
	
}
