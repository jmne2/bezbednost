package jmn.controller;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;

import jmn.annotation.PermissionType;
import jmn.configuration.AES;
import jmn.izvod.IzvodWS;
import jmn.izvod.data.Izvod;
import jmn.izvod.data.StavkaIzvodabanka;
import jmn.izvod.data.ZahtevZaIzvod;
import jmn.models.DEK;
import jmn.models.DnevnoStanje;
import jmn.models.Racun;
import jmn.models.SifrarnikMetoda;
import jmn.models.StavkaIzvoda;
import jmn.pojo.UserData;
import jmn.services.DEKService;
import jmn.services.DnevnoStanjeService;
import jmn.services.PreduzeceService;
import jmn.services.RacunService;
import jmn.services.StavkaIzvodaService;

@Controller
public class DnevnoStanjeController {

	final static Logger logger = Logger.getLogger(DnevnoStanjeController.class);
	
	@Autowired
	private DnevnoStanjeService dss;
	
	@Autowired
	private RacunService rs;
	
	@Autowired
	private PreduzeceService ps;
	
	@Autowired
	private StavkaIzvodaService sis;
	
	@Autowired
	private DEKService dks;
	
	@PermissionType("DnevnaStanja:view")
	@RequestMapping(value="/stanja/", method=RequestMethod.GET)
	public ResponseEntity<HashMap<String, Object>> fetchStanja(HttpSession session){
		UserData u = (UserData)session.getAttribute("user");
		int pb = u.getPreduzece().getPib();
		DEK kk = dks.findByPreduzece(u.getPreduzece());
		kk.setValue(AES.decryptDEK(kk.getValue(), Integer.toString(pb)));
		List<Racun> racuni = rs.findByPreduzece(u.getPreduzece());
		List<DnevnoStanje> stanja = new ArrayList<DnevnoStanje>();
		for(Racun r : racuni){
			List<DnevnoStanje> temp = dss.findByRacun(r);
			for(DnevnoStanje dn : temp){
				dn.getRacun().setBrRacuna(AES.decryptCBC(dn.getRacun().getBrRacuna(),kk.getValue(), kk.getIv()));
			}
			stanja.addAll(temp);
			r.setBrRacuna(AES.decryptCBC(r.getBrRacuna(), kk.getValue(), kk.getIv()));
		}
		
		HashMap<String, Object> resp = new HashMap<String, Object>();
		resp.put("racuni", racuni);
		if(stanja.isEmpty())
			return new ResponseEntity<HashMap<String, Object>>(resp, HttpStatus.OK);
		resp.put("stanja", stanja);
		return new ResponseEntity<HashMap<String, Object>>(resp, HttpStatus.OK);
	}
	
	@PermissionType("DnevnaStanja:add")
	@RequestMapping(value = "/stanja/", method = RequestMethod.POST)
	public ResponseEntity<Void> createStanja(HttpSession session, @RequestBody DnevnoStanje stanje, UriComponentsBuilder ucBuilder) throws DatatypeConfigurationException{
	    System.out.println("STIGLO JE OVO: " + stanje.getDatumIzvoda() + " " + stanje.getRacun());
	    UserData u = (UserData)session.getAttribute("user");
		int pb = u.getPreduzece().getPib();
		DEK kk = dks.findByPreduzece(u.getPreduzece());
		kk.setValue(AES.decryptDEK(kk.getValue(), Integer.toString(pb)));

		DnevnoStanje dnv = dss.findByDatumIzvodaAndRacun(stanje.getDatumIzvoda(), stanje.getRacun());
	    if(dnv != null){
	    	java.util.Date td = new java.util.Date();
	    	java.sql.Date td2 = new java.sql.Date(td.getTime());
	    	System.out.println(td2.toString());
	    	if(stanje.getDatumIzvoda().toString().equals(td2.toString())){
	    		System.out.println("ZA DANASNJI DAN OMG");
	    		try {
					//kreiranje web servisa
					URL wsdlLocation = new URL("http://localhost:8081/banka-ws/services/IzvodWS?wsdl");
					QName serviceName = new QName("http://izvod", "IzvodWSService");
					QName portName = new QName("http://izvod", "IzvodWSPort");
					Service service = Service.create(wsdlLocation, serviceName);
					IzvodWS izvod = service.getPort(portName, IzvodWS.class);
				//	salje se kod banke sa kojom se razgovara:
					izvod.setBankCode(stanje.getRacun().getBrRacuna().substring(0, 3));
				// slanje zahteva za izvod sa danasnjim datumom:
					ZahtevZaIzvod zahtev = new ZahtevZaIzvod();
					//java.util.Date date = new java.util.Date();
					GregorianCalendar c = new GregorianCalendar();
					c.setTime(stanje.getDatumIzvoda());
					XMLGregorianCalendar date2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
					zahtev.setBrojRacuna(stanje.getRacun().getBrRacuna());
					zahtev.setDatum(date2);
					zahtev.setRedniBrPreseka(1);

					Izvod response = izvod.getIzvod(zahtev);

					System.out.println("IMA RAZLIKE U STANJIMA: " + (dnv.getNovoStanje() != response.getNovoStanje()));
					if(dnv.getNovoStanje() != response.getNovoStanje()){
						Racun r = rs.findOne(dnv.getRacun().getId());
						dnv.setNovoStanje(response.getNovoStanje());
						dnv.setPrethodnoStanje(response.getPrethodnoStanje());
						dnv.setRezervisano(0);
						dnv.setRacun(r);
						dnv.setStanjeNaTeret(response.getUkupnoNaTeret());
						dnv.setStanjeUKorist(response.getUkupnoUKorist());
						dnv = dss.saveDnevnoStanje(dnv);
						for(StavkaIzvodabanka stavka : response.getStavke().getStavke()){
							java.util.Date d = stavka.getDatumNaloga().toGregorianCalendar().getTime();
							java.sql.Date bla = new java.sql.Date(d.getTime());
							java.util.Date d2 = stavka.getDatumValute().toGregorianCalendar().getTime();
							java.sql.Date bla2 = new java.sql.Date(d2.getTime());
							String rcnUpl = AES.encryptCBC(stavka.getRacunUplatioca(), kk.getValue(), kk.getIv());
							String rcnPrm = AES.encryptCBC(stavka.getRacunPrimaoca(), kk.getValue(), kk.getIv());
							StavkaIzvoda stv = new StavkaIzvoda(stavka.getUplatilac(), stavka.getSvrhaPlacanja(), stavka.getPrimalac(), bla,
									bla2, rcnUpl, stavka.getModelZaduzenja(), stavka.getPozivNaBrojZaduzenja(), rcnPrm,
									stavka.getModelOdobrenja(), stavka.getPozivNaBrojOdobrenja(), stavka.getIznos(), stavka.getIznos(),stavka.getSmer(), null, null, null);

							if(!dnv.getStavkeIzvoda().contains(stv)){
								System.out.println("IMA NOVIH STAVKI WOOOOOOOOOO");
								stv.setDnevnoStanje(dnv);
								stv = sis.saveStavkaIzvoda(stv);
								dnv.getStavkeIzvoda().add(stv);
								dnv = dss.saveDnevnoStanje(dnv);
							}
						}
					}
					try {
				        HttpHeaders headers = new HttpHeaders();
				        headers.setLocation(ucBuilder.path("/stanja/{id}").buildAndExpand(dnv.getId()).toUri());
				        logger.warn("Korisnik "+ u.getId() + " uspesno je izvrsio metodu " + SifrarnikMetoda.methods.get(
				 				Thread.currentThread().getStackTrace()[1].getMethodName()));
				        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
			        }
			        catch(Exception e){
			        	logger.error("Greska pri izvrsenju " + SifrarnikMetoda.methods.get(Thread.currentThread().getStackTrace()[1].getMethodName())
			        	+ " za korisnika " + u.getId());
			        	return new ResponseEntity<Void>(HttpStatus.CONFLICT);
			        }
					
			    }
			    catch (MalformedURLException e) {
			    	logger.error("Greska pri izvrsenju " + SifrarnikMetoda.methods.get(Thread.currentThread().getStackTrace()[1].getMethodName())
		        	+ " za korisnika " + u.getId());
					e.printStackTrace();
				}
	    		
	    	}
	    } else {
	    	
		    try {
				//kreiranje web servisa
				URL wsdlLocation = new URL("http://localhost:8081/banka-ws/services/IzvodWS?wsdl");
				QName serviceName = new QName("http://izvod", "IzvodWSService");
				QName portName = new QName("http://izvod", "IzvodWSPort");

				Service service = Service.create(wsdlLocation, serviceName);
				
				IzvodWS izvod = service.getPort(portName, IzvodWS.class);
				
			//	salje se kod banke sa kojom se razgovara:
				izvod.setBankCode(stanje.getRacun().getBrRacuna().substring(0, 3));

			// slanje zahteva za izvod sa danasnjim datumom:
				ZahtevZaIzvod zahtev = new ZahtevZaIzvod();
				//java.util.Date date = new java.util.Date();
				GregorianCalendar c = new GregorianCalendar();
				c.setTime(stanje.getDatumIzvoda());
				XMLGregorianCalendar date2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);

				zahtev.setBrojRacuna(stanje.getRacun().getBrRacuna());
				zahtev.setDatum(date2);
			
				zahtev.setRedniBrPreseka(1);
				Izvod response = izvod.getIzvod(zahtev);
				dnv = new DnevnoStanje();
				Racun r = rs.findOne(stanje.getRacun().getId());
				java.util.Date d = response.getDatumNaloga().toGregorianCalendar().getTime();
				java.sql.Date bla = new java.sql.Date(d.getTime());
				dnv.setDatumIzvoda(bla);
				dnv.setNovoStanje(response.getNovoStanje());
				dnv.setPrethodnoStanje(response.getPrethodnoStanje());
				dnv.setRezervisano(0);
				dnv.setRacun(r);
				dnv.setStanjeNaTeret(response.getUkupnoNaTeret());
				dnv.setStanjeUKorist(response.getUkupnoUKorist());
				dnv = dss.saveDnevnoStanje(dnv);
				for(StavkaIzvodabanka stavka : response.getStavke().getStavke()){
					java.util.Date dd = stavka.getDatumNaloga().toGregorianCalendar().getTime();
					java.sql.Date blabic = new java.sql.Date(dd.getTime());
					java.util.Date d2 = stavka.getDatumValute().toGregorianCalendar().getTime();
					java.sql.Date bla2 = new java.sql.Date(d2.getTime());
					String rcnUpl = AES.encryptCBC(stavka.getRacunUplatioca(), kk.getValue(), kk.getIv());
					String rcnPrm = AES.encryptCBC(stavka.getRacunPrimaoca(), kk.getValue(), kk.getIv());
					StavkaIzvoda stv = new StavkaIzvoda(stavka.getUplatilac(), stavka.getSvrhaPlacanja(), stavka.getPrimalac(), blabic,	
							bla2, rcnUpl, stavka.getModelZaduzenja(), stavka.getPozivNaBrojZaduzenja(), rcnPrm,stavka.getModelOdobrenja(),
							stavka.getPozivNaBrojOdobrenja(), stavka.getIznos(),stavka.getIznos(), stavka.getSmer(), null, null, null);

					stv.setDnevnoStanje(dnv);
					stv = sis.saveStavkaIzvoda(stv);
					dnv.getStavkeIzvoda().add(stv);
					dnv = dss.saveDnevnoStanje(dnv);
				}
				try {
			        HttpHeaders headers = new HttpHeaders();
			        headers.setLocation(ucBuilder.path("/stanja/{id}").buildAndExpand(dnv.getId()).toUri());
			        logger.warn("Korisnik "+ u.getId() + " uspesno je izvrsio metodu " + SifrarnikMetoda.methods.get(
			 				Thread.currentThread().getStackTrace()[1].getMethodName()));
			        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
		        }
		        catch(Exception e){
		        	logger.error("Greska pri izvrsenju " + SifrarnikMetoda.methods.get(Thread.currentThread().getStackTrace()[1].getMethodName())
		        	+ " za korisnika " + u.getId());
		        	return new ResponseEntity<Void>(HttpStatus.CONFLICT);
		        }
				
		    }
		    catch (MalformedURLException e) {
		    	logger.error("Greska pri izvrsenju " + SifrarnikMetoda.methods.get(Thread.currentThread().getStackTrace()[1].getMethodName())
	        	+ " za korisnika " + u.getId());
					e.printStackTrace();
			}
	    }
		 return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	    }
		
	@PermissionType("DnevnaStanja:search")
	@RequestMapping(value = "/stanja/search/", method = RequestMethod.POST)
	    public ResponseEntity<?> searchStanja(HttpSession session, @RequestBody DnevnoStanje stanje) {
			List<DnevnoStanje> stanja = new ArrayList<DnevnoStanje>();
			if(stanje.getRacun() != null){
				Racun r = rs.findOne(stanje.getRacun().getId());
				stanja.add(dss.findByDatumIzvodaAndRacun(stanje.getDatumIzvoda(), r));
			}
				
			else
				stanja = dss.findByDatumIzvoda(stanje.getDatumIzvoda());
		 if(stanja == null)
			 return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);

	     return new ResponseEntity<List<DnevnoStanje>>(stanja, HttpStatus.OK);
	    }
}
