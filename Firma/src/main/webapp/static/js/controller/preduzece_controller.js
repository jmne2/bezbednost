'use strict';

angular.module('repo3App').controller('PreduzeceController', ['$scope','$uibModal', 'PreduzeceService', 
                                                                    function($scope,$uibModal,PreduzeceService) {
    var self = this;
    self.adresa={id:null};
    self.adrese=[];
    self.preduzece = {};
    self.preduzeca = [];
    self.delatnost = {};
    self.delatnosti = [];
    self.show = false;
    self.logged = false;
    self.children = [""];
    
    self.submit = submit;
    self.edit = edit;
    self.remove = remove;
    self.reset = reset;
    self.search = search;
    self.currState = "edit";
    self.showAdreses = showAdreses;
    self.showWorks = showWorks;
    self.indSelected = -1;
    $scope.indSelectedVote = -1;
    $scope.required = true;
    
    $scope.$on('dispAfterLog', function(event, args) {
    	if(self === args.ctrl){
    		self.logged = true;
    		self.show = true;
    		self.preduzece = args.info.preduzece;
    		fetchAllPreduzeca();
    	} else {
    		self.logged = false;
    		self.show = false;
    	}
    });
    
    $scope.$on('changeState', function(event, args) {    	
    	if(self === args.ctrl){
    		self.currState = args.state;
    		$scope.required = (self.currState === "search") ? false : true;
    		self.preduzece = {};
    	}
    });
    $scope.$on('hide', function(event, args) {
    	self.show = false;
    	
    });
    $scope.$on('resetEvent', function(event, args) {    	
    	if(self === args.ctrl){
    		self.currState = "edit";
    		fetchAllPreduzeca();
    	}
    });
    $scope.$on('changeSelection', function(event, args) {    	
    	if(self === args.ctrl){
    		switch (args.direction) {
	            case 'prev':
	                prev();
	                break;
	            case 'next':
	                next();
	                break;
	            case 'first':
	            	first();
	            	break;
	            case 'last':
	            	last();
	            	break;
    		}
    	}
    });
    
    $scope.$on('deleteSel', function(event, args) {    	
    	if(self === args.ctrl){
    		if($scope.indSelectedVote != -1){
    			var selected = self.preduzeca[$scope.indSelectedVote];
    			remove(selected.id);
    			self.currState = "edit";
    		}
    	}
    });
    
    function first(){
    	$scope.indSelectedVote = 0;
    	self.indSelected = $scope.indSelectedVote;
    }
    function prev(){
    	if(($scope.indSelectedVote != 0) && ($scope.indSelectedVote != -1)){
    		$scope.indSelectedVote = $scope.indSelectedVote - 1;
    		self.indSelected = $scope.indSelectedVote;
    	}
    }
    
    function next(){
    	if(($scope.indSelectedVote != self.preduzeca.length) && ($scope.indSelectedVote != -1)){
    		$scope.indSelectedVote = $scope.indSelectedVote + 1;
    		self.indSelected = $scope.indSelectedVote;
    	}
    }
    
    function last(){
    	$scope.indSelectedVote = self.preduzeca.length - 1;
    	self.indSelected = $scope.indSelectedVote;
    }
 
    $scope.indSelectedVote = null;
    $scope.setSelected = function (indSelectedVote) {
       $scope.indSelectedVote = indSelectedVote;
       self.indSelected = $scope.indSelectedVote;
       if(self.currState === "edit"){
    	   var id = self.preduzeca[indSelectedVote].id;
    	   edit(id);
   		}
    };
   
    function showAdreses() {

        var modalInstance = $uibModal.open({
          templateUrl: 'myModalContent.html',
          controller: ModalInstanceCtrl,
          resolve: {
            items: function () {
              return self.adrese;
            },
            type: function(){
          	  return "Adresu";
            }
          }
        });

        modalInstance.result.then(function (selectedItem) {
          self.adresa = self.adrese[selectedItem];
        }, function () {
        });
      };

        function showWorks() {

            var modalInstance = $uibModal.open({
              templateUrl: 'myModalContent.html',
              controller: ModalInstanceCtrl,
              resolve: {
                items: function () {
                  return self.delatnosti;
                },
                type: function(){
              	  return "Delatnost";
                }
              }
            });

            modalInstance.result.then(function (selectedItem) {
              self.delatnost = self.delatnosti[selectedItem];
            }, function () {
            });
          };

    function fetchAllPreduzeca(){
    	reset();
    	PreduzeceService.fetchAllPreduzeca()
            .then(
            function(d) {
            	self.preduzeca = [];
                self.adrese = d["adrese"];
                self.preduzeca = d["preduzeca"];
                self.delatnosti = d["delatnosti"];
                //$scope.indSelectedVote = 0;
            },
            function(errResponse){
                console.error('Error while fetching Receits');
            }
        );
    }
 
    function createPreduzece(preduzece){
    	PreduzeceService.createPreduzece(preduzece)
            .then(
            fetchAllPreduzeca,
            function(errResponse){
            	alert(errResponse.data);
                console.error('Error while creating User');
            }
        );
    }
 
    function updatePreduzece(preduzece, id){
    	PreduzeceService.updatePreduzece(preduzece, id)
            .then(
            fetchAllPreduzeca,
            function(errResponse){
                console.error('Error while updating User');
            }
        );
    }
 
    function deletePreduzece(id){
    	PreduzeceService.deletePreduzece(id)
            .then(
            fetchAllPreduzeca,
            function(errResponse){
                console.error('Error while deleting User');
            }
        );
    }
 
    function submit() {
    	if (self.currState === "add"){
    		if(self.adresa.id != null && !angular.isUndefined(self.adresa.id) && self.delatnost.id != null	
    				&& !angular.isUndefined(self.delatnost.id)){
    			self.preduzece.adresa = self.adresa;
	            self.preduzece.sifraDelatnosti = self.delatnost;
	            createPreduzece(self.preduzece);
    		}
        }else if (self.currState === "edit"){
        	if(self.adresa.id != null && !angular.isUndefined(self.adresa.id) && self.delatnost.id != null	
        			&& !angular.isUndefined(self.delatnost.id)){
        		self.preduzece.adresa = self.adresa;
	            self.preduzece.sifraDelatnosti = self.delatnost;
	            updatePreduzece(self.preduzece, self.preduzece.id);
        	}
        } else if (self.currState === "search"){
        	self.preduzece.adresa = self.adresa;
            self.preduzece.sifraDelatnosti = self.delatnost;
        	search(self.preduzece);
        }
    }
 
    function edit(id){
        for(var i = 0; i < self.preduzeca.length; i++){
            if(self.preduzeca[i].id === id) {
                self.preduzece = angular.copy(self.preduzeca[i]);
                self.adresa = self.preduzece.adresa;
                self.delatnost = self.preduzece.sifraDelatnosti;
                break;
            }
        }
    }
 
    function remove(id){
        if(self.preduzece.id === id) {//clean form if the user to be deleted is shown there.
            reset();
        }
        deletePreduzece(id);
    }
 
 
    function reset(){
        self.preduzece={};
        self.adresa= {};
        self.delatnost={};
        $scope.myForm.$setPristine(); //reset Form
    }
    
    function search(preduzece){
    	PreduzeceService.searchPreduzeca(preduzece)
        .then(
        function(d) {
        	self.preduzeca = d;
            $scope.indSelectedVote = -1;
            self.indSelected = $scope.indSelectedVote;
        },
        function(errResponse){
            console.error('Error while fetching Users');
        }
        );
    }
    
    var ModalInstanceCtrl = function ($scope, $uibModalInstance, items, type) {

    	  $scope.items = items;
    	  $scope.selected = {
    	    item: -1
    	  };
    	  $scope.type = type;

    	  $scope.ok = function () {
    		  $uibModalInstance.close($scope.selected.item);
    	  };

    	  $scope.cancel = function () {
    		  $uibModalInstance.dismiss('cancel');
    	  };
    	};
}]);