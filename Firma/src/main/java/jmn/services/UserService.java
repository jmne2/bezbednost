package jmn.services;

import java.util.List;

import jmn.models.User;

public interface UserService {

	List<User> findAll();

	User saveUser(User us);

	User findOne(Long id);

	void delete(Long id);

	void delete(User us);
	
	User findByUsernameAndPassword(String username, String password);
	
	User findByUsername(String username);
}
