package jmn.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jmn.models.Role;
import jmn.repositories.RoleRepository;

@Service
public class RoleServiceImpl implements RoleService{

	@Autowired
	private RoleRepository rr;
	
	@Override
	public List<Role> findAll() {
		return (List<Role>)rr.findAll();
	}

	@Override
	public Role saveRole(Role role) {
		return rr.save(role);
	}

	@Override
	public Role findOne(Long id) {
		return rr.findOne(id);
	}

	@Override
	public void delete(Long id) {
		rr.delete(id);
	}

	@Override
	public void delete(Role role) {
		rr.delete(role);
	}

	@Override
	public Role findByNaziv(String naziv) {
		
		return rr.findByNaziv(naziv);
	}

}
