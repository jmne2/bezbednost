
package izvod.data;

import java.sql.Date;
import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "mt102", propOrder = {
    "idPoruke",
    "swiftBankeD",
    "racunBankeD",
    "swiftBankeP",
    "racunBankeP",
    "ukupanIznos",
    "sifraValute",
    "datumValute",
    "datum",
    "nalozi"
})
public class Mt102 implements java.io.Serializable {

    @XmlElement(required = true)
    private String idPoruke;
    @XmlElement(required = true)
    private String swiftBankeD;
    @XmlElement(required = true)
    private String racunBankeD;
    @XmlElement(required = true)
    private String swiftBankeP;
    @XmlElement(required = true)
    private String racunBankeP;
    private double ukupanIznos;
    @XmlElement(required = true)
    private String sifraValute;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    private XMLGregorianCalendar datumValute;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    private XMLGregorianCalendar datum;
    @XmlElement(required = true)
    private ArrayList<Nalog> nalozi;

    
    public Mt102(){}
    
	public Mt102(String idPoruke, String swiftBankeD, String racunBankeD, String swiftBankeP, String racunBankeP,
			double ukupanIznos, String sifraValute, XMLGregorianCalendar datumValute, XMLGregorianCalendar datum,
			ArrayList<Nalog> nalozi) {
		super();
		this.idPoruke = idPoruke;
		this.swiftBankeD = swiftBankeD;
		this.racunBankeD = racunBankeD;
		this.swiftBankeP = swiftBankeP;
		this.racunBankeP = racunBankeP;
		this.ukupanIznos = ukupanIznos;
		this.sifraValute = sifraValute;
		this.datumValute = datumValute;
		this.datum = datum;
		this.nalozi = nalozi;
	}

	public String getIdPoruke() {
		return idPoruke;
	}

	public void setIdPoruke(String idPoruke) {
		this.idPoruke = idPoruke;
	}

	public String getSwiftBankeD() {
		return swiftBankeD;
	}

	public void setSwiftBankeD(String swiftBankeD) {
		this.swiftBankeD = swiftBankeD;
	}

	public String getRacunBankeD() {
		return racunBankeD;
	}

	public void setRacunBankeD(String racunBankeD) {
		this.racunBankeD = racunBankeD;
	}

	public String getSwiftBankeP() {
		return swiftBankeP;
	}

	public void setSwiftBankeP(String swiftBankeP) {
		this.swiftBankeP = swiftBankeP;
	}

	public String getRacunBankeP() {
		return racunBankeP;
	}

	public void setRacunBankeP(String racunBankeP) {
		this.racunBankeP = racunBankeP;
	}

	public double getUkupanIznos() {
		return ukupanIznos;
	}

	public void setUkupanIznos(double ukupanIznos) {
		this.ukupanIznos = ukupanIznos;
	}

	public String getSifraValute() {
		return sifraValute;
	}

	public void setSifraValute(String sifraValute) {
		this.sifraValute = sifraValute;
	}

	public XMLGregorianCalendar getDatumValute() {
		return datumValute;
	}

	public void setDatumValute(XMLGregorianCalendar datumValute) {
		this.datumValute = datumValute;
	}

	public XMLGregorianCalendar getDatum() {
		return datum;
	}

	public void setDatum(XMLGregorianCalendar datum) {
		this.datum = datum;
	}

	public ArrayList<Nalog> getNalozi() {
		return nalozi;
	}

	public void setNalozi(ArrayList<Nalog> nalozi) {
		this.nalozi = nalozi;
	}



}
