package certificateApp.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import certificateApp.models.Role;

@Repository
public interface RoleRepository extends CrudRepository<Role, Long>{

	public List<Role> findByName(String name);
	
}
