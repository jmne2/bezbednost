package certificateApp.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import certificateApp.models.User;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
	public User findByUsernameAndPassword(String username, String password);
	public User findByName(String name);
	public User findByUsername(String username);
}
