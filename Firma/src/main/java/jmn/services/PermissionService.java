package jmn.services;

import java.util.List;

import jmn.models.Permission;

public interface PermissionService {
	List<Permission> findAll();

	Permission savePermission(Permission perm);

	Permission findOne(Long id);

	void delete(Long id);

	void delete(Permission perm);
}
