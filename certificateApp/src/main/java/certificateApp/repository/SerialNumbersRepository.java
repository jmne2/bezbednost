package certificateApp.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import certificateApp.models.SerialNumbers;

@Repository
public interface SerialNumbersRepository extends CrudRepository<SerialNumbers, Long>{
	public SerialNumbers findBySerialNumber(String serialNumber);
	public List<SerialNumbers> findByUsername(String username);
	public SerialNumbers findByAlias(String alias);
	public List<SerialNumbers> findByIssuerSerNum(String issuerSerNum);
}
