
package jmn.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;

import jmn.annotation.PermissionType;
import jmn.configuration.AES;
import jmn.models.DEK;
import jmn.models.DnevnoStanje;
import jmn.models.Racun;
import jmn.models.SifrarnikMetoda;
import jmn.models.StavkaIzvoda;
import jmn.pojo.UserData;
import jmn.services.DEKService;
import jmn.services.DnevnoStanjeService;
import jmn.services.RacunService;
import jmn.services.StavkaIzvodaService;

@Controller
public class StavkaIzvodaController {

	final static Logger logger = Logger.getLogger(StavkaIzvodaController.class);
	
	@Autowired
	private StavkaIzvodaService sis;
	
	@Autowired
	private DnevnoStanjeService dss;
	
	@Autowired
	private RacunService rs;

	@Autowired
	private DEKService dks;

	@PermissionType("StavkeIzvoda:view")
	@RequestMapping(value="/stavkeIzvoda/", method=RequestMethod.GET)
	public ResponseEntity<HashMap<String, Object>> listAllStavkeIzvoda(HttpSession session){
		UserData u = (UserData)session.getAttribute("user");
		int pb = u.getPreduzece().getPib();
		DEK kk = dks.findByPreduzece(u.getPreduzece());
		kk.setValue(AES.decryptDEK(kk.getValue(), Integer.toString(pb)));

		HashMap<String, Object> resp = new HashMap<String, Object>();
		List<DnevnoStanje> stanja = new ArrayList<DnevnoStanje>();
		List<Racun> racuni = rs.findByPreduzece(u.getPreduzece());
		List<StavkaIzvoda> stavke = new ArrayList<StavkaIzvoda>();
		for(Racun r : racuni){
			stanja.addAll(dss.findByRacun(r));
		}
		for(DnevnoStanje st : stanja){
			stavke.addAll(sis.findByDnevnoStanje(st));
		}
		resp.put("stanja", stanja);
		
		for(StavkaIzvoda stavka : stavke){
			stavka.setRacunDuznika(AES.decryptCBC(stavka.getRacunDuznika(), kk.getValue(), kk.getIv()));
			stavka.setRacunPrimaoca(AES.decryptCBC(stavka.getRacunPrimaoca(), kk.getValue(), kk.getIv()));
		}
		logger.warn("Korisnik "+ u.getId() + " uspesno je izvrsio metodu " + SifrarnikMetoda.methods.get(
 				Thread.currentThread().getStackTrace()[1].getMethodName()));
		if(stavke.isEmpty())
			return new ResponseEntity<HashMap<String, Object>>(resp, HttpStatus.OK);
		resp.put("stavke", stavke);
		
		return new ResponseEntity<HashMap<String, Object>>(resp, HttpStatus.OK);
	}
	
	@PermissionType("StavkeIzvoda:search")
	@RequestMapping(value = "/stavkeIzvoda/search/", method = RequestMethod.POST)
	    public ResponseEntity<?> searchStavkeIzvoda(HttpSession session, @RequestBody StavkaIzvoda stavka, UriComponentsBuilder ucBuilder) {
		List<StavkaIzvoda> stavke = null;
		
		 if(stavka.getDnevnoStanje().getId() != null){
			DnevnoStanje dr = dss.findOne(stavka.getDnevnoStanje().getId());
		 	stavke = sis.findByDnevnoStanje(dr);
		 }
        if(stavke == null)
			 return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		 
	     return new ResponseEntity<List<StavkaIzvoda>>(stavke, HttpStatus.OK);
    }
}
