<html>
<head>
<meta charset="ISO-8859-1" content="text/html; charset=UTF-8" >
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<title>Get certificate</title>
<script type="text/javascript">
		function getFormData($form){
		    var unindexed_array = $form.serializeArray();
		    var indexed_array = {};
	
		    $.map(unindexed_array, function(n, i){
		        indexed_array[n['name']] = n['value'];
		    });
	
		    return indexed_array;
		}
		
		$(document).ready(function(){
			$("#errorBlock").hide();
			$("#errorUname").hide();
			$("#errorPass").hide();
		
			$("#logUsername").on('blur', function(){
				if($("#logUsername").val().length === 0){
					$("#errorUname").show();
				} else {
					$("#errorUname").hide();
				}
			});
			$("#logPass").on('blur', function(){
				if($("#logPass").val().length === 0){
					$("#errorPass").show();
				} else {
					$("#errorPass").hide();
				}
			});
		});
</script>
</head>
<body>
<br></br><br></br>
	<div class="container-fluid" align="center" style=" display: table; margin: auto; width:40%; ">
		<div class=" col-md-6 col-md" id="loginBlock" >
			<form id="logForm" role="form" action="" method="post">
				<fieldset>
					<label>Username:</label><br>
					<input type="text" class="form-control" name="username" id="logUsername"  ><br>
					<div id="errorUname" >
						<p style="color: red"> Username is empty</p>
					</div>
					<label>Password:</label><br>
					<input type="password" class="form-control" name="password" id="logPass"><br>
					<div id="errorPass" >
						<p style="color: red"> Password is empty</p>
					</div>
					<input class="btn btn-lg btn-primary" type="submit" value="Sign in">
				</fieldset>
			</form> <br>
			</div>
		</div>
		
	<script type="text/javascript">
	
	$(document).on('submit','#logForm',function(e){
		e.preventDefault();
		var unameVal = $("#logUsername").val();
		var passVal = $("#logPass").val();
		if(unameVal.length != 0 && passVal.length != 0){ 
			var $form = $("#logForm");
			var d = getFormData($form);
			var s = JSON.stringify(d);
			 $.ajax({
	             type: "POST",
	             url: "/login/",
	             data: s,
	             contentType : "application/json",
	             complete: function(datas){
	            	 var loc = datas.responseText;
	            	 if(loc===""){
	            		 $.ajax({
	                         type: "POST",
	                         url: "/company/",
	                         complete: function(datas2){
	                        	 var locc2 = datas2.responseText;
	                        	 var locc22 = locc2.substring(locc2.lastIndexOf('/') + 1);
	                        	 window.location = locc22;
	                         }
	            		 });
	            	 }
	            	 else{
		            	 var loc2 = loc.substring(loc.lastIndexOf('/') + 1);
		            	 window.location = loc;
	            	 }
	             }
	         });
		}
		else {
			$("#errorUname").show();
			$("#errorPass").show();
		}
	});
	</script>
</body>
</html>