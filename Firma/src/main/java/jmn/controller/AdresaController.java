package jmn.controller;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;

import jmn.annotation.PermissionType;
import jmn.models.Adresa;
import jmn.models.Drzava;
import jmn.models.NaseljenoMesto;
import jmn.services.AdresaService;
import jmn.services.NaseljenoMestoService;

@Controller
public class AdresaController {

	@Autowired
	private AdresaService as;
	
	@Autowired
	private NaseljenoMestoService nms;
	
	@PermissionType("Adrese:view")
	@RequestMapping(value="/adrese/", method=RequestMethod.GET)
	public ResponseEntity<HashMap<String, Object>> listAllAdrese(HttpSession session){
		HashMap<String, Object> resp = new HashMap<String, Object>();
		List<NaseljenoMesto> mesta = nms.findAll();
		resp.put("mesta", mesta);
		List<Adresa> adrese = as.findAll();
		if(adrese.isEmpty())
			return new ResponseEntity<HashMap<String, Object>>(resp, HttpStatus.OK);
		resp.put("adrese", adrese);
		return new ResponseEntity<HashMap<String, Object>>(resp, HttpStatus.OK);
	}
	@PermissionType("Adrese:add")
	@RequestMapping(value = "/adrese/", method = RequestMethod.POST)
	    public ResponseEntity<Void> createAdresu(HttpSession session, @RequestBody Adresa adresa, UriComponentsBuilder ucBuilder) {
	        NaseljenoMesto d = nms.findOne(adresa.getNasMesto().getId());
	        adresa.setNasMesto(d);
	        Adresa a = as.saveAdresa(adresa);
	        try {
		        HttpHeaders headers = new HttpHeaders();
		        headers.setLocation(ucBuilder.path("/adrese/{id}").buildAndExpand(a.getId()).toUri());
		        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	        }
	        catch(Exception e){
	        	return new ResponseEntity<Void>(HttpStatus.CONFLICT);
	        }
	    }
	@PermissionType("Adrese:edit")
	@RequestMapping(value = "/adrese/{id}", method = RequestMethod.POST)
	    public ResponseEntity<?> updateAdresu(HttpSession session, @PathVariable("id") long id, @RequestBody Adresa adresa) {
	 
		 	Adresa exists = as.findOne(id);
		 	if(exists == null){
		 		return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		 	}
		 	exists.setBroj(adresa.getBroj());
		 	exists.setUlica(adresa.getUlica());
		 	exists.setNasMesto(adresa.getNasMesto());
		 	exists = as.saveAdresa(exists);
		 	return new ResponseEntity<Adresa>(exists, HttpStatus.OK);
	    }
	@PermissionType("Adrese:delete")
	 @RequestMapping(value = "/adrese/{id}", method = RequestMethod.DELETE)
	    public ResponseEntity<?> deleteAdresu(HttpSession session, @PathVariable("id") long id) {
		 Adresa exists = as.findOne(id);
		 if(exists == null)
			 return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		 
		 as.delete(exists);
	     return new ResponseEntity<Adresa>(HttpStatus.NO_CONTENT);
	    }
	 
	@PermissionType("Adrese:search")
	@RequestMapping(value = "/adrese/search/", method = RequestMethod.POST)
	    public ResponseEntity<?> searchAdrese(HttpSession session, @RequestBody Adresa adresa, UriComponentsBuilder ucBuilder) {
		 if(adresa.getUlica() == null)
			 adresa.setUlica("");
		 
		 List<Adresa> adrese = null;   
		 if(adresa.getNasMesto().getId() != null){
			NaseljenoMesto nm = nms.findOne(adresa.getNasMesto().getId());
			adrese = as.findByUlicaContainingAndBrojAndNasMesto(adresa.getUlica(), adresa.getBroj(), nm);
		 }else
			 adrese = as.findByUlicaContainingAndBroj(adresa.getUlica(), adresa.getBroj());
        if(adrese == null)
			 return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		 
	     return new ResponseEntity<List<Adresa>>(adrese, HttpStatus.OK);
    }
	
}
