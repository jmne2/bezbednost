package jmn.models;

import static javax.persistence.CascadeType.ALL;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Drzava implements Serializable{

	@Id
	@GeneratedValue
	private Long id;
	
	@Column(nullable = false)
	private String oznaka;
	
	@Column(nullable = false)
	private String naziv;
	
	@OneToMany(cascade={ALL}, fetch=FetchType.LAZY, mappedBy="drzava")
	@JsonIgnore
	private Set<NaseljenoMesto> nasMesta = new HashSet<NaseljenoMesto>();
	
	public Drzava(){
		
	}

	public Drzava(String oznaka, String naziv) {
		super();
		this.oznaka = oznaka;
		this.naziv = naziv;
	}

	public String getOznaka() {
		return oznaka;
	}

	public void setOznaka(String oznaka) {
		this.oznaka = oznaka;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public Long getId() {
		return id;
	}

	public Set<NaseljenoMesto> getNasMesta() {
		return nasMesta;
	}

	public void setNasMesta(Set<NaseljenoMesto> nasMesta) {
		this.nasMesta = nasMesta;
	}
	
	public void addNasMesto(NaseljenoMesto nasMesto){
		this.nasMesta.add(nasMesto);
		if(nasMesto.getDrzava()==null || nasMesto.getDrzava().getOznaka()!=this.oznaka){
			nasMesto.setDrzava(this);
		}
	}
	
	
}
