
insert into permission (naziv) values ('Adrese:view');
insert into permission (naziv) values ('Adrese:add');
insert into permission (naziv) values ('Adrese:edit');
insert into permission (naziv) values ('Adrese:delete');
insert into permission (naziv) values ('Adrese:search');
insert into permission (naziv) values ('Banke:view');
insert into permission (naziv) values ('Banke:add');
insert into permission (naziv) values ('Banke:edit');
insert into permission (naziv) values ('Banke:delete');
insert into permission (naziv) values ('Banke:search');
insert into permission (naziv) values ('DnevnaStanja:view');
insert into permission (naziv) values ('DnevnaStanja:add');
insert into permission (naziv) values ('DnevnaStanja:search');
insert into permission (naziv) values ('Drzave:view');
insert into permission (naziv) values ('Drzave:add');
insert into permission (naziv) values ('Drzave:edit');
insert into permission (naziv) values ('Drzave:delete');
insert into permission (naziv) values ('Drzave:search');
insert into permission (naziv) values ('NaloziZaPlacanje:view');
insert into permission (naziv) values ('NaloziZaPlacanje:add');
insert into permission (naziv) values ('NaloziZaPlacanje:edit');
insert into permission (naziv) values ('NaloziZaPlacanje:delete');
insert into permission (naziv) values ('NaloziZaPlacanje:search');
insert into permission (naziv) values ('NaseljenaMesta:view');
insert into permission (naziv) values ('NaseljenaMesta:add');
insert into permission (naziv) values ('NaseljenaMesta:edit');
insert into permission (naziv) values ('NaseljenaMesta:delete');
insert into permission (naziv) values ('NaseljenaMesta:search');
insert into permission (naziv) values ('PoslovneGodine:view');
insert into permission (naziv) values ('PoslovneGodine:add');
insert into permission (naziv) values ('PoslovneGodine:edit');
insert into permission (naziv) values ('PoslovneGodine:delete');
insert into permission (naziv) values ('PoslovneGodine:search');
insert into permission (naziv) values ('PoslovniPartner:view');
insert into permission (naziv) values ('PoslovniPartner:add');
insert into permission (naziv) values ('PoslovniPartner:edit');
insert into permission (naziv) values ('PoslovniPartner:delete');
insert into permission (naziv) values ('PoslovniPartner:search');
insert into permission (naziv) values ('PredloziZaPlacanje:view');
insert into permission (naziv) values ('PredloziZaPlacanje:add');
insert into permission (naziv) values ('PredloziZaPlacanje:edit');
insert into permission (naziv) values ('PredloziZaPlacanje:delete');
insert into permission (naziv) values ('PredloziZaPlacanje:search');
insert into permission (naziv) values ('Preduzeca:view');
insert into permission (naziv) values ('Preduzeca:add');
insert into permission (naziv) values ('Preduzeca:edit');
insert into permission (naziv) values ('Preduzeca:delete');
insert into permission (naziv) values ('Preduzeca:search');
insert into permission (naziv) values ('Racuni:view');
insert into permission (naziv) values ('Racuni:add');
insert into permission (naziv) values ('Racuni:edit');
insert into permission (naziv) values ('Racuni:delete');
insert into permission (naziv) values ('Racuni:search');
insert into permission (naziv) values ('RacuniPartnera:view');
insert into permission (naziv) values ('RacuniPartnera:add');
insert into permission (naziv) values ('RacuniPartnera:edit');
insert into permission (naziv) values ('RacuniPartnera:delete');
insert into permission (naziv) values ('RacuniPartnera:search');
insert into permission (naziv) values ('SifrarnikDelatnosti:view');
insert into permission (naziv) values ('SifrarnikDelatnosti:add');
insert into permission (naziv) values ('SifrarnikDelatnosti:edit');
insert into permission (naziv) values ('SifrarnikDelatnosti:delete');
insert into permission (naziv) values ('SifrarnikDelatnosti:search');
insert into permission (naziv) values ('StaSePlaca:view');
insert into permission (naziv) values ('StaSePlaca:add');
insert into permission (naziv) values ('StaSePlaca:edit');
insert into permission (naziv) values ('StaSePlaca:delete');
insert into permission (naziv) values ('StaSePlaca:search');
insert into permission (naziv) values ('StavkeIzvoda:view');
insert into permission (naziv) values ('StavkeIzvoda:search');
insert into permission (naziv) values ('Fakture:view');
insert into permission (naziv) values ('Fakture:edit');
insert into permission (naziv) values ('Fakture:search');
insert into permission (naziv) values ('ZatvaranjeIF:view');
insert into permission (naziv) values ('ZatvaranjeIF:add');
insert into permission (naziv) values ('ZatvaranjeIF:edit');
insert into permission (naziv) values ('ZatvaranjeIF:delete');
insert into permission (naziv) values ('ZatvaranjeIF:search');
insert into permission (naziv) values ('ZatvaranjeUF:view');
insert into permission (naziv) values ('ZatvaranjeUF:add');
insert into permission (naziv) values ('ZatvaranjeUF:edit');
insert into permission (naziv) values ('ZatvaranjeUF:delete');
insert into permission (naziv) values ('ZatvaranjeUF:search');

insert into role (naziv) values ('admin');
insert into role (naziv) values ('manager');
insert into role (naziv) values ('bookkeeper');

select * from permission;

insert into role_permission (role_id, permission_id) values (1, 1);
insert into role_permission (role_id, permission_id) values (1, 2);
insert into role_permission (role_id, permission_id) values (1, 3);
insert into role_permission (role_id, permission_id) values (1, 4);
insert into role_permission (role_id, permission_id) values (1, 5);
insert into role_permission (role_id, permission_id) values (1, 6);
insert into role_permission (role_id, permission_id) values (1, 7);
insert into role_permission (role_id, permission_id) values (1, 8);
insert into role_permission (role_id, permission_id) values (1, 9);
insert into role_permission (role_id, permission_id) values (1, 10);
insert into role_permission (role_id, permission_id) values (3, 11);
insert into role_permission (role_id, permission_id) values (3, 12);
insert into role_permission (role_id, permission_id) values (3, 13);
insert into role_permission (role_id, permission_id) values (1, 14);
insert into role_permission (role_id, permission_id) values (1, 15);
insert into role_permission (role_id, permission_id) values (1, 16);
insert into role_permission (role_id, permission_id) values (1, 17);
insert into role_permission (role_id, permission_id) values (1, 18);
insert into role_permission (role_id, permission_id) values (3, 19);
insert into role_permission (role_id, permission_id) values (3, 20);
insert into role_permission (role_id, permission_id) values (3, 21);
insert into role_permission (role_id, permission_id) values (2, 21);
insert into role_permission (role_id, permission_id) values (3, 22);
insert into role_permission (role_id, permission_id) values (3, 23);
insert into role_permission (role_id, permission_id) values (1, 24);
insert into role_permission (role_id, permission_id) values (1, 25);
insert into role_permission (role_id, permission_id) values (1, 26);
insert into role_permission (role_id, permission_id) values (1, 27);
insert into role_permission (role_id, permission_id) values (1, 28);
insert into role_permission (role_id, permission_id) values (2, 29);
insert into role_permission (role_id, permission_id) values (2, 30);
insert into role_permission (role_id, permission_id) values (2, 31);
insert into role_permission (role_id, permission_id) values (2, 32);
insert into role_permission (role_id, permission_id) values (2, 33);
insert into role_permission (role_id, permission_id) values (2, 34);
insert into role_permission (role_id, permission_id) values (2, 35);
insert into role_permission (role_id, permission_id) values (2, 36);
insert into role_permission (role_id, permission_id) values (2, 37);
insert into role_permission (role_id, permission_id) values (2, 38);
insert into role_permission (role_id, permission_id) values (2, 39);
insert into role_permission (role_id, permission_id) values (2, 40);
insert into role_permission (role_id, permission_id) values (2, 41);
insert into role_permission (role_id, permission_id) values (2, 42);
insert into role_permission (role_id, permission_id) values (2, 43);
insert into role_permission (role_id, permission_id) values (1, 44);
insert into role_permission (role_id, permission_id) values (1, 45);
insert into role_permission (role_id, permission_id) values (1, 46);
insert into role_permission (role_id, permission_id) values (1, 47);
insert into role_permission (role_id, permission_id) values (1, 48);
insert into role_permission (role_id, permission_id) values (2, 49);
insert into role_permission (role_id, permission_id) values (2, 50);
insert into role_permission (role_id, permission_id) values (2, 51);
insert into role_permission (role_id, permission_id) values (2, 52);
insert into role_permission (role_id, permission_id) values (2, 53);
insert into role_permission (role_id, permission_id) values (2, 54);
insert into role_permission (role_id, permission_id) values (2, 55);
insert into role_permission (role_id, permission_id) values (2, 56);
insert into role_permission (role_id, permission_id) values (2, 57);
insert into role_permission (role_id, permission_id) values (2, 58);
insert into role_permission (role_id, permission_id) values (1, 59);
insert into role_permission (role_id, permission_id) values (1, 60);
insert into role_permission (role_id, permission_id) values (1, 61);
insert into role_permission (role_id, permission_id) values (1, 62);
insert into role_permission (role_id, permission_id) values (1, 63);
insert into role_permission (role_id, permission_id) values (3, 64);
insert into role_permission (role_id, permission_id) values (3, 65);
insert into role_permission (role_id, permission_id) values (3, 66);
insert into role_permission (role_id, permission_id) values (3, 67);
insert into role_permission (role_id, permission_id) values (3, 68);
insert into role_permission (role_id, permission_id) values (3, 69);
insert into role_permission (role_id, permission_id) values (2, 69);
insert into role_permission (role_id, permission_id) values (3, 70);
insert into role_permission (role_id, permission_id) values (2, 70);
insert into role_permission (role_id, permission_id) values (3, 71);
insert into role_permission (role_id, permission_id) values (3, 72);
insert into role_permission (role_id, permission_id) values (3, 73);
insert into role_permission (role_id, permission_id) values (3, 74);
insert into role_permission (role_id, permission_id) values (3, 75);
insert into role_permission (role_id, permission_id) values (3, 76);
insert into role_permission (role_id, permission_id) values (3, 77);
insert into role_permission (role_id, permission_id) values (3, 78);
insert into role_permission (role_id, permission_id) values (3, 79);
insert into role_permission (role_id, permission_id) values (3, 80);
insert into role_permission (role_id, permission_id) values (3, 81);
insert into role_permission (role_id, permission_id) values (3, 82);
insert into role_permission (role_id, permission_id) values (3, 83);

insert into sifrarnik_delatnosti (naziv_delatnosti, sifra) value ('delatnost1','balbla');
insert into sifrarnik_delatnosti (naziv_delatnosti, sifra) value ('delatnost2','heheheh');
insert into sifrarnik_delatnosti (naziv_delatnosti, sifra) value ('delatnost3','mayday');
insert into sifrarnik_delatnosti (naziv_delatnosti, sifra) value ('delatnost4','sifrasifrasifra');

insert into drzava (oznaka, naziv) values ('FRD', 'Ferelden');
insert into drzava (oznaka, naziv) values ('ORL', 'Orlais');
insert into drzava (oznaka, naziv) values ('FRM', 'Free Marches');
insert into drzava (oznaka, naziv) values ('ANV', 'Antiva');
insert into drzava (oznaka, naziv) values ('TVN', 'Tevinter');

insert into naseljeno_mesto (naziv, post_broj, drzava) values ('Ostagar', '11245', 1);
insert into naseljeno_mesto (naziv, post_broj, drzava) values ('Krocari Wilds', '11246', 1);
insert into naseljeno_mesto (naziv, post_broj, drzava) values ('Lothering', '11247', 1);
insert into naseljeno_mesto (naziv, post_broj, drzava) values ('Denerim', '11254', 1);
insert into naseljeno_mesto (naziv, post_broj, drzava) values ('Val Royeaux', '12368', 2);
insert into naseljeno_mesto (naziv, post_broj, drzava) values ('Halamshiral', '12366', 2);
insert into naseljeno_mesto (naziv, post_broj, drzava) values ('Val Chevin', '12360', 2);
insert into naseljeno_mesto (naziv, post_broj, drzava) values ('Ghislain', '12371', 2);
insert into naseljeno_mesto (naziv, post_broj, drzava) values ('Kirkwall', '14188', 3);
insert into naseljeno_mesto (naziv, post_broj, drzava) values ('Starkhaven', '14199', 3);
insert into naseljeno_mesto (naziv, post_broj, drzava) values ('Wycome', '14190', 3);
insert into naseljeno_mesto (naziv, post_broj, drzava) values ('Antiva City', '13522', 4);
insert into naseljeno_mesto (naziv, post_broj, drzava) values ('Rivain', '13521', 4);
insert into naseljeno_mesto (naziv, post_broj, drzava) values ('Cumberland', '13527', 4);
insert into naseljeno_mesto (naziv, post_broj, drzava) values ('Minrathos', '15111', 5);
insert into naseljeno_mesto (naziv, post_broj, drzava) values ('Qarinus', '15120', 5);

insert into adresa (broj, ulica, nas_mesto) values (12, 'Maric Way', 1);
insert into adresa (broj, ulica, nas_mesto) values (1, 'Chantry Pass', 3);
insert into adresa (broj, ulica, nas_mesto) values (40, 'Kingsway Lane', 4);
insert into adresa (broj, ulica, nas_mesto) values (11, 'Elven River', 4);
insert into adresa (broj, ulica, nas_mesto) values (3, 'Redflow', 2);
insert into adresa (broj, ulica, nas_mesto) values (20, 'Celene Avenue', 6);
insert into adresa (broj, ulica, nas_mesto) values (5, 'Chevalier Passage', 6);
insert into adresa (broj, ulica, nas_mesto) values (18, 'Justiniva Street', 7);
insert into adresa (broj, ulica, nas_mesto) values (24, 'Radiant Blvd', 8);
insert into adresa (broj, ulica, nas_mesto) values (10, 'Draugr Way', 9);
insert into adresa (broj, ulica, nas_mesto) values (6, 'Nightingale Flock', 10);
insert into adresa (broj, ulica, nas_mesto) values (2, 'Hawke Avenue', 11);
insert into adresa (broj, ulica, nas_mesto) values (11, 'Halla Pass', 12);
insert into adresa (broj, ulica, nas_mesto) values (14, 'Pillaging Street', 13);
insert into adresa (broj, ulica, nas_mesto) values (30, 'Soleil', 14);
insert into adresa (broj, ulica, nas_mesto) values (9, 'Magisterium Blvd', 15);
insert into adresa (broj, ulica, nas_mesto) values (16, 'Pavus Street', 16);
insert into adresa (broj, ulica, nas_mesto) values (1, 'Imperius Rex', 15);

INSERT INTO preduzece (`br_telefona`,`email`,`fax`,`maticni_broj`,`naziv`,`pib`,`adresa`,`sifra_delatnosti`) VALUES
(063334698,'mejl@preduceze1.org',063334689,12345678,'Preduzece 1',123456789,5,1);
INSERT INTO preduzece (`br_telefona`,`email`,`fax`,`maticni_broj`,`naziv`,`pib`,`adresa`,`sifra_delatnosti`) VALUES
(067335648,'prmr1@nestoOvde.org',0675896312,12344479,'Preduzece 2',183466789,9,2);
INSERT INTO preduzece (`br_telefona`,`email`,`fax`,`maticni_broj`,`naziv`,`pib`,`adresa`,`sifra_delatnosti`) VALUES
(067337896,'saskia@scoiatel.org',0675899932,12344888,'Preduzece 3',183466990,12,3);

insert into poslovna_godina (godina, zakljucena, preduzece) values (2014, true, 2);

INSERT INTO banka(`naziv`)VALUES('Trevelyan Sovereins');
INSERT INTO banka(`naziv`)VALUES('Mahakham Credit');
INSERT INTO banka(`naziv`)VALUES('Vellen Group');

INSERT INTO poslovni_partner(`br_telefona`,`email`,`fax`,`maticni_broj`,`naziv`,`pib`,`vrsta`,`adresa`,`preduzece`,
`sifra_delatnosti`)VALUES(063265578,'emphyr@Emreis.nlfg',064785527,88698744,'Emreis Navigation', 332147885,'prodaje',
10, 2,1), (067112578,'cirilla@lionfling.cntr',067711527,80168704,'Lion Transportation Co.', 428847801,
'prodaje', 5, 3,2), (3219764534,'ghtfgvdh@fhbdjbn.fbh',2625646346,64946341,'Viper School Poisons',943416436,'kupuje',18,1,4);

INSERT INTO racun_partnera (`br_racuna`,`banka`,`poslovni_partner`)VALUES('144865489077340078', 1,1);
INSERT INTO racun_partnera (`br_racuna`,`banka`,`poslovni_partner`)VALUES('234905481174550070', 2,2);

INSERT INTO predlog_placanja (`broj`,`datum`,`status`, `preduzece`)VALUES(33586,'2013-08-30','odobren', 1),(33550,'2015-08-12','odobren', 2), (33512,'2015-06-30','odobren', 2),
(32999,'2012-04-10','odbijen', 2), (11112,'2014-08-30','odobren', 1), (11200,'2014-02-28','na cekanju', 1), (2220,'2014-12-01','odbijen', 3),
(620,'2015-05-12','odobren', 3);

INSERT INTO faktura (`broj_fakture`, `datum_fakture`, `datum_valute`, `oznaka_valute`, `preostali_iznos`, `uk_iznos`, `ukupan_porez`, `ukupan_rabat`, `ukupno_robaiusluge`, `vrednost_robe`, `vrednost_usluga`, `vrsta_fakture`, `poslovna_godina`, `poslovni_partner`, `preduzece`, `racun_partnera`)
VALUES ('1', '2017-01-01', '2017-01-01', 'rsd', '1', '1', '1', '1', '1', '1', '1', 'ulazne', '1', '1', '2', '2'),
('2', '2017-02-10', '2017-02-10', 'rsd', '1', '1', '1', '1', '1', '1', '1', 'ulazne', 1, 1, 2, 1);
INSERT INTO `racun` VALUES (1,'123456789111111111',3,2);



select * from predlog_placanja;

INSERT INTO `sta_se_placa` VALUES (1,500,'123456789111111111',2,3),(2,1450,'123456789111111111',2,3),(3,5000,'123456789111111111',2,3),
(4,6000,'123456789111111111',2,2),(5,3020,'123456789111111111',2,2);

select * from sta_se_placa;

select * from nalog_za_placanje;

INSERT INTO `dnevno_stanje` VALUES (1,'2017-06-14',1800,1500,0,0,1800,1);
INSERT INTO `stavka_izvoda` VALUES (1,'2017-06-14 00:00:00','2017-06-14 00:00:00','Mika Mikic Bg',300,2,2,'222','333','Pera Peric Novi Sad','123456789222222222','123456789111111111','P','ISPIT',1),
(2,'2017-06-14 00:00:00','2017-06-14 00:00:00','Mika Mikic Bg',300,2,2,'222','333','Pera Peric Novi Sad','123456789222222222','123456789111111111','P','ISPIT',1);



