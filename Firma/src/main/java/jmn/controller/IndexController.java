package jmn.controller;

import java.net.MalformedURLException;
import java.net.URL;
import java.security.SecureRandom;
import java.sql.Date;
import java.util.GregorianCalendar;

import javax.servlet.http.HttpSession;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import jmn.annotation.UserRequired;
import jmn.izvod.IzvodWS;
import jmn.izvod.data.NalogBanka;
import jmn.models.SifrarnikMetoda;
import jmn.models.User;
import jmn.pojo.UserData;
import jmn.pojo.UserHolder;
import jmn.services.PermissionService;
import jmn.services.PreduzeceService;
import jmn.services.RoleService;
import jmn.services.UserService;
import jmn.soapHandler.MyHandlerResolver;

@Controller
public class IndexController {

	final static Logger logger = Logger.getLogger(IndexController.class);
	
	@Autowired
	private UserService us;
	
	@Autowired
	private RoleService rs;
	
	@Autowired
	private PermissionService ps;
	
	@Autowired
	private PreduzeceService prs;
	
	 @RequestMapping(value="/", method = RequestMethod.GET)
	    public String getIndexPage(HttpSession session) throws DatatypeConfigurationException {
		 System.out.println("aloooooo"); 
		 session.setMaxInactiveInterval(60*5);
		 talkToBank(session);
	        return "index";
	    }
	 
	 public void talkToBank(HttpSession session) throws DatatypeConfigurationException {
		 System.out.println("testiiing");
			try {
				//kreiranje web servisa
				//https://localhost:8443/banka-ws/services/IzvodWS?wsdl
				URL wsdlLocation = new URL("http://localhost:8081/banka-ws/services/IzvodWS?wsdl");
				QName serviceName = new QName("http://izvod", "IzvodWSService");
				QName portName = new QName("http://izvod", "IzvodWSPort");

				Service service = Service.create(wsdlLocation, serviceName);
				
				service.setHandlerResolver(new MyHandlerResolver());
				
				IzvodWS izvod = service.getPort(portName, IzvodWS.class);
				
			//	salje se kod banke sa kojom se razgovara:
				izvod.setBankCode("123");
	
			// slanje zahteva za izvod sa danasnjim datumom:
				/*ZahtevZaIzvod zahtev = new ZahtevZaIzvod();
				java.util.Date date = new java.util.Date();
				GregorianCalendar c = new GregorianCalendar();
				c.setTime(date);
				XMLGregorianCalendar date2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);

				zahtev.setBrojRacuna("123456789222222222");
				zahtev.setDatum(date2);
				
				//TODO: razresiti izmenu rednog broja preseka
				zahtev.setRedniBrPreseka(1);
				
				Izvod response = izvod.getIzvod(zahtev);
				System.out.println("RESPONSE JE " + response.getPrethodnoStanje() + "  " + response.getNovoStanje() + "  " + response.getUkupnoNaTeret());
		*/
		// slanje naloga za prenos banci:
				
				NalogBanka nb = new NalogBanka();
				java.util.Date date = new java.util.Date();
				Date datum = new Date(date.getTime());
				System.out.println("Datum je " + datum.toString());

				GregorianCalendar c = new GregorianCalendar();
				c.setTime(date);
				XMLGregorianCalendar date2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);

				nb.setDatumNaloga(date2);
				nb.setDatumValute(date2);
				nb.setHitno(true);
				nb.setIdPoruke("bla");
				nb.setIznos(300.00);
				nb.setModelOdobrenja(2);
				nb.setModelZaduzenja(2);
				nb.setOznakaValute("DIN");
				nb.setPozivNaBrojOdobrenja("222");
				nb.setPozivNaBrojZaduzenja("333");
				nb.setPrimalac("Pera Peric Novi Sad");
				nb.setRacunPrimaoca("456456789111111111");
				nb.setUplatilac("Mika Mikic Bg");
				nb.setRacunUplatioca("123456789222222222");
				
				// set bank code naspram racuna uplatioca
				
				nb.setSvrhaUplate("ISPIT");
				izvod.sendNalog(nb);
				
				
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} 
	    }
	
	 @RequestMapping(value="/login/", method = RequestMethod.POST)
	    public ResponseEntity<?> login(@RequestBody UserHolder uh, HttpSession session) {
		 	logger.info("Stiglo : " + uh.getUname());
		 	User u = us.findByUsername(uh.getUname());
		 	UserData u1 = new UserData(u.getId(), u.getUsername(),u.getRoles(), u.getPreduzece());
		 	if(BCrypt.checkpw(uh.getPname(), u.getPassword())){
		 		logger.warn("Korisnik "+ u.getId() + " uspesno je izvrsio metodu " + SifrarnikMetoda.methods.get(
		 				Thread.currentThread().getStackTrace()[1].getMethodName()));
		 		session.setAttribute("user", u1);
		 	} else {
		 		logger.error("Greska prilikom izvrsenja metode " +  SifrarnikMetoda.methods.get(Thread.currentThread().getStackTrace()[1].getMethodName()) 
		 		+ " za korisnika " + u.getId());
		 		return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		 	}
	        return new ResponseEntity<UserData>(u1, HttpStatus.OK);
	 }
	 
	 @UserRequired
	 @RequestMapping(value="/logout/", method = RequestMethod.POST)
	 public ModelAndView logout(HttpSession session) {
		 	UserData u = (UserData) session.getAttribute("user");
		 	session.setAttribute("user", null);
		 	session.invalidate();
		 	logger.warn("Korisnik "+ u.getId() + " uspesno je izvrsio metodu " + SifrarnikMetoda.methods.get(
	 				Thread.currentThread().getStackTrace()[1].getMethodName()));
	        return new ModelAndView("redirect:/");
	 }
	 @RequestMapping(value="/check/", method = RequestMethod.GET)
	    public ResponseEntity<?> check(HttpSession session) {
		 	UserData u = (UserData)session.getAttribute("user");
		 	if(u!= null)
		 		return new ResponseEntity<UserData>(u, HttpStatus.OK);
			return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	    }
	 
	 @UserRequired
	 @RequestMapping(value="/change/", method = RequestMethod.POST)
	    public Object changePass(@RequestBody UserHolder uh, HttpSession session) {
		 	UserData u = (UserData)session.getAttribute("user");
		 	if(u == null)
		 		return new ModelAndView("redirect:/");
		 	User us1 = us.findOne(u.getId());
		 	if(us1 == null) 
		 		return new ModelAndView("redirect:/");
		 	
		 	String slt = BCrypt.gensalt(15, new SecureRandom());
			String hashed = BCrypt.hashpw(uh.getPname(), slt);
			us1.setPassword(hashed);
			us1.setSalt(slt);
			us1 = us.saveUser(us1);
			session.setAttribute("user", u);
			logger.warn("Korisnik "+ u.getId() + " uspesno je izvrsio metodu " + SifrarnikMetoda.methods.get(
	 				Thread.currentThread().getStackTrace()[1].getMethodName()));
	        return new ResponseEntity<UserData>(u, HttpStatus.OK);
	 }
	 
	
}
