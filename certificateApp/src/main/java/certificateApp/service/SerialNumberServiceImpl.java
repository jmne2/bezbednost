package certificateApp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import certificateApp.models.SerialNumbers;
import certificateApp.repository.SerialNumbersRepository;

@Service
@Transactional
public class SerialNumberServiceImpl implements SerialNumbersService{

	@Autowired
	private SerialNumbersRepository sr ;
	
	@Override
	public List<SerialNumbers> findAll() {
		return (List<SerialNumbers>) sr.findAll();
	}

	@Override
	public SerialNumbers saveSerialNumbers(SerialNumbers sn) {
		return sr.save(sn);
	}

	@Override
	public SerialNumbers findOne(long id) {
		return sr.findOne(id);
	}

	@Override
	public SerialNumbers findBySerialNumber(String serialNumber) {
		return sr.findBySerialNumber(serialNumber);
	}

	@Override
	public void delete(SerialNumbers sn) {
		sr.delete(sn);
	}

	@Override
	public List<SerialNumbers> findByUsername(String username) {
		return sr.findByUsername(username);
	}

	@Override
	public SerialNumbers findByAlias(String alias) {
		return sr.findByAlias(alias);
	}

	@Override
	public List<SerialNumbers> findByIssuerSerNum(String issuerSerNum) {
		return sr.findByIssuerSerNum(issuerSerNum);
	}

	
}
