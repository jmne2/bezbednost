package jmn.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class DEK {

	@Id
	@GeneratedValue
	private Long id;
	
	@Column(nullable = false)
	private String value;
	
	@Column(nullable = false)
	private String iv;
	
	@OneToOne(fetch = FetchType.LAZY)
	private Preduzece preduzece;
	
	public DEK() {
	}

	public DEK(String value, String iv, Preduzece preduzece) {
		super();
		this.value = value;
		this.preduzece = preduzece;
		this.iv = iv;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Long getId() {
		return id;
	}

	public Preduzece getPreduzece() {
		return preduzece;
	}

	public void setPreduzece(Preduzece preduzece) {
		this.preduzece = preduzece;
	}

	public String getIv() {
		return iv;
	}

	public void setIv(String iv) {
		this.iv = iv;
	}
	
	
}
