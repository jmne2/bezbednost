package jmn.controller;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;

import jmn.annotation.PermissionType;
import jmn.configuration.AES;
import jmn.models.Banka;
import jmn.models.DEK;
import jmn.models.Preduzece;
import jmn.models.Racun;
import jmn.models.SifrarnikMetoda;
import jmn.pojo.UserData;
import jmn.services.BankaService;
import jmn.services.DEKService;
import jmn.services.PreduzeceService;
import jmn.services.RacunService;

@Controller
public class RacunController {

	final static Logger logger = Logger.getLogger(RacunController.class);
	
	@Autowired
	private RacunService rs;
	
	@Autowired
	private PreduzeceService ps;
	
	@Autowired
	private BankaService bs;
	
	@Autowired
	private DEKService dks;
	
	@PermissionType("Racuni:view")
	@RequestMapping(value="/racuni/", method=RequestMethod.GET)
	public ResponseEntity<HashMap<String, Object>> fetchRacune(HttpSession session){
		UserData u = (UserData)session.getAttribute("user");
		int pb = u.getPreduzece().getPib();
		List<Racun> racuni = rs.findByPreduzece(u.getPreduzece());
		DEK kk = dks.findByPreduzece(u.getPreduzece());
		kk.setValue(AES.decryptDEK(kk.getValue(), Integer.toString(pb)));
		for(Racun r : racuni){
			r.setBrRacuna(AES.decryptCBC(r.getBrRacuna(), kk.getValue(), kk.getIv()));
		}
		List<Banka> banke = bs.findAll();
		List<Preduzece> preduzeca = ps.findAll();
		HashMap<String, Object> resp = new HashMap<String, Object>();
		resp.put("banke", banke);
		resp.put("preduzeca", preduzeca);
		if(racuni.isEmpty())
			return new ResponseEntity<HashMap<String, Object>>(resp, HttpStatus.OK);
		resp.put("racuni", racuni);
		return new ResponseEntity<HashMap<String, Object>>(resp, HttpStatus.OK);
	}
	
	@PermissionType("Racuni:add")
	@RequestMapping(value = "/racuni/", method = RequestMethod.POST)
	public ResponseEntity<Void> createRacun(HttpSession session, @RequestBody Racun racun, UriComponentsBuilder ucBuilder) {
			UserData u = (UserData) session.getAttribute("user");
			int pb = u.getPreduzece().getPib();
			DEK kk = dks.findByPreduzece(u.getPreduzece());
			kk.setValue(AES.decryptDEK(kk.getValue(), Integer.toString(pb)));
			racun.setBrRacuna(AES.encryptCBC(racun.getBrRacuna(), kk.getValue(), kk.getIv()));
	        Racun d = rs.saveRacun(racun); 
	        try {
		        HttpHeaders headers = new HttpHeaders();
		        headers.setLocation(ucBuilder.path("/racuni/{id}").buildAndExpand(d.getId()).toUri());
		        logger.warn("Korisnik "+ u.getId() + " uspesno je izvrsio metodu " + SifrarnikMetoda.methods.get(
			 				Thread.currentThread().getStackTrace()[1].getMethodName()));
		        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	        }
	        catch(Exception e){
	        	logger.error("Greska pri izvrsenju " + SifrarnikMetoda.methods.get(Thread.currentThread().getStackTrace()[1].getMethodName())
	        	+ " za korisnika " + u.getId());
	        	return new ResponseEntity<Void>(HttpStatus.CONFLICT);
	        }
	    }
	@PermissionType("Racuni:edit")
	 @RequestMapping(value = "/racuni/{id}", method = RequestMethod.POST)
	    public ResponseEntity<?> updateRacun(HttpSession session, @PathVariable("id") long id, @RequestBody Racun racun) {
	 		UserData u = (UserData) session.getAttribute("user");	 
			Racun exists = rs.findOne(id);
		 	if(exists == null){
		 		logger.error("Greska pri izvrsenju " + SifrarnikMetoda.methods.get(Thread.currentThread().getStackTrace()[1].getMethodName())
	        	+ " za korisnika " + u.getId());
		 		return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		 	}

			int pb = u.getPreduzece().getPib();
			DEK kk = dks.findByPreduzece(u.getPreduzece());
			kk.setValue(AES.decryptDEK(kk.getValue(), Integer.toString(pb)));
		 	Banka b = bs.findOne(racun.getBanka().getId());
		 	Preduzece p = ps.findOne(racun.getPreduzece().getId());
		 	exists.setBanka(b);
		 	exists.setBrRacuna(AES.decryptCBC(racun.getBrRacuna(), kk.getValue(), kk.getIv()));
		 	exists.setPreduzece(p);
		 	exists = rs.saveRacun(exists);
	        logger.warn("Korisnik "+ u.getId() + " uspesno je izvrsio metodu " + SifrarnikMetoda.methods.get(
		 				Thread.currentThread().getStackTrace()[1].getMethodName()));
		 	return new ResponseEntity<Racun>(exists, HttpStatus.OK);
	    }
	
	@PermissionType("Racuni:delete")
	@RequestMapping(value = "/racuni/{id}", method = RequestMethod.DELETE)
	    public ResponseEntity<?> deleteRacun(HttpSession session, @PathVariable("id") long id) {
		UserData u = (UserData) session.getAttribute("user");
		Racun exists = rs.findOne(id); 
		 if(exists == null){
				logger.error("Greska pri izvrsenju " + SifrarnikMetoda.methods.get(Thread.currentThread().getStackTrace()[1].getMethodName())
	        	+ " za korisnika " + u.getId());
			 return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		 }
		 rs.delete(id);
	        logger.warn("Korisnik "+ u.getId() + " uspesno je izvrsio metodu " + SifrarnikMetoda.methods.get(
		 				Thread.currentThread().getStackTrace()[1].getMethodName()));
	     return new ResponseEntity<Racun>(HttpStatus.NO_CONTENT);
	    }
	
	@PermissionType("Racuni:search")
	@RequestMapping(value = "/racuni/search/", method = RequestMethod.POST)
	    public ResponseEntity<?> searchRacuni(HttpSession session, @RequestBody Racun racun) {
			if(racun.getBrRacuna() == null)
				racun.setBrRacuna("");
			List<Racun> racuni = null;
			if(racun.getBanka() != null)
				racuni = rs.findByPreduzeceAndBanka(racun.getPreduzece(),racun.getBanka());
			else
				racuni = rs.findByPreduzece(racun.getPreduzece());
		 if(racuni == null)
			 return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);

	     return new ResponseEntity<List<Racun>>(racuni, HttpStatus.OK);
	    }
}
