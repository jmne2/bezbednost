'use strict';

angular.module('repo3App').controller('ZatvaranjeIFController', ['$scope', 'ZatvaranjeIFService', '$filter', function($scope,ZatvaranjeIFService, $filter) {
   console.log("USAO U KONTROLER STAVKI FAKTURE!")
	var self = this;
    self.stavke = [];
    self.stavkaIzvodaZaZatvaranje = {};
    self.ifakture = [];
    self.ifakturaZaZatvaranje = {};
    self.zatvorene = [];
    self.razvezane = [];
    self.partneri = [];
    self.show = false;
    self.logged = false;
    self.children = [""];
    self.iznos = 0;
    self.iznosFakture = "0";
    self.Zatvaranje ={};
    
    self.dugmici = true;
    self.stavkeIzvodashow = false;
    self.ifaktureShow = false;
    self.stornoIzlaznihShow = false;
    self.formaDugmiciShow = false;
    self.report = false;
    
    self.showDugmici = showDugmici;
    self.showZatvaranje = showZatvaranje;
    self.showStorno = showStorno;
    self.showReport = showReport;
    self.razvezi = razvezi;
    self.zatvori = zatvori;
    self.zatvoriAuto = zatvoriAuto;
    
    
    self.submit = submit;
    self.reset = reset;
//    self.search = search;
    self.close = close;
    self.currState = "edit";
    self.indSelected = -1;
    self.indSelectedF = -1;
    self.indSelectedS = -1;
    $scope.indSelectedVote = -1;
    $scope.indSelectedVoteF = -1;
    $scope.indSelectedVoteS = -1;
    $scope.required = true;
    
    

    
    $scope.$on('dispAfterLog', function(event, args) {
    	if(self === args.ctrl){
    		self.logged = true;
    		self.show = true;
    	} else {
    		self.logged = false;
    		self.show = false;
    	}
    });
    
    $scope.$on('changeState', function(event, args) {    	
    	if(self === args.ctrl){
    		self.currState = args.state;
    		console.log(self.currState);
    		if(self.currState === "add"){
    		self.currState = "edit";
    		}
    	//	$scope.required = (self.currState === "search") ? false : true;
    	}
    });
    
    $scope.$on('hide', function(event, args) {
    	self.show = false;
    	
    });
    $scope.$on('resetEvent', function(event, args) {    	
    	if(self === args.ctrl){
    		self.currState = "edit";
    		fetchAllStavkeIzvoda();
    		fetchAllIlazneFakture();
    		fetchAllZatvoreneFakture();
    	}
    });
     
    $scope.$on('deleteSel', function(event, args) {    	
    	if(self === args.ctrl){
    		showStorno();
    		self.currState = "edit";
    	}
    });
 
    $scope.select = function(item) {
        item.selected ? item.selected = false : item.selected = true;
      }
 /*   
    $scope.selectOther = function(item) {
        item.selectedOther ? item.selectedOther = false : item.selectedOther = true;
      }
    
    $scope.selectOther1 = function(item) {
        item.selectedOther1 ? item.selectedOther1 = false : item.selectedOther1 = true;
      }
    
    $scope.getAllSelectedRows = function() {
    	self.ifaktureZaZatvaranje = $filter("filter")(self.ifakture, {
          selected: true
        }, true);
        
    	$filter("filter")(self.ifakture, {
            selected: false
          }, false);
    	
        self.stavkeIzvodaZaZatvaranje = $filter("filter")(self.stavke,{
        	selectedOther: true}, true);
        console.log(self.ifaktureZaZatvaranje);
        console.log(self.stavkeIzvodaZaZatvaranje);
        
      }
    */
    $scope.getSelectedZaRazvezivanje = function (){
    	self.razvezane = $filter("filter")(self.zatvorene,{
        	selectedOther1: true}, true);
    	
    	if (self.stornoIzlaznihShow){
        	razveziIzlazne(self.razvezane);
        }
    }
    
    
   
    $scope.indSelectedVote = null;
    $scope.setSelected = function (indSelectedVote) {
       $scope.indSelectedVote = indSelectedVote;
       self.indSelected = indSelectedVote;
       if(self.currState === "edit"){
    	   var id = self.partneri[indSelectedVote].id;
    	   edit(id);
   		}
    };
      
    $scope.setSelectedF = function (indSelectedVoteF) {
        $scope.indSelectedVoteF = indSelectedVoteF;
        self.indSelectedF = indSelectedVoteF;
        self.ifakturaZaZatvaranje = self.ifakture[indSelectedVoteF];
        
     };  
     
     $scope.setSelectedS = function (indSelectedVoteS) {
         $scope.indSelectedVoteS = indSelectedVoteS;
         self.indSelectedS = indSelectedVoteS;
         self.stavkaIzvodaZaZatvaranje = self.stavke[indSelectedVoteS];
         self.iznos = self.stavkaIzvodaZaZatvaranje.iznos;
         self.iznosFakture = self.stavkaIzvodaZaZatvaranje.iznos;
         
      }; 
    
    function fetchAllPoslovniPartneri(){
    	ZatvaranjeIFService.fetchAllPoslovniPartneri()
            .then(
            function(d) {
            	self.partneri = d;
            },
            function(errResponse){
                console.error('Error while fetching poslovni partneri');
            }
        );
    }
    
    function fetchAllStavkeIzvoda(id){
    	ZatvaranjeIFService.fetchAllStavkeIzvoda(id)
            .then(
            function(d) {
            	self.stavke = d;
            },
            function(errResponse){
                console.error('Error while fetching stavke izvoda');
            }
        );
    }
 
    
    function fetchAllIlazneFakture(id){
    	ZatvaranjeIFService.fetchAllIlazneFakture(id)
            .then(
            function(d) {
            	self.ifakture = d;
            },
            function(errResponse){
                console.error('Error while fetching izlazne fakture');
            }
        );
    }
    
 
    function fetchAllZatvoreneFakture(){
    	ZatvaranjeIFService.fetchAllZatvoreneFakture()
            .then(
            function(d) {
            	self.zatvorene = d;
            	console.log(d);
            },
            function(errResponse){
                console.error('Error while fetching izlazne fakture');
            }
        );
    }
    
 //   function closeIzlazneFakture(izlazneFaktura,stavkeIzvoda, iznos){
    function closeIzlazneFakture(zatvaranjeIF){
    //	ZatvaranjeIFService.closeIzlazneFakture(izlazneFaktura,stavkeIzvoda, iznos)
    	ZatvaranjeIFService.closeIzlazneFakture(zatvaranjeIF)
        .then(
        		console.log("zatvoreno")
      ,
        function(errResponse){
            console.error('Error while closing invoices');
        }
        );
    }
    
    
    function razveziIzlazne(zatvorenaStavka){
    	ZatvaranjeIFService.razveziIzlazne(zatvorenaStavka)
        .then(
        fetchAllZatvoreneFakture(),
        function(errResponse){
            console.error('Error while closing invoices');
        }
        );
    }
    
    function AutomatskoZatvaranje(){
    	ZatvaranjeIFService.AutomatskoZatvaranje()
        .then(
        fetchAllZatvoreneFakture(),
        function(errResponse){
            console.error('Error while closing invoices auto');
        }
        );
    }
    
    function submit() {
    	if (self.currState === "add"){
    		self.currState = "edit";
        }else if (self.currState === "edit"){
        } else if (self.currState === "search"){}
	        reset();
    }
 
    function edit(id){
    	fetchAllStavkeIzvoda(id);
		fetchAllIlazneFakture(id);
    }
 

 
 
    function reset(){
    	
    }
 /*   
    function search(stavkaFakture){
    	StavkeFaktureService.searchStavkeFakture(stavkaFakture)
        .then(
        function(d) {
        	self.stavkeFakture = d;
            $scope.indSelectedVote = -1;
            self.indSelected = $scope.indSelectedVote;
        },
        function(errResponse){
            console.error('Error while fetching stavke fakture');
        }
        );
    }
    */
    
    function zatvori(){
    	if(!angular.isDefined(self.ifakturaZaZatvaranje) || self.ifakturaZaZatvaranje===null){
    		alert("Selektujte fakturu za zatvaranje");
    	}else if(self.stavkaIzvodaZaZatvaranje === undefined){
    		alert("Selektujte stavku izvoda za zatvaranje");
    	}else{
    		self.Zatvaranje.iznos = self.iznos;
    		self.Zatvaranje.idFakture = self.ifakturaZaZatvaranje.id;
    		self.Zatvaranje.idStavke = self.stavkaIzvodaZaZatvaranje.id;
    	//	closeIzlazneFakture(self.ifakturaZaZatvaranje,self.stavkaIzvodaZaZatvaranje, self.iznos);
    		closeIzlazneFakture(self.Zatvaranje);
 /*   	var id = self.partneri[$scope.indSelectedVote].id;
    	edit(id);
    	self.stavkaIzvodaZaZatvaranje = {};
        self.ifakturaZaZatvaranje = {};
        self.iznos = 0; 
        self.indSelectedF = -1;
        self.indSelectedS = -1;
        $scope.indSelectedVoteF = -1;
        $scope.indSelectedVoteS = -1;*/
    	}
    }
    
    function zatvoriAuto(){
    	AutomatskoZatvaranje();
    	alert("Fakture su automatski zatvorene!")
    }
    
    function razvezi(){
    	
    }
    
    function showDugmici(){
    	self.dugmici = true;
        self.stavkeIzvodashow = false;
        self.ifaktureShow = false;
        self.stornoIzlaznihShow = false;
        self.formaDugmiciShow = false;
    }
    
    function showZatvaranje(){
    	fetchAllPoslovniPartneri();
    	self.dugmici = false;
        self.stavkeIzvodashow = true;
        self.ifaktureShow = true;
        self.stornoIzlaznihShow = false;
        self.formaDugmiciShow = true;
        
    }
    
    function showStorno(){
    	self.dugmici = false;
        self.stavkeIzvodashow = false;
        self.ifaktureShow = false;
        self.stornoIzlaznihShow = true;
        self.formaDugmiciShow = true;
        fetchAllZatvoreneFakture();
    }
    
    function showReport(){
    	self.dugmici = false;
    	self.stavkeIzvodashow = false;
	    self.ifaktureShow = false;
	    self.stornoIzlaznihShow = false;
	    self.formaDugmiciShow = false;
		self.report = true;
    }
}]);