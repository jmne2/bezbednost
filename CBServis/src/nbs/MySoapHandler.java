package nbs;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.StringWriter;
import java.security.Key;
import java.security.KeyException;
import java.security.KeyStore;
import java.security.PublicKey;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.xml.crypto.AlgorithmMethod;
import javax.xml.crypto.KeySelector;
import javax.xml.crypto.KeySelectorException;
import javax.xml.crypto.KeySelectorResult;
import javax.xml.crypto.XMLCryptoContext;
import javax.xml.crypto.XMLStructure;
import javax.xml.crypto.dsig.CanonicalizationMethod;
import javax.xml.crypto.dsig.DigestMethod;
import javax.xml.crypto.dsig.Reference;
import javax.xml.crypto.dsig.SignatureMethod;
import javax.xml.crypto.dsig.SignedInfo;
import javax.xml.crypto.dsig.Transform;
import javax.xml.crypto.dsig.XMLSignature;
import javax.xml.crypto.dsig.XMLSignatureFactory;
import javax.xml.crypto.dsig.dom.DOMSignContext;
import javax.xml.crypto.dsig.dom.DOMValidateContext;
import javax.xml.crypto.dsig.keyinfo.KeyInfo;
import javax.xml.crypto.dsig.keyinfo.KeyInfoFactory;
import javax.xml.crypto.dsig.keyinfo.KeyValue;
import javax.xml.crypto.dsig.keyinfo.X509Data;
import javax.xml.crypto.dsig.spec.C14NMethodParameterSpec;
import javax.xml.crypto.dsig.spec.TransformParameterSpec;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFactory;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

public class MySoapHandler implements SOAPHandler<SOAPMessageContext>{

	@Override
	public void close(MessageContext arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean handleFault(SOAPMessageContext arg0) {
        System.out.println("Not supported yet.");
        return false;
	}

	@Override
	public boolean handleMessage(SOAPMessageContext context) {
		//can't add a header when one is already present
		System.out.println("*******SOAP HANDLER NBS**************");
		 Boolean outboundProperty = 
		            (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
		        if (outboundProperty.booleanValue()) {		// izlazna poruka
		            SOAPMessage message = context.getMessage();
		            SOAPPart soapPart = message.getSOAPPart();

		            try {
		            	SOAPEnvelope soapEnvelope = soapPart.getEnvelope();
			            Source source = soapPart.getContent();
			            
		                Node root = null;

		                root = ((DOMSource) source).getNode();

		                XMLSignatureFactory fac = XMLSignatureFactory
		                        .getInstance("DOM");

		                SOAPFactory factory = SOAPFactory.newInstance();

		                // uri u referenci "" pokazuje na root dokumenta
		                Reference ref = fac.newReference("", fac.newDigestMethod(DigestMethod.SHA1, null),
		                        Collections.singletonList(fac.newTransform(Transform.ENVELOPED, (TransformParameterSpec) null)),
		                        null, null);
		                
		                SignedInfo si = fac.newSignedInfo(fac.newCanonicalizationMethod(CanonicalizationMethod.INCLUSIVE,
		                        (C14NMethodParameterSpec) null),
		                        fac.newSignatureMethod(SignatureMethod.RSA_SHA1, null),
		                        Collections.singletonList(ref));

		                // Load the KeyStore and get the signing key and certificate.
		                KeyStore ks = KeyStore.getInstance("JKS");
		                String current = new java.io.File( "." ).getCanonicalPath();
		                System.out.println("Current dir:"+current);
		                String currentDir = System.getProperty("user.dir");
		                System.out.println("Current dir using System:" +currentDir);
		                ks.load(new FileInputStream("../webapps/nbs-ws/WEB-INF/keyStore/keyStoreNBS"), "pass".toCharArray());
		                KeyStore.PrivateKeyEntry keyEntry =
		                        (KeyStore.PrivateKeyEntry) ks.getEntry("nbs", new KeyStore.PasswordProtection("pk".toCharArray()));
		                X509Certificate cert = (X509Certificate) keyEntry.getCertificate();

		                KeyInfoFactory kif2 = fac.getKeyInfoFactory();
		                List x509Content = new ArrayList(); 
		               x509Content.add(cert.getSubjectX500Principal().getName());
		                x509Content.add(cert);
		                X509Data xd = kif2.newX509Data(x509Content);
		                KeyInfo ki = kif2.newKeyInfo(Collections.singletonList(xd));

		                KeyValue kv = kif2.newKeyValue(cert.getPublicKey());
		                 //  Element header = getFirstChildElement(root.getDocumentElement());
		               SOAPHeader header = soapEnvelope.getHeader();
		                DOMSignContext dsc = new DOMSignContext(keyEntry.getPrivateKey(), header ); // .getdocumentElement();
		                 XMLSignature signature = fac.newXMLSignature(si, ki);
		                signature.sign(dsc);
		                
	/*	                DOMSource domSource = new DOMSource(soapEnvelope.getBody().getFirstChild());
		                StringWriter writer = new StringWriter();
		                StreamResult result = new StreamResult(writer);
		                TransformerFactory tf = TransformerFactory.newInstance();
		                Transformer transformer = tf.newTransformer();
		                transformer.transform(domSource, result);
		           //     String dk = AES.initDEK();
		                File f = new File("../webapps/nbs-ws/WEB-INF/keyStore/simKey");
	        			String dk = "";
	        			try (BufferedReader br = new BufferedReader(new FileReader(f))) {

	        				String sCurrentLine;

	        				while ((sCurrentLine = br.readLine()) != null) {
	        					dk += sCurrentLine;
	        				}

	        			} catch (IOException e) {
	        				e.printStackTrace();
	        			}
		                
		                String newBr = AES.encryptDEK(writer.toString(), dk);
		                System.out.println("NEWBR JE " + newBr);
		                
		                soapEnvelope.getBody().removeContents();
//		                soapEnvelope.getBody().setNodeValue(newBr);
		                soapEnvelope.getBody().addTextNode(newBr);
		*/                message.saveChanges();
		               
			        	context.setMessage(message);
		                
		                Transformer transformer = TransformerFactory.newInstance().newTransformer();
		                transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		                transformer.transform(new DOMSource(root), new StreamResult(System.out));
		                return true;
		            } catch (Exception e) {
		                System.out.println("Exception in handler: " + e);
		            }
		        } else {	// dolazna poruka - inbound -> verifikacija potpisa i sadrzaja poruke
		        	System.out.println("**********VALIDATING INCOMING MESSAGE**************");
		        	XMLSignatureFactory fac = XMLSignatureFactory.getInstance("DOM");
		        	SOAPMessage message = context.getMessage();
		        	SOAPPart soapPart2 = message.getSOAPPart();
			        SOAPEnvelope soapEnvelope2;
					try {
							soapEnvelope2 = soapPart2.getEnvelope();
							SOAPHeader header2 = soapEnvelope2.getHeader();
				        	
							 Source source = soapPart2.getContent();
					            
				                Node root = null;

				                root = ((DOMSource) source).getNode();
							Transformer transformer = TransformerFactory.newInstance().newTransformer();
			                transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			                transformer.transform(new DOMSource(root), new StreamResult(System.out));
			                   
							
				        	 NodeList nl = header2.getElementsByTagNameNS(XMLSignature.XMLNS, "Signature");
			        			if (nl.getLength() == 0) {
			        			  System.out.println("Cannot find Signature element ");
			        			  for(int i = 0; i< nl.getLength(); i++){
			        				  Node nn = nl.item(0);
			        				  System.out.println("FOUND EL "+ nn.getNodeName() + nn.getNodeValue());
			        			  }
			        			} 
			        			else {
			        				System.out.println("FOUND ELEM ");

				        			DOMValidateContext valContext2 = new DOMValidateContext(new KeyValueKeySelector(), nl.item(0)); 
						        	 XMLSignature sig = fac.unmarshalXMLSignature(valContext2);
						        	 
						        	 boolean validated = sig.validate(valContext2);
						        	 System.out.println("*************SIGNATURE VALIDATION " + validated + "*************");
						        	 if(!validated){
						        		 boolean sv =sig.getSignatureValue().validate(valContext2);
						        		System.out.println("signature validation status: " + sv); 
					        			Iterator i = sig.getSignedInfo().getReferences().iterator();
					        					for (int j=0; i.hasNext(); j++) {
					        					  boolean refValid = ((Reference)i.next()).validate(valContext2);
					        					  System.out.println("*********REFERENCE VALIDATION " + refValid + "************");
					        					} 
						        	 }
			        			}
			   /*     			String strToDecrypt = soapEnvelope2.getBody().getTextContent();
			        			File f = new File("../webapps/nbs-ws/WEB-INF/keyStore/simKey");
			        			String secret = "";
			        			try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(f), "UTF-8"))) {

			        				String sCurrentLine;

			        				while ((sCurrentLine = br.readLine()) != null) {
			        					secret += sCurrentLine;
			        				}

			        			} catch (IOException e) {
			        				e.printStackTrace();
			        			}
			        			String body = AES.decryptDEK(strToDecrypt, secret);
			        			soapEnvelope2.getBody().removeContents();
			        			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
			        			docBuilderFactory.setNamespaceAware(true);
			        			DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
			        			Document d = docBuilder.parse(new InputSource(new StringReader(body)));
			        			soapEnvelope2.getBody().addDocument(d);
			        */			
			        			
			        			
						} catch (SOAPException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
			       }
		        
		        return true;
	}

	/**
	 * KeySelector which retrieves the public key out of the KeyValue element
	 * and returns it. NOTE: If the key algorithm doesn't match signature
	 * algorithm, then the public key will be ignored.
	 */
	private static class KeyValueKeySelector extends KeySelector {
		public KeySelectorResult select(KeyInfo keyInfo,
				KeySelector.Purpose purpose, AlgorithmMethod method,
				XMLCryptoContext context) throws KeySelectorException {
			
			if (keyInfo == null) {
				throw new KeySelectorException("Null KeyInfo object!");
			}
			SignatureMethod sm = (SignatureMethod) method;
			List<?> list = keyInfo.getContent();

			for (int i = 0; i < list.size(); i++) {
				XMLStructure xmlStructure = (XMLStructure) list.get(i);
				if (xmlStructure instanceof X509Data) {
					PublicKey pk = null;
					List<?> l = ((X509Data) xmlStructure).getContent();
					if (l.size() > 0) {
						X509Certificate cert = (X509Certificate) l.get(1);
						pk = cert.getPublicKey();
						if (algEquals(sm.getAlgorithm(), pk.getAlgorithm())) {
							return new SimpleKeySelectorResult(pk);
						}
					}
				}
				if (xmlStructure instanceof KeyValue) {
					PublicKey pk = null;
					try {
						pk = ((KeyValue) xmlStructure).getPublicKey();
					} catch (KeyException ke) {
						throw new KeySelectorException(ke);
					}
					// make sure algorithm is compatible with method
					if (algEquals(sm.getAlgorithm(), pk.getAlgorithm())) {
						return new SimpleKeySelectorResult(pk);
					}
				}
			}
			throw new KeySelectorException("No KeyValue element found!");
		}

		// @@@FIXME: this should also work for key types other than DSA/RSA
		static boolean algEquals(String algURI, String algName) {
			if (algName.equalsIgnoreCase("DSA")
					&& algURI.equalsIgnoreCase(SignatureMethod.DSA_SHA1)) {
				return true;
			} else if (algName.equalsIgnoreCase("RSA")
					&& algURI.equalsIgnoreCase(SignatureMethod.RSA_SHA1)) {
				return true;
			} else {
				return false;
			}
		}
	}

	private static class SimpleKeySelectorResult implements KeySelectorResult {
		private PublicKey pk;

		SimpleKeySelectorResult(PublicKey pk) {
			this.pk = pk;
		}

		public Key getKey() {
			return pk;
		}
}
	
	@Override
	public Set<QName> getHeaders() {
		return  new TreeSet();
	}

}
