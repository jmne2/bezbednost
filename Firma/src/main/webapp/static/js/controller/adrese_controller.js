'use strict';

angular.module('repo3App').controller('AdresaController', ['$scope','$uibModal', 'AdresaService', function($scope,$uibModal,AdresaService) {
    var self = this;
    self.adresa={};
    self.adrese=[];
    self.mesto = {};
    self.mesta = [];
    self.show = false;
    self.logged = false;
    self.children = [""];
    
    self.submit = submit;
    self.edit = edit;
    self.remove = remove;
    self.reset = reset;
    self.search = search;
    self.currState = "edit";
    self.showPueblo = showPueblo;
    self.indSelected = -1;
    $scope.indSelectedVote = -1;
    $scope.required = true;
    
    $scope.$on('hidingYoCountries', function(event, args) {
    	fetchAllAdrese();
    	self.show = args.arg[1];
    	self.currState = "edit";
    });
    
    $scope.$on('dispAfterLog', function(event, args) {
    	if(self === args.ctrl){
    		self.logged = true;
    		self.show = true;
    		if(args.parent != null){
    			self.adresa.nasMesto = args.parent.mesta[args.parent.indSelected];
    			search(self.adresa);
    		} else {
    			fetchAllAdrese();
    		}
    		
    	} else {
    		self.logged = false;
    		self.show = false;
    	}
    });
    
    $scope.$on('changeState', function(event, args) {    	
    	if(self === args.ctrl){
    		self.currState = args.state;
    		$scope.required = (self.currState === "search") ? false : true;
    		self.adresa = {};
    	}
    });
    $scope.$on('hide', function(event, args) {
    	self.show = false;
    	
    });
    $scope.$on('resetEvent', function(event, args) {    	
    	if(self === args.ctrl){
    		self.currState = "edit";
    		fetchAllAdrese();
    	}
    });
    $scope.$on('changeSelection', function(event, args) {    	
    	if(self === args.ctrl){
    		switch (args.direction) {
	            case 'prev':
	                prev();
	                break;
	            case 'next':
	                next();
	                break;
	            case 'first':
	            	first();
	            	break;
	            case 'last':
	            	last();
	            	break;
    		}
    	}
    });
    
    $scope.$on('deleteSel', function(event, args) {    	
    	if(self === args.ctrl){
    		if($scope.indSelectedVote != -1){
    			var selected = self.adrese[$scope.indSelectedVote];
    			remove(selected.id);
    			self.currState = "edit";
    		}
    	}
    });
    
    function first(){
    	$scope.indSelectedVote = 0;
    	self.indSelected = $scope.indSelectedVote;
    }
    function prev(){
    	if(($scope.indSelectedVote != 0) && ($scope.indSelectedVote != -1)){
    		$scope.indSelectedVote = $scope.indSelectedVote - 1;
    		self.indSelected = $scope.indSelectedVote;
    	}
    }
    
    function next(){
    	if(($scope.indSelectedVote != self.adrese.length) && ($scope.indSelectedVote != -1)){
    		$scope.indSelectedVote = $scope.indSelectedVote + 1;
    		self.indSelected = $scope.indSelectedVote;
    	}
    }
    
    function last(){
    	$scope.indSelectedVote = self.adrese.length - 1;
    	self.indSelected = $scope.indSelectedVote;
    }
 
    $scope.indSelectedVote = null;
    $scope.setSelected = function (indSelectedVote) {
       $scope.indSelectedVote = indSelectedVote;
       self.indSelected = $scope.indSelectedVote;
       if(self.currState === "edit"){
    	   var id = self.adrese[indSelectedVote].id;
    	   edit(id);
   		}
    };
   
    function showPueblo() {

        var modalInstance = $uibModal.open({
          templateUrl: 'myModalContent.html',
          controller: ModalInstanceCtrl,
          resolve: {
            items: function () {
              return self.mesta;
            }
          }
        });

        modalInstance.result.then(function (selectedItem) {
          self.mesto = self.mesto[selectedItem];
        }, function () {
        });
      };
    
    function fetchAllAdrese(){
    	AdresaService.fetchAllAdrese()
            .then(
            function(d) {
            	self.adrese = [];
                self.adrese = d["adrese"];
                self.mesta = d["mesta"];
                //$scope.indSelectedVote = 0;
            },
            function(errResponse){
                console.error('Error while fetching Users');
            }
        );
    }
 
    function createAdresu(adresa){
    	AdresaService.createAdresu(adresa)
            .then(
            fetchAllAdrese,
            function(errResponse){
                console.error('Error while creating User');
            }
        );
    }
 
    function updateAdresu(adresa, id){
    	AdresaService.updateAdresu(adresa, id)
            .then(
            fetchAllAdrese,
            function(errResponse){
                console.error('Error while updating User');
            }
        );
    }
 
    function deleteAdresu(id){
    	AdresaService.deleteAdresu(id)
            .then(
            fetchAllAdrese,
            function(errResponse){
                console.error('Error while deleting User');
            }
        );
    }
 
    function submit() {
    	if (self.currState === "add"){
    		if(self.mesto.id != null && !angular.isUndefined(self.mesto.id)){
	            self.adresa.nasMesto = self.mesto;
	            createMesto(self.adresa);
    		}
        }else if (self.currState === "edit"){
        	if(self.adresa.id != null && !angular.isUndefined(self.adresa.id)){
	        	self.adresa.nasMesto = self.mesto;
	            updateAdresu(self.adresa, self.adresa.id);
        	}
        } else if (self.currState === "search"){
        	self.adresa.nasMesto = self.mesto;
        	search(self.adresa);
        }
	        reset();
    }
 
    function edit(id){
        console.log('id to be edited', id);
        for(var i = 0; i < self.adrese.length; i++){
            if(self.adrese[i].id === id) {
                self.adresa = angular.copy(self.adrese[i]);
                self.mesto = self.adresa.nasMesto;
                break;
            }
        }
    }
 
    function remove(id){
        console.log('id to be deleted', id);
        if(self.adresa.id === id) {//clean form if the user to be deleted is shown there.
            reset();
        }
        deleteAdresu(id);
    }
 
 
    function reset(){
        self.mesto={};
        self.adresa = {};
        $scope.myForm.$setPristine(); //reset Form
    }
    
    function search(adresa){
    	AdresaService.searchAdrese(adresa)
        .then(
        function(d) {
        	self.adrese = d;
            $scope.indSelectedVote = -1;
            self.indSelected = $scope.indSelectedVote;
        },
        function(errResponse){
            console.error('Error while fetching Users');
        }
        );
    }
    
    var ModalInstanceCtrl = function ($scope, $uibModalInstance, items) {

    	  $scope.items = items;
    	  $scope.selected = {
    	    item: -1
    	  };

    	  $scope.ok = function () {
    		  $uibModalInstance.close($scope.selected.item);
    	  };

    	  $scope.cancel = function () {
    		  $uibModalInstance.dismiss('cancel');
    	  };
    	};
}]);