package certificateApp.util;

import java.util.Set;

import javax.interceptor.Interceptor;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response.Status;

import org.apache.log4j.Logger;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import certificateApp.annotations.Authenticate;
import certificateApp.annotations.PermissionType;
import certificateApp.models.Permission;
import certificateApp.models.Role;
import certificateApp.models.User;

@Interceptor
@Authenticate
public class LoginInterceptor implements HandlerInterceptor{

	public LoginInterceptor(){
		super();
	}
	
	private static Logger log = Logger.getLogger(AuthenticationInterceptor.class);

	@Context
	private HttpServletRequest request;

	
	
	@Override
	public void afterCompletion(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, Exception arg3)
			throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void postHandle(HttpServletRequest arg0, HttpServletResponse response, Object handler, ModelAndView arg3)
			throws Exception {

		User user = (User) request.getSession().getAttribute("user");
		log.info("user: "+user);
		if (user == null) {
			throw new ServiceException("Not logged in", Status.UNAUTHORIZED);
		}	
		System.out.println("INTERCEPTING");
		//Method method = arg2.getMethod();
		if (handler instanceof HandlerMethod) {
		    HandlerMethod method = (HandlerMethod) handler;
		    if (method.getMethod().isAnnotationPresent(PermissionType.class)) {
		    	PermissionType annotation = method.getMethodAnnotation(PermissionType.class);
				String permission = annotation.value(); 
				System.out.println("Permisija potrebna : " + permission);
				Set<Role> userRoles = user.getAllowed();
				for(Role role : userRoles){
					System.out.println("Rola " + role.getName());
					Set<Permission> userPerm = role.getPermissions();
					for(Permission perm : userPerm){
						System.out.println("Permisija " + perm.getName());
						if(permission.equals(perm.getName())){
							//Object result = context.proceed();
							//return true;
						}
							
					}
				}
				throw new ServiceException("Not authorized", Status.UNAUTHORIZED);
		    }
		}
	}

	@Override
	public boolean preHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

}
