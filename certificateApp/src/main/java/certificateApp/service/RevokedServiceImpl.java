package certificateApp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import certificateApp.models.Revoked;
import certificateApp.repository.RevokedRepository;

@Service
@Transactional
public class RevokedServiceImpl implements RevokedService{

	@Autowired
	private RevokedRepository rer ;
	
	@Override
	public List<Revoked> findAll() {
		return (List<Revoked>) rer.findAll();
	}

	@Override
	public Revoked saveRevoked(Revoked re) {
		return rer.save(re);
	}

	@Override
	public Revoked findOne(long id) {
		return rer.findOne(id);
	}

	@Override
	public Revoked findBySerialNumber(String serialNumber) {
		return rer.findBySerialNumber(serialNumber);
	}

	@Override
	public void delete(Revoked re) {
		rer.delete(re);
	}

}
