package jmn.services;

import java.util.List;

import jmn.models.Faktura;
import jmn.models.StavkaFakture;


public interface StavkaFaktureService {
	List<StavkaFakture> findAll();
	StavkaFakture saveStavkaFakture(StavkaFakture stFa);
	StavkaFakture findOne(Long id);
	void delete(Long id);
	void delete(StavkaFakture stavkaFakture);
	List<StavkaFakture>  findByStavkaFakture(Faktura stavkaFakture);
	List<StavkaFakture> findByJedinicaMereAndJedinicnaCenaAndKolicinaAndNazivRobeUslugeContainingAndProcenatRabataAndUkupanPorezAndVrednostAndStavkaFakture(double jedinicaMere, double jedinicnaCena, double kolicina,String nazivRobeIUsluga,double procenatRabata,double ukupanPorez,double vrednost, Faktura stavkaFakture);
}
