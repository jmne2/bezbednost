<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<title>Get certificate</title>
<script type="text/javascript">
		function getFormData($form){
		    var unindexed_array = $form.serializeArray();
		    var indexed_array = {};
	
		    $.map(unindexed_array, function(n, i){
		        indexed_array[n['name']] = n['value'];
		    });
	
		    return indexed_array;
		}
</script>
</head>
<body>
<br></br><br></br>
	<div class="container-fluid" align="center" style=" display: table; margin: auto; width:40%; ">
		<div class=" col-md-6 col-md" id="loginBlock" >
			<form id="logForm" role="form" action="" method="post">
				<fieldset>
					<label>Username:</label><br>
					<input type="text" class="form-control" name="username" id="logUsername"  ><br>
					<label>Lozinka:</label><br>
					<input type="password" class="form-control" name="password" id="logPass"><br>
					<input class="btn btn-lg btn-primary" type="submit" value="Prijavi se">
				</fieldset>
			</form> <br>
			</div>
		</div>
		
	<script type="text/javascript">
	
	$(document).on('submit','#logForm',function(e){
		e.preventDefault();
		var $form = $("#logForm");
		var data = getFormData($form);
		var s = JSON.stringify(data);
		 $.ajax({
             type: "GET",
             url: "/login/",
             data: s,
             contentType : "application/json",
             complete: function(datas){
            	 var loc = datas.responseJSON.message;
            	 var loc2 = loc.substring(loc.lastIndexOf('/') + 1);
            	 window.location = loc2;
             },
             error: function (datas) {
                 alert(datas.responseText);
             }
         });
	});
	</script>
</body>
</html>