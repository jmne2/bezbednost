package certificateApp.controller;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringReader;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Signature;
import java.security.SignatureException;
import java.security.cert.CRLReason;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1Integer;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.DERIA5String;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.DERTaggedObject;
import org.bouncycastle.asn1.ocsp.BasicOCSPResponse;
import org.bouncycastle.asn1.ocsp.CertID;
import org.bouncycastle.asn1.ocsp.OCSPRequest;
import org.bouncycastle.asn1.ocsp.OCSPResponse;
import org.bouncycastle.asn1.ocsp.Request;
import org.bouncycastle.asn1.ocsp.ResponseBytes;
import org.bouncycastle.asn1.ocsp.RevokedInfo;
import org.bouncycastle.asn1.ocsp.SingleResponse;
import org.bouncycastle.asn1.ocsp.TBSRequest;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x509.AccessDescription;
import org.bouncycastle.asn1.x509.AuthorityInformationAccess;
import org.bouncycastle.asn1.x509.AuthorityKeyIdentifier;
import org.bouncycastle.asn1.x509.Extension;
import org.bouncycastle.asn1.x509.Extensions;
import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.asn1.x509.GeneralNames;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cert.ocsp.CertificateID;
import org.bouncycastle.cert.ocsp.OCSPReqBuilder;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.operator.OperatorCreationException;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

import certificateApp.annotations.PermissionType;
import certificateApp.generator.OCSPResponder;
import certificateApp.models.Revoked;
import certificateApp.models.SerialNumbers;
import certificateApp.models.User;
import certificateApp.service.RevokedService;
import certificateApp.service.SerialNumbersService;
import certificateApp.service.UserService;

@Controller
@RequestMapping("/ocsp/")
public class OCSPController {

	@Autowired
	private SerialNumbersService snser;
	@Autowired
	private RevokedService rs;
	@Autowired
	private UserService us;
	
	public OCSPController(){
		
	}
	
	public OCSPController(SerialNumbersService snser, RevokedService rs, UserService us){
		this.snser = snser;
		this.rs = rs;
		this.us = us;
	}

	@PermissionType("withdrawCert")
	@RequestMapping(value="withdrawCert/", method = RequestMethod.POST, produces="application/json")
	public @ResponseBody String  withdrawCert(@RequestBody String data, HttpSession session) throws JSONException{
		String[] datas = data.split("&");
		String numStr = datas[0].split("=")[1];
		String reason = datas[1].split("=")[1];
		
		BigInteger bi = new BigInteger(numStr);		//(numStr, 16)
		String num = bi.toString(16);						//()
		
		System.out.println("BROJ KOJI SE POVLACI : " + num);
		
		User u = (User)session.getAttribute("user");
		SerialNumbers serialNumber = snser.findBySerialNumber(num);
		
		if(serialNumber==null){
			return "No certificate with serial number" + numStr;
		}
		else{
			return withdrawCert(num, reason, u);
		}
	}
	
	public String withdrawCert(String num, String reason, User u){

		int reasonInt = Integer.parseInt(reason);
		List<SerialNumbers> sn = snser.findByUsername(u.getUsername());
		for(SerialNumbers s : sn){
			if(s.getSerialNumber().equals(num)){
				if(rs.findBySerialNumber(s.getSerialNumber())==null){
					setRevoked(num, reasonInt);
					return "Withdrawn";
				}
				else{
					return "Already revoked";
				}
			}
			else{
				List<SerialNumbers> sn2 = snser.findByIssuerSerNum(s.getSerialNumber());
				for(SerialNumbers s2 : sn2){
					if(s2.getSerialNumber().equals(num)){
						if(rs.findBySerialNumber(s2.getSerialNumber())==null){
							setRevoked(num, reasonInt);
							return "Withdrawn";
						}
						else{
							return "Already revoked";
						}
					}
				}
			}
		}
		return "Not authorized";
	}
	
	public void setRevoked(String num, int reason){
		Date date = new Date();
		Revoked revoked = new Revoked();
		revoked.setSerialNumber(num);
		revoked.setDate(date);
		CRLReason r;
		switch(reason){
		case 1: {
			r = CRLReason.KEY_COMPROMISE;
			break;
		}		
		case 2: {
			r = CRLReason.PRIVILEGE_WITHDRAWN;
			break;
		}
		case 3: {
			r = CRLReason.AFFILIATION_CHANGED;
			break;
		}
		default: {
			r = CRLReason.UNSPECIFIED;
			break;
		}
		}
		revoked.setReason(r.ordinal());
		
		rs.saveRevoked(revoked);
		
		List<SerialNumbers> children = snser.findByIssuerSerNum(num);
		for(SerialNumbers sn : children){
			if(rs.findBySerialNumber(sn.getSerialNumber())==null){
				setRevoked(sn.getSerialNumber(), 1);
			}
		}
		
	}
	
	@PermissionType("findWithdrawCert")
	@RequestMapping(value="findwithdrawCert/", method = RequestMethod.POST, produces="application/json")
	public @ResponseBody String  findwithdrawCert(@RequestBody String data, HttpSession session) throws OperatorCreationException, Exception{
		String numStr = data.split("=")[1];
		return getWithdrawnStatus(numStr);
		
	}
	
	public String getWithdrawnStatus(String numStr) throws IOException, CertificateException, NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException, SignatureException, ParseException{
		
		BigInteger bi = new BigInteger(numStr);
		String num = bi.toString(16);
		System.out.println("Trazi se ser broj : " + num);
		SerialNumbers sn = snser.findBySerialNumber(num);
		String username = sn.getUsername();
		User u = us.findByUsername(username);	
		X509Certificate cert = null;
	
			String role = u.getAllowed().iterator().next().getName();
			System.out.println("uLoga je " + role);
			if(role.equals("admin")){
				cert = readCert(new File("cerNBS/"), numStr);
			}
			else if(role.equals("banka")){
				cert = readCert(new File("cerBanks/"), numStr);
			}
			else{
				File f = new File("cerCompanies/");
				File[] files = f.listFiles();
				for(File file : files){
					cert = readCert(file, numStr);
					if(cert!=null){
						break;
					}
				}
			}
		
		byte[] b = cert.getExtensionValue("2.5.29.35");
		ASN1OctetString akiOc = ASN1OctetString.getInstance(b);
		AuthorityKeyIdentifier aki = AuthorityKeyIdentifier.getInstance(akiOc.getOctets());
		
		byte[] issuerPublicKey = aki.getKeyIdentifier();
		System.out.println("JAVNI KLJUC JE : " + issuerPublicKey);
		GeneralNames gn = aki.getAuthorityCertIssuer();
		System.out.println("GENERAL NAMES JE " + gn);
		BigInteger issuerSerNum = aki.getAuthorityCertSerialNumber();
		System.out.println("SERIJSKI BROJ " + issuerSerNum);
		
		byte[] b2 = cert.getExtensionValue("1.3.6.1.5.5.7.1.1");
		 ByteArrayInputStream bais = new ByteArrayInputStream(b2);
        ASN1InputStream ais1 = new ASN1InputStream(bais);
         DEROctetString oct = (DEROctetString) (ais1.readObject());
        ASN1InputStream ais2 = new ASN1InputStream(oct.getOctets());
         AuthorityInformationAccess authorityInformationAccess = AuthorityInformationAccess.getInstance(ais2.readObject());

         AccessDescription[] accessDescriptions = authorityInformationAccess.getAccessDescriptions();
         String ocspLocation = "";
        for (AccessDescription accessDescription : accessDescriptions) {

          GeneralName gnn =  accessDescription.getAccessLocation();
          DERIA5String str = (DERIA5String) ((DERTaggedObject) gnn.toASN1Primitive()).getObject();
           ocspLocation = str.getString();
        }
        
        X509Certificate responderCert = readCert(new File(ocspLocation), null);
		OCSPReqBuilder reqGen = new OCSPReqBuilder();
		reqGen.addRequest(new CertificateID(new CertID(CertificateID.HASH_SHA1, new DEROctetString(gn.getEncoded()), new DEROctetString(issuerPublicKey), new ASN1Integer(cert.getSerialNumber()))));

		Request re = new Request(new CertID(CertificateID.HASH_SHA1, new DEROctetString(gn.getEncoded()), new DEROctetString(issuerPublicKey), new ASN1Integer(cert.getSerialNumber())), new Extensions(new Extension[0]));
		DERSequence seq = new DERSequence(re);
		
		// requestor name, request list, request extensions
		TBSRequest r = new TBSRequest(new GeneralName(new X500Name(cert.getSubjectX500Principal().getName())), seq, new Extensions(new Extension[0]));
		OCSPRequest req = new OCSPRequest(r, null);
		OCSPResponder responder = new OCSPResponder();
		responder.setSnser(this.snser);
		responder.setUs(this.us);
		responder.setRs(this.rs);
		responder.sendRequest(req);
		
		OCSPResponse response =  responder.check();
		System.out.println("RESPONSE STATUS JE : " +response.getResponseStatus().toString() + response.getResponseStatus().getValue());

		ResponseBytes rb = response.getResponseBytes();
		ASN1OctetString aos = rb.getResponse();
		ASN1InputStream inp = new ASN1InputStream(aos.getOctets());
		
		BasicOCSPResponse bres = BasicOCSPResponse.getInstance(inp.readObject());
		
         Signature signature = Signature.getInstance("SHA1withRSA", "BC");
         signature.initVerify(responderCert.getPublicKey());
         signature.update(bres.getTbsResponseData().getEncoded());
         boolean valid = signature.verify(bres.getSignature().getBytes());
         if(!valid){
        	 System.out.println("Response was invalid");
        	 return "-1";
         }
		System.out.println("VALIDAN " + valid);
		System.out.println("PRODUCED AT : " + bres.getTbsResponseData().getProducedAt().getDate().toString());
		SingleResponse sr = SingleResponse.getInstance(bres.getTbsResponseData().getResponses().getObjectAt(0));
		
		System.out.println("SIngle response : " + sr.getCertID() + sr.getCertStatus() + sr.getThisUpdate());
		System.out.println("Single response status: "+ sr.getCertStatus().getTagNo());
		
		HashMap<String, Object> hm = new HashMap<String, Object>();
		hm.put("tagNo", sr.getCertStatus().getTagNo());
		if(sr.getCertStatus().getTagNo()==1){
			hm.put("reason", ((RevokedInfo)sr.getCertStatus().getStatus()).getRevocationReason().toString());
			hm.put("time", ((RevokedInfo)sr.getCertStatus().getStatus()).getRevocationTime().getDate());
		}
		 Gson gson = new Gson();
     	 String json = gson.toJson(hm); 
     	 return json;
		
	//	return sr.getCertStatus().getTagNo(); 
	}
	
	
	
	public X509Certificate readCert(File f, String num) throws CertificateException{
		if(num==null){
			return readFile(f);
		}
		File[] files = f.listFiles();
		for(File file : files){
			String certNum = file.getName().substring(11, file.getName().length()-4);
			if (certNum.equals(num)){
				return readFile(file);
			}
		}
		
		 return null;  
	}
	public X509Certificate readFile(File file){
		byte[] bFile = new byte[(int) file.length()];
		
		FileInputStream fis;
		X509Certificate cert;
		X509CertificateHolder certHolder;
		try {
			fis = new FileInputStream(file);
			fis.read(bFile);
		     fis.close();
		     String reqString = new String(bFile, "UTF-8");
		     
		     String temp = new String(bFile);
		  
		      StringReader sr = new StringReader(temp);
		      PEMParser pp = new PEMParser(sr);
		      Object parsedObj = pp.readObject();
		      System.out.println("PemParser returned: " + parsedObj);
		       if (parsedObj instanceof X509Certificate)
		       {
		          cert = (X509Certificate)parsedObj;
					if(pp!=null){
						pp.close();
					}
					return cert;
		       }
		       else if(parsedObj instanceof X509CertificateHolder){
		    	   certHolder = (X509CertificateHolder)parsedObj;
		    	   if(pp!=null){
						pp.close();
					}
		    	   JcaX509CertificateConverter certConverter = new JcaX509CertificateConverter();
					certConverter = certConverter.setProvider("BC");
					return certConverter.getCertificate(certHolder);

		       }
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CertificateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
		}  
		return null;
	}
	
	public SerialNumbersService getSnser() {
		return snser;
	}

	public void setSnser(SerialNumbersService snser) {
		this.snser = snser;
	}

	public RevokedService getRs() {
		return rs;
	}

	public void setRs(RevokedService rs) {
		this.rs = rs;
	}
	
	
}
