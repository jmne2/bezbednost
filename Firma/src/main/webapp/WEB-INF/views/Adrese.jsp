<div class="generic-container" id="Adrese" ng-controller="AdresaController as ctrl" >
          <script type="text/ng-template" id="myModalContent.html">
        	<div class="modal-header">
            	<h3>Izaberite Drzavu</h3>
        	</div>
        	<div class="modal-body">
            	<ul>
                	<li ng-repeat="item in items">
                    	<a ng-click="selected.item = $index">{{ item.naziv }}</a>
                	</li>
            	</ul>
            	Selected: <b>{{ selected.item }} </b>
        	</div>
        		<div class="modal-footer">
            	<button class="btn btn-primary" ng-click="ok()">OK</button>
            	<button class="btn btn-warning" ng-click="cancel()">Cancel</button>
        	</div>
    	</script>
         <div>
          <div class="panel panel-default">
                <!-- Default panel contents -->
              <div class="panel-heading"><span class="lead">Lista Adresa </span></div>
              <div class="tablecontainer">
                  <table class="table table-hover">
                      <thead>
                          <tr>
                              <th>ID</th>
                              <th>Ulica</th>
                              <th>Broj</th>
                              <th>Naseljeno Mesto</th>
                          </tr>
                      </thead>
                      <tbody>
                      <!-- ng-click="setSelected(d.id)" ng-class="{selected: d.id === idSelectedVote}" -->
                          <tr ng-repeat="a in ctrl.adrese" ng-click="setSelected($index)" ng-class="{selected: $index === indSelectedVote}">
                              <td><span ng-bind="a.id"></span></td>
                              <td><span ng-bind="a.ulica"></span></td>
                              <td><span ng-bind="a.broj"></span></td>
                              <td><span ng-bind="a.nasMesto.naziv"></span></td>
                          </tr>
                      </tbody>
                  </table>
              </div>
          </div>
          <div class="panel panel-default">
              <div class="formcontainer">
                  <form ng-submit="ctrl.submit()" name="myForm" class="form-horizontal">
                      <input type="hidden" ng-model="ctrl.adresa.id" />
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="ulica">Ulica</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="ctrl.adresa.ulica" id="ulica" class="naziv form-control input-sm" placeholder="Unesite ulicu" ng-required="required" ng-minlength="{{ctrl.currState === 'search' ? 0 : 5}}"/>
                                  <div class="has-error" ng-show="myForm.$dirty">
                                      <span ng-show="myForm.ulica.$error.required">This is a required field</span>
                                      <span ng-show="myForm.ulica.$error.minlength">Minimum length required is 1</span>
                                      <span ng-show="myForm.ulica.$invalid">This field is invalid </span>
                                  </div>
                              </div>
                          </div>
                      </div>
                         
                       
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="broj">Broj</label>
                              <div class="col-md-7">
                                  <input type="number" ng-model="ctrl.adresa.broj" id="broj" class="form-control input-sm" placeholder="Unesite Broj" ng-required="required" ng-minlength="{{ctrl.currState === 'search' ? 0 : 1}}" ng-maxlength="5"/>
                                  <div class="has-error" ng-show="myForm.$dirty">
                                      <span ng-show="myForm.broj.$error.required">This is a required field</span>
                                      <span ng-show="myForm.broj.$error.minlength">Minimum length required is 1</span>
                                      <span ng-show="myForm.broj.$error.maxlength">Maximum length required is 5</span>
                                      <span ng-show="myForm.broj.$invalid">This field is invalid </span>
                                  </div>
                              </div>
                          </div>
                      </div>
 					
 					<div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="maticna">Naseljeno Mesto</label>
                              <button type="button" ng-click="ctrl.showPueblo()" class="btn btn-info btn-sm">Izaberi MEsto</button>
                              <div class="col-md-7">
                              <label class="col-md-2 control-lable" id="maticna">{{ctrl.mesto.naziv}}</label>
                              </div>
                          </div>
                      </div>
 					
                      <div class="row">
                          <div class="form-actions floatRight">
                              <input type="submit"  id ="subId" value="Commit" class="btn btn-primary btn-sm" ng-disabled="myForm.$invalid">
                              <button type="button" ng-click="ctrl.reset()" class="btn btn-warning btn-sm" ng-disabled="myForm.$pristine">Rollback</button>
                               <label>{{ctrl.currState}}</label>
                          </div>
                      </div>
                  </form>
              </div>
          </div>
          </div>
      </div>