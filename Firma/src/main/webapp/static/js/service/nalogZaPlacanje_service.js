
'use strict';
 
angular.module('repo3App').factory('NalogService', ['$http', '$q', 'urls', function($http, $q, urls){
 
    //var SERVICE_URI = 'http://localhost:8080/drzave/';
 
    var factory = {
        fetchAllNaloge: fetchAllNaloge,
        createNalog: createNalog,
        updateNalog:updateNalog,
        deleteNalog:deleteNalog,
        searchNaloge: searchNaloge,
        sendNalog: sendNalog
    };
 
    return factory;
 
    function fetchAllNaloge() {
        var deferred = $q.defer();
        $http.get(urls.NALOG_SERVICE)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while fetching Reciepts');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
    function createNalog(nalog) {
        var deferred = $q.defer();
        $http.post(urls.NALOG_SERVICE, nalog)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while creating Reciept');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
 
    function updateNalog(nalog, id) {
        var deferred = $q.defer();
        $http.post(urls.NALOG_SERVICE+id, nalog)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
    function deleteNalog(id) {
        var deferred = $q.defer();
        $http.delete(urls.NALOG_SERVICE+id)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
    function searchNaloge(nalog){
    	var deferred = $q.defer();
        $http.post(urls.NALOG_SERVICE+"search/", nalog)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
    function sendNalog(id) {
        var deferred = $q.defer();
        $http.post(urls.NALOG_SERVICE+"/send/",id)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
 
}]);