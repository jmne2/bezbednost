package jmn.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import jmn.models.Banka;
import jmn.models.PoslovniPartner;
import jmn.models.Racun;
import jmn.models.RacunPartnera;

public interface RacunPartneraRepository extends CrudRepository<RacunPartnera, Long>{

	public List<RacunPartnera> findByPoslovniPartnerAndBanka(PoslovniPartner poslovniPartner, Banka banka);
	public List<RacunPartnera> findByPoslovniPartner(PoslovniPartner poslovniPartner);

}
