package certificateApp.controller;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.ArrayList;
import java.util.Random;

import javax.servlet.http.HttpSession;

import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.openssl.jcajce.JcaPEMWriter;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.pkcs.PKCSException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

import certificateApp.annotations.PermissionType;
import certificateApp.generator.KeyStoreReader;
import certificateApp.generator.KeyStoreWriter;
import certificateApp.generator.NewCertificate;
import certificateApp.generator.ReadCSR;
import certificateApp.models.SerialNumbers;
import certificateApp.models.User;
import certificateApp.service.RevokedService;
import certificateApp.service.SerialNumbersService;

@Controller
@RequestMapping("/csr/")
public class CSRController {
	
	@Autowired
	private SerialNumbersService snser;
	@Autowired
	private RevokedService rs;
	
	@PermissionType("showCsr")
	@RequestMapping(value="/showCsr/", method = RequestMethod.POST)
	public @ResponseBody String showCsr(@RequestBody String data, HttpSession session) throws IOException, OperatorCreationException, PKCSException{
		
		String numStr = data.split("=")[1];
		System.out.println("FAJL ZA CITANJE CSR-A JE:"+numStr);
		ReadCSR reader = new ReadCSR();
		User u = (User)session.getAttribute("user");
		boolean valid = reader.readcsr(new File("csr"+u.getName()+"/"+numStr));
		if(!valid){
			return "";
		}
		X500Name x500Name = reader.getX500name();

    	return x500Name.toString();
    	
    //	return "OK";
	}
	
	@PermissionType("showCsrList")
	@RequestMapping(value="/showCsrList/", method = RequestMethod.POST, produces="application/json")
	public @ResponseBody String showCsrList(HttpSession session) throws IOException{
		
		User u = (User)session.getAttribute("user");
		 File file = new File("csr"+u.getName());
	
		 System.out.println("FIle " + file.getName());
		 File[] files = file.listFiles();  
		 System.out.println("Size" + files.length);
		 ArrayList<String> filenames = new ArrayList<String>();
		 for (File f:files)  
		 {  
			 filenames.add(f.getName());
		 }
		 Gson gson = new Gson();
     	 String json = gson.toJson(filenames);
     	 return json;		
    	
	}
	
	@PermissionType("approveCsr")
	@RequestMapping(value="/approveCsr/", method = RequestMethod.POST)
	public @ResponseBody String approveCsr(@RequestBody String data, HttpSession session) throws IOException, KeyStoreException, NoSuchProviderException, NoSuchAlgorithmException, CertificateException, OperatorCreationException, PKCSException{
		
		String[] datas = data.split("&");
		String id =datas[0].split("=")[1];
		String parentAlias =datas[1].split("=")[1];
		String parentKsPass = datas[2].split("=")[1];
		String parentPkPass = datas[3].split("=")[1];
//		String childAlias = datas[4].split("=")[1];	//ne treba
		parentAlias=parentAlias.substring(3, parentAlias.length()-3);
		
		String[] endUsr = id.split("-");
		String usrnm = endUsr[1];
		String aliasEndUsera = endUsr[2];
		String serNumEndUsera = endUsr[3].split("\\.")[0];
		
		
		ReadCSR reader = new ReadCSR();
		User u = (User)session.getAttribute("user");
		File f = new File("csr"+u.getName()+"/"+id);
		boolean valid = reader.readcsr(f);
		
		if(!valid){
			return "INVALID CSR";
		}
		
		X500Name x500name = reader.getX500name();
		PublicKey publicKey = reader.getPublicKey();
//		String serNum = countSerialNumber();
		BigInteger bi = new BigInteger(serNumEndUsera, 16);
		
		NewCertificate nc = new NewCertificate(null, serNumEndUsera);
		nc.setRs(rs);
		SerialNumbers snum = (SerialNumbers)snser.findByAlias(parentAlias);
		
		KeyStore ks = KeyStore.getInstance("JKS", "SUN");
		BufferedInputStream in = new BufferedInputStream(new FileInputStream("keyStores/keyStore"+u.getName()));
		ks.load(in, parentKsPass.toCharArray());
		KeyStoreReader keyStoreReader = new KeyStoreReader();
		//X509Certificate parentCert = (X509Certificate) keyStoreReader.readCertificate("keyStores/keyStoreBank", parentKsPass, parentAlias);
		System.out.println("alias : " + parentAlias);
		X509Certificate parentCert = (X509Certificate)ks.getCertificate(parentAlias);
		PrivateKey parentPrivateKey = keyStoreReader.readPrivateKey("keyStores/keyStore"+u.getName(), parentKsPass, parentAlias, parentPkPass);
		String parentSerNum = parentCert.getSerialNumber().toString(16);
		
		String revoked = nc.approveCsr(x500name, publicKey, parentCert, parentPrivateKey);
		if(revoked.equals("revoked")){
			return "REVOKED";
		}
		// u keystore-u banke cuvam i novi sertifikat firme koji je izdala
		
		X509Certificate cert = nc.getCert();
		ks.setCertificateEntry("childAlias", cert);
		//SerialNumbers sn = new SerialNumbers(serNumEndUsera, aliasEndUsera, "empty", parentSerNum);
		SerialNumbers sn = snser.findByAlias(aliasEndUsera);
		sn.setIssuerSerNum(parentSerNum);
		snser.saveSerialNumbers(sn);
		
		
		File ff = new File("cerCompanies/cer"+usrnm);
		ff.mkdir();
	//	File fff = new File("cer"+usrnm+"/cer-"+aliasEndUsera+".cer");
		File fff = new File("cerCompanies/cer"+usrnm+"/Certificate"+bi.toString()+".cer");
		fff.createNewFile();
		if(!fff.exists()){
			fff.mkdir();
		}
	
		BufferedWriter w = new BufferedWriter(new FileWriter(fff.getPath()));
		StringWriter sw = new StringWriter();
		JcaPEMWriter pemWriter = new JcaPEMWriter(sw);
		pemWriter.writeObject(cert);
		pemWriter.close();
		
		sw.close();
		w.write(sw.toString());
		w.flush();
		w.close();
		System.out.println("Sacuvan izgenerisan sertifikat za usera "+ usrnm + "u folderu " + fff.getName());
		f.delete();
		return "OK";
    	
	}
	
	@PermissionType("saveApprovedCert")
	@RequestMapping(value="saveApprovedCert/", method = RequestMethod.POST)
	public @ResponseBody String saveApprovedCert(@RequestBody String data, HttpSession session) throws IOException, KeyStoreException, NoSuchProviderException, NoSuchAlgorithmException, CertificateException{
		String[] passwords = data.split("&");
		String passKs = passwords[0].split("=")[1];
		String passPk = passwords[1].split("=")[1];
		User u = (User)session.getAttribute("user");
		
		File fff = new File("pks");
		File[] listOfPks = fff.listFiles();
		File ffff = listOfPks[0];
		FileInputStream fis = new FileInputStream(ffff);
		System.out.println("FILE " + ffff.getName());
		String serNum = ffff.getName().split("-")[2].split("\\.")[0];
	    DataInputStream dis = new DataInputStream(fis);
	    byte[] keyBytes = new byte[(int)ffff.length()];
	    dis.readFully(keyBytes);
	    
	    fis.close();
	    PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(keyBytes);
	    KeyFactory kf = KeyFactory.getInstance("RSA");
		PrivateKey pk = null;
		try {
			pk = kf.generatePrivate(spec);
		} catch (InvalidKeySpecException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		dis.close();
		BigInteger bi = new BigInteger(serNum, 16);
		
		File f = new File("cerCompanies/cer"+u.getUsername()+"/Certificate"+bi.toString()+".cer");
		CertificateFactory cf = CertificateFactory.getInstance("X.509");

		 BufferedInputStream in = new BufferedInputStream(new FileInputStream(f));
		 	X509Certificate cert = null;
			while (in.available() > 0) {
			    cert = (X509Certificate) cf.generateCertificate(in); 
			    System.out.println("Cert:\n===================\n" + cert.toString() + "\n");
			    
				
			}
			KeyStoreWriter keyStoreWriter = new KeyStoreWriter();
			String ksName = "keyStore"+u.getUsername();
			File file = new File("keyStores/");
			File[] listOfFiles2 = file.listFiles();
			boolean exists2 = false;
			for(File file2 : listOfFiles2){
				if(file2.getName().equals(ksName))
					exists2 = true;
			}
			if(!exists2){
				keyStoreWriter.loadKeyStore(null, passKs.toCharArray());
			}
			else{
				keyStoreWriter.loadKeyStore("keyStores/"+ksName,passKs.toCharArray());
			}
			//keyStoreWriter.loadKeyStore("keyStores/"+ksName,passKs.toCharArray());
			
			String alias = snser.findBySerialNumber(serNum).getAlias();
			keyStoreWriter.write(alias, pk, passPk.toCharArray(), cert);
			//keyStoreWriter.writeCert(alias, cert);
			keyStoreWriter.saveKeyStore("keyStores/"+ksName,passKs.toCharArray());
			System.out.println("DONE");
			in.close();
			ffff.delete();
		return "OK";
	}
	
}
