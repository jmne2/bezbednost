package jmn.controller;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;

import jmn.annotation.PermissionType;
import jmn.models.Adresa;
import jmn.models.PoslovniPartner;
import jmn.models.Preduzece;
import jmn.models.SifrarnikDelatnosti;
import jmn.pojo.UserData;
import jmn.services.AdresaService;
import jmn.services.PoslovniPartnerService;
import jmn.services.PreduzeceService;
import jmn.services.SifrarnikDelatnostiService;

@Controller
public class PoslovniPartnerController {

	@Autowired
	private PoslovniPartnerService pps;
	
	@Autowired
	private PreduzeceService ps;
	
	@Autowired
	private SifrarnikDelatnostiService sds;
	
	@Autowired
	private AdresaService as;
	
	
	@PermissionType("PoslovniPartner:view")
	@RequestMapping(value="/partneri/", method=RequestMethod.GET)
	public ResponseEntity<HashMap<String, Object>> fetchPartnere(HttpSession session){
		UserData u = (UserData) session.getAttribute("user");
		List<PoslovniPartner> partneri = pps.findByPreduzece(u.getPreduzece());
		List<Adresa> adrese = as.findAll();
		List<Preduzece> preduzeca = ps.findAll();
		List<SifrarnikDelatnosti> delatnosti = sds.findAll();
		HashMap<String, Object> resp = new HashMap<String, Object>();
		resp.put("adrese", adrese);
		resp.put("preduzeca", preduzeca);
		resp.put("delatnosti", delatnosti);
		if(partneri.isEmpty())
			return new ResponseEntity<HashMap<String, Object>>(resp, HttpStatus.OK);
		resp.put("partneri", partneri);
		return new ResponseEntity<HashMap<String, Object>>(resp, HttpStatus.OK);
	}
	
	@PermissionType("PoslovniPartner:add")
	@RequestMapping(value = "/partneri/", method = RequestMethod.POST)
	public ResponseEntity<?> createPartnera(HttpSession session, @RequestBody PoslovniPartner partner, UriComponentsBuilder ucBuilder) {
			PoslovniPartner p = pps.findByPib(partner.getPib());
			String error = "";
			if(p != null){
				error = "pib mora biti jedinstven";
				HttpHeaders headers = new HttpHeaders();
				headers.setContentType(MediaType.TEXT_PLAIN);
				return new ResponseEntity<String>(error, headers, HttpStatus.INTERNAL_SERVER_ERROR);
			}
			
			p = pps.findByMaticniBroj(partner.getMaticniBroj());
			
			if(p != null){
				error = "maticni broj mora biti jedinstven";
				HttpHeaders headers = new HttpHeaders();
				headers.setContentType(MediaType.TEXT_PLAIN);
				return new ResponseEntity<String>(error, headers, HttpStatus.INTERNAL_SERVER_ERROR);
			}
				
			p = pps.savePoslovniPartner(partner); 
	        
			try {
		        HttpHeaders headers = new HttpHeaders();
		        headers.setLocation(ucBuilder.path("/partneri/{id}").buildAndExpand(p.getId()).toUri());
		        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	        }
	        catch(Exception e){
	        	return new ResponseEntity<Void>(HttpStatus.CONFLICT);
	        }
	    }
	@PermissionType("PoslovniPartner:edit")
	 @RequestMapping(value = "/partneri/{id}", method = RequestMethod.POST)
	    public ResponseEntity<?> updatePartnera(HttpSession session, @PathVariable("id") long id, @RequestBody PoslovniPartner partner) {
	 
			PoslovniPartner exists = pps.findOne(id);
		 	if(exists == null){
		 		return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		 	}
		 	Adresa a = as.findOne(exists.getAdresa().getId());
		 	Preduzece p = ps.findOne(partner.getPreduzece().getId());
		 	SifrarnikDelatnosti d = sds.findOne(p.getSifraDelatnosti().getId());
		 	exists.setAdresa(a);;
		 	exists.setSifraDelatnosti(d);
		 	exists.setPreduzece(p);
		 	exists.setBrTelefona(partner.getBrTelefona());
		 	exists.setEmail(partner.getEmail());
		 	exists.setMaticniBroj(partner.getMaticniBroj());
		 	exists.setFax(partner.getFax());
		 	exists.setNaziv(partner.getNaziv());
		 	exists.setPib(partner.getPib());
		 	exists = pps.savePoslovniPartner(exists);
		 	return new ResponseEntity<PoslovniPartner>(exists, HttpStatus.OK);
	    }
	
	@PermissionType("PoslovniPartner:delete")
	@RequestMapping(value = "/partneri/{id}", method = RequestMethod.DELETE)
	    public ResponseEntity<?> deletePartnera(HttpSession session, @PathVariable("id") long id) {
		PoslovniPartner exists = pps.findOne(id); 
		 if(exists == null)
			 return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		 
		 pps.delete(id);
	     return new ResponseEntity<PoslovniPartner>(HttpStatus.NO_CONTENT);
	    }
	
	@PermissionType("PoslovniPartner:search")
	@RequestMapping(value = "/partneri/search/", method = RequestMethod.POST)
	    public ResponseEntity<?> searchPartnere(HttpSession session, @RequestBody PoslovniPartner partner) {
			List<PoslovniPartner> partneri = null;
			if(partner.getNaziv() == null)
				partner.setNaziv("");
			if(partner.getPib() == -1){
				if(partner.getPreduzece() != null)
					partneri = pps.findByNazivAndPreduzece(partner.getNaziv(), partner.getPreduzece());
			} else {
				partneri = pps.findByNazivAndPibAndPreduzece(partner.getNaziv(), partner.getPib(), partner.getPreduzece());
			}
				
		 if(partneri == null)
			 return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);

	     return new ResponseEntity<List<PoslovniPartner>>(partneri, HttpStatus.OK);
	    }
}
