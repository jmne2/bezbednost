package jmn.services;

import java.util.List;

import jmn.models.Drzava;

public interface DrzavaService {

	List<Drzava> findAll();
	Drzava saveDrzava(Drzava drz);
	Drzava findOne(Long id);
	void delete(Long id);
	void delete(Drzava drz);
	List<Drzava> findByOznakaAndNaziv(String oznaka, String naziv);
	
}
