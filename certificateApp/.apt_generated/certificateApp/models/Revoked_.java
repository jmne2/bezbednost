package certificateApp.models;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Revoked.class)
public abstract class Revoked_ {

	public static volatile SingularAttribute<Revoked, Date> date;
	public static volatile SingularAttribute<Revoked, String> serialNumber;
	public static volatile SingularAttribute<Revoked, Long> Id;

}

