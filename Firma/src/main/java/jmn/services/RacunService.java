package jmn.services;

import java.util.List;

import jmn.models.Banka;
import jmn.models.Preduzece;
import jmn.models.Racun;

public interface RacunService {

	List<Racun> findAll();

	Racun saveRacun(Racun r);

	Racun findOne(Long id);

	void delete(Long id);

	void delete(Racun r);

	List<Racun> findByPreduzeceAndBanka(Preduzece preduzece, Banka banka);

	List<Racun> findByPreduzece(Preduzece preduzece);
	
	Racun findByBrRacuna(String brRacuna);
}
