
'use strict';
 
angular.module('repo3App').factory('PredlogService', ['$http', '$q', 'urls', function($http, $q, urls){
 
    //var SERVICE_URI = 'http://localhost:8080/drzave/';
 
    var factory = {
        fetchAllPredloge: fetchAllPredloge,
        createPredlog: createPredlog,
        updatePredlog:updatePredlog,
        deletePredlog:deletePredlog,
        searchPredloge: searchPredloge
    };
 
    return factory;
 
    function fetchAllPredloge() {
        var deferred = $q.defer();
        $http.get(urls.PREDLOG_SERVICE)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
    function createPredlog(predlog) {
        var deferred = $q.defer();
        $http.post(urls.PREDLOG_SERVICE, predlog)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
 
    function updatePredlog(predlog, id) {
        var deferred = $q.defer();
        $http.post(urls.PREDLOG_SERVICE+id, predlog)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
    function deletePredlog(id) {
        var deferred = $q.defer();
        $http.delete(urls.PREDLOG_SERVICE+id)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
    function searchPredloge(predlog){
    	var deferred = $q.defer();
        $http.post(urls.PREDLOG_SERVICE+"search/", predlog)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
}]);