package jmn.aspect;

import java.lang.reflect.Method;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import jmn.annotation.PermissionType;
import jmn.controller.IndexController;
import jmn.models.Permission;
import jmn.models.Role;
import jmn.models.SifrarnikMetoda;
import jmn.models.User;
import jmn.pojo.UserData;
import jmn.services.RoleService;
import jmn.services.UserService;

@Aspect
//Mora @Component anotacija inace nece ucitati askept klasu
@Component
public class PermissionAspect {

	final static Logger logger = Logger.getLogger(PermissionAspect.class);
	
	@Autowired
	private UserService us;
	
	@Autowired
	private RoleService rs;
	
	@Around("execution(* jmn.controller.*.*(..)) && args(session,..) && @annotation(jmn.annotation.PermissionType)")
	public Object validator(ProceedingJoinPoint jp, HttpSession session){
		System.out.println("hijacked : " + jp.getSignature().getName());
		UserData u2 = (UserData)session.getAttribute("user");
		User u = us.findOne(u2.getId());
		if(u != null){
			MethodSignature signature = (MethodSignature) jp.getSignature();
		    Method method = signature.getMethod();
		    PermissionType myAnnotation = method.getAnnotation(PermissionType.class);
			boolean weGood = false;
		    for(Role r : u.getRoles()){
		    	for(Permission p : r.getPermissions()){
		    		if(myAnnotation.value().equals(p.getNaziv())){
		    			weGood = true;
		    			break;
		    		}
		    	}
		    }
		    Object retVal = null;
		    if(weGood){
				try {
					retVal = jp.proceed();
				} catch (Throwable e) {
					// TODO Auto-generated catch block
					logger.error("Greska prilikom izvrsenja metode " + SifrarnikMetoda.methods.get(jp.getSignature().getName()));
					e.printStackTrace();
				}
		    } else {
				logger.error("Prisput metodi " + SifrarnikMetoda.methods.get(jp.getSignature().getName()) + " bez adekvatnih permisija");
		    }
		    return retVal;
		} else {
			logger.error("Neuspesan pristup " + SifrarnikMetoda.methods.get(jp.getSignature().getName()));
			return null;
		}
	} 

	
	@Around("execution(* jmn.controller.*.*(..)) && args(session,..) && @annotation(jmn.annotation.UserRequired)")
	public Object existenceChecker(ProceedingJoinPoint jp, HttpSession session){
		System.out.println("hijacked : " + jp.getSignature().getName());
		UserData u2 = (UserData)session.getAttribute("user");
		User u = us.findOne(u2.getId());
		Object retVal = null;
		if(u != null){
			MethodSignature signature = (MethodSignature) jp.getSignature();
		    	try {
					retVal = jp.proceed();
				} catch (Throwable e) {
					// TODO Auto-generated catch block
					logger.error("Greska prilikom izvrsenja metode " + SifrarnikMetoda.methods.get(jp.getSignature().getName()));
					e.printStackTrace();
				}
		    return retVal;
		} else {
			logger.error("Neuspesan pristup " + SifrarnikMetoda.methods.get(jp.getSignature().getName()));
			return retVal;
		}
	} 

}
