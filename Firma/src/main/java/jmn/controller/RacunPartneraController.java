package jmn.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;

import jmn.annotation.PermissionType;
import jmn.configuration.AES;
import jmn.models.Banka;
import jmn.models.DEK;
import jmn.models.PoslovniPartner;
import jmn.models.RacunPartnera;
import jmn.models.SifrarnikMetoda;
import jmn.pojo.UserData;
import jmn.services.BankaService;
import jmn.services.DEKService;
import jmn.services.PoslovniPartnerService;
import jmn.services.RacunPartneraService;

@Controller
public class RacunPartneraController {

	final static Logger logger = Logger.getLogger(RacunPartneraController.class);
	
	@Autowired
	private RacunPartneraService rps;
	
	@Autowired
	private PoslovniPartnerService pps;
	
	@Autowired
	private BankaService bs;
	
	@Autowired
	private DEKService dks;
	
	@PermissionType("RacuniPartnera:view")
	@RequestMapping(value="/racuniPartnera/", method=RequestMethod.GET)
	public ResponseEntity<HashMap<String, Object>> fetchRacunePartnera(HttpSession session){
		UserData u = (UserData)session.getAttribute("user");
		int pb = u.getPreduzece().getPib();
		DEK kk = dks.findByPreduzece(u.getPreduzece());
		kk.setValue(AES.decryptDEK(kk.getValue(), Integer.toString(pb)));
	
		List<PoslovniPartner> partneri = pps.findByPreduzece(u.getPreduzece());
		List<RacunPartnera> racuni = new ArrayList<RacunPartnera>();
		for(PoslovniPartner p : partneri){
			List<RacunPartnera> temp = rps.findByPoslovniPartner(p);
			for(RacunPartnera rr : temp){
				rr.setBrRacuna(AES.decryptCBC(rr.getBrRacuna(),kk.getValue(), kk.getIv()));
			}
			racuni.addAll(temp);
		}
		List<Banka> banke = bs.findAll();
		
		HashMap<String, Object> resp = new HashMap<String, Object>();
		resp.put("banke", banke);
		resp.put("partneri", partneri);
		if(racuni.isEmpty())
			return new ResponseEntity<HashMap<String, Object>>(resp, HttpStatus.OK);
		resp.put("racuni", racuni);
		return new ResponseEntity<HashMap<String, Object>>(resp, HttpStatus.OK);
	}
	
	@PermissionType("RacuniPartnera:add")
	@RequestMapping(value = "/racuniPartnera/", method = RequestMethod.POST)
	public ResponseEntity<Void> createRacunPartnera(HttpSession session, @RequestBody RacunPartnera racun, UriComponentsBuilder ucBuilder) {
		UserData u = (UserData)session.getAttribute("user");
		int pb = u.getPreduzece().getPib();
		DEK kk = dks.findByPreduzece(u.getPreduzece());
		kk.setValue(AES.decryptDEK(kk.getValue(), Integer.toString(pb)));
		racun.setBrRacuna(AES.encryptCBC(racun.getBrRacuna(),kk.getValue(), kk.getIv()));
			RacunPartnera d = rps.saveRacunPartnera(racun); 
	        try {
		        HttpHeaders headers = new HttpHeaders();
		        headers.setLocation(ucBuilder.path("/racuniPartnera/{id}").buildAndExpand(d.getId()).toUri());
		        logger.warn("Korisnik "+ u.getId() + " uspesno je izvrsio metodu " + SifrarnikMetoda.methods.get(
			 				Thread.currentThread().getStackTrace()[1].getMethodName()));
		        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	        }
	        catch(Exception e){
	        	logger.error("Greska pri izvrsenju " + SifrarnikMetoda.methods.get(Thread.currentThread().getStackTrace()[1].getMethodName())
	        	+ " za korisnika " + u.getId());
	        	return new ResponseEntity<Void>(HttpStatus.CONFLICT);
	        }
	    }
	@PermissionType("RacuniPartnera:edit")
	 @RequestMapping(value = "/racuniPartnera/{id}", method = RequestMethod.POST)
	    public ResponseEntity<?> updateRacunPartnera(HttpSession session, @PathVariable("id") long id, @RequestBody RacunPartnera racun) {
		UserData u = (UserData)session.getAttribute("user");
		int pb = u.getPreduzece().getPib();
		DEK kk = dks.findByPreduzece(u.getPreduzece());
		kk.setValue(AES.decryptDEK(kk.getValue(), Integer.toString(pb)));

			RacunPartnera exists = rps.findOne(id);
		 	if(exists == null){
		 		logger.error("Greska pri izvrsenju " + SifrarnikMetoda.methods.get(Thread.currentThread().getStackTrace()[1].getMethodName())
	        	+ " za korisnika " + u.getId());
		 		return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		 	}
		 	Banka b = bs.findOne(racun.getBanka().getId());
		 	PoslovniPartner p = pps.findOne(racun.getPoslovniPartner().getId());
		 	exists.setBanka(b);
		 	exists.setBrRacuna(AES.encryptCBC(racun.getBrRacuna(), kk.getValue(), kk.getIv()));
		 	exists.setPoslovniPartner(p);
		 	exists = rps.saveRacunPartnera(exists);
	        logger.warn("Korisnik "+ u.getId() + " uspesno je izvrsio metodu " + SifrarnikMetoda.methods.get(
		 				Thread.currentThread().getStackTrace()[1].getMethodName()));
		 	return new ResponseEntity<RacunPartnera>(exists, HttpStatus.OK);
	    }
	
	@PermissionType("RacuniPartnera:delete")
	@RequestMapping(value = "/racuniPartnera/{id}", method = RequestMethod.DELETE)
	    public ResponseEntity<?> deleteRacunPartnera(HttpSession session, @PathVariable("id") long id) {
		UserData u = (UserData)session.getAttribute("user");
		RacunPartnera exists = rps.findOne(id); 
		 if(exists == null){
			 logger.error("Greska pri izvrsenju " + SifrarnikMetoda.methods.get(Thread.currentThread().getStackTrace()[1].getMethodName())
	        	+ " za korisnika " + u.getId());
			 return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		 }
		 rps.delete(id);
        logger.warn("Korisnik "+ u.getId() + " uspesno je izvrsio metodu " + SifrarnikMetoda.methods.get(
		 				Thread.currentThread().getStackTrace()[1].getMethodName()));
	     return new ResponseEntity<RacunPartnera>(HttpStatus.NO_CONTENT);
	    }
	
	@PermissionType("RacuniPartnera:search")
	@RequestMapping(value = "/racuniPartnera/search/", method = RequestMethod.POST)
	    public ResponseEntity<?> searchRacuniPartnera(HttpSession session, @RequestBody RacunPartnera racun) {
			if(racun.getBrRacuna() == null)
				racun.setBrRacuna("");
			List<RacunPartnera> racuni = null;
			if(racun.getBanka() != null)
				racuni = rps.findByPoslovniPartnerAndBanka(racun.getPoslovniPartner(), racun.getBanka());
			else
				racuni = rps.findByPoslovniPartner(racun.getPoslovniPartner());
		 if(racuni == null)
			 return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);

	     return new ResponseEntity<List<RacunPartnera>>(racuni, HttpStatus.OK);
	    }
}
