package jmn.services;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jmn.models.PredlogPlacanja;
import jmn.models.Preduzece;
import jmn.repositories.PredlogPlacanjaRepository;

@Service
public class PredlogPlacanjaServiceImpl implements PredlogPlacanjaService{

	@Autowired
	private PredlogPlacanjaRepository pr;
	
	@Override
	public List<PredlogPlacanja> findAll() {
		return (List<PredlogPlacanja>) pr.findAll();
	}

	@Override
	public PredlogPlacanja savePredlogPlacanja(PredlogPlacanja p) {
		return pr.save(p);
	}

	@Override
	public PredlogPlacanja findOne(Long id) {
		return pr.findOne(id);
	}

	@Override
	public void delete(Long id) {
		pr.delete(id);
	}

	@Override
	public void delete(PredlogPlacanja p) {
		pr.delete(p);
	}

	@Override
	public List<PredlogPlacanja> findByDatumAndStatusContainingAndBrojContaining(Date datum, String status,
			String broj) {
		return pr.findByDatumAndStatusContainingAndBrojContaining(datum, status, broj);
	}

	@Override
	public List<PredlogPlacanja> findByStatusAndBroj(String status, String broj) {
		return pr.findByStatusContainingAndBrojContaining(status, broj);
	}

	@Override
	public List<PredlogPlacanja> findByPreduzece(Preduzece preduzece) {
		return pr.findByPreduzece(preduzece);
	}

	@Override
	public List<PredlogPlacanja> findByStatusAndPreduzece(String status, Preduzece preduzece) {
		return pr.findByStatusAndPreduzece(status, preduzece);
	}

}
