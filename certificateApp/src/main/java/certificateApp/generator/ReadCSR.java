package certificateApp.generator;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.Security;

import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.operator.ContentVerifierProvider;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentVerifierProviderBuilder;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;
import org.bouncycastle.pkcs.PKCSException;
import org.bouncycastle.pkcs.jcajce.JcaPKCS10CertificationRequest;


public class ReadCSR {
	
	private X500Name x500name;
	private PublicKey publicKey;
	
	public ReadCSR(){
		
	}
	
	public boolean readcsr(File f) throws IOException, PKCSException, OperatorCreationException{
		//PEMReader pm = new PEMReader();
		// SubjectKeyIdentifier subjectKeyIdentifier = new SubjectKeyIdentifier();
		//final ByteArrayInputStream in = new ByteArrayInputStream();
		
		
		byte[] bFile = new byte[(int) f.length()];
		FileInputStream fis;
		boolean valid = false;
		try {
			fis = new FileInputStream(f);
			fis.read(bFile);
		     fis.close();
		     String reqString = new String(bFile, "UTF-8");
		     
		     String temp = new String(bFile);
		     System.out.println("TEMP " +temp);
		      String temp2 = temp.replace("-----BEGIN NEW CERTIFICATE REQUEST-----", "");
		      temp2 = temp2.replace("-----END NEW CERTIFICATE REQUEST-----", "");
		      temp2 = temp2.trim();
		      System.out.println(temp2);
		      //System.out.println("Private key\n"+privKeyPEM);

		      StringReader sr = new StringReader(temp2);
		      PEMParser pp = new PEMParser(sr);
		      Object parsedObj = pp.readObject();
		      System.out.println("PemParser returned: " + parsedObj);
		       if (parsedObj instanceof PKCS10CertificationRequest)
		       {
		          JcaPKCS10CertificationRequest jcaPKCS10CertificationRequest = new JcaPKCS10CertificationRequest((PKCS10CertificationRequest)parsedObj);
		          try {
					System.out.println("" + jcaPKCS10CertificationRequest.getPublicKey());
					 x500name = jcaPKCS10CertificationRequest.getSubject();
					 
					 publicKey = jcaPKCS10CertificationRequest.getPublicKey();
					 
					 ContentVerifierProvider contentVerifierProvider = new JcaContentVerifierProviderBuilder()
							    .setProvider("BC").build(publicKey);
					 valid = jcaPKCS10CertificationRequest.isSignatureValid(contentVerifierProvider);
					
					 
				} catch (InvalidKeyException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (NoSuchAlgorithmException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		          finally{
		        	  if (pp != null)
				       {
				          pp.close();
				       }
		          }
		       }
		}finally{}
	
		return valid;       
	/*	      byte[] decoded = Base64.getMimeDecoder().decode(privKeyPEM);
		      System.out.println("DUZINA JE " + decoded.length);
		      String decodedStr = new String(decoded, "UTF-8");
		      System.out.println("DUZINA 2 JW " + decodedStr.length());
		      System.out.println(decodedStr);
		      PKCS10CertificationRequest csr = new PKCS10CertificationRequest(decoded);

		     
		 //    PKCS10CertificationRequest csr = convertPemToPKCS10CertificationRequest(reqString);
		     X500Name x500Name = csr.getSubject();
			 System.out.println("x500Name is: " + x500Name + "\n");

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
	}
	
	private PKCS10CertificationRequest convertPemToPKCS10CertificationRequest(String pem) {
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
        PKCS10CertificationRequest csr = null;
        ByteArrayInputStream pemStream = null;
        try {
            pemStream = new ByteArrayInputStream(pem.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException ex) {
            System.out.println("UnsupportedEncodingException, convertPemToPublicKey");
        }

        Reader pemReader = new BufferedReader(new InputStreamReader(pemStream));
        PEMParser pemParser = new PEMParser(pemReader);

        try {
            Object parsedObj = pemParser.readObject();

            System.out.println("PemParser returned: " + parsedObj);

            if (parsedObj instanceof PKCS10CertificationRequest) {
                csr = (PKCS10CertificationRequest) parsedObj;

            }
        } catch (IOException ex) {
            System.out.println("IOException, convertPemToPublicKey");
        }

        return csr;
    }

	public X500Name getX500name() {
		return x500name;
	}

	public void setX500name(X500Name x500name) {
		this.x500name = x500name;
	}

	public PublicKey getPublicKey() {
		return publicKey;
	}

	public void setPublicKey(PublicKey publicKey) {
		this.publicKey = publicKey;
	}
	
	
}
