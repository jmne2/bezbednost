package certificateApp.models;

public class Certificate {
	
	private String commonName;
	private String surname;
	private String givenName;
	private String organization;
	private String organizationUnit;
	private String country;
	private String email;
	private Boolean selfSigned;
	private String alias;
	private String passwordKs;	//sifra za keystore
	private String passwordPk;	//sifra za izvlacenje privatnog kljuca
	private Boolean isCa;
	private String bankName;
	
	public Certificate(){
		super();
	}

	public Certificate(String commonName, String surname, String givenName, String organization,
			String organizationUnit, String country, String email, String alias, Boolean selfSigned, String passwordKs, String passwordPk, String bankName) {
		super();
		this.commonName = commonName;
		this.surname = surname;
		this.givenName = givenName;
		this.organization = organization;
		this.organizationUnit = organizationUnit;
		this.country = country;
		this.email = email;
		this.alias = alias;
		this.selfSigned = selfSigned;
		this.passwordKs = passwordKs;
		this.passwordPk = passwordPk;
		this.bankName = bankName;
	}

	public String getPasswordPk() {
		return passwordPk;
	}

	public void setPasswordPk(String passwordPk) {
		this.passwordPk = passwordPk;
	}

	public String getPasswordKs() {
		return passwordKs;
	}

	public void setPasswordKs(String passwordKs) {
		this.passwordKs = passwordKs;
	}

	public String getCommonName() {
		return commonName;
	}

	public void setCommonName(String commonName) {
		this.commonName = commonName;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getGivenName() {
		return givenName;
	}

	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}

	public String getOrganization() {
		return organization;
	}

	public void setOrganization(String organization) {
		this.organization = organization;
	}

	public String getOrganizationUnit() {
		return organizationUnit;
	}

	public void setOrganizationUnit(String organizationUnit) {
		this.organizationUnit = organizationUnit;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public Boolean getSelfSigned() {
		return selfSigned;
	}

	public void setSelfSigned(Boolean selfSigned) {
		this.selfSigned = selfSigned;
	}

	public Boolean getIsCa() {
		return isCa;
	}

	public void setIsCa(Boolean isCa) {
		this.isCa = isCa;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	

}
