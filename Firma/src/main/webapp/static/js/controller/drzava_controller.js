'use strict';
 
//angular.module('repo3App').controller('DrzavaController', ['$scope', 'DrzavaService', DRZCtrl]);
angular.module('repo3App').controller('DrzavaController', DRZCtrl);

function DRZCtrl($scope, DrzavaService) {
    var self = this;
    self.drzava={id:null, oznaka:'', naziv:''};
    self.drzave=[];
    self.show = false;
    self.logged = false;
 
    self.submit = submit;
    self.edit = edit;
    self.remove = remove;
    self.reset = reset;
    self.first = first;
    self.prev = prev;
    self.next = next;
    self.last = last;
    self.search = search;
    self.currState = "edit";
    self.children = ["NaseljenaMesta"];
    self.indSelected = -1;
    $scope.indSelectedVote = -1;
    $scope.required = true;
    
    $scope.$on('changeState', function(event, args) {    	
    	if(self === args.ctrl){
    		self.currState = args.state;
    		$scope.required = (self.currState === "search") ? false : true;
    		self.drzava = {};
    	}
    });
    $scope.$on('resetEvent', function(event, args) {    	
    	if(self === args.ctrl){
    		self.currState = "edit";
    		fetchAllDrzave();
    	}
    });
    //TODO: change or scrap!!!
    $scope.$on('hidingYoCountries', function(event, args) {
    	self.show = args.arg[0];
    	
    });
    $scope.$on('hide', function(event, args) {
    	self.show = false;
    	
    });
    $scope.$on('changeSelection', function(event, args) {    	
    	if(self === args.ctrl){
    		switch (args.direction) {
	            case 'prev':
	                prev();
	                break;
	            case 'next':
	                next();
	                break;
	            case 'first':
	            	first();
	            	break;
	            case 'last':
	            	last();
	            	break;
    		}
    	}
    });
    
    $scope.$on('deleteSel', function(event, args) {    	
    	if(self === args.ctrl){
    		if($scope.indSelectedVote != -1){
    			var selectedDrz = self.drzave[$scope.indSelectedVote];
    			remove(selectedDrz.id);
    			self.currState = "edit";
    		}
    	}
    });
    $scope.$on('dispAfterLog', function(event, args) {
    	if(self === args.ctrl){
    		self.logged = true;
    		self.show = true;
    		fetchAllDrzave();
    	} else {
    		self.logged = false;
    		self.show = false;
    	}
    });
    
    function indexOfRole(propertyName,lookingForValue,array) {
        for (var i in array) {
            if (array[i][propertyName] == lookingForValue) {
                return i;
            }
        }
        return -1;
    }
    
    function first(){
    	$scope.indSelectedVote = 0;
    	self.indSelected = $scope.indSelectedVote;
    }
    function prev(){
    	if(($scope.indSelectedVote != 0) && ($scope.indSelectedVote != -1)){
    		$scope.indSelectedVote = $scope.indSelectedVote - 1;
    		self.indSelected = $scope.indSelectedVote;
    	}
    }
    
    function next(){
    	if(($scope.indSelectedVote != self.drzave.length) && ($scope.indSelectedVote != -1)){
    		$scope.indSelectedVote = $scope.indSelectedVote + 1;
    		self.indSelected = $scope.indSelectedVote;
    	}
    }
    
    function last(){
    	$scope.indSelectedVote = self.drzave.length - 1;
    	self.indSelected = $scope.indSelectedVote;
    }
    
    $scope.setSelected = function (indSelectedVote) {
    	$scope.indSelectedVote = indSelectedVote;
    	self.indSelected = indSelectedVote;
    	if(self.currState === "edit"){
    		var id = self.drzave[indSelectedVote].id;
    		edit(id);
    	}
    };
    $scope.changeState = function(state){
    	if(self.currState != state){
    		self.currState = state;
    	}
    }
    
    function fetchAllDrzave(){
        DrzavaService.fetchAllDrzave()
            .then(
            function(d) {
            	self.drzave = d;
                $scope.indSelectedVote = -1;
                self.indSelected = -1;
                self.show = true;
                if (self.currState === "delete"){
                	self.currState = "edit";
                }
            },
            function(errResponse){
                console.error('Error while fetching Users');
            }
        );
    }
 
    function createDrzava(drzava){
        DrzavaService.createDrzava(drzava)
            .then(
            fetchAllDrzave,
            function(errResponse){
                console.error('Error while creating User');
            }
        );
    }
 
    function updateDrzava(drzava, id){
        DrzavaService.updateDrzava(drzava, id)
            .then(
            fetchAllDrzave,
            function(errResponse){
                console.error('Error while updating User');
            }
        );
    }
 
    function deleteDrzava(id){
        DrzavaService.deleteDrzava(id)
            .then(
            fetchAllDrzave,
            function(errResponse){
                console.error('Error while deleting User');
            }
        );
    }
 
    function submit() {
        
    	if(self.currState === "add"){
            console.log('Saving New THing', self.drzava);
            createDrzava(self.drzava);        	
        } else if(self.currState === "edit"){
            updateDrzava(self.drzava, self.drzava.id);
            console.log('Thing updated with id ', self.drzava.id);
        } else if (self.currState === "search"){
        	search(self.drzava);
        }
        reset();
    }
 
    function edit(id){
        console.log('id to be edited', id);
        for(var i = 0; i < self.drzave.length; i++){
            if(self.drzave[i].id === id) {
                self.drzava = angular.copy(self.drzave[i]);
                break;
            }
        }
    }
 
    function remove(id){
        console.log('id to be deleted', id);
        if(self.drzava.id === id) {//clean form if the user to be deleted is shown there.
            reset();
        }
        deleteDrzava(id);
    }
 
    function search(drzava){
    	DrzavaService.searchDrzave(drzava)
        .then(
        function(d) {
        	self.drzave = d;
            $scope.indSelectedVote = -1;
        },
        function(errResponse){
            console.error('Error while fetching Users');
        }
        );
    }
    
    function reset(){
        self.drzava={};
        $scope.myForm.$setPristine(); //reset Form
    }
};