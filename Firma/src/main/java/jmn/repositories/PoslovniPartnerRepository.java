package jmn.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import jmn.models.PoslovniPartner;
import jmn.models.Preduzece;

public interface PoslovniPartnerRepository extends CrudRepository<PoslovniPartner, Long>{

	public List<PoslovniPartner> findByNazivAndPibAndPreduzece(String naziv, int pib, Preduzece preduzece);
	public List<PoslovniPartner> findByNazivAndPreduzece(String naziv, Preduzece preduzece);
	public List<PoslovniPartner> findByPreduzece(Preduzece preduzece);
	public List<PoslovniPartner> findByPreduzeceAndVrsta(Preduzece preduzece, String vrsta);
	public PoslovniPartner findByPib(int pib);
	public PoslovniPartner findByMaticniBroj(Long maticniBroj);
}
