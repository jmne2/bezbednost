package jmn.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jmn.models.NalogZaPlacanje;
import jmn.models.PredlogPlacanja;
import jmn.repositories.NalogZaPlacanjeRepository;

@Service
public class NalogZaPlacanjeServiceImpl implements NalogZaPlacanjeService{

	@Autowired
	private NalogZaPlacanjeRepository nzpr;
	
	@Override
	public List<NalogZaPlacanje> findAll() {
		return (List<NalogZaPlacanje>) nzpr.findAll();
	}

	@Override
	public NalogZaPlacanje saveNalogZaPlacanje(NalogZaPlacanje nzp) {
		return nzpr.save(nzp);
	}

	@Override
	public NalogZaPlacanje findOne(Long id) {
		return nzpr.findOne(id);
	}

	@Override
	public void delete(Long id) {
		nzpr.delete(id);
	}

	@Override
	public void delete(NalogZaPlacanje nzp) {
		nzpr.delete(nzp);
	}

	@Override
	public List<NalogZaPlacanje> findByUplatilacAndPrimalac(String uplatilac, String primalac) {
		return nzpr.findByUplatilacAndPrimalac(uplatilac, primalac);
	}

	@Override
	public List<NalogZaPlacanje> findByUplatilacContainingAndSvrhaUplateContainingAndPrimalacContainingAndSifraPlacanjaContainingAndValutaContainingAndIznosAndBrModelaAndPozivNaBrojContaining(
			String uplatilac, String svrhaUplate, String primalac, String sifraPlacanja, String valuta, double iznos,
			int brModela, String pozivNaBroj) {
		
		return nzpr.findByUplatilacContainingAndSvrhaUplateContainingAndPrimalacContainingAndSifraPlacanjaContainingAndValutaContainingAndIznosAndBrModelaAndPozivNaBrojContaining(uplatilac, svrhaUplate, primalac, sifraPlacanja, valuta, iznos, brModela, pozivNaBroj);
	}

	@Override
	public List<NalogZaPlacanje> findByUplatilacContainingAndSvrhaUplateContainingAndPrimalacContainingAndSifraPlacanjaContainingAndValutaContainingAndIznosAndBrModelaAndPozivNaBrojContainingAndZaPredlog(
			String uplatilac, String svrhaUplate, String primalac, String sifraPlacanja, String valuta, double iznos,
			int brModela, String pozivNaBroj, PredlogPlacanja zaPredlog) {
		return nzpr.findByUplatilacContainingAndSvrhaUplateContainingAndPrimalacContainingAndSifraPlacanjaContainingAndValutaContainingAndIznosAndBrModelaAndPozivNaBrojContainingAndZaPredlog(uplatilac, svrhaUplate, primalac, sifraPlacanja, valuta, iznos, brModela, pozivNaBroj, zaPredlog);
	}

	
}
