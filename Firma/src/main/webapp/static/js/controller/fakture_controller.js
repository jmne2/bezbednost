'use strict';
 
angular.module('repo3App').controller('FakturaController', ['$scope', 'FaktureService', 
                                                                    function($scope,FaktureService) {
	var self = this;
    self.ufaktura={};
    self.ufakture=[];
    self.ifaktura={};
    self.ifakture=[];
    self.godina={};
    self.godine=[];
    self.partner={};
    self.partneri=[];
    self.preduzece={};
    self.show = false;
    self.dugmici = true;
    self.ufaktureshow = false;
    self.ifaktureshow = false;
    self.children = ['StavkeFakture'];
    self.logged = false;
    self.us = {};
    
    self.edit = edit;
    self.editIzlazna = editIzlazna;
    self.reset = reset;
    self.first = first;
    self.prev = prev;
    self.next = next;
    self.last = last;
    self.search = search;
    self.searchIzlazna = searchIzlazna;
    self.back = back;
    self.showIzlazne = showIzlazne;
    self.showUlazne = showUlazne;
    
    $scope.disableForm = true;
    self.currState = "edit";
    self.indSelected = -1;
    self.indSelectedIzlazne = -1;
    $scope.indSelectedVote = -1;
    $scope.indSelectedVoteIzlazne = -1;
    $scope.required = true;
    
    
    $scope.open1 = function() {
        $scope.popup1.opened = true;
    };
    
    $scope.$on('changeState', function(event, args) {    	
    	if(self === args.ctrl){
    		self.currState = args.state;
    		$scope.required = (self.currState === "search") ? false : true;
    		$scope.disableForm = true;
    		$scope.disableForm = (self.currState === "search") ? false : true;
    		self.ufaktura = {};
    		self.ifaktura = {};
    	}
    });
    
    $scope.$on('resetEvent', function(event, args) {    	
    	if(self === args.ctrl){
    		self.currState = "edit";
    		if(self.ufaktureshow)
    			fetchAllUlazneFakture();
    		if(self.ifaktureshow)
    			fetchAllIzlazneFakture();
    	}
    });
    //TODO: change or scrap!!!
    /*
    $scope.$on('hidingYoCountries', function(event, args) {
    	self.show = args.arg[0];
    	
    });*/
    $scope.$on('hide', function(event, args) {
    	self.show = false;
    	
    });
    $scope.$on('changeSelection', function(event, args) {    	
    	if(self === args.ctrl){
    		switch (args.direction) {
	            case 'prev':
	                prev();
	                break;
	            case 'next':
	                next();
	                break;
	            case 'first':
	            	first();
	            	break;
	            case 'last':
	            	last();
	            	break;
    		}
    	}
    });
    
    $scope.$on('dispAfterLog', function(event, args) {
    	if(self === args.ctrl){
    		self.logged = true;
    		self.us = args.info;
    		self.show = true;
    		self.preduzece = args.info.preduzece;
    //		self.ufaktura.preduzece = self.us.preduzece;
    //		search(self.ufaktura);
    //		fetchAllUlazneFakture(self.preduzece);
    	} else {
    		self.logged = false;
    		self.show = false;
    	}
    });
    
    
    function first(){
    	$scope.indSelectedVote = 0;
    	self.indSelected = $scope.indSelectedVote;
    }
    function prev(){
    	if(($scope.indSelectedVote != 0) && ($scope.indSelectedVote != -1)){
    		$scope.indSelectedVote = $scope.indSelectedVote - 1;
    		self.indSelected = $scope.indSelectedVote;
    	}
    }
    
    function next(){
    	if(self.ufaktureshow){
	    	if(($scope.indSelectedVote != self.ufakture.length) && ($scope.indSelectedVote != -1)){
	    		$scope.indSelectedVote = $scope.indSelectedVote + 1;
	    		self.indSelected = $scope.indSelectedVote;
	    	}
    	}else{
    		if(($scope.indSelectedVote != self.ifakture.length) && ($scope.indSelectedVote != -1)){
        		$scope.indSelectedVote = $scope.indSelectedVote + 1;
        		self.indSelected = $scope.indSelectedVote;
        	}
    	}
    }
    
    function last(){
    	if(self.ufaktureshow){
    		$scope.indSelectedVote = self.ufakture.length - 1;
    	}else {
    		$scope.indSelectedVote = self.ifakture.length - 1;
    	}
    	self.indSelected = $scope.indSelectedVote;
    }
    
    $scope.setSelected = function (indSelectedVote) {
    	$scope.indSelectedVote = indSelectedVote;
    	self.indSelected = indSelectedVote;
    	if(self.currState === "edit"){
    		if(self.ufaktureshow){
	    		var id = self.ufakture[indSelectedVote].id;
	    		edit(id);
    		}
    		
    		if(self.ifaktureshow){
    			var id = self.ifakture[indSelectedVote].id;
	    		editIzlazna(id);
    		}
    	}
    };
    $scope.changeState = function(state){
    	if(self.currState != state){
    		self.currState = state;
    	}
    }
    
    function fetchAllUlazneFakture(){
    	FaktureService.fetchAllUlazneFakture()
            .then(
            function(d) {
            	self.ufakture = d;
                $scope.indSelectedVote = -1;
                self.indSelected = -1;
                self.show = true;
                
                if (self.currState === "delete"){
                	self.currState = "edit";
                }
            },
            function(errResponse){
                console.error('Error while fetching Ulazne Fakture');
            }
        );
    }
    
    function fetchAllIzlazneFakture(){
    	FaktureService.fetchAllIzlazneFakture()
            .then(
            function(d) {
            	self.ifakture = d;
                $scope.indSelectedVote = -1;
                self.indSelected = -1;
                self.show = true;
                
                if (self.currState === "delete"){
                	self.currState = "edit";
                }
            },
            function(errResponse){
                console.error('Error while fetching Ulazne Fakture');
            }
        );
    }
 

    function submit() {
    	
    	if(self.currState === "add"){
            alert("Akcija ne moze da se izvrsi!");       	
        } else if(self.currState === "edit"){
        	alert("Akcija ne moze da se izvrsi!");
        } else if (self.currState === "search" && self.ufaktureshow){
        	search(self.ufaktura);
        } else if (self.currState === "search" && self.ifaktureshow){
        	search(self.ifaktura);
        }
        reset();
    }
 
    function edit(id){
        console.log('id to be edited', id);
        for(var i = 0; i < self.ufakture.length; i++){
            if(self.ufakture[i].id === id) {
                self.ufaktura = angular.copy(self.ufakture[i]);
                break;
            }
        }
    }
    
    function editIzlazna(id){
        console.log('id to be edited', id);
        for(var i = 0; i < self.ifakture.length; i++){
            if(self.ifakture[i].id === id) {
                self.ifaktura = angular.copy(self.ifakture[i]);
                break;
            }
        }
    }
 /*
    function remove(id){
        console.log('id to be deleted', id);
        if(self.godina.id === id) {//clean form if the user to be deleted is shown there.
            reset();
        }
        deleteGodinu(id);
    }
 */
    function search(ulaznaFaktura){
    	FaktureService.searchUlaznaFaktura(ulaznaFaktura)
        .then(
        function(d) {
        	if(self.ufaktureshow){
        	self.ufakture = d;
        	}else{
        		self.ifakture=d;
        	}
            $scope.indSelectedVote = -1;
        },
        function(errResponse){
            console.error('Error while fetching Ulazne Fakture');
        }
        );
    }
    
    function searchIzlazna(izlaznaFaktura){
    	FaktureService.searchIzlaznaFaktura(izlaznaFaktura)
        .then(
        function(d) {
        	self.ifakture = d;
            $scope.indSelectedVote = -1;
        },
        function(errResponse){
            console.error('Error while fetching Ulazne Fakture');
        }
        );
    }
    
    function reset(){
        self.ufaktura={};
        self.ifaktura={};
        $scope.myForm.$setPristine(); //reset Form
    }
    
    function showUlazne(){
    	self.ufaktura={};
        self.ufakture=[];
        self.ifaktura={};
        self.ifakture=[];
        self.currState = "edit";
        $scope.disableForm = true;
    	$scope.indSelectedVote = -1;
    	$scope.indSelectedVoteIzlazne = -1;
    	fetchAllUlazneFakture();
        self.dugmici = false;
        self.ufaktureshow = true;
        self.ifaktureshow = false;
        
    }
    function showIzlazne(){
    	self.ufaktura={};
        self.ufakture=[];
        self.ifaktura={};
        self.ifakture=[];
        self.currState = "edit";
        $scope.disableForm = true;
    	$scope.indSelectedVote = -1;
    	$scope.indSelectedVoteIzlazne = -1;
    	fetchAllIzlazneFakture();
        self.dugmici = false;
        self.ufaktureshow = false;
        self.ifaktureshow = true;
        
        
    }
    
    function back(){
        self.ufaktura={};
        self.ufakture=[];
        self.ifaktura={};
        self.ifakture=[];
        self.currState = "edit";
        $scope.disableForm = true;
        $scope.indSelectedVote = -1;
    	$scope.indSelectedVoteIzlazne = -1;
        self.dugmici = true;
        self.ufaktureshow = false;
        self.ifaktureshow = false;
        
    }
}]);
