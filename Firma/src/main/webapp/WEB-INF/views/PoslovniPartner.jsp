<div class="generic-container" id="PoslovniPartner" ng-controller="PoslovniPartnerController as ctrl">
          <script type="text/ng-template" id="myModalContent.html">
        	<div class="modal-header">
            	<h3>Izaberite {{type}}</h3>
        	</div>
        	<div class="modal-body">
            	<ul>
                	<li ng-repeat="item in items">
                    	<a ng-click="selected.item = $index" ng-if="type != 'Adresu' && type != 'Delatnost'">{{ item.naziv }}</a>
						<a ng-click="selected.item = $index" ng-if="type === 'Delatnost'">{{ item.nazivDelatnosti }}</a>
						<a ng-click="selected.item = $index" ng-if="type === 'Adresu'">{{ item.ulica }} {{item.broj}}</a>
                	</li>
            	</ul>
            	Selected: <b>{{ selected.item }} </b>
        	</div>
        		<div class="modal-footer">
            	<button class="btn btn-primary" ng-click="ok()">OK</button>
            	<button class="btn btn-warning" ng-click="cancel()">Cancel</button>
        	</div>
    	</script>
         <div>
          <div class="panel panel-default">
                <!-- Default panel contents -->
              <div class="panel-heading"><span class="lead">Lista Partnera </span></div>
              <div class="tablecontainer">
                  <table class="table table-hover">
                      <thead>
                          <tr>
                              <th>ID</th>
                              <th>Naziv</th>
                              <th>PIB</th>
                              <th>Maticni Broj</th>
                              <th>Broj Telefona</th>
                              <th>Fax</th>
                              <th>Email</th>
                              <th>Vrsta</th>
                              <th>Delatnost</th>
                              <th>Adresa</th>
                              <th>Preduzece</th>
                          </tr>
                      </thead>
                      <tbody>
                      <!-- ng-click="setSelected(d.id)" ng-class="{selected: d.id === idSelectedVote}" -->
                          <tr ng-repeat="pp in ctrl.partneri" ng-click="setSelected($index)" ng-class="{selected: $index === indSelectedVote}">
                              <td><span ng-bind="pp.id"></span></td>
                              <td><span ng-bind="pp.naziv"></span></td>
                              <td><span ng-bind="pp.pib"></span></td>
                              <td><span ng-bind="pp.maticniBroj"></span></td>
                              <td><span ng-bind="pp.brTelefona"></span></td>
                              <td><span ng-bind="pp.fax"></span></td>
                              <td><span ng-bind="pp.email"></span></td>
                              <td><span ng-bind="pp.vrsta"></span></td>
                              <td><span ng-bind="pp.sifraDelatnosti.nazivDelatnosti"></span></td>
                              <td><span ng-bind="pp.adresa.ulica"></span><span ng-bind="pp.adresa.broj"></span></td>
                              <td><span ng-bind="pp.preduzece.naziv"></span></td>
                          </tr>
                      </tbody>
                  </table>
              </div>
          </div>
          <div class="panel panel-default">
              <div class="formcontainer">
                  <form ng-submit="ctrl.submit()" name="myForm" class="form-horizontal">
                      <input type="hidden" ng-model="ctrl.partner.id" />
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="naziv">Naziv Partnera</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="ctrl.partner.naziv" id="naziv" class="naziv form-control input-sm" placeholder="Unesite naziv" ng-required="required" ng-minlength="1"/>
                                  <div class="has-error" ng-show="myForm.$dirty">
                                      <span ng-show="myForm.naziv.$error.required">This is a required field</span>
                                      <span ng-show="myForm.naziv.$error.minlength">Minimum length required is 1</span>
                                      <span ng-show="myForm.naziv.$invalid">This field is invalid </span>
                                  </div>
                              </div>
                          </div>
                      </div>
 					<div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="pib">PIB</label>
                              <div class="col-md-7">
                                  <input type="number" ng-model="ctrl.partner.pib" id="pib" class="naziv form-control input-sm" placeholder="Unesite pib" ng-required="required" ng-minlength="9" ng-maxlength="9"/>
                                  <div class="has-error" ng-show="myForm.$dirty">
                                      <span ng-show="myForm.pib.$error.required">This is a required field</span>
                                      <span ng-show="myForm.pib.$error.minlength">Minimum length required is 9</span>
                                      <span ng-show="myForm.pib.$error.maxlength">Maximum length required is 9</span>
                                      <span ng-show="myForm.pib.$invalid">This field is invalid </span>
                                  </div>
                              </div>
                          </div>
                      </div>
                    <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="matBr">Maticni Broj</label>
                              <div class="col-md-7">
                                  <input type="number" ng-model="ctrl.partner.maticniBroj" id="matBr" class="naziv form-control input-sm" placeholder="Unesite maticni broj" ng-required="required" ng-minlength="8" ng-maxlength="8"/>
                                  <div class="has-error" ng-show="myForm.$dirty">
                                      <span ng-show="myForm.matBr.$error.required">This is a required field</span>
                                      <span ng-show="myForm.matBr.$error.minlength">Minimum length required is 8</span>
                                      <span ng-show="myForm.matBr.$error.maxlength">Maximum length required is 8</span>
                                      <span ng-show="myForm.matBr.$invalid">This field is invalid </span>
                                  </div>
                              </div>
                          </div>
                      </div>
                    <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="brTel">Broj Telefona</label>
                              <div class="col-md-7">
                                  <input type="number" ng-model="ctrl.partner.brTelefona" id="brTel" class="naziv form-control input-sm" placeholder="Unesite broj telefona" ng-required="required" ng-minlength="9" ng-maxlength="10"/>
                                  <div class="has-error" ng-show="myForm.$dirty">
                                      <span ng-show="myForm.brTel.$error.required">This is a required field</span>
                                      <span ng-show="myForm.brTel.$error.minlength">Minimum length required is 9</span>
                                      <span ng-show="myForm.brTel.$error.maxlength">Maximum length required is 10</span>
                                      <span ng-show="myForm.brTel.$invalid">This field is invalid </span>
                                  </div>
                              </div>
                          </div>
                      </div>
                    <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="fax">Fax</label>
                              <div class="col-md-7">
                                  <input type="number" ng-model="ctrl.partner.fax" id="fax" class="naziv form-control input-sm" placeholder="Unesite broj faxa" ng-minlength="9" ng-maxlength="10"/>
                                  <div class="has-error" ng-show="myForm.$dirty">
                                      <span ng-show="myForm.fax.$error.required">This is a required field</span>
                                      <span ng-show="myForm.fax.$error.minlength">Minimum length required is 9</span>
                                      <span ng-show="myForm.fax.$error.maxlength">Maximum length required is 10</span>
                                      <span ng-show="myForm.fax.$invalid">This field is invalid </span>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="em">Email</label>
                              <div class="col-md-7">
                                  <input type="email" ng-model="ctrl.partner.email" id="em" class="naziv form-control input-sm" placeholder="Unesite email" ng-required="required" ng-minlength="1"/>
                                  <div class="has-error" ng-show="myForm.$dirty">
                                      <span ng-show="myForm.em.$error.required">This is a required field</span>
                                      <span ng-show="myForm.em.$error.minlength">Minimum length required is 1</span>
                                      <span ng-show="myForm.em.$invalid">This field is invalid </span>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="vrsta">Vrsta</label>
                              <div class="col-md-7">
                                  <select ng-init="ctrl.partner.vrsta = ctrl.partner.vrsta || ctrl.vrste[0]" ng-model="ctrl.partner.vrsta" id="vrsta" placeholder="Unesite email" ng-options="item for item in ctrl.vrste"></select>
                              </div>
                          </div>
                      </div>
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="delatnost">Delatnost</label>
                              <button type="button" ng-click="ctrl.showWorks()" class="btn btn-info btn-sm">Izaberi Delatnosti</button>
                              <div class="col-md-7">
                              <label class="col-md-2 control-lable" id="delatnost">{{ctrl.delatnost.nazivDelatnosti}}</label>
                              </div>
                          </div>
                      </div>
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="adresa">Adresa</label>
                              <button type="button" ng-click="ctrl.showAdreses()" class="btn btn-info btn-sm">Izaberi Adresu</button>
                              <div class="col-md-7">
                              <label class="col-md-2 control-lable" id="adresa">{{ctrl.adresa.ulica}} {{ctrl.adresa.broj}}</label>
                              </div>
                          </div>
                      </div>      
 					<div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="preduzece">Preduzece</label>
                              <div class="col-md-7">
                              <label class="col-md-2 control-lable" id="preduzece">{{ctrl.preduzece.naziv}}</label>
                              </div>
                          </div>
                      </div>
                      <div class="row">
                          <div class="form-actions floatRight">
                              <input type="submit"  id ="subId" value="Commit" class="btn btn-primary btn-sm" ng-disabled="myForm.$invalid">
                              <button type="button" ng-click="ctrl.reset()" class="btn btn-warning btn-sm" ng-disabled="myForm.$pristine">Rollback</button>
                               <label>{{ctrl.currState}}</label>
                          </div>
                      </div>
                  </form>
              </div>
          </div>
          </div>
      </div>