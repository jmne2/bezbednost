package jmn.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;

import jmn.annotation.PermissionType;
import jmn.models.DnevnoStanje;
import jmn.models.Faktura;
import jmn.models.Racun;
import jmn.models.StavkaIzvoda;
import jmn.models.User;
import jmn.models.ZatvaranjeUF;
import jmn.services.DnevnoStanjeService;
import jmn.services.FakturaService;
import jmn.services.RacunService;
import jmn.services.StavkaIzvodaService;
import jmn.services.ZatvaranjeUFService;

@Controller
public class ZatvaranjeUFController {

	@Autowired
	private ZatvaranjeUFService zif;
	
	@Autowired
	private StavkaIzvodaService sis;
	
	@Autowired
	private FakturaService fs;
	
	@Autowired
	private DnevnoStanjeService dss;
	
	@Autowired
	private RacunService rs;
	
	@PermissionType("ZatvaranjeUF:view")
	@RequestMapping(value="/zatvaranjeUF/stavkeIzvoda/", method=RequestMethod.POST)
	public ResponseEntity<List<StavkaIzvoda>> listAllStavkeIzvoda(HttpSession session){
		System.out.println("USAO U KONTROLER ZATVARANJE IF");
		User u = (User) session.getAttribute("user");
		List<Racun> racuni = rs.findByPreduzece(u.getPreduzece());
		
		List<DnevnoStanje> stanja = new ArrayList<DnevnoStanje>();
		List<StavkaIzvoda> stavke = new ArrayList<StavkaIzvoda>();
		List<StavkaIzvoda> otvoreneStavke = new ArrayList<StavkaIzvoda>();
		List<ZatvaranjeUF> zatvoreneStavke = zif.findAll();
		System.out.println("DUZINA ZATVORENIH STAVKI JE" +zatvoreneStavke.size());
		
		
		for(Racun r : racuni){
			stanja.addAll(dss.findByRacun(r));
			System.out.println("RACUNI PREDUZECA"+ u.getPreduzece().getNaziv() +"SU" + r.getBrRacuna() + "A NJEGOVO PREDUZECE JE:" + r.getPreduzece().getNaziv());
		}
		
		for(DnevnoStanje st : stanja){
			stavke.addAll(sis.findByDnevnoStanje(st));
			System.out.println("DNEVNO STANJE ZA DAN:" + st.getDatumIzvoda());
		}
		

			for(StavkaIzvoda st : stavke){
				if(zatvoreneStavke.isEmpty() && st.getSmer().equalsIgnoreCase("D") ){
					otvoreneStavke.add(st);
				}else{
				for(ZatvaranjeUF zf : zatvoreneStavke){
					if(!st.getId().equals(zf.getStavkaIzvoda().getId()) && st.getSmer().equalsIgnoreCase("D")){
						otvoreneStavke.add(st);
							
					}
				}
			}
		}
		System.out.println("DUZINA STAVKI " + otvoreneStavke.size());
		if(otvoreneStavke.isEmpty()){
			 return new ResponseEntity<List<StavkaIzvoda>>(HttpStatus.NOT_FOUND);
		}
		 
	     return new ResponseEntity<List<StavkaIzvoda>>(otvoreneStavke, HttpStatus.OK);
	}
	
	@PermissionType("ZatvaranjeUF:view")
	@RequestMapping(value = "/zatvaranjeUF/fakture/", method = RequestMethod.POST)
	    public ResponseEntity<List<Faktura>> listAllFakture(HttpSession session) {
		 User user = (User)session.getAttribute("user");
		 List<Faktura> izlazneFakture = fs.findByPreduzeceAndVrstaFaktureAndPreostaliIznosGreaterThan(user.getPreduzece(), "ulazne", 0); 
		 if(izlazneFakture==null)
			 return new ResponseEntity<List<Faktura>>(HttpStatus.NOT_FOUND);
		 
	     return new ResponseEntity<List<Faktura>>(izlazneFakture, HttpStatus.OK);
    }
	
	 
	@PermissionType("ZatvaranjeUF:add")
	@RequestMapping(value = "/zatvaranjeUF/close/", method = RequestMethod.POST)
	    public ResponseEntity<?> searchFakture(@RequestBody List<Faktura> izlazneFakture,@RequestBody List<StavkaIzvoda> stavkeIzvoda, UriComponentsBuilder ucBuilder) {
		 
		ZatvaranjeUF closing = new ZatvaranjeUF();
		 
	     return new ResponseEntity<>(closing, HttpStatus.OK);
    }
}
