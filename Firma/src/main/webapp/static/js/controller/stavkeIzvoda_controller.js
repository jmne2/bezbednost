'use strict';
 
angular.module('repo3App').controller('StavkeIzvodaController', PGDNCtrl);

function PGDNCtrl($scope, $uibModal, StavkeIzvodaService) {
    var self = this;
    self.stavka={};
    self.stavke=[];
    self.stanja = [];
    self.stanje = {};
    self.show = false;
    self.children = [];
    self.logged = false;
    self.us = {};
    
    self.submit = submit;
    self.reset = reset;
    self.first = first;
    self.prev = prev;
    self.next = next;
    self.last = last;
    self.search = search;
    self.showIzvode = showIzvode;
    self.currState = "view";
    self.indSelected = -1;
    $scope.indSelectedVote = -1;
    $scope.required = true;
    $scope.popup1 = {
    	    opened: false
    };
    
    $scope.open1 = function() {
        $scope.popup1.opened = true;
    };
    $scope.popup2 = {
    	    opened: false
    };
    
    $scope.open2 = function() {
        $scope.popup2.opened = true;
    };
    
    $scope.$on('changeState', function(event, args) {    	
    	if(self === args.ctrl){
    		self.currState = args.state;
    		$scope.required = (self.currState === "search") ? false : true;
    		self.stavka = {};
    	}
    });
    $scope.$on('resetEvent', function(event, args) {    	
    	if(self === args.ctrl){
    		self.currState = "view";
    		self.stavka = {};
    		fetchAllStavke();
    	}
    });
    $scope.$on('hide', function(event, args) {
    	self.show = false;
    	
    });
    $scope.$on('changeSelection', function(event, args) {    	
    	if(self === args.ctrl){
    		switch (args.direction) {
	            case 'prev':
	                prev();
	                break;
	            case 'next':
	                next();
	                break;
	            case 'first':
	            	first();
	            	break;
	            case 'last':
	            	last();
	            	break;
    		}
    	}
    });
    
    $scope.$on('deleteSel', function(event, args) {    	
    	if(self === args.ctrl){
    		if($scope.indSelectedVote != -1){
    			var selectedDrz = self.stavke[$scope.indSelectedVote];
    			self.currState = "view";
    		}
    	}
    });
    $scope.$on('dispAfterLog', function(event, args) {
    	if(self === args.ctrl){
    		self.logged = true;
    		self.us = args.info;
    		self.show = true;
    		self.stavka = {};
    		if(args.parent != null){
    			self.stavka.dnevnoStanje = args.parent.stanja[args.parent.indSelected];
    			fetchAllStavke();
    			search(self.stavka);
    		} else {
    			fetchAllStavke();
    		}
    	} else {
    		self.logged = false;
    		self.show = false;
    	}
    });
    
    function first(){
    	$scope.indSelectedVote = 0;
    	self.indSelected = $scope.indSelectedVote;
    }
    function prev(){
    	if(($scope.indSelectedVote != 0) && ($scope.indSelectedVote != -1)){
    		$scope.indSelectedVote = $scope.indSelectedVote - 1;
    		self.indSelected = $scope.indSelectedVote;
    	}
    }
    
    function next(){
    	if(($scope.indSelectedVote != self.stavke.length) && ($scope.indSelectedVote != -1)){
    		$scope.indSelectedVote = $scope.indSelectedVote + 1;
    		self.indSelected = $scope.indSelectedVote;
    	}
    }
    
    function last(){
    	$scope.indSelectedVote = self.stavke.length - 1;
    	self.indSelected = $scope.indSelectedVote;
    }
    
    $scope.setSelected = function (indSelectedVote) {
    	$scope.indSelectedVote = indSelectedVote;
    	self.indSelected = indSelectedVote;
    };
    $scope.changeState = function(state){
    	if(self.currState != state){
    		self.currState = state;
    	}
    }
    
    function fetchAllStavke(){
    	StavkeIzvodaService.fetchAllStavke()
            .then(
            function(d) {
            	self.stavke = d["stavke"];
            	self.stanja = d["stanja"];
                $scope.indSelectedVote = -1;
                self.indSelected = -1;
                self.show = true;
            },
            function(errResponse){
                console.error('Error while fetching Users');
            }
        );
    }
 
    function submit() {
    	
    	if (self.currState === "search"){
        	search(self.stavka);
        }
    }
 
    function search(stavka){
    	StavkeIzvodaService.searchStavke(stavka)
        .then(
        function(d) {
        	self.stavke = d;
            $scope.indSelectedVote = -1;
        },
        function(errResponse){
            console.error('Error while fetching Users');
        }
        );
    }
    
    function reset(){
        self.stavka = {};
        $scope.myForm.$setPristine(); //reset Form
    }
    

    function showIzvode() {

        var modalInstance = $uibModal.open({
          templateUrl: 'myModalContent.html',
          controller: ModalInstanceCtrl,
          resolve: {
            items: function () {
              return self.stanja;
            }
          }
        });

        modalInstance.result.then(function (selectedItem) {
          self.stanje = self.stanja[selectedItem];
        }, function () {
        });
      };
      
      var ModalInstanceCtrl = function ($scope, $uibModalInstance, items) {

    	  $scope.items = items;
    	  $scope.selected = {
    	    item: -1
    	  };

    	  $scope.ok = function () {
    		  $uibModalInstance.close($scope.selected.item);
    	  };

    	  $scope.cancel = function () {
    		  $uibModalInstance.dismiss('cancel');
    	  };
    	};

};