'use strict';
 
angular.module('repo3App').factory('AdresaService', ['$http', '$q', 'urls', function($http, $q, urls){
 
   
 
    var factory = {
        fetchAllAdrese: fetchAllAdrese,
        createAdresu: createAdresu,
        updateAdresu: updateAdresu,
        deleteAdresu: deleteAdresu,
        searchAdrese: searchAdrese
    };
 
    return factory;
 
    function fetchAllAdrese() {
        var deferred = $q.defer();
        $http.get(urls.ADRESE_SERVICE)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while fetching Users');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
    function createAdresu(adresa) {
        var deferred = $q.defer();
        $http.post(urls.ADRESE_SERVICE, adresa)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
 
    function updateAdresu(adresa, id) {
        var deferred = $q.defer();
        $http.post(urls.ADRESE_SERVICE+id, adresa)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
    function deleteAdresu(id) {
        var deferred = $q.defer();
        $http.delete(urls.ADRESE_SERVICE+id)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
    function searchAdrese(adresa){
    	var deferred = $q.defer();
        $http.post(urls.ADRESE_SERVICE+"search/", adresa)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
}]);