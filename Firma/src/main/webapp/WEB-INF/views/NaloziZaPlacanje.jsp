<div class="generic-container" id="NaloziZaPlacanje" ng-controller="NalogController as ctrl">
          <script type="text/ng-template" id="myModalContent.html">
        	<div class="modal-header">
            	<h3>Izaberite {{type}}</h3>
        	</div>
        	<div class="modal-body">
            	<ul>
                	<li ng-repeat="item in items">
						<a ng-click="selected.item = $index" ng-if="type != 'Racune Partnera'">{{ item.broj}}</a>
						<a ng-click="selected.item = $index" ng-if="type === 'Racune Partnera'">{{ item.brRacuna }}</a>
                	</li>
            	</ul>
            	Selected: <b>{{ selected.item }} </b>
        	</div>
        		<div class="modal-footer">
            	<button class="btn btn-primary" ng-click="ok()">OK</button>
            	<button class="btn btn-warning" ng-click="cancel()">Cancel</button>
        	</div>
    	</script>
         <div>
          <div class="panel panel-default">
                <!-- Default panel contents -->
              <div class="panel-heading"><span class="lead">Lista Naloga </span></div>
              <div class="tablecontainer">
                  <table class="table table-hover">
                      <thead>
                          <tr>
                              <th>ID</th>
                              <th>Uplatilac</th>
                              <th>Racun uplatioca</th>
                              <th>Broj Modela</th>
                              <th>Poziv Na Broj</th>
                              <th>Svrha Uplate</th>
                              <th>Primalac</th>
                              <th>Sifra Placanja</th>
                              <th>Iznos</th>
                              <th>Valuta</th>
                              <th>Racun Primaoca</th>
                              <th>Broj Modela Odobrenja</th>
                              <th>Poziv Na Broj Odobrenja</th>
                              <th>Tip slanja</th>
                              <th>Broj i Status Predloga</th>
                              <th width="10%"></th>
                          </tr>
                      </thead>
                      <tbody>
                      <!-- ng-click="setSelected(d.id)" ng-class="{selected: d.id === idSelectedVote}" -->
                          <tr ng-repeat="n in ctrl.nalozi" ng-click="setSelected($index)" ng-class="{selected: $index === indSelectedVote}">
                              <td><span ng-bind="n.id"></span></td>
                              <td><span ng-bind="n.uplatilac"></span></td>
                              <td><span ng-bind="n.racunUplatioca.brRacuna"></span></td>
                              <td><span ng-bind="n.brModela"></span></td>
                              <td><span ng-bind="n.pozivNaBroj"></span></td>
                              <td><span ng-bind="n.svrhaUplate"></span></td>
                              <td><span ng-bind="n.primalac"></span></td>
                              <td><span ng-bind="n.sifraPlacanja"></span></td>
                              <td><span ng-bind="n.iznos"></span></td>
                              <td><span ng-bind="n.valuta"></span></td>
                              <td><span ng-bind="n.racunPrimaoca.brRacuna"></span></td>
                              <td><span ng-bind="n.brModelaOdobrenja"></span></td>
                              <td><span ng-bind="n.pozivNaBrojOdobrenja"></span></td>
                              <td><span ng-if="n.hitno">Hitno</span> <span ng-if="!n.hitno">Regularno</span></td>
                              <td><span ng-bind="n.zaPredlog.broj"></span> | <span ng-bind="n.zaPredlog.status"></span></td>
                              <td><button type="button" ng-click="ctrl.sendNalog(n.id)" class="btn btn-success btn-sm">Prosledi Banci</button></td>
                          </tr>
                      </tbody>
                  </table>
              </div>
          </div>
          <div class="panel panel-default">
              <div class="formcontainer">
                  <form ng-submit="ctrl.submit()" name="myForm" class="form-horizontal">
                      <input type="hidden" ng-model="ctrl.nalog.id" />
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="naziv">Uplatilac</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="ctrl.us.preduzece.naziv" id="naziv" class="naziv form-control input-sm" placeholder="Unesite uplatioca" disabled/>
                              </div>
                          </div>
                      </div>
 					<div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="svrha">Svrha Uplate</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="ctrl.nalog.svrhaUplate" id="svrha" class="naziv form-control input-sm" placeholder="Unesite svrhu uplate" ng-required="required" ng-minlength="1" ng-maxlength="200"/>
                                  <div class="has-error" ng-show="myForm.$dirty">
                                      <span ng-show="myForm.svrha.$error.required">This is a required field</span>
                                      <span ng-show="myForm.svrha.$error.minlength">Minimum length required is 1</span>
                                      <span ng-show="myForm.svrha.$error.maxlength">Maximum length required is 200</span>
                                      <span ng-show="myForm.svrha.$invalid">This field is invalid </span>
                                  </div>
                              </div>
                          </div>
                      </div>
                    <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="primalac">Primalac</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="ctrl.nalog.primalac" id="primalac" class="naziv form-control input-sm" placeholder="Unesite primaoca" ng-required="required" ng-minlength="1" ng-maxlength="20"/>
                                  <div class="has-error" ng-show="myForm.$dirty">
                                      <span ng-show="myForm.primalac.$error.required">This is a required field</span>
                                      <span ng-show="myForm.primalac.$error.minlength">Minimum length required is 1</span>
                                      <span ng-show="myForm.primalac.$error.maxlength">Maximum length required is 20</span>
                                      <span ng-show="myForm.primalac.$invalid">This field is invalid </span>
                                  </div>
                              </div>
                          </div>
                      </div>
                    <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="sifra">Sifra Placanja</label>
                              <div class="col-md-7">
                                  <input type="number" ng-model="ctrl.nalog.sifraPlacanja" id="sifra" class="naziv form-control input-sm" placeholder="Unesite sifru placanja" ng-required="required" ng-minlength="1" ng-maxlength="5"/>
                                  <div class="has-error" ng-show="myForm.$dirty">
                                      <span ng-show="myForm.sifra.$error.required">This is a required field</span>
                                      <span ng-show="myForm.sifra.$error.minlength">Minimum length required is 1</span>
                                      <span ng-show="myForm.sifra.$error.maxlength">Maximum length required is 5</span>
                                      <span ng-show="myForm.sifra.$invalid">This field is invalid </span>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="iznos">Iznos</label>
                              <div class="col-md-7">
                                  <input type="number" ng-model="ctrl.nalog.iznos" id="iznos" class="naziv form-control input-sm" placeholder="Unesite iznos" ng-required="required" ng-minlength="1" ng-maxlength="18"/>
                                  <div class="has-error" ng-show="myForm.$dirty">
                                      <span ng-show="myForm.iznos.$error.required">This is a required field</span>
                                      <span ng-show="myForm.iznos.$error.minlength">Minimum length required is 1</span>
                                      <span ng-show="myForm.iznos.$error.maxlength">Maximum length required is 18</span>
                                      <span ng-show="myForm.iznos.$invalid">This field is invalid </span>
                                  </div>
                              </div>
                          </div>
                      </div>
                    <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="valuta">Valuta</label>
                              <div class="col-md-7">
                              <select ng-init="ctrl.nalog.valuta = ctrl.nalog.valuta || ctrl.valute[0]" ng-model="ctrl.nalog.valuta" id="valuta" ng-required="required" ng-options="item for item in ctrl.valute"></select>
                              </div>
                          </div>
                      </div>
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="racunPrimaoca">Racun Primaoca</label>
                              <button type="button" ng-click="ctrl.showRacune()" class="btn btn-info btn-sm">Izaberi Racun</button>
                              <div class="col-md-7">
                              <label class="col-md-2 control-lable" id="racunPrimaoca">{{ctrl.racun.brRacuna}}</label>
                              </div>
                          </div>
                      </div>
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="brModela">Broj Modela</label>
                              <div class="col-md-7">
                                  <input type="number" ng-model="ctrl.nalog.brModela" id="brModela" class="naziv form-control input-sm" placeholder="Unesite broj modela" ng-required="required" ng-minlength="2"
                                  ng-maxlength="2"/>
                                  <div class="has-error" ng-show="myForm.$dirty">
                                      <span ng-show="myForm.brModela.$error.required">This is a required field</span>
                                      <span ng-show="myForm.brModela.$error.minlength">Minimum length required is 2</span>
                                      <span ng-show="myForm.brModela.$error.maxlength">Maximum length required is 2</span>
                                      <span ng-show="myForm.brModela.$invalid">This field is invalid </span>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="brModelaOdb">Broj Modela Odobrenja</label>
                              <div class="col-md-7">
                                  <input type="number" ng-model="ctrl.nalog.brModelaOdobrenja" id="brModelaOdb" class="naziv form-control input-sm" placeholder="Unesite broj modela odobrenja" ng-required="required" ng-minlength="2"
                                  ng-maxlength="2"/>
                                  <div class="has-error" ng-show="myForm.$dirty">
                                      <span ng-show="myForm.brModelaOdb.$error.required">This is a required field</span>
                                      <span ng-show="myForm.brModelaOdb.$error.minlength">Minimum length required is 2</span>
                                      <span ng-show="myForm.brModelaOdb.$error.maxlength">Maximum length required is 2</span>
                                      <span ng-show="myForm.brModelaOdb.$invalid">This field is invalid </span>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="poziv">Poziv na broj</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="ctrl.nalog.pozivNaBroj" id="poziv" class="naziv form-control input-sm" placeholder="Unesite poziv na broj" ng-required="required" ng-minlength="20"
                                  ng-maxlength="20"/>
                                  <div class="has-error" ng-show="myForm.$dirty">
                                      <span ng-show="myForm.poziv.$error.required">This is a required field</span>
                                      <span ng-show="myForm.poziv.$error.minlength">Minimum length required is 20</span>
                                      <span ng-show="myForm.poziv.$error.maxlength">Maximum length required is 20</span>
                                      <span ng-show="myForm.poziv.$invalid">This field is invalid </span>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="pozivOdb">Poziv na broj odobrenja</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="ctrl.nalog.pozivNaBrojOdobrenja" id="pozivOdb" class="naziv form-control input-sm" placeholder="Unesite poziv na broj" ng-required="required" ng-minlength="20"
                                  ng-maxlength="20"/>
                                  <div class="has-error" ng-show="myForm.$dirty">
                                      <span ng-show="myForm.pozivOdb.$error.required">This is a required field</span>
                                      <span ng-show="myForm.pozivOdb.$error.minlength">Minimum length required is 20</span>
                                      <span ng-show="myForm.pozivOdb.$error.maxlength">Maximum length required is 20</span>
                                      <span ng-show="myForm.pozivOdb.$invalid">This field is invalid </span>
                                  </div>
                              </div>
                          </div>
                      </div>
                      
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="predlog">Predlog za placanje</label>
                              <button type="button" ng-click="ctrl.showPredloge()" class="btn btn-info btn-sm">Izaberi predlog</button>
                              <div class="col-md-7">
                              <label class="col-md-2 control-lable" id="predlog">{{ctrl.predlog.broj}}</label>
                              </div>
                          </div>
                      </div>
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="hitan">Hitno</label>
                              <div class="col-md-7">
                              	<md-checkbox ng-model="ctrl.nalog.hitno" ng-init="false" aria-label="Hitno"></md-checkbox>
                              </div>
                          </div>
                      </div>
                      <div class="row">
                          <div class="form-actions floatRight">
                              <input type="submit"  id ="subId" value="Commit" class="btn btn-primary btn-sm" ng-disabled="myForm.$invalid">
                              <button type="button" ng-click="ctrl.reset()" class="btn btn-warning btn-sm" ng-disabled="myForm.$pristine">Rollback</button>
                               <label>{{ctrl.currState}}</label>
                          </div>
                      </div>
                  </form>
              </div>
          </div>
          </div>
      </div>