package jmn.services;

import java.util.List;

import jmn.models.Faktura;
import jmn.models.PredlogPlacanja;
import jmn.models.StaSePlaca;

public interface StaSePlacaService {

	List<StaSePlaca> findAll();

	StaSePlaca saveStaSePlaca(StaSePlaca sp);

	StaSePlaca findOne(Long id);

	void delete(Long id);

	void delete(StaSePlaca sp);
	
	List<StaSePlaca> findByFaktura(Faktura ulaznaFaktura);
	
	List<StaSePlaca> findByPredlogPlacanja(PredlogPlacanja predlogPlacanja);
}
