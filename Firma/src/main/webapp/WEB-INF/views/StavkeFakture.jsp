<div class="generic-container" ng-controller="StavkeFaktureController as ctrl" id="StavkeFakture">
       <div>
          <div class="panel panel-default">
                <!-- Default panel contents -->
                
                <div class="panel-heading"><span class="lead">Lista stavki fakture</span></div>
              <div class="tablecontainer">
                  <table class="table table-hover">
                      <thead>
                          <tr>
                              <th>ID.</th>
                              <th>Redni broj</th>
                              <th>Naziv robe ili usluga</th>
                              <th>Kolicina</th>
                              <th>Jedinica mere</th>
                              <th>Jedinicna cena</th>
                              <th>Vrednost</th>
                              <th>Procenat rabata</th>
                              <th>Iznos rabata</th>
                              <th>Umanjeno za rabat</th>
                          </tr>
                      </thead>
                      <tbody>
                      <!-- ng-click="setSelected(d.id)" ng-class="{selected: d.id === idSelectedVote}" -->
                          <tr ng-repeat="u in ctrl.stavkeFakture" ng-click="setSelected($index)" ng-class="{selected: $index === indSelectedVote}">
                              <td><span ng-bind="u.id"></span></td>
                              <td><span ng-bind="u.redniBroj"></span></td>
                              <td><span ng-bind="u.nazivRobeUsluge"></span></td>
                              <td><span ng-bind="u.kolicina"></span></td>
                              <td><span ng-bind="u.jedinicaMere"></span></td>
                              <td><span ng-bind="u.jedinicnaCena"></span></td>
                              <td><span ng-bind="u.procenatRabata"></span></td>
                              <td><span ng-bind="u.iznosRabata"></span></td>
                              <td><span ng-bind="u.umanjenoZaRabat"></span></td>
                          </tr>
                      </tbody>
                  </table>
              </div>
          </div>
          
          <div class="panel panel-default">
              <div class="formcontainer">
                  <form ng-submit="ctrl.submit()" name="myForm" class="form-horizontal">
                      <input type="hidden" ng-model="ctrl.stavkaFakture.id" />
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="redniBroj">Redni broj</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="ctrl.stavkaFakture.redniBroj" id="redniBroj" class="naziv form-control input-sm" placeholder="Unesite redni broj stavke fakture"/>
                                  <div class="has-error" ng-show="myForm.$dirty">
                                  </div>
                              </div>
                          </div>
                      </div>
                         
                       
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="nazivRobeUsluge">Naziv robe ili usluge</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="ctrl.stavkaFakture.nazivRobeUsluge" id="nazivRobeUsluge" class="form-control input-sm" placeholder="Unesite naziv robe ili usluge" />
                                  <div class="has-error" ng-show="myForm.$dirty">
                                  </div>
                              </div>
                          </div>
                      </div>
                      
                             <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="kolicina">Kolicina mere</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="ctrl.stavkaFakture.kolicina" id="kolicina" class="form-control input-sm" placeholder="Unesite kolicinu" />
                                  <div class="has-error" ng-show="myForm.$dirty">
                                  </div>
                              </div>
                          </div>
                      </div>
                      
                           <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="jedinicaMere">Jedinica mere</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="ctrl.stavkaFakture.jedinicaMere" id="jedinicaMere" class="form-control input-sm" placeholder="Unesite jedinicu mere" />
                                  <div class="has-error" ng-show="myForm.$dirty">
                                  </div>
                              </div>
                          </div>
                      </div>
                      
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="jedinicnaCena">Jedinicna cena</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="ctrl.stavkaFakture.jedinicnaCena" id="jedinicnaCena" class="form-control input-sm" placeholder="Unesite jedinicna cena" />
                                  <div class="has-error" ng-show="myForm.$dirty">
                                  </div>
                              </div>
                          </div>
                      </div>
                      
                         <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="vrednost">Vrednost</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="ctrl.stavkaFakture.vrednost" id="vrednost" class="form-control input-sm" placeholder="Unesite vrednost" />
                                  <div class="has-error" ng-show="myForm.$dirty">
                                  </div>
                              </div>
                          </div>
                      </div>
 					
 					   <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="procenatRabata">Procenat rabata</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="ctrl.stavkaFakture.procenatRabata" id="procenatRabata" class="form-control input-sm" placeholder="Unesite procenat rabata" />
                                  <div class="has-error" ng-show="myForm.$dirty">
                                  </div>
                              </div>
                          </div>
                      </div>
                      
                         <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="iznosRabata">Iznos rabata</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="ctrl.stavkaFakture.iznosRabata" id="iznosRabata" class="form-control input-sm" placeholder="Unesite iznos rabata" />
                                  <div class="has-error" ng-show="myForm.$dirty">
                                  </div>
                              </div>
                          </div>
                      </div>
                      
                         <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="umanjenoZaRabat">Umanjeno za rabat</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="ctrl.stavkaFakture.umanjenoZaRabat" id="umanjenoZaRabat" class="form-control input-sm" placeholder="Unesite umanjeno za rabat" />
                                  <div class="has-error" ng-show="myForm.$dirty">
                                  </div>
                              </div>
                          </div>
                      </div>
 					
                      <div class="row">
                          <div class="form-actions floatRight">
                              <input type="submit"  id ="subId" value="Commit" class="btn btn-primary btn-sm" ng-disabled="myForm.$invalid">
                              <button type="button" ng-click="ctrl.reset()" class="btn btn-warning btn-sm" ng-disabled="myForm.$pristine">Rollback</button>
                               <label>{{ctrl.currState}}</label>
                          </div>
                      </div>
                  </form>
              </div>
          </div>
          
</div>