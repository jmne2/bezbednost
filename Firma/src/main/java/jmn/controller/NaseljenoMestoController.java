package jmn.controller;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;

import jmn.annotation.PermissionType;
import jmn.models.Drzava;
import jmn.models.NaseljenoMesto;
import jmn.services.DrzavaService;
import jmn.services.NaseljenoMestoService;

@Controller
public class NaseljenoMestoController {

	@Autowired
	private NaseljenoMestoService nms;
	
	@Autowired
	private DrzavaService drz;
	
	@PermissionType("NaseljenaMesta:view")
	@RequestMapping(value="/mesta/", method=RequestMethod.GET)
	public ResponseEntity<HashMap<String, Object>> listAll(HttpSession session){
		List<Drzava> drzave = drz.findAll();

		HashMap<String, Object> resp = new HashMap<String, Object>();
		resp.put("drzave", drzave);
		List<NaseljenoMesto> mesta = nms.findAll();
		if(mesta.isEmpty())
			return new ResponseEntity<HashMap<String, Object>>(resp, HttpStatus.OK);
		resp.put("mesta", mesta);
		return new ResponseEntity<HashMap<String, Object>>(resp, HttpStatus.OK);
	}
	@PermissionType("NaseljenaMesta:add")
	@RequestMapping(value = "/mesta/", method = RequestMethod.POST)
	    public ResponseEntity<Void> createNaseljenoMesto(HttpSession session, @RequestBody NaseljenoMesto mesto, UriComponentsBuilder ucBuilder) {
	        System.out.println("Creating Mesto " + mesto.getNaziv());
	        Drzava d = drz.findOne(mesto.getDrzava().getId());
	        mesto.setDrzava(d);
	        NaseljenoMesto n = nms.save(mesto);
	        try {
		        HttpHeaders headers = new HttpHeaders();
		        headers.setLocation(ucBuilder.path("/mesta/{id}").buildAndExpand(n.getId()).toUri());
		        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	        }
	        catch(Exception e){
	        	return new ResponseEntity<Void>(HttpStatus.CONFLICT);
	        }
	    }
	@PermissionType("NaseljenaMesta:edit")
	@RequestMapping(value = "/mesta/{id}", method = RequestMethod.POST)
	    public ResponseEntity<?> updateNaseljenoMesto(HttpSession session, @PathVariable("id") long id, @RequestBody NaseljenoMesto mesto) {
	 
		 	NaseljenoMesto exists = nms.findOne(id);
		 	if(exists == null){
		 		return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		 	}
		 	exists.setNaziv(mesto.getNaziv());
		 	exists.setPostBroj(mesto.getPostBroj());
		 	exists.setDrzava(mesto.getDrzava());
		 	//exists.setAdrese(mesto.getAdrese());
		 	
		 	exists = nms.save(exists);
		 	return new ResponseEntity<NaseljenoMesto>(exists, HttpStatus.OK);
	    }
	@PermissionType("NaseljenaMesta:delete")
	 @RequestMapping(value = "/mesta/{id}", method = RequestMethod.DELETE)
	    public ResponseEntity<?> deleteNaseljenoMesto(HttpSession session, @PathVariable("id") long id) {
		 NaseljenoMesto exists = nms.findOne(id);
		 if(exists == null)
			 return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		 
		 nms.delete(exists);
	     return new ResponseEntity<NaseljenoMesto>(HttpStatus.NO_CONTENT);
	    }
	 
	@PermissionType("NaseljenaMesta:search")
	@RequestMapping(value = "/mesta/search/", method = RequestMethod.POST)
	    public ResponseEntity<?> searchNaseljenoMesto(HttpSession session, @RequestBody NaseljenoMesto mesto, UriComponentsBuilder ucBuilder) {
		 if(mesto.getNaziv() == null)
			 mesto.setNaziv("");
		 if(mesto.getPostBroj() == null)
			 mesto.setPostBroj("");
		 List<NaseljenoMesto> mesta = null;   
		 if(mesto.getDrzava().getId() != null){
			Drzava dr = drz.findOne(mesto.getDrzava().getId());
		 	mesta = nms.findByNazivContainingAndPostBrojContainingAndDrzava(mesto.getNaziv(),mesto.getPostBroj(), dr);
		 }else
			 mesta = nms.findByNazivContainingAndPostBrojContaining(mesto.getNaziv(), mesto.getPostBroj());
        if(mesta == null)
			 return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		 
	     return new ResponseEntity<List<NaseljenoMesto>>(mesta, HttpStatus.OK);
    }
}
