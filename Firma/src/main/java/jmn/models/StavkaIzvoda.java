package jmn.models;

import static javax.persistence.CascadeType.ALL;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class StavkaIzvoda implements Serializable{

	
	@Id
	@GeneratedValue
	private Long id;
	
	@Column(nullable = false)
	private String duznik;
	
	@Column (nullable = true)
	private String svrhaPlacanja;
	
	@Column(nullable = false)
	private String primalac;
	
	@Column(nullable = false)
	private Date datumNaloga;
	
	@Column(nullable = false)
	private Date datumValute;
	
	@Column(nullable = false)
	private String racunDuznika;
	
	@Column(nullable = false)
	private int modelZaduzenja;
	
	@Column(nullable = false)
	private String pozivNaBrojZaduzenja;
	
	@Column(nullable = false)
	private String racunPrimaoca;
	
	@Column(nullable = false)
	private int modelOdobrenja;
	
	@Column(nullable = false)
	private String pozivNaBrojOdobrenja;
	
	public double getPreostaliIznos() {
		return preostaliIznos;
	}


	public void setPreostaliIznos(double preostaliIznos) {
		this.preostaliIznos = preostaliIznos;
	}

	@Column(nullable = false)
	private double iznos;
	
	@Column(nullable = false)
	private double preostaliIznos;
	
	@Column(nullable = false)			//duguje/potrazuje
	private String smer;
	
	
//	@Column(nullable = true)			//gotovinski, bezgotovinski, obracunski, preknjizavanje
//	private String vrstaPlacanja;
	
	@OneToMany(cascade={ALL}, fetch=FetchType.LAZY, mappedBy="stavkaIzvoda")		//no
	@JsonIgnore
	private Set<ZatvaranjeUF> listaZatvaranjaUF = new HashSet<ZatvaranjeUF>();
	
	@OneToMany(cascade={ALL}, fetch=FetchType.LAZY, mappedBy="stavkaIzvoda")		//no
	@JsonIgnore
	private Set<ZatvaranjeIF> listaZatvaranjaIF = new HashSet<ZatvaranjeIF>();
	
	@ManyToOne()
	@JoinColumn(name="dnevnoStanje")
	private DnevnoStanje dnevnoStanje;

	public StavkaIzvoda() {
	}


	public String getDuznik() {
		return duznik;
	}


	public void setDuznik(String duznik) {
		this.duznik = duznik;
	}


	public String getSvrhaPlacanja() {
		return svrhaPlacanja;
	}


	public void setSvrhaPlacanja(String svrhaPlacanja) {
		this.svrhaPlacanja = svrhaPlacanja;
	}


	public String getPrimalac() {
		return primalac;
	}


	public void setPrimalac(String primalac) {
		this.primalac = primalac;
	}


	public Date getDatumNaloga() {
		return datumNaloga;
	}


	public void setDatumNaloga(Date datumNaloga) {
		this.datumNaloga = datumNaloga;
	}


	public Date getDatumValute() {
		return datumValute;
	}


	public void setDatumValute(Date datumValute) {
		this.datumValute = datumValute;
	}


	public String getRacunDuznika() {
		return racunDuznika;
	}


	public void setRacunDuznika(String racunDuznika) {
		this.racunDuznika = racunDuznika;
	}


	public int getModelZaduzenja() {
		return modelZaduzenja;
	}


	public void setModelZaduzenja(int modelZaduzenja) {
		this.modelZaduzenja = modelZaduzenja;
	}


	public String getPozivNaBrojZaduzenja() {
		return pozivNaBrojZaduzenja;
	}


	public void setPozivNaBrojZaduzenja(String pozivNaBrojZaduzenja) {
		this.pozivNaBrojZaduzenja = pozivNaBrojZaduzenja;
	}


	public String getRacunPrimaoca() {
		return racunPrimaoca;
	}


	public void setRacunPrimaoca(String racunPrimaoca) {
		this.racunPrimaoca = racunPrimaoca;
	}


	public int getModelOdobrenja() {
		return modelOdobrenja;
	}


	public void setModelOdobrenja(int modelOdobrenja) {
		this.modelOdobrenja = modelOdobrenja;
	}


	public String getPozivNaBrojOdobrenja() {
		return pozivNaBrojOdobrenja;
	}


	public void setPozivNaBrojOdobrenja(String pozivNaBrojOdobrenja) {
		this.pozivNaBrojOdobrenja = pozivNaBrojOdobrenja;
	}


	public void setId(Long id) {
		this.id = id;
	}


	

	public StavkaIzvoda(String duznik, String svrhaPlacanja, String primalac, Date datumNaloga, Date datumValute,
			String racunDuznika, int modelZaduzenja, String pozivNaBrojZaduzenja, String racunPrimaoca,
			int modelOdobrenja, String pozivNaBrojOdobrenja, double iznos, double preostaliIznos, String smer,
			Set<ZatvaranjeUF> listaZatvaranjaUF, Set<ZatvaranjeIF> listaZatvaranjaIF, DnevnoStanje dnevnoStanje) {
		super();
		this.duznik = duznik;
		this.svrhaPlacanja = svrhaPlacanja;
		this.primalac = primalac;
		this.datumNaloga = datumNaloga;
		this.datumValute = datumValute;
		this.racunDuznika = racunDuznika;
		this.modelZaduzenja = modelZaduzenja;
		this.pozivNaBrojZaduzenja = pozivNaBrojZaduzenja;
		this.racunPrimaoca = racunPrimaoca;
		this.modelOdobrenja = modelOdobrenja;
		this.pozivNaBrojOdobrenja = pozivNaBrojOdobrenja;
		this.iznos = iznos;
		this.preostaliIznos = preostaliIznos;
		this.smer = smer;
		this.listaZatvaranjaUF = listaZatvaranjaUF;
		this.listaZatvaranjaIF = listaZatvaranjaIF;
		this.dnevnoStanje = dnevnoStanje;
	}


	public double getIznos() {
		return iznos;
	}

	public void setIznos(double iznos) {
		this.iznos = iznos;
	}

	public String getSmer() {
		return smer;
	}

	public void setSmer(String smer) {
		this.smer = smer;
	}


	public Set<ZatvaranjeUF> getListaZatvaranjaUF() {
		return listaZatvaranjaUF;
	}

	public void setListaZatvaranjaUF(Set<ZatvaranjeUF> listaZatvaranjaUF) {
		this.listaZatvaranjaUF = listaZatvaranjaUF;
	}
	
	public void addZatvaranjeUF(ZatvaranjeUF zuf){
		this.listaZatvaranjaUF.add(zuf);
		if(!zuf.getStavkaIzvoda().equals(this))
			zuf.setStavkaIzvoda(this);
	}

	public Set<ZatvaranjeIF> getListaZatvaranjaIF() {
		return listaZatvaranjaIF;
	}

	public void setListaZatvaranjaIF(Set<ZatvaranjeIF> listaZatvaranjaIF) {
		this.listaZatvaranjaIF = listaZatvaranjaIF;
	}

	public void addZatvranjeIF(ZatvaranjeIF zif){
		this.listaZatvaranjaIF.add(zif);
		if(!zif.getStavkaIzvoda().equals(this))
			zif.setStavkaIzvoda(this);
	}

	public Long getId() {
		return id;
	}

	public DnevnoStanje getDnevnoStanje() {
		return dnevnoStanje;
	}

	public void setDnevnoStanje(DnevnoStanje dnevnoStanje) {
		this.dnevnoStanje = dnevnoStanje;
	}
	
}
