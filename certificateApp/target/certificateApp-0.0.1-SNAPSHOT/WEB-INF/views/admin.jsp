<html>
<head>
<meta charset="ISO-8859-1"     >
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<title>Get certificate</title>
<script type="text/javascript">
		function getFormData($form){
		    var unindexed_array = $form.serializeArray();
		    var indexed_array = {};
	
		    $.map(unindexed_array, function(n, i){
		        indexed_array[n['name']] = n['value'];
		    });
	
		    return indexed_array;
		}
</script>
</head>
<body>
	<br></br><br></br>
	<div class="container-fluid" align="center" style=" display: table; margin: auto; width:40%; ">
		<div class=" col-md-6 col-md" id="certBlock" >
			<form id="certForm" role="form" action="" method="post">
				<fieldset>
					<label>Common name:</label><br>
					<input type="text" class="form-control" name="commonName" id="commonNameId"  ><br>
					<label>Surname:</label><br>
					<input type="text" class="form-control" name="surname" id="surnameId"><br>
					<label>Given name:</label><br>
					<input type="text" class="form-control" name="givenName" id="givenName"><br>
					<label>Organization:</label><br>
					<input type="text" class="form-control" name="organization" id="organizationId"><br>
					<label>Organization unit:</label><br>
					<input type="text" class="form-control" name="organizationUnit" id="organizationUnitId"><br>
					<label>Country:</label><br>
					<input type="text" class="form-control" name="country" id="countryId"><br>
					<label>Email:</label><br>
					<input type="text" class="form-control" name="email" id="emailId"><br>
					<label>Self-signed</label>
					<input type="checkbox" name="selfSigned" value="false"><br>
					<input class="btn btn-lg btn-primary" type="submit" value="OK">
				</fieldset>
			</form> <br>
		</div>
	</div>
	
	<script type="text/javascript">
	
	$(document).on('submit','#certForm',function(e){
		e.preventDefault();
		var $form = $("#certForm");
		var data = getFormData($form);
		var s = JSON.stringify(data);
		 $.ajax({
             type: "POST",
             url: "cert/",
             data: s,
             contentType : "application/json",
             statusCode: {
            	 	200: function(xhr) {
            	 		alert("GOOD YOU GOD");
            	 	}
            	  }
         });
	});
	</script>
</body>
</html>