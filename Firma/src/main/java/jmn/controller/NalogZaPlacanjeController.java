
package jmn.controller;

import java.net.MalformedURLException;
import java.net.URL;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;

import jmn.annotation.PermissionType;
import jmn.configuration.AES;
import jmn.izvod.IzvodWS;
import jmn.izvod.data.NalogBanka;
import jmn.models.DEK;
import jmn.models.Faktura;
import jmn.models.NalogZaPlacanje;
import jmn.models.PoslovniPartner;
import jmn.models.PredlogPlacanja;
import jmn.models.Preduzece;
import jmn.models.Racun;
import jmn.models.RacunPartnera;
import jmn.models.SifrarnikMetoda;
import jmn.models.StaSePlaca;
import jmn.pojo.UserData;
import jmn.services.DEKService;
import jmn.services.NalogZaPlacanjeService;
import jmn.services.PoslovniPartnerService;
import jmn.services.PredlogPlacanjaService;
import jmn.services.PreduzeceService;
import jmn.services.RacunPartneraService;
import jmn.services.RacunService;

@Controller
public class NalogZaPlacanjeController {

	final static Logger logger = Logger.getLogger(NalogZaPlacanjeController.class);
	
		@Autowired
		private NalogZaPlacanjeService npsr;
		
		@Autowired
		private PredlogPlacanjaService pps;
		
		@Autowired
		private PreduzeceService ps;
		
		@Autowired
		private RacunService rs;
		
		@Autowired
		private PoslovniPartnerService pss;
		
		@Autowired
		private RacunPartneraService rps;

		@Autowired
		private DEKService dks;
		
		@PermissionType("NaloziZaPlacanje:view")
		@RequestMapping(value="/nalozi/", method=RequestMethod.GET)
		public ResponseEntity<HashMap<String, Object>> fetchNalozi(HttpSession session){
			HashMap<String, Object> resp = new HashMap<String, Object>();
			UserData u = (UserData) session.getAttribute("user");
			Preduzece pp = ps.findOne(u.getPreduzece().getId());
			int pb = u.getPreduzece().getPib();
			DEK kk = dks.findByPreduzece(u.getPreduzece());
			kk.setValue(AES.decryptDEK(kk.getValue(), Integer.toString(pb)));
			List<RacunPartnera> racuni = new ArrayList<RacunPartnera>();
			List<PredlogPlacanja> predlozi = pps.findByStatusAndPreduzece("odobren", pp);
			List<PoslovniPartner> listaPartnera = pss.findByPreduzece(pp);
			for(PoslovniPartner a : listaPartnera){
				List<RacunPartnera> temp = rps.findByPoslovniPartner(a);
				for(RacunPartnera rp : temp){
					rp.setBrRacuna(AES.decryptCBC(rp.getBrRacuna(), kk.getValue(), kk.getIv()));
				}
				racuni.addAll(temp);
			}
			resp.put("predlozi", predlozi);
			resp.put("racuni", racuni);
			List<NalogZaPlacanje> nalozi = npsr.findAll();
			for(NalogZaPlacanje n : nalozi){
				n.getRacunPrimaoca().setBrRacuna(AES.decryptCBC(n.getRacunPrimaoca().getBrRacuna(), kk.getValue(), kk.getIv()));
				n.getRacunUplatioca().setBrRacuna(AES.decryptCBC(n.getRacunUplatioca().getBrRacuna(), kk.getValue(), kk.getIv()));
			}
			if(nalozi.isEmpty())
				return new ResponseEntity<HashMap<String, Object>>(resp, HttpStatus.OK);
			resp.put("nalozi", nalozi);
			return new ResponseEntity<HashMap<String, Object>>(resp, HttpStatus.OK);
		}
		
		@PermissionType("NaloziZaPlacanje:add")
		@RequestMapping(value = "/nalozi/", method = RequestMethod.POST)
		public ResponseEntity<Void> createNalogZaPlacanje(HttpSession session, @RequestBody NalogZaPlacanje np, UriComponentsBuilder ucBuilder) {
				UserData u = (UserData)session.getAttribute("user");
				int pb = u.getPreduzece().getPib();
				DEK kk = dks.findByPreduzece(u.getPreduzece());
				kk.setValue(AES.decryptDEK(kk.getValue(), Integer.toString(pb)));

				RacunPartnera r = rps.findOne(np.getRacunPrimaoca().getId());
		        PredlogPlacanja predlog = pps.findOne(np.getZaPredlog().getId());
		        for(StaSePlaca ss : predlog.getListaStaSePlaca()){
		        	Faktura faktura = ss.getFaktura();
		        	faktura.getRacunPartnera().setBrRacuna(AES.decryptCBC(faktura.getRacunPartnera().getBrRacuna(), kk.getValue(),
		        			kk.getIv()));
		        	if(faktura.getRacunPartnera().getBrRacuna().equals(np.getRacunPrimaoca().getBrRacuna())){
		        		NalogZaPlacanje nalog = new NalogZaPlacanje();
		        		nalog.setRacunPrimaoca(r);
		        		nalog.setBrModela(np.getBrModela());
		        		nalog.setIznos(ss.getIznos());
		        		nalog.setPozivNaBroj(np.getPozivNaBroj());
		        		nalog.setPrimalac(np.getPrimalac());
		        		Racun racun = rs.findByBrRacuna(ss.getSaKogRacuna());
		        		nalog.setRacunUplatioca(racun);
		        		nalog.setSifraPlacanja(np.getSifraPlacanja());
		        		nalog.setSvrhaUplate(np.getSvrhaUplate());
		        		nalog.setUplatilac(np.getUplatilac());
		        		nalog.setValuta(np.getValuta());
		        		nalog.setZaPredlog(predlog);
		        		nalog.setPozivNaBrojOdobrenja(np.getPozivNaBrojOdobrenja());
		        		nalog.setBrModelaOdobrenja(np.getBrModelaOdobrenja());
		        		nalog.setHitno(np.isHitno());
				        NalogZaPlacanje d = npsr.saveNalogZaPlacanje(nalog);
		        	}
		        }
		        logger.warn("Korisnik "+ u.getId() + " uspesno je izvrsio metodu " + SifrarnikMetoda.methods.get(
		 				Thread.currentThread().getStackTrace()[1].getMethodName()));
		
		        return new ResponseEntity<Void>(HttpStatus.OK);
		        
		 }
		@PermissionType("NaloziZaPlacanje:edit")
		 @RequestMapping(value = "/nalozi/{id}", method = RequestMethod.POST)
		    public ResponseEntity<?> updateNalogZaPlacanje(HttpSession session, @PathVariable("id") long id, @RequestBody NalogZaPlacanje nzp) {
				UserData u = (UserData)session.getAttribute("user");
			
			NalogZaPlacanje exists = npsr.findOne(id);
			 	if(exists == null){
			 		logger.error("Greska pri izvrsenju " + SifrarnikMetoda.methods.get(Thread.currentThread().getStackTrace()[1].getMethodName())
		        	+ " za korisnika " + u.getId());
			 		return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
			 	}
			 	
			 	exists.setBrModela(nzp.getBrModela());
			 	exists.setIznos(nzp.getIznos());
			 	exists.setPozivNaBroj(nzp.getPozivNaBroj());
			 	exists.setPrimalac(nzp.getPrimalac());
			 	Racun racun = rs.findOne(nzp.getRacunUplatioca().getId());
			 	exists.setRacunUplatioca(racun);
			 	RacunPartnera r = rps.findOne(nzp.getRacunPrimaoca().getId());
			 	exists.setRacunPrimaoca(r);
			 	exists.setSifraPlacanja(nzp.getSifraPlacanja());
			 	exists.setSvrhaUplate(nzp.getSvrhaUplate());
			 	exists.setUplatilac(nzp.getUplatilac());
			 	exists.setValuta(nzp.getValuta());
			 	exists.setZaPredlog(nzp.getZaPredlog());
			 	exists.setPozivNaBrojOdobrenja(nzp.getPozivNaBrojOdobrenja());
        		exists.setBrModelaOdobrenja(nzp.getBrModelaOdobrenja());
        		exists.setHitno(nzp.isHitno());
		        
                logger.warn("Korisnik "+ u.getId() + " uspesno je izvrsio metodu " + SifrarnikMetoda.methods.get(
		 				Thread.currentThread().getStackTrace()[1].getMethodName()));
			 	return new ResponseEntity<NalogZaPlacanje>(exists, HttpStatus.OK);
		    }
		
		@PermissionType("NaloziZaPlacanje:delete")
		@RequestMapping(value = "/nalozi/{id}", method = RequestMethod.DELETE)
		    public ResponseEntity<?> deleteNalogZaPlacanje(HttpSession session, @PathVariable("id") long id) {
				UserData u = (UserData)session.getAttribute("user");
			NalogZaPlacanje exists = npsr.findOne(id);
			 if(exists == null){
				logger.error("Greska pri izvrsenju " + SifrarnikMetoda.methods.get(Thread.currentThread().getStackTrace()[1].getMethodName())
		        	+ " za korisnika " + u.getId());
				 return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
			 }
			 npsr.delete(id);
			 logger.warn("Korisnik "+ u.getId() + " uspesno je izvrsio metodu " + SifrarnikMetoda.methods.get(
		 				Thread.currentThread().getStackTrace()[1].getMethodName()));
		     return new ResponseEntity<NalogZaPlacanje>(HttpStatus.NO_CONTENT);
		    }
		
		@PermissionType("NaloziZaPlacanje:search")
		@RequestMapping(value = "/nalozi/search/", method = RequestMethod.POST)
		    public ResponseEntity<?> searchNalogZaPlacanje(HttpSession session, @RequestBody NalogZaPlacanje nzp) {
			UserData u = (UserData)session.getAttribute("user");
			int pb = u.getPreduzece().getPib();
			DEK kk = dks.findByPreduzece(u.getPreduzece());
			kk.setValue(AES.decryptDEK(kk.getValue(), Integer.toString(pb)));
			List<NalogZaPlacanje> nalozi = null;
			PredlogPlacanja p = pps.findOne(nzp.getZaPredlog().getId());
			if(p != null)
				nalozi = npsr.findByUplatilacContainingAndSvrhaUplateContainingAndPrimalacContainingAndSifraPlacanjaContainingAndValutaContainingAndIznosAndBrModelaAndPozivNaBrojContainingAndZaPredlog(
						nzp.getUplatilac(), nzp.getSvrhaUplate(), nzp.getPrimalac(), nzp.getSifraPlacanja(), nzp.getValuta(),
						nzp.getIznos(), nzp.getBrModela(), nzp.getPozivNaBroj(), p);
			else
			 nalozi = npsr.findByUplatilacContainingAndSvrhaUplateContainingAndPrimalacContainingAndSifraPlacanjaContainingAndValutaContainingAndIznosAndBrModelaAndPozivNaBrojContaining(
					 nzp.getUplatilac(), nzp.getSvrhaUplate(), nzp.getPrimalac(), nzp.getSifraPlacanja(), nzp.getValuta(), nzp.getIznos(), 
					 nzp.getBrModela(), nzp.getPozivNaBroj());
			if(nalozi == null)
				 return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
			for(NalogZaPlacanje n : nalozi){
				n.getRacunPrimaoca().setBrRacuna(AES.decryptCBC(n.getRacunPrimaoca().getBrRacuna(), kk.getValue(), kk.getIv()));
				n.getRacunUplatioca().setBrRacuna(AES.decryptCBC(n.getRacunUplatioca().getBrRacuna(), kk.getValue(), kk.getIv()));
			}
		     return new ResponseEntity<List<NalogZaPlacanje>>(nalozi, HttpStatus.OK);
		    }
		
		@PermissionType("NaloziZaPlacanje:add")
		@RequestMapping(value = "/nalozi/send/", method = RequestMethod.POST)
		public ResponseEntity<Void> sendNalogZaPlacanje(HttpSession session, @RequestBody Long id, UriComponentsBuilder ucBuilder) throws DatatypeConfigurationException {
				UserData u = (UserData)session.getAttribute("user");
				int pb = u.getPreduzece().getPib();
				DEK kk = dks.findByPreduzece(u.getPreduzece());
				kk.setValue(AES.decryptDEK(kk.getValue(), Integer.toString(pb)));
		        NalogZaPlacanje nalog = npsr.findOne(id);
		        
		        System.out.println("Saljes ovo: " + nalog.getPrimalac() + " " + nalog.getIznos() + " " + nalog.getPozivNaBroj());
		        
		        try {
					//kreiranje web servisa
					URL wsdlLocation = new URL("http://localhost:8081/banka-ws/services/IzvodWS?wsdl");
					QName serviceName = new QName("http://izvod", "IzvodWSService");
					QName portName = new QName("http://izvod", "IzvodWSPort");

					Service service = Service.create(wsdlLocation, serviceName);
					
					IzvodWS izvod = service.getPort(portName, IzvodWS.class);
					
					//	salje se kod banke sa kojom se razgovara:
					nalog.getRacunPrimaoca().setBrRacuna(AES.decryptCBC(nalog.getRacunPrimaoca().getBrRacuna(), kk.getValue(), kk.getIv()));
					nalog.getRacunUplatioca().setBrRacuna(AES.decryptCBC(nalog.getRacunUplatioca().getBrRacuna(), kk.getValue(), kk.getIv()));
					izvod.setBankCode(nalog.getRacunUplatioca().getBrRacuna().substring(0, 3));
					System.out.println("Kod Banke je : " + nalog.getRacunUplatioca().getBrRacuna().substring(0, 3));
					// slanje naloga za prenos banci:
					NalogBanka nb = new NalogBanka();
					GregorianCalendar c = new GregorianCalendar();
					c.setTime(nalog.getZaPredlog().getDatum());
					XMLGregorianCalendar date2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);

					nb.setDatumNaloga(date2);
					nb.setDatumValute(date2);
					nb.setHitno(nalog.isHitno());
					SecureRandom rngesus = new SecureRandom();
					nb.setIdPoruke(rngesus.toString());
					nb.setSvrhaUplate(nalog.getSvrhaUplate());
					nb.setIznos(nalog.getIznos());
					nb.setModelOdobrenja(nalog.getBrModelaOdobrenja());
					nb.setModelZaduzenja(nalog.getBrModela());
					nb.setOznakaValute(nalog.getValuta());
					nb.setPozivNaBrojOdobrenja(nalog.getPozivNaBrojOdobrenja());
					nb.setPozivNaBrojZaduzenja(nalog.getPozivNaBroj());
					nb.setPrimalac(nalog.getPrimalac());
					nb.setRacunPrimaoca(nalog.getRacunPrimaoca().getBrRacuna());
					nb.setUplatilac(nalog.getUplatilac());
					nb.setRacunUplatioca(nalog.getRacunUplatioca().getBrRacuna());
					
					izvod.sendNalog(nb);
					logger.warn("Korisnik "+ u.getId() + " uspesno je izvrsio metodu " + SifrarnikMetoda.methods.get(
			 				Thread.currentThread().getStackTrace()[1].getMethodName()));
			    
				} catch (MalformedURLException e) {
					e.printStackTrace();
					logger.error("Greska pri izvrsenju " + SifrarnikMetoda.methods.get(Thread.currentThread().getStackTrace()[1].getMethodName())
		        	+ " za korisnika " + u.getId());
					return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
				}
		        
		    	return new ResponseEntity<Void>(HttpStatus.OK);
		 }

}
