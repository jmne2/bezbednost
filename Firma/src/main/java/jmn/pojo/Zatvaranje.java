package jmn.pojo;

public class Zatvaranje {

	private double iznos;
	private Long idStavke;
	private Long idFakture;
	
	public Zatvaranje(){
	}

	public Zatvaranje(double iznos, Long idStavke, Long idFakture) {
		super();
		this.iznos = iznos;
		this.idStavke = idStavke;
		this.idFakture = idFakture;
	}

	public double getIznos() {
		return iznos;
	}

	public void setIznos(double iznos) {
		this.iznos = iznos;
	}

	public Long getIdStavke() {
		return idStavke;
	}

	public void setIdStavke(Long idStavke) {
		this.idStavke = idStavke;
	}

	public Long getIdFakture() {
		return idFakture;
	}

	public void setIdFakture(Long idFakture) {
		this.idFakture = idFakture;
	}
	
}
