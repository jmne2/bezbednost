'use strict';
 
var app = angular.module('repo3App',['ui.bootstrap', 'ngMaterial']);

app.run(['$http', function($http) {
    //initialize get if not there
    if (!$http.defaults.headers.get) {
        $http.defaults.headers.get = {};    
    }    

    // Answer edited to include suggestions from comments
    // because previous version of code introduced browser-related errors

    //disable IE ajax request caching
    //$httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    // extra
    $http.defaults.headers.get['Cache-Control'] = 'no-cache';
    $http.defaults.headers.get['Pragma'] = 'no-cache';
}]);

app.run(function($rootScope) {
    /*
        Receive emitted message and broadcast it.
        Event names must be distinct or browser will blow up!
    */
    $rootScope.$on('changeCountryDisp', function(event, args) {
        $rootScope.$broadcast('hidingYoCountries', args);
    });
    $rootScope.$on('changeTableState', function(event, args) {
        $rootScope.$broadcast('changeState', args);
    });
    $rootScope.$on('resetTable', function(event, args) {
        $rootScope.$broadcast('resetEvent', args);
    });
    $rootScope.$on('removeSel', function(event, args) {
        $rootScope.$broadcast('deleteSel', args);
    });
    
    $rootScope.$on('selRow', function(event, args) {
        $rootScope.$broadcast('changeSelection', args);
    });
    
    $rootScope.$on('logged', function(event, args) {
        $rootScope.$broadcast('dispAfterLog', args);
    });
    $rootScope.$on('dontShowYourFace', function(event, args) {
        $rootScope.$broadcast('hide', args);
    });
});

app.factory('IndexService', ['$http', '$q', 'urls', function($http, $q, urls){
	  
    var factory = {
       submitCred: submitCred,
       checkUser: checkUser,
       resetPass: resetPass,
       logoutUser: logoutUser
    };
 
    return factory;
    
    function submitCred(us){
    	var deferred = $q.defer();
        $http.post(urls.BASE+'/login/', us)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
    function checkUser(){
    	var deferred = $q.defer();
        $http.get(urls.CHK_USR)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
    function logoutUser(){
    	var deferred = $q.defer();
        $http.post(urls.BASE+'logout/')
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    function resetPass(us){
    	var deferred = $q.defer();
        $http.post(urls.BASE+'/change/', us)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
}]);



app.controller('AppCtrl', AppCtrl);
//app.controller('DrzavaCtrl', DrzavaCtrl);

function AppCtrl($scope, $injector, IndexService){
	
	$scope.us = {};
	$scope.options = [];
	$scope.mustLogin = true;
	$scope.showChange = false;
	//nizovi su za onemogucenje dugmica i opcije za linkovanje
	$scope.dozvl = ["search","view","edit","add","delete","link"];
	$scope.disabled = [true, true,true,true,true,true];
	$scope.regular = ["startOptions", "changePass", "loginBlock"];
	$scope.links = [];
	$scope.parent = null;
	
	checkUser();
	
	$scope.goTo = function(id){
		$scope.templateURL = id;	
	}
	
	//oh God this thing - iliti funkcija koja se okida kada korisnik izabere koju tabelu hoce da vidi/obradi/etc.
	//Bitna stvar prosledjeni id mora se poklapati sa nazivom tabele da bi ovo radilo
	$scope.setBtns = function(){
		var id = $scope.templateURL;
		if($scope.regular.indexOf(id) == -1){
			var contr = null;
			var elem = document.getElementById(id); //znaci da divovi sto drze tabele/forme moraju da imaju id-eve iste kao prvi deo permisije
			var angElem = angular.element(elem);
			contr = angElem.controller();
			$scope.disabled = [true, true,true,true,true,true];
			//ovaj forEach u forEach je da se vidi koje sve permisije ima korisnik, da mi omogucila adekvatne dugmice
			angular.forEach($scope.us.roles, function(value, index){
				var p = $scope.us.roles[index].permissions;
				angular.forEach(p, function(val2, ind2){ 
					var pIme = p[ind2].naziv.split(':');
					if(id === pIme[0]){ //proveravaj permisije samo za izabranu tabelu
						var ind = $scope.dozvl.indexOf(pIme[1]); //cisto vrsta optimizacije pronalaska elementa u nizu za dugmice
						$scope.disabled[ind] = false;
					} else if(contr.children.indexOf(pIme[0]) != -1){ //vidi da li ima permisiju za "decu" tabele
						var ind = $scope.dozvl.indexOf("link");
						$scope.disabled[ind] = false;
					}
				});
				//ovo je ako nije dozvoljeno editovanje, odmah zahtevam izmenu stanja jer je defaultno edit
				if($scope.disabled[2]){
					var enablovano = $scope.disabled.indexOf(true);
					$scope.$emit('changeTableState', {ctrl: contr, state: $scope.dozvl[enablovano]});
				}
			});
			$scope.links = contr.children; //naputi opcije za link izabrane tabele sa njenom decom (sto su opet id-evi)
			$scope.$emit("logged", {info: $scope.us, ctrl:contr, parent: $scope.parent});
			$scope.parent = null;
		}
	} 
	
	$scope.submitLog = function(){
		var us = $scope.us;
		IndexService.submitCred(us)
        .then(
    		function(d) {
    			if(d != ""){
    				$scope.us = d;
    				//$scope.mustLogin = false;
    				
    				angular.forEach(d.roles, function(value, index){
    					var p = d.roles[index].permissions;
    					//da napunim listu mogucih tabela za korisnika
    					angular.forEach(p, function(val2, ind2){
    						var pIme = p[ind2].naziv.split(':');
    						if($scope.options.indexOf(pIme[0]) == -1){
    							$scope.options.push(pIme[0]);
    						}
    					});
    				});
    				$scope.templateURL = $scope.regular[0];
    				$scope.mustLogin = false;
    			} else {
    				$scope.mustLogin = true;
    				$scope.templateURL = $scope.regular[2];
    			}
            },
        function(errResponse){
            console.error('Error while logging user');
            $scope.logForm.$setPristine();
        }
        );
	}
	
	$scope.logOut = function(){
		IndexService.logoutUser()
        .then(
    		function(d) {
    			$scope.us = {};
    			$scope.options = [];
    			$scope.mustLogin = true;
    			$scope.templateURL = $scope.regular[2];
            },
        function(errResponse){
            console.error('Error while creating User');
        }
        );
	}

	$scope.showChangeP = function(){
		$scope.templateURL = $scope.regular[1];
		$scope.mustLogin = true;
		//$scope.$emit("dontShowYourFace", {});
	}
	
	$scope.changeP = function(){
		var us = {};
		us.uname = "nekiStringWooohoooo";
		us.pname = $scope.us.pname;
		IndexService.resetPass(us)
        .then(
    		function(d) {
    			if(d != ""){
    				$scope.us = d;
    				angular.forEach(d.roles, function(value, index){
    					var p = d.roles[index].permissions;
    					//da napunim listu mogucih tabela za korisnika
    					angular.forEach(p, function(val2, ind2){
    						var pIme = p[ind2].naziv.split(':');
    						if($scope.options.indexOf(pIme[0]) == -1){
    							$scope.options.push(pIme[0]);
    						}
    					});
    				});
    				$scope.templateURL = $scope.regular[0];
    				$scope.mustLogin = false;
    				$scope.disabled = [true, true,true,true,true,true];
    			}
            },
        function(errResponse){
            console.error('Error while logging user');
        }
        );
	}
	
	$scope.backToStart = function(){
		$scope.$emit("dontShowYourFace", {info: $scope.us});
		$scope.showStart = true;
		$scope.disabled = [true, true,true,true,true,true];
		$scope.templateURL = $scope.regular[0];
		$scope.parent = null;
	}
	function checkUser(){
		IndexService.checkUser()
        .then(
    		function(d) {
    			if(d === ""){
    				$scope.templateURL = $scope.regular[2];
    				$scope.mustLogin = true;
    				//salji event za hidovanje SVEGA!
    			} else {
    				$scope.us = d;
    				$scope.mustLogin = false;
    				angular.forEach(d.roles, function(value, index){
    					var p = d.roles[index].permissions;
    					angular.forEach(p, function(val2, ind2){
    						var pIme = p[ind2].naziv.split(':');
    						if($scope.options.indexOf(pIme[0]) == -1){
    							$scope.options.push(pIme[0]);
    						}
    					});
    				});
    				$scope.templateURL = $scope.regular[0];
    			}
            },
        function(errResponse){
            console.error('Error while looking for user');
        }
        );
	}
	
	$scope.addClick = function(){
		var contr = getControllerOfActive();
		$scope.$emit('changeTableState', {ctrl: contr, state: "add"});
	}
	
	$scope.searchClick = function(){
		var contr = getControllerOfActive();
		$scope.$emit('changeTableState', {ctrl: contr, state: "search"});
		var el = document.getElementById("subId");
		el.setAttribute("ng-disabled", "false");
		el = angular.element(el);
		compile(el);
	}
	
	$scope.removeClick = function(){
		var contr = getControllerOfActive();
		$scope.$emit('removeSel', {ctrl: contr});
	}
	
	$scope.refreshClick = function(){
		var contr = getControllerOfActive();
		$scope.$emit('resetTable', {ctrl: contr});
	}
	$scope.first = function(){
		var contr = getControllerOfActive();
		$scope.$emit('selRow', {ctrl: contr, direction: "first"});
	}
	$scope.prev = function(){
		var contr = getControllerOfActive();
		$scope.$emit('selRow', {ctrl: contr, direction: "prev"});
	}
	$scope.next = function(){
		var contr = getControllerOfActive();
		$scope.$emit('selRow', {ctrl: contr, direction: "next"});
	}
	$scope.last = function(){
		var contr = getControllerOfActive();
		$scope.$emit('selRow', {ctrl: contr, direction: "last"});
	}
	$scope.linkClick = function (id){
		
		$scope.parent = getControllerOfActive();
		$scope.templateURL = id;
		/*var elem = document.getElementById(id)
		var angElem = angular.element(elem);
		var contr = angElem.controller();
		var parentContr = getControllerOfActive();
		$scope.disabled = [true, true,true,true,true,true];
		angular.forEach($scope.us.roles, function(value, index){
			var p = $scope.us.roles[index].permissions;
			angular.forEach(p, function(val2, ind2){
				var pIme = p[ind2].naziv.split(':');
				if(id === pIme[0]){
					var ind = $scope.dozvl.indexOf(pIme[1]);
					$scope.disabled[ind] = false;
				}else if(contr.children.indexOf(pIme[0]) != -1){
					var ind = $scope.dozvl.indexOf("link");
					$scope.disabled[ind] = false;
				}
			});
			if($scope.disabled[2]){
				$scope.$emit('changeTableState', {ctrl: contr, state: "add"});
			}
		});
		$scope.$emit("logged", {info: $scope.us, ctrl:contr, parent: parentContr});
		$scope.links = contr.children;
		
		var el = document.getElementById("drzId");
		el.setAttribute("ng-show", "false");
		el = angular.element(el);
		//el.attr("ng-show", "false");
		//compile(el);
		el = document.getElementById("nasMestaId");
		el.setAttribute("ng-show", "true");
		el = angular.element(el);
		//el.attr("ng-show", "true");
		//compile(el);*/
	}
	
	function getControllerOfActive(){
		var div = document.getElementsByClassName("generic-container");
		var angElem = angular.element(div);
		var contr = angElem.controller();
		return contr;
	}
	
	

	function compile(element){
		  var el2 = angular.element(element);    
		  $scope = el2.scope();
		    $injector = el2.injector();
		    $injector.invoke(function($compile){
		       $compile(el2)($scope)
		    })     
		}
};
app.directive('stringToNumber', function() {
	  return {
  	    require: 'ngModel',
  	    link: function(scope, element, attrs, ngModel) {
  	      ngModel.$parsers.push(function(value) {
  	        return '' + value;
  	      });
  	      ngModel.$formatters.push(function(value) {
  	        return parseFloat(value);
  	      });
  	    }
}});
app.constant('urls', {
    BASE: 'http://localhost:8080/',
    CHK_USR: 'http://localhost:8080/check/',
    DRZAVA_SERVICE : 'http://localhost:8080/drzave/',
    MESTO_SERVICE : 'http://localhost:8080/mesta/',
    SIFRARNIK_DELATNOSTI_SERVICE : 'http://localhost:8080/delatnosti/',
    RACUN_SERVICE: 'http://localhost:8080/racuni/',
    RACUNPARTN_SERVICE: 'http://localhost:8080/racuniPartnera/',
    PARTNER_SERVICE: 'http://localhost:8080/partneri/',
    PREDUZECE_SERVICE: 'http://localhost:8080/preduzeca/',
    GODINE_SERVICE: 'http://localhost:8080/godine/',
    PREDLOG_SERVICE: 'http://localhost:8080/predlozi/',
    NALOG_SERVICE: 'http://localhost:8080/nalozi/',
    FAKTURE_SERVICE: 'http://localhost:8080/fakture/',
    ADRESE_SERVICE: 'http://localhost:8080/adrese/',
    BANKE_SERVICE: 'http://localhost:8080/banke/',
    STANJE_SERVICE: 'http://localhost:8080/stanja/',
    STIZVODA_SERVICE: 'http://localhost:8080/stavkeIzvoda/',
    STAVKE_FAKTURE_SERVICE: 'http://localhost:8080/stavkeFakture/',
    ZATVARANJE_UF_SERVICE: 'http://localhost:8080/zatvaranjeUF/',
    ZATVARANJE_IF_SERVICE: 'http://localhost:8080/zatvaranjeIF/',
    PLACA_SERVICE: 'http://localhost:8080/staSePlaca/',

});