package jmn.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jmn.models.PoslovnaGodina;
import jmn.models.Preduzece;
import jmn.repositories.PoslovnaGodinaRepository;

@Service
public class PoslovnaGodinaServiceImpl implements PoslovnaGodinaService{

	@Autowired
	private PoslovnaGodinaRepository pgr;
	
	@Override
	public List<PoslovnaGodina> findAll() {
		return (List<PoslovnaGodina>)pgr.findAll();
	}

	@Override
	public PoslovnaGodina savePoslovnaGodina(PoslovnaGodina pg) {
		return pgr.save(pg);
	}

	@Override
	public PoslovnaGodina findOne(Long id) {
		return pgr.findOne(id);
	}

	@Override
	public void delete(Long id) {
		pgr.delete(id);
	}

	@Override
	public void delete(PoslovnaGodina pg) {
		pgr.delete(pg);
	}

	@Override
	public List<PoslovnaGodina> findByGodinaAndPreduzece(int godina, Preduzece preduzece) {
		return pgr.findByGodinaAndPreduzece(godina, preduzece);
	}

	@Override
	public List<PoslovnaGodina> findByPreduzece(Preduzece preduzece) {
		return pgr.findByPreduzece(preduzece);
	}

}
