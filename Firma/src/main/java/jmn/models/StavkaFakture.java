package jmn.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class StavkaFakture {

	@Id
	@GeneratedValue
	private Long id;
	
	@Column (nullable=false)
	private Long redniBroj;
	
	@Column (nullable=false)
	private String nazivRobeUsluge;
	
	@Column (nullable=false)
	private double kolicina;
	
	@Column (nullable=false)
	private String jedinicaMere;
	
	@Column (nullable=false)
	private double jedinicnaCena;
	
	@Column (nullable=false)
	private double vrednost;
	
	@Column (nullable=true)
	private double procenatRabata;
	
	@Column (nullable=true)
	private double iznosRabata;
	
	@Column (nullable=true)
	private double umanjenoZaRabat;
	
	@Column (nullable=false)
	private double ukupanPorez;
	
	@ManyToOne()
	@JoinColumn(name="stavkaFakture")
	private Faktura stavkaFakture;

	public StavkaFakture() {
	}

	public StavkaFakture(Long redniBroj, String nazivRobeUsluge, double kolicina, String jedinicaMere,
			double jedinicnaCena, double vrednost, double procenatRabata, double iznosRabata, double umanjenoZaRabat,
			double ukupanPorez, Faktura stavkaFakture) {
		super();
		this.redniBroj = redniBroj;
		this.nazivRobeUsluge = nazivRobeUsluge;
		this.kolicina = kolicina;
		this.jedinicaMere = jedinicaMere;
		this.jedinicnaCena = jedinicnaCena;
		this.vrednost = vrednost;
		this.procenatRabata = procenatRabata;
		this.iznosRabata = iznosRabata;
		this.umanjenoZaRabat = umanjenoZaRabat;
		this.ukupanPorez = ukupanPorez;
		this.stavkaFakture = stavkaFakture;
	}

	public Long getRedniBroj() {
		return redniBroj;
	}

	public void setRedniBroj(Long redniBroj) {
		this.redniBroj = redniBroj;
	}

	public String getNazivRobeUsluge() {
		return nazivRobeUsluge;
	}

	public void setNazivRobeUsluge(String nazivRobeUsluge) {
		this.nazivRobeUsluge = nazivRobeUsluge;
	}

	public double getKolicina() {
		return kolicina;
	}

	public void setKolicina(double kolicina) {
		this.kolicina = kolicina;
	}

	public String getJedinicaMere() {
		return jedinicaMere;
	}

	public void setJedinicaMere(String jedinicaMere) {
		this.jedinicaMere = jedinicaMere;
	}

	public double getJedinicnaCena() {
		return jedinicnaCena;
	}

	public void setJedinicnaCena(double jedinicnaCena) {
		this.jedinicnaCena = jedinicnaCena;
	}

	public double getVrednost() {
		return vrednost;
	}

	public void setVrednost(double vrednost) {
		this.vrednost = vrednost;
	}

	public double getProcenatRabata() {
		return procenatRabata;
	}

	public void setProcenatRabata(double procenatRabata) {
		this.procenatRabata = procenatRabata;
	}

	public double getIznosRabata() {
		return iznosRabata;
	}

	public void setIznosRabata(double iznosRabata) {
		this.iznosRabata = iznosRabata;
	}

	public double getUmanjenoZaRabat() {
		return umanjenoZaRabat;
	}

	public void setUmanjenoZaRabat(double umanjenoZaRabat) {
		this.umanjenoZaRabat = umanjenoZaRabat;
	}

	public double getUkupanPorez() {
		return ukupanPorez;
	}

	public void setUkupanPorez(double ukupanPorez) {
		this.ukupanPorez = ukupanPorez;
	}

	public Faktura getStavkaFakture() {
		return stavkaFakture;
	}

	public void setStavkaFakture(Faktura stavkaFakture) {
		this.stavkaFakture = stavkaFakture;
		if(stavkaFakture.getListaStavkiFakture()==null || !stavkaFakture.getListaStavkiFakture().contains(this)){
			stavkaFakture.addStavkuFakture(this);
		}
	}

	public Long getId() {
		return id;
	}
	
	
}
