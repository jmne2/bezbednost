package jmn.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import jmn.models.Faktura;
import jmn.models.PoslovnaGodina;
import jmn.models.PoslovniPartner;
import jmn.models.Preduzece;

public interface FakturaRepository extends CrudRepository<Faktura, Long>{

	public List<Faktura> findByBrojFaktureAndPoslovnaGodinaAndPoslovniPartner(Long brojFakture, PoslovnaGodina poslovnaGodina,
			PoslovniPartner poslovniPartner);
	public List<Faktura> findByBrojFaktureAndPoslovnaGodina(Long brojFakture, PoslovnaGodina poslovnaGodina);
	public List<Faktura> findByBrojFaktureAndPoslovniPartner(Long brojFakture, PoslovniPartner poslovniPartner);
	//za pronalazak ulaznih faktura nekog preduzeca
	public List<Faktura> findByVrstaFaktureAndPreduzece(String vrstaFakture, Preduzece preduzece);
	public List<Faktura> findByPreduzeceAndBrojFaktureAndVrstaFakture(Preduzece preduzece, String brojFakture, String vrstaFakture);
	public List<Faktura> findByPreduzeceAndVrstaFaktureAndPreostaliIznosGreaterThan(Preduzece preduzece,String vrstaFakture, double preostaliIznos);
	public List<Faktura> findByPreduzeceAndVrstaFaktureAndPoslovniPartnerAndPreostaliIznosGreaterThan(Preduzece preduzece,String vrstaFakture,PoslovniPartner poslovniPartner, double preostaliIznos);
}
