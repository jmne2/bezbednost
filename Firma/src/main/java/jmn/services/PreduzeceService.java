package jmn.services;

import java.util.List;

import jmn.models.Preduzece;

public interface PreduzeceService {

	List<Preduzece> findAll();

	Preduzece savePreduzece(Preduzece p);

	Preduzece findOne(Long id);

	void delete(Long id);

	void delete(Preduzece p);
	
	List<Preduzece> findByNazivAndPib(String naziv, int pib);
	
	Preduzece findByPib(int pib);
	
	Preduzece findByMaticniBroj(Long maticniBroj);
}
