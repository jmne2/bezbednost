package jmn.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import jmn.models.PoslovnaGodina;
import jmn.models.Preduzece;

public interface PoslovnaGodinaRepository extends CrudRepository<PoslovnaGodina, Long>{

	public List<PoslovnaGodina> findByGodinaAndPreduzece(int godina, Preduzece preduzece);
	
	public List<PoslovnaGodina> findByPreduzece(Preduzece preduzece);
}
