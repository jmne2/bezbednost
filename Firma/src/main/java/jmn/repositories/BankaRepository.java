package jmn.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import jmn.models.Banka;

public interface BankaRepository extends CrudRepository<Banka, Long>{

	public List<Banka> findByNazivContaining(String naziv);
}
