package certificateApp.service;

import java.util.List;

import certificateApp.models.Revoked;

public interface RevokedService {
	List<Revoked> findAll();
	Revoked saveRevoked(Revoked re);
	Revoked findOne(long id);
	Revoked findBySerialNumber(String serialNumber);
	void delete(Revoked re);
}
