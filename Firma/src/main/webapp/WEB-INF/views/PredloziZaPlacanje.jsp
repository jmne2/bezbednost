<div class="generic-container" ng-controller="PredlogController as ctrl" id="PredloziZaPlacanje">
       <div>
          <div class="panel panel-default">
                <!-- Default panel contents -->
              <div class="panel-heading"><span class="lead">Lista Predloga za placanje </span></div>
              <div class="tablecontainer">
                  <table class="table table-hover">
                      <thead>
                          <tr>
                              <th>ID.</th>
                              <th>Broj</th>
                              <th>Datum</th>
                              <th>Status</th>
                          </tr>
                      </thead>
                      <tbody>
                      <!-- ng-click="setSelected(d.id)" ng-class="{selected: d.id === idSelectedVote}" -->
                          <tr ng-repeat="p in ctrl.predlozi" ng-click="setSelected($index)" ng-class="{selected: $index === indSelectedVote}">
                              <td><span ng-bind="p.id"></span></td>
                              <td><span ng-bind="p.broj"></span></td>
                              <td><span ng-bind="p.datum.substr(8,2)"></span>.<span ng-bind="p.datum.substr(5,2)"></span>.<span ng-bind="p.datum.substr(0,4)"></span></td>
                              <td><span ng-bind="p.status"></span></td>
                          </tr>
                      </tbody>
                  </table>
              </div>
          </div>
          <div class="panel panel-default">
              <div class="formcontainer">
                  <form ng-submit="ctrl.submit()" name="myForm" class="form-horizontal">
                      <input type="hidden" ng-model="ctrl.predlog.id" />
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="broj">Broj</label>
                              <div class="col-md-7">
                                  <input type="number" ng-model="ctrl.predlog.broj" string-to-number id="broj" class="oznaka form-control input-sm" placeholder="Unesite broj predloga" ng-required="required" ng-minlength="{{ctrl.currState === 'search' ? 0 : 1}}" ng-maxlength="5"/>
                                  <div class="has-error" ng-show="myForm.$dirty">
                                      <span ng-show="myForm.broj.$error.required">This is a required field</span>
                                      <span ng-show="myForm.broj.$error.minlength">Minimum length required is 1</span>
                                      <span ng-show="myForm.broj.$error.maxlength">Maximum length required is 5</span>
                                      <span ng-show="myForm.broj.$invalid">This field is invalid </span>
                                  </div>
                              </div>
                          </div>
                      </div>
                                                
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="datum">Datum</label>
                              <div class="col-md-7">
                              <input type="text" class="form-control" ng-model="ctrl.predlog.datum | date: 'dd.MM.yyyy'" placeholder="Unesite datum" disabled ng-required="required"  />
                              <div uib-datepicker-popup="dd-MM-yyyy" ng-model="ctrl.predlog.datum" is-open="popup1.opened" close-text="Close" ></div>
					          <span class="input-group-btn">
					            <button type="button" class="btn btn-default" ng-click="open1()"><i class="glyphicon glyphicon-calendar"></i></button>
					          </span>
                              </div>
                          </div>
                      </div>
 					
 					<div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="status">Status</label>
                              <div class="col-md-7">
                                   <select ng-init="ctrl.predlog.status = ctrl.predlog.status || ctrl.status[0]" ng-model="ctrl.predlog.status" id="vrsta" placeholder="Unesite vrstu partnera" ng-required="required" ng-options="item for item in ctrl.status"></select>
                              </div>
                          </div>
                      </div>
                      
                      <div class="row">
                          <div class="form-actions floatRight">
                              <input type="submit" id ="subId" value="Commit" class="btn btn-primary btn-sm" ng-disabled="myForm.$invalid">
                              <button type="button" ng-click="ctrl.reset()" class="btn btn-warning btn-sm" ng-disabled="myForm.$pristine">Rollback</button>
                              <label>{{ctrl.currState}}</label>
                          </div>
                      </div>
                  </form>
              </div>
          </div>
 
      </div>
      </div>
