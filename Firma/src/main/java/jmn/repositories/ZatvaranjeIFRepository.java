package jmn.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import jmn.models.Faktura;
import jmn.models.StavkaIzvoda;
import jmn.models.ZatvaranjeIF;

public interface ZatvaranjeIFRepository extends CrudRepository<ZatvaranjeIF, Long>{
	
	public List<ZatvaranjeIF> findByIzlaznaFaktura(Faktura izlaznaFaktura);
	public List<ZatvaranjeIF> findByStavkaIzvoda(StavkaIzvoda stavkaIzvoda);
}
