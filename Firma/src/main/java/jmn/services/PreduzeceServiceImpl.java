package jmn.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jmn.models.Preduzece;
import jmn.repositories.PreduzeceRepository;

@Service
public class PreduzeceServiceImpl implements PreduzeceService{

	@Autowired
	private PreduzeceRepository pr;
	
	@Override
	public List<Preduzece> findAll() {
		return (List<Preduzece>)pr.findAll();
	}

	@Override
	public Preduzece savePreduzece(Preduzece p) {
		return pr.save(p);
	}

	@Override
	public Preduzece findOne(Long id) {
		return pr.findOne(id);
	}

	@Override
	public void delete(Long id) {
		pr.delete(id);
	}

	@Override
	public void delete(Preduzece p) {
		pr.delete(p);
	}

	@Override
	public List<Preduzece> findByNazivAndPib(String naziv, int pib) {
		return pr.findByNazivContainingAndPib(naziv, pib);
	}

	@Override
	public Preduzece findByPib(int pib) {
		// TODO Auto-generated method stub
		return pr.findByPib(pib);
	}

	@Override
	public Preduzece findByMaticniBroj(Long maticniBroj) {
		return pr.findByMaticniBroj(maticniBroj);
	}

}
