package certificateApp.generator;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringReader;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Calendar;
import java.util.Date;

import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1GeneralizedTime;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.DERBitString;
import org.bouncycastle.asn1.DERNull;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.ocsp.BasicOCSPResponse;
import org.bouncycastle.asn1.ocsp.CertID;
import org.bouncycastle.asn1.ocsp.CertStatus;
import org.bouncycastle.asn1.ocsp.OCSPRequest;
import org.bouncycastle.asn1.ocsp.OCSPResponse;
import org.bouncycastle.asn1.ocsp.OCSPResponseStatus;
import org.bouncycastle.asn1.ocsp.Request;
import org.bouncycastle.asn1.ocsp.ResponderID;
import org.bouncycastle.asn1.ocsp.ResponseBytes;
import org.bouncycastle.asn1.ocsp.ResponseData;
import org.bouncycastle.asn1.ocsp.RevokedInfo;
import org.bouncycastle.asn1.ocsp.SingleResponse;
import org.bouncycastle.asn1.ocsp.TBSRequest;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.x509.CRLReason;
import org.bouncycastle.asn1.x509.Extension;
import org.bouncycastle.asn1.x509.Extensions;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.openssl.PEMParser;
import org.springframework.beans.factory.annotation.Autowired;

import certificateApp.models.Revoked;
import certificateApp.models.SerialNumbers;
import certificateApp.models.User;
import certificateApp.service.RevokedService;
import certificateApp.service.SerialNumbersService;
import certificateApp.service.UserService;

public class OCSPResponder {
	
	@Autowired
	private SerialNumbersService snser;
	@Autowired
	private UserService us;
	@Autowired
	private RevokedService rs;
	
	private X509Certificate reqCert;
	private String reqCertSerNum;
	private CertID certId;
	private X509Certificate responderCert;
	private PrivateKey responderPrivateKey;
	
	public OCSPResponder() throws CertificateException{
		KeyStoreReader keyStoreReader = new KeyStoreReader();
		String passwordKs = "pass";
		String passwordPk = "pk";
		String alias = "nbs";
		responderCert= (X509Certificate) keyStoreReader.readCertificate("keyStores/keyStoreNBS", passwordKs, alias);
		responderPrivateKey = keyStoreReader.readPrivateKey("keyStores/keyStoreNBS", passwordKs, alias, passwordPk);

	}
	
	public void sendRequest(OCSPRequest request) throws CertificateException{
		TBSRequest tbsreq = request.getTbsRequest();
		Request req = (Request) tbsreq.getRequestList().getObjectAt(0);
		CertID certId = req.getReqCert();
		this.certId = certId;
		BigInteger serNum = certId.getSerialNumber().getPositiveValue();
		this.reqCertSerNum = serNum.toString(16);
		this.reqCert = findCert(serNum);
			
/*		
		byte[] b = reqCert.getExtensionValue("2.5.29.35");
		ASN1OctetString akiOc = ASN1OctetString.getInstance(b);
		AuthorityKeyIdentifier aki = AuthorityKeyIdentifier.getInstance(akiOc.getOctets());
		
		byte[] issuerPublicKey = aki.getKeyIdentifier();
		System.out.println("JAVNI KLJUC JE : " + issuerPublicKey);
		GeneralNames gn = aki.getAuthorityCertIssuer();
		System.out.println("GENERAL NAMES JE " + gn);
		BigInteger issuerSerNum = aki.getAuthorityCertSerialNumber();
		System.out.println("SERIJSKI BROJ " + issuerSerNum);
		this.parentCert = findCert(issuerSerNum);
	*/	
	}
	
	public OCSPResponse check() {
		
		OCSPResponseStatus status;
//OVOO SAAAM OBRISALALAAAAA ; CIJI CERTID IDDEEE I CIJI PODACII????
		
		//		CertID certid = new CertID(CertificateID.HASH_SHA1, new DEROctetString(reqCert.getIssuerDN().getName().getBytes()), new DEROctetString(parentCert.getPublicKey().getEncoded()), new ASN1Integer(parentCert.getSerialNumber()));
		
		Revoked revoked = rs.findBySerialNumber(reqCertSerNum);
//		RevokedInfo revokedInfo = new RevokedInfo(new ASN1GeneralizedTime(new Date()), null);
		CertStatus cerStatus = null;
		if(revoked == null){		// (0, DERNull), (revokedInfo), (1, revokedInfo), (2, UnknownInfo)
			cerStatus = new CertStatus(0, new DERNull()); 		
		}
		else{
			RevokedInfo revokedInfo = new RevokedInfo(new ASN1GeneralizedTime(revoked.getDate()), CRLReason.lookup(revoked.getReason()));
			cerStatus = new CertStatus(revokedInfo);
		}
		Date thisUpdate = new Date();
		Date nextUpdate = new Date(); 
		Calendar c = Calendar.getInstance();
		c.setTime(thisUpdate);
		c.add(Calendar.DATE, 365);  
		nextUpdate = c.getTime();
		
		SingleResponse singleResp = new SingleResponse(certId, cerStatus, new ASN1GeneralizedTime(thisUpdate), new ASN1GeneralizedTime(nextUpdate), new Extensions(new Extension[0]));
		
		ResponderID responderID = new ResponderID(new X500Name(responderCert.getSubjectDN().getName()));
		ASN1EncodableVector aev = new ASN1EncodableVector();
		aev.add(singleResp);
		DERSequence seq = new DERSequence(aev);
		ResponseData responseData = new ResponseData(responderID, new ASN1GeneralizedTime(new Date()), seq ,new Extensions(new Extension[0]));
		
		Signature signature;
		ASN1Encodable params = null;
		try {
			signature = Signature.getInstance("SHA1WithRSA", "BC");
			signature.initSign(responderPrivateKey);
			signature.update(responseData.getEncoded());
			params = new DEROctetString(reqCert.getEncoded());
			
			DERSequence seq2 = new DERSequence(params);
			
			BasicOCSPResponse basicResp = new BasicOCSPResponse(responseData, new AlgorithmIdentifier(PKCSObjectIdentifiers.sha256WithRSAEncryption), new DERBitString(signature.sign()), seq2);
			DEROctetString dos;
			try {
				dos = new DEROctetString(basicResp.getEncoded());
				status = new OCSPResponseStatus(0);		// successful 
				
				OCSPResponse response = new OCSPResponse(status, new ResponseBytes(new ASN1ObjectIdentifier("1.3.6.1.5.5.7.48.1.1"), dos ));
				return response;	
			} catch (IOException e) {
				status = new OCSPResponseStatus(2);	//internal error
				e.printStackTrace();
			}
			
		} catch (NoSuchAlgorithmException | NoSuchProviderException | InvalidKeyException | SignatureException | IOException | CertificateEncodingException e) {
			status = new OCSPResponseStatus(2);	//internal error
			e.printStackTrace();
		}
		 return null;	
	}
	
	public X509Certificate findCert(BigInteger serNum) throws CertificateException{
		String num = serNum.toString(16);
		
		SerialNumbers sn = snser.findBySerialNumber(num);
		String username = sn.getUsername();
		User u = us.findByUsername(username);
		X509Certificate cert = null;
		
			String role = u.getAllowed().iterator().next().getName();
			if(role.equals("admin")){
				cert = readCert(new File("cerNBS/"), serNum.toString());
				return cert;
			}
			else if(role.equals("banka")){
				cert = readCert(new File("cerBanks/"), serNum.toString());
				return cert;
			}
			else{
				File f = new File("cerCompanies/");
				File[] files = f.listFiles();
				for(File file : files){
					cert = readCert(file, serNum.toString());
					if(cert!=null){
						return cert;
					}
				}
			}
		return null;
	}
	
	public X509Certificate readCert(File f, String num) throws CertificateException{
		File[] files = f.listFiles();
		System.out.println("duzina fajla " + f.getName() + " je "+ files.length);
		for(File file : files){
			String certNum = file.getName().substring(11, file.getName().length()-4);
			System.out.println("POREDIM " + certNum + " i " + num);
			if (certNum.equals(num)){
				byte[] bFile = new byte[(int) file.length()];
				
				FileInputStream fis;
				X509Certificate cert;
				X509CertificateHolder certHolder;
				try {
					fis = new FileInputStream(file);
					fis.read(bFile);
				     fis.close();
				     String reqString = new String(bFile, "UTF-8"); 
				     
				     String temp = new String(bFile);
				  
				      StringReader sr = new StringReader(temp);
				      PEMParser pp = new PEMParser(sr);
				      Object parsedObj = pp.readObject();
				      System.out.println("PemParser returned: " + parsedObj);
				       if (parsedObj instanceof X509Certificate)
				       {
				          cert = (X509Certificate)parsedObj;
							if(pp!=null){
								pp.close();
							}
							return cert;
				       }
				       else if(parsedObj instanceof X509CertificateHolder){
				    	   certHolder = (X509CertificateHolder)parsedObj;
				    	   if(pp!=null){
								pp.close();
							}
				    	   JcaX509CertificateConverter certConverter = new JcaX509CertificateConverter();
							certConverter = certConverter.setProvider("BC");
							return certConverter.getCertificate(certHolder);

				       }
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				finally{
				}  
			}
		}
		
		 return null;  
	}

	public void setSnser(SerialNumbersService snser) {
		this.snser = snser;
	}

	public void setUs(UserService us) {
		this.us = us;
	}
	
	public void setRs(RevokedService rs) {
		this.rs = rs;
	}

	public X509Certificate getResponderCert() {
		return responderCert;
	}

	public void setResponderCert(X509Certificate responderCert) {
		this.responderCert = responderCert;
	}

	
	
	public PrivateKey getResponderPrivateKey() {
		return responderPrivateKey;
	}

	public void setResponderPrivateKey(PrivateKey responderPrivateKey) {
		this.responderPrivateKey = responderPrivateKey;
	}
}
