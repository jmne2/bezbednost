<div class="generic-container" id="NaseljenaMesta" ng-controller="NaseljenoMestoController as ctrl" >
          <script type="text/ng-template" id="myModalContent.html">
        	<div class="modal-header">
            	<h3>Izaberite Drzavu</h3>
        	</div>
        	<div class="modal-body">
            	<ul>
                	<li ng-repeat="item in items">
                    	<a ng-click="selected.item = $index">{{ item.naziv }}</a>
                	</li>
            	</ul>
            	Selected: <b>{{ selected.item }} </b>
        	</div>
        		<div class="modal-footer">
            	<button class="btn btn-primary" ng-click="ok()">OK</button>
            	<button class="btn btn-warning" ng-click="cancel()">Cancel</button>
        	</div>
    	</script>
         <div>
          <div class="panel panel-default">
                <!-- Default panel contents -->
              <div class="panel-heading"><span class="lead">Lista Mesta </span></div>
              <div class="tablecontainer">
                  <table class="table table-hover">
                      <thead>
                          <tr>
                              <th>ID</th>
                              <th>Naziv</th>
                              <th>Postanski Broj</th>
                              <th>Maticna Drzava</th>
                              <th width="20%"></th>
                          </tr>
                      </thead>
                      <tbody>
                      <!-- ng-click="setSelected(d.id)" ng-class="{selected: d.id === idSelectedVote}" -->
                          <tr ng-repeat="m in ctrl.mesta" ng-click="setSelected($index)" ng-class="{selected: $index === indSelectedVote}">
                              <td><span ng-bind="m.id"></span></td>
                              <td><span ng-bind="m.naziv"></span></td>
                              <td><span ng-bind="m.postBroj"></span></td>
                              <td><span ng-bind="m.drzava.naziv"></span></td>
                          </tr>
                      </tbody>
                  </table>
              </div>
          </div>
          <div class="panel panel-default">
              <div class="formcontainer">
                  <form ng-submit="ctrl.submit()" name="myForm" class="form-horizontal">
                      <input type="hidden" ng-model="ctrl.mesto.id" />
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="naziv">Naziv</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="ctrl.mesto.naziv" id="naziv" class="naziv form-control input-sm" placeholder="Unesite naziv naseljenog mesta" ng-required="required" ng-minlength="3"/>
                                  <div class="has-error" ng-show="myForm.$dirty">
                                      <span ng-show="myForm.naziv.$error.required">This is a required field</span>
                                      <span ng-show="myForm.naziv.$error.minlength">Minimum length required is 3</span>
                                      <span ng-show="myForm.naziv.$invalid">This field is invalid </span>
                                  </div>
                              </div>
                          </div>
                      </div>
                         
                       
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="postanski">Postanski broj</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="ctrl.mesto.postBroj" id="postanski" class="form-control input-sm" placeholder="Unesite Pstanski Broj" ng-required="required" ng-minlength="{{ctrl.currState === 'search' ? 0 : 5}}" ng-maxlength="5"/>
                                  <div class="has-error" ng-show="myForm.$dirty">
                                      <span ng-show="myForm.postanski.$error.required">This is a required field</span>
                                      <span ng-show="myForm.postanski.$error.minlength">Minimum length required is 5</span>
                                      <span ng-show="myForm.postanski.$invalid">This field is invalid </span>
                                  </div>
                              </div>
                          </div>
                      </div>
 					
 					<div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="maticna">Maticna Drzava</label>
                              <button type="button" ng-click="ctrl.showCountries()" class="btn btn-info btn-sm">Izaberi Drzavu</button>
                              <div class="col-md-7">
                              <label class="col-md-2 control-lable" id="maticna">{{ctrl.drzava.naziv}}</label>
                              </div>
                          </div>
                      </div>
 					
                      <div class="row">
                          <div class="form-actions floatRight">
                              <input type="submit"  id ="subId" value="Commit" class="btn btn-primary btn-sm" ng-disabled="myForm.$invalid">
                              <button type="button" ng-click="ctrl.reset()" class="btn btn-warning btn-sm" ng-disabled="myForm.$pristine">Rollback</button>
                               <label>{{ctrl.currState}}</label>
                          </div>
                      </div>
                  </form>
              </div>
          </div>
          </div>
      </div>