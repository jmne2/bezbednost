package jmn.services;

import java.util.List;

import jmn.models.DnevnoStanje;
import jmn.models.PoslovniPartner;
import jmn.models.StavkaIzvoda;

public interface StavkaIzvodaService {

	List<StavkaIzvoda> findAll();

	StavkaIzvoda saveStavkaIzvoda(StavkaIzvoda si);

	StavkaIzvoda findOne(Long id);

	void delete(Long id);

	void delete(StavkaIzvoda si);

	List<StavkaIzvoda> findByDnevnoStanje(DnevnoStanje dnevnoStanje);
	
	List<StavkaIzvoda> findByDnevnoStanjeAndSmerIgnoreCase(DnevnoStanje dnevnoStanje, String smer);
}
