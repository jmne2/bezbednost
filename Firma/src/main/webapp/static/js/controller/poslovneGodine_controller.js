'use strict';
 
angular.module('repo3App').controller('PoslovnaGodinaController', PGDNCtrl);

function PGDNCtrl($scope, PoslovnaGodinaService) {
    var self = this;
    self.godina={id:null, zakljucena:false};
    self.godine=[];
    self.show = false;
    self.children = ['UlazneFakture', 'IzlazneFakture'];
    self.logged = false;
    self.us = {};
    
    self.submit = submit;
    self.edit = edit;
    self.remove = remove;
    self.reset = reset;
    self.first = first;
    self.prev = prev;
    self.next = next;
    self.last = last;
    self.search = search;
    self.currState = "edit";
    self.indSelected = -1;
    $scope.indSelectedVote = -1;
    $scope.required = true;
    
    $scope.$on('changeState', function(event, args) {    	
    	if(self === args.ctrl){
    		self.currState = args.state;
    		$scope.required = (self.currState === "search") ? false : true;
    		self.drzava = {};
    	}
    });
    $scope.$on('resetEvent', function(event, args) {    	
    	if(self === args.ctrl){
    		self.currState = "edit";
    		self.godina.godina = 0;
    		self.godina.preduzece = self.us.preduzece;
    		fetchAllGodine();
    	}
    });
    //TODO: change or scrap!!!
    $scope.$on('hidingYoCountries', function(event, args) {
    	self.show = args.arg[0];
    	
    });
    $scope.$on('hide', function(event, args) {
    	self.show = false;
    	
    });
    $scope.$on('changeSelection', function(event, args) {    	
    	if(self === args.ctrl){
    		switch (args.direction) {
	            case 'prev':
	                prev();
	                break;
	            case 'next':
	                next();
	                break;
	            case 'first':
	            	first();
	            	break;
	            case 'last':
	            	last();
	            	break;
    		}
    	}
    });
    
    $scope.$on('deleteSel', function(event, args) {    	
    	if(self === args.ctrl){
    		if($scope.indSelectedVote != -1){
    			var selectedDrz = self.godine[$scope.indSelectedVote];
    			remove(selectedDrz.id);
    			self.currState = "edit";
    		}
    	}
    });
    $scope.$on('dispAfterLog', function(event, args) {
    	if(self === args.ctrl){
    		self.logged = true;
    		self.us = args.info;
    		self.show = true;
    		self.godina.godina = 0;
    		self.godina.preduzece = self.us.preduzece;
    		fetchAllGodine();
    		//fetchAllGodine();
    	} else {
    		self.logged = false;
    		self.show = false;
    	}
    });
    
    function first(){
    	$scope.indSelectedVote = 0;
    	self.indSelected = $scope.indSelectedVote;
    }
    function prev(){
    	if(($scope.indSelectedVote != 0) && ($scope.indSelectedVote != -1)){
    		$scope.indSelectedVote = $scope.indSelectedVote - 1;
    		self.indSelected = $scope.indSelectedVote;
    	}
    }
    
    function next(){
    	if(($scope.indSelectedVote != self.godine.length) && ($scope.indSelectedVote != -1)){
    		$scope.indSelectedVote = $scope.indSelectedVote + 1;
    		self.indSelected = $scope.indSelectedVote;
    	}
    }
    
    function last(){
    	$scope.indSelectedVote = self.godine.length - 1;
    	self.indSelected = $scope.indSelectedVote;
    }
    
    $scope.setSelected = function (indSelectedVote) {
    	$scope.indSelectedVote = indSelectedVote;
    	self.indSelected = indSelectedVote;
    	if(self.currState === "edit"){
    		var id = self.godine[indSelectedVote].id;
    		edit(id);
    	}
    };
    $scope.changeState = function(state){
    	if(self.currState != state){
    		self.currState = state;
    	}
    }
    
    function fetchAllGodine(){
        PoslovnaGodinaService.fetchAllGodine()
            .then(
            function(d) {
            	self.godine = d;
                $scope.indSelectedVote = -1;
                self.indSelected = -1;
                self.show = true;
                if (self.currState === "delete"){
                	self.currState = "edit";
                }
            },
            function(errResponse){
                console.error('Error while fetching Users');
            }
        );
    }
 
    function createGodinu(godina){
        PoslovnaGodinaService.createGodinu(godina)
            .then( function(){
            	self.godina = {};
            	self.godina.godina = 0;
            	reset();
            	fetchAllGodine();
            },
            function(errResponse){
                console.error('Error while creating User');
            }
        );
    }
 
    function updateGodinu(godina, id){
        PoslovnaGodinaService.updateGodinu(godina, id)
            .then(function(){
            	self.godina.godina = 0;
            	fetchAllGodine();
            },
            function(errResponse){
                console.error('Error while updating User');
            }
        );
    }
 
    function deleteGodinu(id){
        PoslovnaGodinaService.deleteGodinu(id)
            .then(function(){
            	self.godina.godina = 0;
            	fetchAllGodine();
            },
            function(errResponse){
                console.error('Error while deleting User');
            }
        );
    }
 
    function submit() {
    	
    	self.godina.preduzece = self.us.preduzece;
    	if(self.currState === "add"){
            createGodinu(self.godina);        	
        } else if(self.currState === "edit"){
            updateGodinu(self.godina, self.godina.id);
        } else if (self.currState === "search"){
        	search(self.godina);
        }
    }
 
    function edit(id){
        console.log('id to be edited', id);
        for(var i = 0; i < self.godine.length; i++){
            if(self.godine[i].id === id) {
                self.godina = angular.copy(self.godine[i]);
                break;
            }
        }
    }
 
    function remove(id){
        console.log('id to be deleted', id);
        if(self.godina.id === id) {//clean form if the user to be deleted is shown there.
            reset();
        }
        deleteGodinu(id);
    }
 
    function search(godina){
    	PoslovnaGodinaService.searchGodine(godina)
        .then(
        function(d) {
        	self.godine = d;
            $scope.indSelectedVote = -1;
        },
        function(errResponse){
            console.error('Error while fetching Users');
        }
        );
    }
    
    function reset(){
        self.godina.godina = 0;
        $scope.myForm.$setPristine(); //reset Form
    }
};