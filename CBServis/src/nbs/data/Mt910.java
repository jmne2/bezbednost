
package nbs.data;

import java.sql.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "mt910",  propOrder = {
    "idPoruke",
    "swiftBankeP",
    "racunBankeP",
    "idPorukeNaloga",
    "datumValute",
    "iznos",
    "sifraValute"
})
public class Mt910 implements java.io.Serializable{

    @XmlElement(required = true)
    protected String idPoruke;
    @XmlElement(required = true)
    protected String swiftBankeP;
    @XmlElement(required = true)
    protected String racunBankeP;
    @XmlElement(required = true)
    protected String idPorukeNaloga;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected Date datumValute;
    protected double iznos;
    @XmlElement(required = true)
    protected String sifraValute;

    public void setSifraValute(String value) {
        this.sifraValute = value;
    }

	public Mt910(String idPoruke, String swiftBankeP, String racunBankeP, String idPorukeNaloga,
			Date datumValute, double iznos, String sifraValute) {
		super();
		this.idPoruke = idPoruke;
		this.swiftBankeP = swiftBankeP;
		this.racunBankeP = racunBankeP;
		this.idPorukeNaloga = idPorukeNaloga;
		this.datumValute = datumValute;
		this.iznos = iznos;
		this.sifraValute = sifraValute;
	}

    public Mt910(){
    
    }

	public String getIdPoruke() {
		return idPoruke;
	}

	public void setIdPoruke(String idPoruke) {
		this.idPoruke = idPoruke;
	}

	public String getSwiftBankeP() {
		return swiftBankeP;
	}

	public void setSwiftBankeP(String swiftBankeP) {
		this.swiftBankeP = swiftBankeP;
	}

	public String getRacunBankeP() {
		return racunBankeP;
	}

	public void setRacunBankeP(String racunBankeP) {
		this.racunBankeP = racunBankeP;
	}

	public String getIdPorukeNaloga() {
		return idPorukeNaloga;
	}

	public void setIdPorukeNaloga(String idPorukeNaloga) {
		this.idPorukeNaloga = idPorukeNaloga;
	}

	public Date getDatumValute() {
		return datumValute;
	}

	public void setDatumValute(Date datumValute) {
		this.datumValute = datumValute;
	}

	public double getIznos() {
		return iznos;
	}

	public void setIznos(double iznos) {
		this.iznos = iznos;
	}

	public String getSifraValute() {
		return sifraValute;
	}
    
    
}
