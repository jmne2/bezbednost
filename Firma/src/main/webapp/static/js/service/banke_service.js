
'use strict';
 
angular.module('repo3App').factory('BankaService', ['$http', '$q', 'urls', function($http, $q, urls){
 
 
    var factory = {
        fetchAllBanke: fetchAllBanke,
        createBanku: createBanku,
        updateBanku:updateBanku,
        deleteBanku:deleteBanku,
        searchBanke: searchBanke
    };
 
    return factory;
 
    function fetchAllBanke() {
        var deferred = $q.defer();
        $http.get(urls.BANKE_SERVICE)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while fetching Users');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
    function createBanku(banka) {
        var deferred = $q.defer();
        $http.post(urls.BANKE_SERVICE, banka)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while creating User');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
 
    function updateBanku(banka, id) {
        var deferred = $q.defer();
        $http.post(urls.BANKE_SERVICE+id, banka)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
    function deleteBanku(id) {
        var deferred = $q.defer();
        $http.delete(urls.BANKE_SERVICE+id)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while deleting User');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
    function searchBanke(banka){
    	var deferred = $q.defer();
        $http.post(urls.BANKE_SERVICE+"search/", banka)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
}]);