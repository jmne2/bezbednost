'use strict';
 
angular.module('repo3App').factory('FaktureService', ['$http', '$q', 'urls', function($http, $q, urls){
 
 
    var factory = {
        fetchAllUlazneFakture: fetchAllUlazneFakture,
        fetchAllIzlazneFakture: fetchAllIzlazneFakture,
        searchUlaznaFaktura: searchUlaznaFaktura,
        searchIzlaznaFaktura : searchIzlaznaFaktura
    };
 
    return factory;
 
    function fetchAllUlazneFakture(preduzece) {
        var deferred = $q.defer();
        $http.get(urls.FAKTURE_SERVICE+"ulazne/")
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while fetching ulazne fakture');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
    function fetchAllIzlazneFakture() {
        var deferred = $q.defer();
        $http.get(urls.FAKTURE_SERVICE+"izlazne/")
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while fetching izlazne fakture');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 

    function searchUlaznaFaktura(ulaznaFaktura){
    	var deferred = $q.defer();
        $http.post(urls.FAKTURE_SERVICE+"search/", ulaznaFaktura)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
    function searchIzlaznaFaktura(izlaznaFaktura){
    	var deferred = $q.defer();
        $http.post(urls.FAKTURE_SERVICE+"search/", izlaznaFaktura)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
}]);