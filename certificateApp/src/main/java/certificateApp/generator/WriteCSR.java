package certificateApp.generator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;

import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.openssl.jcajce.JcaPEMWriter;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;
import org.bouncycastle.pkcs.jcajce.JcaPKCS10CertificationRequestBuilder;
import org.springframework.beans.factory.annotation.Autowired;

import certificateApp.models.SerialNumbers;
import certificateApp.service.SerialNumbersService;


public class WriteCSR {

	@Autowired
	private SerialNumbersService snser;
	
	public WriteCSR(){
	
	}
	
	public void write(KeyPair keypair, X500Name x500name, String bankName,String username, String passKS, String passPk, String alias, String serNum) throws NoSuchAlgorithmException, IOException, InvalidKeyException, SignatureException, OperatorCreationException{
		
		JcaPKCS10CertificationRequestBuilder p10Builder = new JcaPKCS10CertificationRequestBuilder(x500name,  keypair.getPublic());
		JcaContentSignerBuilder csBuilder = new JcaContentSignerBuilder("SHA256withRSA"); 
		ContentSigner signer = csBuilder.build(keypair.getPrivate());
		PKCS10CertificationRequest request = p10Builder.build(signer);
		String data = request.toString();
		System.out.println("DATA: " + data);
		
		bankName=bankName.substring(1, bankName.length()-1);
		File folder = new File("csr"+bankName);
		File[] listOfFiles = folder.listFiles();
		int num = 0;
		if(listOfFiles!=null){
			 num=listOfFiles.length;
		}
		num++;
		
		// NAPRAVITI FAJL AKO NE POSTOJI NA TOJ PUTANJI
		File f1 = new File("csr"+bankName);
		f1.mkdir();
		BigInteger bi = new BigInteger(serNum, 16);
		File f = new File("csr"+bankName+"/csr-"+username+"-"+alias+"-"+bi.toString() + ".csr");
		f.createNewFile();
		BufferedWriter w = new BufferedWriter(new FileWriter(f.getPath()));
		StringWriter sw = new StringWriter();
		JcaPEMWriter pemWriter = new JcaPEMWriter(sw); 
		pemWriter.writeObject(request);
		pemWriter.close();
		
		sw.close();
		w.write(sw.toString());
		w.flush();
		w.close();
		
		SerialNumbers sn = new SerialNumbers(bi.toString(), alias, username, null);
		snser.saveSerialNumbers(sn);
		System.out.println("saved serial number");
		File fff = new File("pks");
		fff.mkdir();
		File ff = new File("pks/"+username+"-"+alias+"-"+bi.toString());
		
		/*w = new BufferedWriter(new FileWriter(ff.getPath()));
		sw = new StringWriter();
		pemWriter = new JcaPEMWriter(sw);
		pemWriter.writeObject(keypair.getPrivate());
		pemWriter.close();
		
		sw.close();
		w.write(sw.toString());
		w.flush();
		w.close();*/
		FileOutputStream fos = new FileOutputStream(ff);
		fos.write(keypair.getPrivate().getEncoded());
		fos.close();
	}

	public SerialNumbersService getSnser() {
		return snser;
	}

	public void setSnser(SerialNumbersService snser) {
		this.snser = snser;
	}
	
	
}
