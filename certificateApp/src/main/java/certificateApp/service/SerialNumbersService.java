package certificateApp.service;

import java.util.List;

import certificateApp.models.SerialNumbers;

public interface SerialNumbersService {
	List<SerialNumbers> findAll();
	SerialNumbers saveSerialNumbers(SerialNumbers sn);
	SerialNumbers findOne(long id);
	SerialNumbers findBySerialNumber(String serialNumber);
	void delete(SerialNumbers sn);
	List<SerialNumbers> findByUsername(String username);
	SerialNumbers findByAlias(String alias);
	List<SerialNumbers> findByIssuerSerNum(String issuerSerNum);
}
