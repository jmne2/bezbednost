package jmn.services;

import java.util.List;

import jmn.models.Faktura;
import jmn.models.PoslovnaGodina;
import jmn.models.PoslovniPartner;
import jmn.models.Preduzece;

public interface FakturaService {

	List<Faktura> findAll();

	Faktura saveFaktura(Faktura izf);

	Faktura findOne(Long id);

	void delete(Long id);

	void delete(Faktura izf);

	List<Faktura> findByBrojFaktureAndPoslovnaGodinaAndPoslovniPartner(Long brojFakture,
			PoslovnaGodina poslovnaGodina, PoslovniPartner poslovniPartner);

	List<Faktura> findByBrojFaktureAndPoslovnaGodina(Long brojFakture, PoslovnaGodina poslovnaGodina);

	List<Faktura> findByBrojFaktureAndPoslovniPartner(Long brojFakture, PoslovniPartner poslovniPartner);

	List<Faktura> findByVrstaFaktureAndPreduzece(String vrstaFakture, Preduzece preduzece);
	
	List<Faktura> findByPreduzeceAndBrojFaktureAndVrstaFakture(Preduzece preduzece, String brojFakture, String vrstaFakture);

	List<Faktura> findByPreduzeceAndVrstaFaktureAndPreostaliIznosGreaterThan(Preduzece preduzece,String vrstaFakture, double preostaliIznos);
	
	List<Faktura> findByPreduzeceAndVrstaFaktureAndPoslovniPartnerAndPreostaliIznosGreaterThan(Preduzece preduzece,String vrstaFakture,PoslovniPartner poslovniPartner, double preostaliIznos);
}
