package jmn.services;

import java.util.List;

import jmn.models.PoslovnaGodina;
import jmn.models.Preduzece;

public interface PoslovnaGodinaService {

	List<PoslovnaGodina> findAll();

	PoslovnaGodina savePoslovnaGodina(PoslovnaGodina pg);

	PoslovnaGodina findOne(Long id);

	void delete(Long id);

	void delete(PoslovnaGodina pg);
	
	List<PoslovnaGodina> findByGodinaAndPreduzece(int godina, Preduzece preduzece);
	List<PoslovnaGodina> findByPreduzece(Preduzece preduzece);
}
