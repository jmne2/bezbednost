package jmn.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jmn.models.Faktura;
import jmn.models.PredlogPlacanja;
import jmn.models.StaSePlaca;
import jmn.repositories.StaSePlacaRepository;

@Service
public class StaSePlacaServiceImpl implements StaSePlacaService{

	@Autowired
	private StaSePlacaRepository ssr;
	
	@Override
	public List<StaSePlaca> findAll() {
		// TODO Auto-generated method stub
		return (List<StaSePlaca>)ssr.findAll();
	}

	@Override
	public StaSePlaca saveStaSePlaca(StaSePlaca sp) {
		// TODO Auto-generated method stub
		return ssr.save(sp);
	}

	@Override
	public StaSePlaca findOne(Long id) {
		// TODO Auto-generated method stub
		return ssr.findOne(id);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		ssr.delete(id);
	}

	@Override
	public void delete(StaSePlaca sp) {
		// TODO Auto-generated method stub
		ssr.delete(sp);
	}

	@Override
	public List<StaSePlaca> findByFaktura(Faktura ulaznaFaktura) {
		// TODO Auto-generated method stub
		return ssr.findByFaktura(ulaznaFaktura);
	}

	@Override
	public List<StaSePlaca> findByPredlogPlacanja(PredlogPlacanja predlogPlacanja) {
		return ssr.findByPredlogPlacanja(predlogPlacanja);
	}

}
