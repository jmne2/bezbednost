<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta charset="ISO-8859-1"     >
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<title>Bank</title>
<style>
p {

	width: 500px;
	max-width: 500px;
	height : auto;	
}
</style>
<script type="text/javascript">

	var user = "${user}";
	if(user === ""){
		window.location.replace("index.jsp");
	}
		function getFormData($form){
		    var unindexed_array = $form.serializeArray();
		    var indexed_array = {};
	
		    $.map(unindexed_array, function(n, i){
		        indexed_array[n['name']] = n['value'];
		    });
	
		    return indexed_array;
		}
		
		$(document).ready(function(){
			
			var errorElem = "<div style=\"color:red\" id=\"errorElem\"> This field is required.</div>";
			
			$(".form-control").on('blur', function(){
				if($(this).val().length === 0){
					$(this).after(errorElem);
				} else {
					$(this).next().remove("#errorElem");
				}
			});
			
			
			$("#showCertDiv").hide();
			$("#findCertDiv").hide();
			$("#showCSRdiv").hide();
			$("#withdrawCertDiv").hide();
			$("#findwithdrawCertDiv").hide();
			$("#showCSRInfo").hide();
			$("#showCsr").show();
			$("#showCer").hide();
		});
	
		function find(){
			$("#findCertDiv").show();
			$("#showCSRdiv").hide();
			$("#withdrawCertDiv").hide();
			$("#findwithdrawCertDiv").hide();
			$("#buttonDownload").hide();
		}
		function withdraw(){
			$("#findCertDiv").hide();
			$("#showCSRdiv").hide();
			$("#withdrawCertDiv").show();
			$("#findwithdrawCertDiv").hide();
		}
		function check(){
			$("#findCertDiv").hide();
			$("#showCSRdiv").hide();
			$("#withdrawCertDiv").hide();
			$("#findwithdrawCertDiv").show();
		}

		function logOut(){
			$.ajax({
	             type: "POST",
	             url: "/logOut/",
	 			contentType:"application/json",
	             complete: function(datas){
	            	 if(datas.status === 200){
	            	 	window.location.replace("index.jsp");
	            	 } else {
		            	 var loc = datas.responseJSON.message;
		            	 var loc2 = loc.substring(loc.lastIndexOf('/') + 1);
		            	 window.location = loc2;
	            	 }
		         }
			});
		}
</script>
</head>
<body>
	<br/>
	<nav class="navbar navbar-inverse">
	<div class="collapse navbar-collapse">
		<ul class="nav navbar-nav">
		<li><button id="findId" class="btn btn-default" onclick="find()" role="presentation">Find certificate</button>
		</li>
		<li><button id="withdrawId" class="btn btn-default" onclick="withdraw()" role="presentation">Withdraw certificate</button>
		</li>
		<li><button id="checkId" class="btn btn-default" onclick="check()" role="presentation">Check certificate status</button>
		</li>
		<li><button id="showCsr" class="btn btn-default" onclick="showCsrList()" role="presentation">Show CSR files</button>
		</li>
		</ul>
		<ul class="nav navbar-nav navbar-right">
	        <li><a href="" onclick="logOut()"><span class=""></span> Log out</a></li>
	    </ul>
	</div>
	</nav>
	<br/><br/>
	<div  class="container-fluid" align="center" style=" display: table; margin: auto; width:40%; " >
		<div id="findCertDiv" class="container-fluid" hidden=true >
			<label>Key store name: </label>
			<input type="text" id="findKsNameId"></br>
			<label>Key store password: </label>
			<input type="text" id="findPasswordKsId"></br>
			<label>Certificate serial number: </label>
			<input type="number" id="serialNumId"></br>
			<button class="btn btn-lg btn-primary" onclick="findCert()">FIND</button>
			<p id="foundResult"></p>
			<button class="btn btn-lg btn-primary" id="buttonDownload" onclick="downloadCert()" hidden=true>DOWNLOAD CERTIFICATE</button>
		</div>
		<div id="showCertDiv" class="container-fluid" align="center" style=" display: table; margin: auto; width:40%; " hidden=true >
			<label>Version:</label>
			<p id=showCertDivVersion></p>
			<label>SerialNumber:</label>
			<p id=showCertDivSerialNumber></p>
			<label>IssuerDN:</label>
			<p id=showCertDivIssuerDN></p>
			<label>Start Date:</label>
			<p id=showCertDivStartDate></p>
			<label>Final Date:</label>
			<p id=showCertDivFinalDate></p>
			<label>SubjectDN:</label>
			<p id=showCertDivSubjectDN></p>
			<label>Public Key:</label>
			<p id=showCertDivPublicKey></p>
			<label>Extensions:</label>
			<label>Is CA: </label>
			<p id=showCertDivExtensions></p>
	</div>
	<div id="showCSRdiv" >
		<ul id="showCSRList" style="position: fixed; left: 0px;">
		</ul>
			
		<div style="position: relative; left: 100px;">
			<p></p>
			<p id="showCSRdivP"></p>
			<div id="showCSRInfo">
			<fieldset>
				<label>Choose parent certificate:</label>
				<select id="aliasSelect"> 
					<c:forEach items="${bankAliases}"  var="alias">
					<option>"${alias}"</option>
				</c:forEach>
				</select><br/>    
				<label> Parent keystore password:</label>
				<input id = "keyStorePassParent" type="text"></input><br/>
				<label> Parent privateKey password:</label>
				<input id="pkPassParent" type="text"></input><br/>
				<p id="showCSRdivP2"></p>
			</fieldset>
			</div>
		</div>	
	</div>
	
	<div id="withdrawCertDiv" class="container-fluid" >
			<label>Certificate serial number WITHDRAW: </label>
			<input type="text" id=serialNumIdWithdraw><br>
			<label>Choose reason for withdrawing: </label><br>
			<select id="selectReasonWithdrawId" >
				<option value="1">Key compromise</option>
				<option value="2">Privilege withdrawn</option>
				<option value="3">Affiliation changed</option>
				<option value="4">Unspecified</option>
			</select>
			<button class="btn btn-lg btn-primary" onclick="withdrawCert()">WITHDRAW</button>
		</div>

		<div id="findwithdrawCertDiv" class="container-fluid" >
			<label>Certificate serial number FIND WITHDRAW: </label>
			<input type="text" id=findWithdraw><br>
			<button class="btn btn-lg btn-primary" onclick="findwithdrawCert()">FIND WITHDRAW</button>
		</div>
	
	</div>
		
	
	
	<script type="text/javascript">
	
	function findCert(){
		var num = $("#serialNumId").val();
		var pass = $("#findPasswordKsId").val();
		var ksName = $("#findKsNameId").val();
		if(num.length != 0 && pass.length != 0){
			$.ajax({
	            type: "POST",
	            url: "findCert/",
	            data: {"ksName": ksName,
	            	"pass": pass,
	            	"serNum": num },
	            contentType : "application/json",
	            dataType: "json",
	            complete: function(response){
	           	 var strResponse=response.responseJSON;
	           	 var strResponseT = response.responseText;
	           	 	if(strResponseT==="WrongPassword"){
	           	 		alert("Wrong password");
	           	 	}
	           	 	else if(strResponseT==="NoExists"){
	           	 		alert("Certificate with that serial number doesn't exist");
	           	 	}
	           	 	else{
		           		 $("#showCertDiv").show();
		           		var version = strResponse.info.version; 
		           	 	var serialNumber = strResponse.info.serialNum;
		           	 	var issuerDN = strResponse.info.issuer;
		           	 	var startDate = strResponse.info.interval.notBefore;
		           	 	var finalDate = strResponse.info.interval.notAfter;
		           	 	var subjectDN = strResponse.info.subject;
		           	 	var publicKey = strResponse.info.pubKey;
		           	 	var isca = strResponse.info.extensions.map.BasicConstraints.ca;
		           	 	
		           		var p = JSON.stringify(version);
		           	 	var p1 = JSON.stringify(serialNumber);
		           	 	var p2 = JSON.stringify(issuerDN);
		           	 	var p3 = JSON.stringify(startDate);
		           	 	var p4 = JSON.stringify(finalDate);
		           	 	var p5 = JSON.stringify(subjectDN);
		           	 	var p6 = JSON.stringify(publicKey);
		           	 	var p7 = JSON.stringify(isca);
		           	 	           	 	
		           		$("#showCertDivVersion").text(p);
		           		$("#showCertDivSerialNumber").text(p1);
		           		$("#showCertDivIssuerDN").text(p2);
		           		$("#showCertDivStartDate").text(p3);
		           		$("#showCertDivFinalDate").text(p4);
		           		$("#showCertDivSubjectDN").text(p5);
		           		$("#showCertDivPublicKey").text(p6);
	           			$("#showCertDivExtensions").text(p7);
						$("#buttonDownload").show();
	           	 	}
		         }
	        });
		}
	}
	
	function showCsr(){
		$.ajax({
             type: "POST",
             url: "/csr/showCsr/",
 			contentType:"application/json",
             complete: function(datas){
            	 var loc = datas.responseJSON.message;
            	 var loc2 = loc.substring(loc.lastIndexOf('/') + 1);
            	 window.location = loc2;
	         }
		});
	}
	
	function showCsrList(){
		$.ajax({
             type: "POST",
             url: "/csr/showCsrList/",
 			contentType:"application/json",
 			dataType:"json",
             complete: function(datas){
            	 $("#certForm").hide();
      			$("#chooseCertForm").hide();
      			$("#findCertDiv").hide();
      			$("#showCSRdiv").hide();
      			$("#withdrawCertDiv").hide();
      			$("#findwithdrawCertDiv").hide();
      			$("#showCSRdiv").show();
            	 var strResponse=datas.responseJSON;
           	$.each(strResponse, function(index, res) 
						{	
            		$("#showCSRList").append("<li>"+res+"</li> <button class=\"btn btn-lg btn-primary\" onclick=\"showCsr('"+res+"')\" id=\""+res+"\" name=\""+res+"\">SHOW</button><br>");
						}); 
	         }
		});
	}
	
	
	function showCsr(kliknuto){
		$.ajax({
             type: "POST",
             url: "/csr/showCsr/",
             data: {"kliknuto": kliknuto},
 			contentType:"application/json",
             complete: function(datas){
            	var list = datas.responseText.split(",");
            	$.each(list, function(index, res) 
				{	
            		$("#showCSRdivP").append(res+"<br>");
				});
            	$("#showCSRInfo").show();
            	$("#showCSRdivP2").append("<button class=\"btn btn-lg btn-primary\" onclick=\"odobriCsr('"+kliknuto+"')\" id = \""+kliknuto+"\" name = \""+kliknuto+"\">APPROVE</button>");
	         }
		});
	}
	function odobriCsr(kliknuto){
		var parentAlias = $("#aliasSelect option:selected").text();
		var parentKsPass = $("#keyStorePassParent").val();
		var parentPkPass = $("#pkPassParent").val();
		if(parentAlias.length != 0 && parentKsPass.length != 0 && parentPkPass != 0){
			$.ajax({
	             type: "POST",
	             url: "/csr/approveCsr/",
	             data: {"id" : kliknuto,
	            	 "parentAlias": parentAlias,
	            	"parentKsPass": parentKsPass,
	            	"parentPkPass": parentPkPass
	             },
	 			contentType:"application/json",
	             success: function(datas){
	            	 if(datas.responseText==="OK"){
		            	alert("APPROVED");
	            	 }
	            	 else if(datas.responseText==="REVOKED"){
	            		 alert("YOUR CERTIFICATE IS REVOKED.");
	            	 }
		         }
			});
		}
	}
	
	function withdrawCert(){
		var num = $("#serialNumIdWithdraw").val();
		var reason = $("#selectReasonWithdrawId").val();
		if(num.length != 0){
			$.ajax({
	            type: "POST",
	            url: "/ocsp/withdrawCert/",
	            data: {"serNum": num,
	            	"reason": reason},
	            contentType : "application/json",
	            dataType: "json",
	            complete: function(response){
	           	 	alert(response.responseText);
		         }
	        });
		}
	}
	function findwithdrawCert(){
		var num = $("#findWithdraw").val();
		if(num.length != 0){
			$.ajax({
	            type: "POST",
	            url: "/ocsp/findwithdrawCert/",
	            data: {"serNum": num},
	            contentType : "application/json",
	            dataType: "json",
	            complete: function(response){
	            	 var strResponse=response.responseJSON;
	            	if(strResponse.tagNo===0){
	           	 		alert("Status: OK" );
	            	}
	            	else if(strResponse.tagNo===1){
	            		alert("Status: REVOKED	" + strResponse.reason + "	  revocation time: " + strResponse.time);
	            	}
	            	else if(strResponse===-1){
	            		alert("Response was invalid");
	            	}
	            	else{
	            		alert("Status: UNKNOWN");
	            	}
		         }
	        });
		}
	}
	
	function downloadCert(){
		$.ajax({
            type: "POST",
            url: "downloadCert/",
            complete: function(response){
           	 	alert("Certificate downloaded");
	         }
        });
	}
	</script>
</body>
</html>