package db;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import izvod.IzvodWS;
import nbs.data.Mt102;
import nbs.data.Mt900;
import nbs.data.Mt910;
import nbs.data.Nalog;

public class Database {

	private Connection connect = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    private ResultSet resultSet = null;
	
    public Database(){
    	try {
    		
			Class.forName("com.mysql.jdbc.Driver");
			connect = DriverManager.getConnection("jdbc:mysql://localhost:3306/jdbcnbs?" + "user=root&password=admin123");
			statement = connect.createStatement();
	  
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	  }
    
    public void doTransaction(String racunBankeD, String racunBankeP, Double iznos) throws Exception {
    	System.out.println("************TRANSACTING " + iznos + " from " + racunBankeD + " to " + racunBankeP + "**********");
        try {
        	String query = "SELECT * FROM RACUNI_BANKI WHERE BRRACUNA = ?";
        	preparedStatement = connect.prepareStatement(query);
            preparedStatement.setString(1, racunBankeD);
            ResultSet resultSet = preparedStatement.executeQuery();
            if(resultSet.next()){
            	double staroStanjeD = resultSet.getDouble("stanje");
            	 query = "SELECT * FROM RACUNI_BANKI WHERE BRRACUNA = ?";
             	preparedStatement = connect.prepareStatement(query);
                 preparedStatement.setString(1, racunBankeP);
                 ResultSet resultSet2 = preparedStatement.executeQuery();
                 if(resultSet2.next()){
                 	double staroStanjeP = resultSet2.getDouble("stanje");
                 	  String query2 = "UPDATE RACUNI_BANKI SET stanje = ? where BRRACUNA = ? ";
                      preparedStatement = connect.prepareStatement(query2);
                      preparedStatement.setDouble(1, staroStanjeD-iznos);
                      preparedStatement.setString(2, racunBankeD);
                      preparedStatement.executeUpdate();
                      
                      query2 = "UPDATE RACUNI_BANKI SET stanje = ? where BRRACUNA = ? ";
                      preparedStatement = connect.prepareStatement(query2);
                      preparedStatement.setDouble(1, staroStanjeP+iznos);
                      preparedStatement.setString(2, racunBankeP);
                      preparedStatement.executeUpdate();
                 }
                 
               
            }
           
            System.out.println("******************FINISHED TRANSACTION******************");
        }
        catch (Exception e) {
            throw e;
        } finally {
          //  close();
        }
    }
    
    public void setTransaction(String bankCode1 , String bankCode2, Double iznos) throws Exception{
    	System.out.println("********SETING TRANSACTION BETWEEN " + bankCode1 + " AND " + bankCode2+ "*******************");
    	String query = "SELECT * FROM RACUNI_BANKI WHERE CODE = ?";
    	try {
			 preparedStatement = connect.prepareStatement(query);
			 preparedStatement.setString(1, bankCode1);
		     ResultSet resultSet = preparedStatement.executeQuery();
		     String brRacuna1 = "";
		     if(resultSet.next()){
		    	 brRacuna1 = resultSet.getString("brracuna");
		     }
		     preparedStatement = connect.prepareStatement(query);
			 preparedStatement.setString(1, bankCode2);
		     ResultSet resultSet2 = preparedStatement.executeQuery();
		     String brRacuna2 = "";
		     if(resultSet2.next()){
		    	 brRacuna2 = resultSet2.getString("brracuna");
		     }
		     System.out.println("********SETING TRANSACTION BETWEEN " + brRacuna1 + " AND " + brRacuna2 + "*******************");
		    	
		     doTransaction(brRacuna1, brRacuna2, iznos);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       
    }
    
 /*   public void settle(ArrayList<Nalog> nalozi){
    	String bankCode2 = nalozi.get(0).getRacunPrimaoca().substring(0, 3);
		//trazimo od druge banke listu naloga za prvu banku
    	
    }*/
    
    public void close() {
        try {
            if (resultSet != null) {
                resultSet.close();
            }

            if (statement != null) {
                statement.close();
            }

            if (connect != null) {
                connect.close();
            }
        } catch (Exception e) {

        }
    }

	public Mt900 createMT900(String bankCode, String idPorukeNaloga, Date datumValute, double iznos, String sifraValute ) throws SQLException {
		System.out.println("TRAZI SE CODE " + bankCode);
		String query = "SELECT * FROM RACUNI_BANKI WHERE CODE = ?";
    	preparedStatement = connect.prepareStatement(query);
        preparedStatement.setString(1, bankCode);
        ResultSet resultSet = preparedStatement.executeQuery();
        if(resultSet.next()){
        	String swiftBankeD = resultSet.getString("SWIFT");
        	String racunBankeD = resultSet.getString("BRRACUNA");
        	Mt900 mt900 = new Mt900("3295", swiftBankeD, racunBankeD,idPorukeNaloga, datumValute, iznos, sifraValute);
        	return mt900;
        }
		return null;
	}

	//banka 1 primalac, banka 2 duznik
	public void createMT102_MT910(String bankCode1, String bankCode2, double ukupanIznos, String sifraValute, Date datumValute, Date datum, ArrayList<Nalog> nalozi, IzvodWS izvod) throws SQLException, DatatypeConfigurationException, MalformedURLException {
		System.out.println("************CREATING MT102 AND MT910 FOR BANK2 " + bankCode1);
		String query = "SELECT * FROM RACUNI_BANKI WHERE CODE = ?";
    	preparedStatement = connect.prepareStatement(query);
        preparedStatement.setString(1, bankCode1);
        ResultSet resultSet = preparedStatement.executeQuery();
        if(resultSet.next()){
        	System.out.println("******CREATING MT102*********");
        	String swiftBankeD = resultSet.getString("SWIFT");
        	String racunBankeD = resultSet.getString("BRRACUNA");
        	query = "SELECT * FROM RACUNI_BANKI WHERE CODE = ?";
        	preparedStatement = connect.prepareStatement(query);
            preparedStatement.setString(1, bankCode2);
            ResultSet resultSet2 = preparedStatement.executeQuery();
            if(resultSet2.next()){
            	String swiftBankeP = resultSet2.getString("SWIFT");
            	String racunBankeP = resultSet2.getString("BRRACUNA");
            	GregorianCalendar c = new GregorianCalendar();
    			c.setTime(datumValute);
    			XMLGregorianCalendar date = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
    			c.setTime(datum);
    			XMLGregorianCalendar date2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
            	Mt102 mt102 = new Mt102("2151", swiftBankeD, racunBankeD, swiftBankeP, racunBankeP, ukupanIznos, sifraValute, datumValute, datum, nalozi);
            	

            	Mt910 mt910 = new Mt910("2152", swiftBankeP, racunBankeP, "3245", datumValute, ukupanIznos, sifraValute);
            	
            	URL wsdlLocation;
    			wsdlLocation = new URL("http://localhost:8083/banka-ws/services/IzvodWS?wsdl");
    			QName serviceName = new QName("http://izvod", "IzvodWSService");
    			QName portName = new QName("http://izvod", "IzvodWSPort");

    			Service service = Service.create(wsdlLocation, serviceName);
    			
    			IzvodWS izvod2 = service.getPort(portName, IzvodWS.class);
            	
    			izvod2.setBankCode(bankCode1);
    			izvod2.receiveMT102AndMT910(mt102, mt910);
            }
        	
        }
	}
}
