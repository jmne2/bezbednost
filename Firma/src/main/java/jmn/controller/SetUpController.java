package jmn.controller;

import java.security.SecureRandom;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import jmn.configuration.AES;
import jmn.models.DEK;
import jmn.models.PoslovniPartner;
import jmn.models.Preduzece;
import jmn.models.Racun;
import jmn.models.RacunPartnera;
import jmn.models.Role;
import jmn.models.User;
import jmn.services.DEKService;
import jmn.services.PoslovniPartnerService;
import jmn.services.PreduzeceService;
import jmn.services.RacunPartneraService;
import jmn.services.RacunService;
import jmn.services.RoleService;
import jmn.services.UserService;

@Controller
public class SetUpController {

	@Autowired
	private UserService us;
	
	@Autowired
	private RoleService rs;
	
	@Autowired
	private RacunService racs;
	@Autowired
	private RacunPartneraService rps;
	
	@Autowired
	private PoslovniPartnerService pps;
	
	@Autowired
	private DEKService dks;
	
	@Autowired
	private PreduzeceService prs;
	
	 @RequestMapping(value="/test", method = RequestMethod.GET)
	    public ModelAndView getTestData() {
		 
		User u1 = new User("bk1", "pass", null, null);
		 User u2 = new User("mng1", "menadjer", null, null);
		 User u3 = new User("admin", "admin123", null, null);
		 //BCrypt.gensalt ce izgenerisati salt za zatati broj iteracija hashovanja (valjda, racuna se to kao 2^^^zadati_broj)
		 //secure random se prosedjuje da moze od nekud poceti generisanje salta
		 String slt = BCrypt.gensalt(15, new SecureRandom());

		 //BCrypt.hashpw hashuje po bcrypt algoritmu, koristeci salt
		 String hashed = BCrypt.hashpw(u1.getPassword(), slt);
		 u1.setPassword(hashed);
		 u1.setSalt(slt);
		 slt = BCrypt.gensalt(15, new SecureRandom());
		 hashed = BCrypt.hashpw(u2.getPassword(), slt);
		 u2.setPassword(hashed);
		 u2.setSalt(slt);
		 slt = BCrypt.gensalt(15, new SecureRandom());
		 hashed = BCrypt.hashpw(u3.getPassword(), slt);
		 u3.setPassword(hashed);
		 u3.setSalt(slt);
		 Role temp = rs.findByNaziv("admin");
		 Role mng = rs.findByNaziv("manager");
		 Role bk = rs.findByNaziv("bookkeeper");
		 u1.getRoles().add(bk);
		 u2.getRoles().add(mng);
		 u3.getRoles().add(temp);
		 Preduzece pr = prs.findByPib(123456789);
		 u1.setPreduzece(pr);
		 u1 = us.saveUser(u1);
		 pr = prs.findByPib(183466789);
		 u2.setPreduzece(pr);
		 u2 = us.saveUser(u2);
		 u3 = us.saveUser(u3);
		 
		 User u4 = new User("bk2", "bookkeeping", null, null);
		 slt = BCrypt.gensalt(15, new SecureRandom());

		 //BCrypt.hashpw hashuje po bcrypt algoritmu, koristeci salt
		 hashed = BCrypt.hashpw(u4.getPassword(), slt);
		 u4.setPassword(hashed);
		 u4.setSalt(slt);
		 Role one = rs.findByNaziv("bookkeeper");
		 u4.getRoles().add(one);
		 pr = prs.findByPib(183466789);
		 u4.setPreduzece(pr);
		 u4 = us.saveUser(u4);
		 
		 return new ModelAndView("redirect:/");
	   }
	
	 @RequestMapping(value="/dndt",  method = RequestMethod.GET)
	 public ModelAndView fixingDB(){
		 
		 List<Preduzece> preduzeca = prs.findAll();
		 for(Preduzece p : preduzeca){
			 String dk = AES.initDEK();
			 System.out.println("TEST PRIMER INICIJALNO " + dk);
			 int pb = p.getPib();
			 String rez = AES.encryptDEK(dk, Integer.toString(pb));
			 System.out.println("TEST PRIMER NAKON ENKRIPCIJE " + rez);
			 String iv = AES.encodeIV();
			 DEK d = new DEK(rez, iv, p);
			 d = dks.saveDEK(d);
			 String decryptVal = AES.decryptDEK(d.getValue(), Integer.toString(pb));
			 System.out.println("TEST PRIMER NAKON DEKRIPCIJE IZ BAZE " + decryptVal);
			 
			 
			 List<Racun> lista = racs.findByPreduzece(p);
			 for(Racun r : lista){
				 String newBr = AES.encryptCBC(r.getBrRacuna(), dk, iv);
				 r.setBrRacuna(newBr);
				 racs.saveRacun(r);
			 }
			
			List<PoslovniPartner> partneri = pps.findByPreduzece(p);
			 for(PoslovniPartner pp : partneri){
				 List<RacunPartnera> racuni = rps.findByPoslovniPartner(pp);
				 for(RacunPartnera racun : racuni){
					 racun.setBrRacuna(AES.encryptCBC(racun.getBrRacuna(), dk, iv));
					 rps.saveRacunPartnera(racun);
				 }
			 }
		 }
		 
		 return new ModelAndView("redirect:/");
	 }
	
}
