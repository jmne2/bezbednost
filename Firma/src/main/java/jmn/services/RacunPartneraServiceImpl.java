package jmn.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jmn.models.Banka;
import jmn.models.PoslovniPartner;
import jmn.models.Racun;
import jmn.models.RacunPartnera;
import jmn.repositories.RacunPartneraRepository;

@Service
public class RacunPartneraServiceImpl implements RacunPartneraService{

	@Autowired
	private RacunPartneraRepository rpr;
	
	@Override
	public List<RacunPartnera> findAll() {
		// TODO Auto-generated method stub
		return (List<RacunPartnera>)rpr.findAll();
	}

	@Override
	public RacunPartnera saveRacunPartnera(RacunPartnera rp) {
		// TODO Auto-generated method stub
		return rpr.save(rp);
	}

	@Override
	public RacunPartnera findOne(Long id) {
		// TODO Auto-generated method stub
		return rpr.findOne(id);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		rpr.delete(id);
	}

	@Override
	public void delete(RacunPartnera rp) {
		// TODO Auto-generated method stub
		rpr.delete(rp);
	}

	@Override
	public List<RacunPartnera> findByPoslovniPartnerAndBanka(PoslovniPartner poslovniPartner, Banka banka) {
		// TODO Auto-generated method stub
		return rpr.findByPoslovniPartnerAndBanka(poslovniPartner, banka);
	}

	@Override
	public List<RacunPartnera> findByPoslovniPartner(PoslovniPartner poslovniPartner) {
		// TODO Auto-generated method stub
		return rpr.findByPoslovniPartner(poslovniPartner);
	}

}
