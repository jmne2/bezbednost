package certificateApp.service;

import java.util.List;

import certificateApp.models.User;

public interface UserService {
	List<User> findAll();
	User saveUser(User user);
	User findOne(long id);
	User findByUsernameAndPassword(String username, String password);
	void delete(User user);
	User findByName(String name);
	User findByUsername(String username);
}
