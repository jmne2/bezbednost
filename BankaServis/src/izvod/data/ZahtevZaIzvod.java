package izvod.data;


import java.sql.Date;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import annotations.SqlDateAdapter;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "zahtevZaIzvod", 
		namespace="http://izvod/data", 
		propOrder = {"brojRacuna",
					"datum",
					"redniBrPreseka"})
public class ZahtevZaIzvod implements java.io.Serializable{

	@XmlElement(name="brojRacuna", required = true)
	@Size(min = 18, max = 18)
	private String brojRacuna;
	
	@XmlElement(name="datum", required = true)
	@XmlJavaTypeAdapter(SqlDateAdapter.class)
	private Date datum;
	
	@XmlElement(name="redniBrPreseka", required = true)
	@Digits(integer = 2, fraction = 0)	
	private int redniBrPreseka;
	
	public ZahtevZaIzvod(){
		
	}
	
	public ZahtevZaIzvod(String brojRacuna, Date datum, int redniBrPreseka) {
		super();
		this.brojRacuna = brojRacuna;
		this.datum = datum;
		this.redniBrPreseka = redniBrPreseka;
	}
	public String getBrojRacuna() {
		return brojRacuna;
	}
	public void setBrojRacuna(String brojRacuna) {
		this.brojRacuna = brojRacuna;
	}
	public Date getDatum() {
		return datum;
	}
	public void setDatum(Date datum) {
		this.datum = datum;
	}
	public int getRedniBrPreseka() {
		return redniBrPreseka;
	}
	public void setRedniBrPreseka(int redniBrPreseka) {
		this.redniBrPreseka = redniBrPreseka;
	}
		
}
