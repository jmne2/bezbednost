package certificateApp.models;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

@Entity
public class User implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long Id;	
	
	@Column(nullable = false, unique = true)
	private String username;
	
	@Column(nullable = false)
	private String password;
	
//	@Column(nullable = false)
//	private String role;
	
	@Column(nullable = false)
	private String name;
	
	@ManyToMany(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinTable(name="user_role", joinColumns = {
			@JoinColumn(name="user_id", nullable=false)},
			inverseJoinColumns = {@JoinColumn(name = "role_id", nullable=false)
	})
	private Set<Role> allowed = new HashSet<Role>();
	
	public User(){}

	public User(String username, String password, String name) {
		super();
		this.username = username;
		this.password = password;
		this.name = name;
	}

	public Long getId(){
		return Id;
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

/*	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
*/
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Role> getAllowed() {
		return allowed;
	}

	public void setAllowed(Set<Role> allowed) {
		this.allowed = allowed;
	}
	
	public void addRole(Role role){
		this.allowed.add(role);
	}
}
