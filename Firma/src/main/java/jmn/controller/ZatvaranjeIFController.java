package jmn.controller;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jmn.annotation.PermissionType;
import jmn.models.DnevnoStanje;
import jmn.models.Faktura;
import jmn.models.PoslovniPartner;
import jmn.models.Racun;
import jmn.models.RacunPartnera;
import jmn.models.SifrarnikMetoda;
import jmn.models.StavkaIzvoda;
import jmn.models.User;
import jmn.models.ZatvaranjeIF;
import jmn.pojo.UserData;
import jmn.pojo.Zatvaranje;
import jmn.services.DnevnoStanjeService;
import jmn.services.FakturaService;
import jmn.services.PoslovniPartnerService;
import jmn.services.RacunPartneraService;
import jmn.services.RacunService;
import jmn.services.StavkaIzvodaService;
import jmn.services.ZatvaranjeIFService;

@Controller
public class ZatvaranjeIFController {

	final static Logger logger = Logger.getLogger(ZatvaranjeIFController.class);
	
	@Autowired
	private ZatvaranjeIFService zif;
	
	@Autowired
	private StavkaIzvodaService sis;
	
	@Autowired
	private FakturaService fs;
	
	@Autowired
	private DnevnoStanjeService dss;
	
	@Autowired
	private RacunService rs;
	
	@Autowired
	private RacunPartneraService rps;
	
	@Autowired
	private PoslovniPartnerService pps;
	
	@PermissionType("ZatvaranjeIF:view")
	@RequestMapping(value="/zatvaranjeIF/stavkeIzvoda/{id}", method=RequestMethod.POST)
	public ResponseEntity<List<StavkaIzvoda>> listAllStavkeIzvoda(HttpSession session, @PathVariable("id") long id){
		User u = (User) session.getAttribute("user");
		List<Racun> racuni = rs.findByPreduzece(u.getPreduzece());
		PoslovniPartner poslovniPartner = pps.findOne(id);
		
		List<DnevnoStanje> stanja = new ArrayList<DnevnoStanje>();
		List<StavkaIzvoda> stavke = new ArrayList<StavkaIzvoda>();
		List<StavkaIzvoda> otvoreneStavke = new ArrayList<StavkaIzvoda>();
		List<RacunPartnera> racuniPartnera = rps.findByPoslovniPartner(poslovniPartner);
		String racuniPartneraCat = "";
		for (RacunPartnera racunPartnera : racuniPartnera) {
			racuniPartneraCat.concat(racunPartnera.getBrRacuna());
		}
		
		for(Racun r : racuni){
			stanja.addAll(dss.findByRacun(r));
		}
		
		for(DnevnoStanje st : stanja){
			
			stavke.addAll(sis.findByDnevnoStanjeAndSmerIgnoreCase(st, "P"));
		}
		
			for(StavkaIzvoda st : stavke){
					for (RacunPartnera racunPartnera : racuniPartnera) {
						System.out.println("Usao u for provere racunaa");
						if(st.getRacunDuznika().equals(racunPartnera.getBrRacuna())){
							if(st.getPreostaliIznos()>0){
							otvoreneStavke.add(st);
							System.out.println("Dodata jedna otvorena stavka za tog partnera");
							}
					}
					}
					
				}
		
			System.out.println("Broj otvorenih stavki je: "+ otvoreneStavke.size());
		if(otvoreneStavke.isEmpty()){
			 return new ResponseEntity<List<StavkaIzvoda>>(HttpStatus.NOT_FOUND);
		}
		 
	     return new ResponseEntity<List<StavkaIzvoda>>(otvoreneStavke, HttpStatus.OK);
	}
	
	@PermissionType("ZatvaranjeIF:view")
	@RequestMapping(value = "/zatvaranjeIF/fakture/{id}", method = RequestMethod.POST)
	    public ResponseEntity<List<Faktura>> listAllFakture(HttpSession session, @PathVariable("id") long id) {
		User user = (User)session.getAttribute("user");
		 PoslovniPartner poslovniPartner = pps.findOne(id);
			System.out.println("Fakture za zatvaranje za poslovnog partnera"+ poslovniPartner.getNaziv()); 

		 List<Faktura> izlazneFakture = fs.findByPreduzeceAndVrstaFaktureAndPoslovniPartnerAndPreostaliIznosGreaterThan(user.getPreduzece(), "izlazne",poslovniPartner, 0); 
		System.out.println("Broj izlaznih faktra je: "+ izlazneFakture.size());
		 if(izlazneFakture==null)
			 return new ResponseEntity<List<Faktura>>(HttpStatus.NOT_FOUND);
		 
	     return new ResponseEntity<List<Faktura>>(izlazneFakture, HttpStatus.OK);
    }
	
	@PermissionType("ZatvaranjeIF:view")
	@RequestMapping(value = "/zatvaranjeIF/poslovniPartneri/", method = RequestMethod.POST)
	    public ResponseEntity<List<PoslovniPartner>> listAllPoslovniPartneri(HttpSession session) {
		 User user = (User)session.getAttribute("user");
		 List<PoslovniPartner> poslovniPartneri = pps.findByPreduzeceAndVrsta(user.getPreduzece(), "kupuje");
		 System.out.println("BROJ POSLOVNIH PARTNERA" + poslovniPartneri.size());
		 if(poslovniPartneri==null)
			 return new ResponseEntity<List<PoslovniPartner>>(HttpStatus.NOT_FOUND);
		 
	     return new ResponseEntity<List<PoslovniPartner>>(poslovniPartneri, HttpStatus.OK);
    }
	
	 
	@PermissionType("ZatvaranjeIF:view")
	@RequestMapping(value = "/zatvaranjeIF/zatvoriIF/", method = RequestMethod.POST)
	//    public ResponseEntity<?> searchFakture(@RequestBody Faktura izlaznaFaktura,@RequestBody StavkaIzvoda stavkaIzvoda, @PathVariable("iznos") String iznos) {
    public ResponseEntity<?> searchFakture( HttpSession session,@RequestBody Zatvaranje zatvaranjeIF) {
	System.out.println("ZATVARANJE");
	Date datumIzvoda = new Date(Calendar.getInstance().getTimeInMillis());
	
	ZatvaranjeIF zatvori = new ZatvaranjeIF();
	zatvori.setDatumIzvoda(datumIzvoda);
	Faktura izlaznaFaktura = fs.findOne(zatvaranjeIF.getIdFakture());
	StavkaIzvoda stavkaIzvoda = sis.findOne(zatvaranjeIF.getIdStavke());
	stavkaIzvoda.setPreostaliIznos(stavkaIzvoda.getPreostaliIznos()-zatvaranjeIF.getIznos());
	izlaznaFaktura.setPreostaliIznos(izlaznaFaktura.getPreostaliIznos()-zatvaranjeIF.getIznos());
	zatvori.setFaktura(izlaznaFaktura);
	zatvori.setStavkaIzvoda(stavkaIzvoda);
	zatvori.setIznos(zatvaranjeIF.getIznos());
	
	sis.saveStavkaIzvoda(stavkaIzvoda);
	fs.saveFaktura(izlaznaFaktura);
	zif.saveZatvaranjeIF(zatvori);
		return new ResponseEntity<Void>(HttpStatus.CREATED);
    }
	
	@PermissionType("ZatvaranjeIF:edit")
	@RequestMapping(value = "/zatvaranjeIF/zatvoreneIF/", method = RequestMethod.POST)
	    public ResponseEntity<List<ZatvaranjeIF>> listAllZatvoreneFakture(HttpSession session) {
		 User user = (User)session.getAttribute("user");
		 List<Faktura> izlazneFakture = fs.findByVrstaFaktureAndPreduzece("izlazne",user.getPreduzece()); 
		 List<ZatvaranjeIF> zatvoreneFakture = new ArrayList<ZatvaranjeIF>();
		 for (Faktura faktura : izlazneFakture) {
			List<ZatvaranjeIF> zatvorene = zif.findByIzlaznaFaktura(faktura);
			if(zatvorene != null){
				zatvoreneFakture.addAll(zatvorene);
			}
		}
		 if(zatvoreneFakture.isEmpty())
			 return new ResponseEntity<List<ZatvaranjeIF>>(HttpStatus.NOT_FOUND);
		 
	     return new ResponseEntity<List<ZatvaranjeIF>>(zatvoreneFakture, HttpStatus.OK);
    }
	
	@PermissionType("ZatvaranjeIF:edit")
	@RequestMapping(value = "/zatvaranjeIF/AutomatskizatvoreneIF/", method = RequestMethod.POST)
	    public ResponseEntity<?> AutomatskoZatvaranje(HttpSession session) {
		 UserData user = (UserData)session.getAttribute("user");
		 List<Faktura> izlazneFakture = fs.findByVrstaFaktureAndPreduzece("izlazne",user.getPreduzece()); 
		 
		 List<DnevnoStanje> stanja = new ArrayList<DnevnoStanje>();
		 List<StavkaIzvoda> stavke = new ArrayList<StavkaIzvoda>();
		 
		 for(DnevnoStanje st : stanja){
				
				stavke.addAll(sis.findByDnevnoStanjeAndSmerIgnoreCase(st, "P"));
		}
		 System.out.println("AUTO ZATVARANJE FOR ");
		 for (StavkaIzvoda stavkaIzvoda : stavke) {
			for (Faktura izlaznaFaktura : izlazneFakture) {
				if(stavkaIzvoda.getDuznik().toLowerCase().contains(izlaznaFaktura.getPoslovniPartner().getNaziv().toLowerCase())){
					System.out.println("ISTO JE IME");
				if(stavkaIzvoda.getPozivNaBrojZaduzenja().equals(izlaznaFaktura.getBrojFakture())){
					System.out.println("POZIV NA BROJ ZADUZENJA I BROJ FAKTURE JEDNAKO ");
					izlaznaFaktura.setPreostaliIznos(izlaznaFaktura.getPreostaliIznos()-stavkaIzvoda.getIznos());
					stavkaIzvoda.setPreostaliIznos(0);
					sis.saveStavkaIzvoda(stavkaIzvoda);
					fs.saveFaktura(izlaznaFaktura);
					ZatvaranjeIF zatvori = new ZatvaranjeIF();
					zatvori.setDatumIzvoda(new Date(Calendar.getInstance().getTimeInMillis()));
					zatvori.setFaktura(izlaznaFaktura);
					zatvori.setStavkaIzvoda(stavkaIzvoda);
					zif.saveZatvaranjeIF(zatvori);
				}
			}
			}
		}
		 
		 logger.warn("Korisnik "+ user.getId() + " uspesno je izvrsio metodu " + SifrarnikMetoda.methods.get(
	 				Thread.currentThread().getStackTrace()[1].getMethodName()));
	     return new ResponseEntity<Void>( HttpStatus.OK);
    }
	
	@PermissionType("ZatvaranjeIF:edit")
	@RequestMapping(value = "/zatvaranjeIF/razveziStavku/", method = RequestMethod.POST)
	    public ResponseEntity<?> razvezi(HttpSession session, @RequestBody List<ZatvaranjeIF> zatvoreneStavke) {
		UserData u = (UserData)session.getAttribute("user");
		System.out.println("razvezi broj stavki" + zatvoreneStavke.size() );
		for (ZatvaranjeIF zatvorenaStavka : zatvoreneStavke) {
			
		ZatvaranjeIF exists = zif.findOne(zatvorenaStavka.getId());
		 if(exists == null){
			 logger.error("Greska pri izvrsenju " + SifrarnikMetoda.methods.get(Thread.currentThread().getStackTrace()[1].getMethodName())
	        	+ " za korisnika " + u.getId());
			 return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		 }
		 Faktura izmeniti = fs.findOne(zatvorenaStavka.getFaktura().getId());
		 izmeniti.setPreostaliIznos(izmeniti.getPreostaliIznos()+zatvorenaStavka.getIznos());
		 izmeniti = fs.saveFaktura(izmeniti);
		 StavkaIzvoda stavkaIzvoda = zatvorenaStavka.getStavkaIzvoda();
		 stavkaIzvoda.setPreostaliIznos(stavkaIzvoda.getPreostaliIznos()+zatvorenaStavka.getIznos());
		 sis.saveStavkaIzvoda(stavkaIzvoda);
		 zif.delete(zatvorenaStavka);
		}
		logger.warn("Korisnik "+ u.getId() + " uspesno je izvrsio metodu " + SifrarnikMetoda.methods.get(
 				Thread.currentThread().getStackTrace()[1].getMethodName()));
	     return new ResponseEntity<ZatvaranjeIF>(HttpStatus.NO_CONTENT);
    }
}
