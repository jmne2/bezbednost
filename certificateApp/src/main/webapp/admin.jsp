<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta charset="ISO-8859-1"     >
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<title>Get certificate</title>
<style>
p {

	width: 500px;
	max-width: 500px;
	height : auto;	
}
</style>
<script type="text/javascript">

	var user = "${user}";
	if(user === ""){
		window.location.replace("index.jsp");
	}
		function getFormData($form){
		    var unindexed_array = $form.serializeArray();
		    var indexed_array = {};
	
		    $.map(unindexed_array, function(n, i){
		        indexed_array[n['name']] = n['value'];
		    });
	
		    return indexed_array;
		}
		
		$(document).ready(function(){
			
			var errorElem = "<div style=\"color:red\" id=\"errorElem\"> This field is required.</div>";
			
			$(".form-control").on('blur', function(){
				if($(this).val().length === 0){
					$(this).after(errorElem);
				} else {
					$(this).next().remove("#errorElem");
				}
			});
			
			
			$("#showCertDiv").hide();
			$("#certForm").hide();
			$("#certForm2").hide();
			$("#findCertDiv").hide();
			$("#withdrawCertDiv").hide();
			$("#findwithdrawCertDiv").hide();
		});
		
		function create(){
			$("#showCertDiv").hide();
			$("#certForm").show();
			$("#findCertDiv").hide();
			$("#withdrawCertDiv").hide();
			$("#findwithdrawCertDiv").hide();
		}
		function find(){
			$("#certForm").hide();
			$("#certForm2").hide();
			$("#chooseCertForm").hide();
			$("#findCertDiv").show();
			$("#withdrawCertDiv").hide();
			$("#findwithdrawCertDiv").hide();
			$("#buttonDownload").hide();
		}
		function withdraw(){
			$("#certForm").hide();
			$("#chooseCertForm").hide();
			$("#findCertDiv").hide();
			$("#withdrawCertDiv").show();
			$("#findwithdrawCertDiv").hide();
			}
		function check(){
			$("#certForm").hide();
			$("#chooseCertForm").hide();
			$("#findCertDiv").hide();
			$("#withdrawCertDiv").hide();
			$("#findwithdrawCertDiv").show();
		}
		
		function logOut(){
			$.ajax({
	             type: "POST",
	             url: "/logOut/",
	 			contentType:"application/json",
	             complete: function(datas){
	            	 document.location.href="/";
		         }
			});
		}
</script>
</head>
<body>
	<br/>
	<nav class="navbar navbar-inverse">
	<div class="collapse navbar-collapse">
		<ul class="nav navbar-nav">
		<li><button id="createId" class="btn btn-default" onclick="create()" role="presentation">Create certificate</button>
		</li>
		<li><button id="findId" class="btn btn-default" onclick="find()" role="presentation">Find certificate</button>
		</li>
		<li><button id="withdrawId" class="btn btn-default" onclick="withdraw()" role="presentation">Withdraw certificate</button>
		</li>
		<li><button id="checkId" class="btn btn-default" onclick="check()" role="presentation">Check certificate status</button>
		</li>
		</ul>
		<ul class="nav navbar-nav navbar-right">
	        <li><a href="" onclick="logOut()"><span class=""></span> Log out</a></li>
	    </ul>
	</div>
	</nav>
	<br/><br/>
	<div  class="container-fluid" align="center" style=" display: table; margin: auto; width:40%; " >
		<div class=" col-md-8 col-md-offset-1 centered" id="certBlock" >
			<form id="certForm" role="form" action="" method="post" hidden=true>
				<fieldset>
					<label>Common name:</label><br>
					<input type="text" class="form-control" name="commonName" id="commonNameId"  ><br>
					<label>Surname:</label><br>
					<input type="text" class="form-control" name="surname" id="surnameId"><br>
					<label>Given name:</label><br>
					<input type="text" class="form-control" name="givenName" id="givenName"><br>
					<label>Organization:</label><br>
					<input type="text" class="form-control" name="organization" id="organizationId"><br>
					<label>Organization unit:</label><br>
					<input type="text" class="form-control" name="organizationUnit" id="organizationUnitId"><br>
					<label>Country:</label><br>
					<input type="text" class="form-control" name="country" id="countryId"><br>
					<label>Email:</label><br>
					<input type="text" class="form-control" name="email" id="emailId"><br>
					<label>Alias:</label>
					<input type="text" class="form-control" name="alias" id="aliasId"><br>
					<label>Password for private key:</label>
					<input type="text" class="form-control" name="passwordPk" id="passwordPkId"><br>
					<label id = "selfSignedLabel">Self-signed</label>
					<input type="checkbox" name="selfSigned" value="true" id="selfSignedId"><br>
					<label>Password for key store:</label>
					<input type="text" class="form-control" name="passwordKs" id="passwordKsId"><br>
					<input class="btn btn-lg btn-primary" type="submit" value="OK">
				</fieldset>
			</form> <br>
			<form id="certForm2" role="form" action="" method="post" hidden=true>
				<fieldset>
					<label>Common name:</label><br>
					<input type="text" class="form-control" name="commonName" id="commonNameId2"  ><br>
					<label>Surname:</label><br>
					<input type="text" class="form-control" name="surname" id="surnameId2"><br>
					<label>Given name:</label><br>
					<input type="text" class="form-control" name="givenName" id="givenName2"><br>
					<label>Organization:</label><br>
					<input type="text" class="form-control" name="organization" id="organizationId2"><br>
					<label>Organization unit:</label><br>
					<input type="text" class="form-control" name="organizationUnit" id="organizationUnitId2"><br>
					<label>Country:</label><br>
					<input type="text" class="form-control" name="country" id="countryId2"><br>
					<label>Email:</label><br>
					<input type="text" class="form-control" name="email" id="emailId2"><br>
					<label>Bank name:</label>
					<select type="text" name="bankName" id="bankNameId">
						<c:forEach items="${banks}"  var="bank">
							<option>"${bank.name}"</option>
						</c:forEach>
					</select><br>
					<label>Alias:</label><br>
					<input type="text" class="form-control" name="alias" id="aliasId2"><br>
					<input class="btn btn-lg btn-primary" type="submit" value="OK">
				</fieldset>
			</form> <br>
		<div class="container-fluid" id="chooseCertDiv" hidden=true>
			<form id="chooseCertForm" role="form" action="" method="post">
				<fieldset>
					<select type="text" name="bank" id="bankId">
						<c:forEach items="${banks}"  var="bank">
							<option>"${bank.name}"</option>
						</c:forEach>
					</select>
					<select type="text" name="ksCert" id="ksCertId">
							
					</select><br/>
					<label>Password for parent private key:</label>
					<input type="text" class="form-control" name="ksCertPassword" id="ksCertPasswordId"></br>
					<label>Password for parent key store:</label>
					<input type="text" class="form-control" name="ksCertKsPassword" id="ksCertPasswordKsId"></br>
					
					<input class="btn btn-lg btn-primary" type="submit" value="CHOOSE">
				</fieldset>
			</form>
		</div>
		</div>
		
		<div id="findCertDiv" class="container-fluid" hidden=true >
			<label>Key store name: </label>
			<input type="text" id="findKsNameId"></br>
			<label>Key store password: </label>
			<input type="text" id="findPasswordKsId"></br>
			<label>Certificate serial number: </label>
			<input type="number" id="serialNumId"></br>
			<button class="btn btn-lg btn-primary" onclick="findCert()">FIND</button>
			<p id="foundResult"></p>
			<button class="btn btn-lg btn-primary" id="buttonDownload" onclick="downloadCert()" hidden=true>DOWNLOAD CERTIFICATE</button>
		</div>
		<div id="showCertDiv" class="container-fluid" align="center" style=" display: table; margin: auto; width:40%; " hidden=true >
			<label>Version:</label>
			<p id=showCertDivVersion></p>
			<label>SerialNumber:</label>
			<p id=showCertDivSerialNumber></p>
			<label>IssuerDN:</label>
			<p id=showCertDivIssuerDN></p>
			<label>Start Date:</label>
			<p id=showCertDivStartDate></p>
			<label>Final Date:</label>
			<p id=showCertDivFinalDate></p>
			<label>SubjectDN:</label>
			<p id=showCertDivSubjectDN></p>
			<label>Public Key:</label>
			<p id=showCertDivPublicKey></p>
			<label>Extensions:</label>
			<label>Is CA: </label>
			<p id=showCertDivExtensions></p>
	</div>
	
		<div id="withdrawCertDiv" class="container-fluid" >
			<label>Certificate serial number WITHDRAW: </label>
			<input type="text" id=serialNumIdWithdraw><br>
			<label>Choose reason for withdrawing: </label><br>
			<select id="selectReasonWithdrawId" >
				<option value="1">Key compromise</option>
				<option value="2">Privilege withdrawn</option>
				<option value="3">Affiliation changed</option>
				<option value="4">Unspecified</option>
			</select>
			<button class="btn btn-lg btn-primary" onclick="withdrawCert()">WITHDRAW</button>
		</div>
		

		<div id="findwithdrawCertDiv" class="container-fluid" >
			<label>Certificate serial number FIND WITHDRAW: </label>
			<input type="text" id=findWithdraw><br>
			<button class="btn btn-lg btn-primary" onclick="findwithdrawCert()">FIND WITHDRAW</button>
		</div>
		
	</div>
		
	
	
	<script type="text/javascript">
	
	$(document).on('submit','#certForm',function(e){
		e.preventDefault();
		var allGood = true;
		$("form#certForm :input").each(function(){
			if($(this).val().length === 0){
				allGood = false;
				return false; 
			} 
		});
		if(allGood){
			var $form = $("#certForm");
			var data = getFormData($form);
			var s = JSON.stringify(data);
			 $.ajax({
	             type: "POST",
	             url: "cert/",
	             data: s,
	             contentType : "application/json",
	             dataType: "json",
			 	complete: function(datas){
			 		var selfSigned = "${selfSigned}";
					var strResponse=datas.responseJSON;
					if(datas.responseText === "selfSigned"){
						alert("CREATED");
						location.reload();
					}
					else{ 
						$("#chooseCertDiv").hidden = false;
						$("#chooseCertDiv").show();
			           	$.each(strResponse, function(index, res) 
							{	
							$("#ksCertId").append("<option>" + res + "</option>");
							});
						$("#chooseCertDiv").hidden = false;
			 		}
			 	},
	             statusCode: {
	            	 	200: function(xhr) {
	            	 		
	            	 	}
	            	  }
	         });
		}
	});
	
	$(document).on('submit','#certForm2',function(e){
		e.preventDefault();
		var allGood = true;
		$("form#certForm2 :input").each(function(){
			if($(this).val().length === 0){
				allGood = false;
				return false; 
			} 
		});
		if(allGood){
			var $form = $("#certForm2");
			var data = getFormData($form);
			var s = JSON.stringify(data);
			 $.ajax({
	             type: "POST",
	             url: "cert2/",
	             data: s,
	             contentType : "application/json",
			 	success: function(datas){
			 		if(datas==="saved"){
			 			alert("saved");
			 			location.reload();
			 		}
			 	},
	             statusCode: {
	            	 	200: function(xhr) {
	            	 		
	            	 	}
	            	  }
	         });
		}
	});
	
	$(document).on('submit','#chooseCertForm',function(e){
		e.preventDefault();
		var fileName = $("#ksCertId").val();
		var password = $("#ksCertPasswordId").val();
		var passwordKs = $("#ksCertPasswordKsId").val();
		var bank = $("#bankId").val();
	
		if(password.length != 0 && passwordKs.length != 0){
			$.ajax({
	            type: "POST",
	            url: "chooseCert/",
	            data: {"bank": bank,
	            "fileName": fileName,
	           	 "password": password,
	           	 "passwordKs": passwordKs},
	            contentType : "application/json",
	            success: function(datas){
	           	 	alert("CREATED");
	           	 	location.reload();
		         },
	           	 error: function(datas){
	           		 alert("ERROR");
	           	 }
	        });
		} else {
			var errorElem = "<div style=\"color:red\" id=\"errorElem\"> This field is required.</div>";
			$("form#chooseCertForm :input").each(function(){
				if($(this).val().length === 0){
					$(this).after(errorElem); 
				} 
			});
		}
	});
	
	function findCert(){
		var num = $("#serialNumId").val();
		var pass = $("#findPasswordKsId").val();
		var ksName = $("#findKsNameId").val();
		if(num.length != 0 && pass.length != 0){
			$.ajax({
	            type: "POST",
	            url: "findCert/",
	            data: {"ksName": ksName,
	            	"pass": pass,
	            	"serNum": num },
	            contentType : "application/json",
	            dataType: "json",
	            complete: function(response){
	           	 var strResponse=response.responseJSON;
	           	 var strResponseT = response.responseText;
	           	 	if(strResponse==="WrongPassword"){
	           	 		alert("Wrong password");
	           	 	}
	           	 	else if(strResponseT==="NoExists"){
	           	 		alert("Certificate with that serial number doesn't exist");
	           	 	}
	           	 	else if(strResponse==="revoked"){
	           	 		alert("Certificate is revoked and cannot be shown.");
	           	 	}
	           	 	else{
		           		 $("#showCertDiv").show();
		           		var version = strResponse.info.version; 
		           	 	var serialNumber = strResponse.info.serialNum;
		           	 	var issuerDN = strResponse.info.issuer;
		           	 	var startDate = strResponse.info.interval.notBefore;
		           	 	var finalDate = strResponse.info.interval.notAfter;
		           	 	var subjectDN = strResponse.info.subject;
		           	 	var publicKey = strResponse.info.pubKey;
		           	 	var isca = strResponse.info.extensions.map.BasicConstraints.ca;
		           	 	
		           		var p = JSON.stringify(version);
		           	 	var p1 = JSON.stringify(serialNumber);
		           	 	var p2 = JSON.stringify(issuerDN);
		           	 	var p3 = JSON.stringify(startDate);
		           	 	var p4 = JSON.stringify(finalDate);
		           	 	var p5 = JSON.stringify(subjectDN);
		           	 	var p6 = JSON.stringify(publicKey);
		           	 	var p7 = JSON.stringify(isca);
		           	 	           	 	
		           		$("#showCertDivVersion").text(p);
		           		$("#showCertDivSerialNumber").text(p1);
		           		$("#showCertDivIssuerDN").text(p2);
		           		$("#showCertDivStartDate").text(p3);
		           		$("#showCertDivFinalDate").text(p4);
		           		$("#showCertDivSubjectDN").text(p5);
		           		$("#showCertDivPublicKey").text(p6);
	           			$("#showCertDivExtensions").text(p7);
						$("#buttonDownload").show();
	           	 	}
		         }
	        });
		}
	}
	
	function withdrawCert(){
		var num = $("#serialNumIdWithdraw").val();
		var reason = $("#selectReasonWithdrawId").val();
		if(num.length != 0){
			$.ajax({
	            type: "POST",
	            url: "/ocsp/withdrawCert/",
	            data: {"serNum": num,
	            	"reason": reason},
	            contentType : "application/json",
	            dataType: "json",
	            complete: function(response){
	            	var strResponse=response.responseJSON;
	           	 	alert(response.responseText);
		         }
	        });
		}
	}
	function findwithdrawCert(){
		var num = $("#findWithdraw").val();
		if(num.length != 0){
			$.ajax({
	            type: "POST",
	            url: "/ocsp/findwithdrawCert/",
	            data: {"serNum": num},
	            contentType : "application/json",
	            dataType: "json",
	            complete: function(response){
	            	 var strResponse=response.responseJSON;
	            	if(strResponse.tagNo===0){
	           	 		alert("Status: OK" );
	            	}
	            	else if(strResponse.tagNo===1){
	            		alert("Status: REVOKED	" + strResponse.reason + "	  revocation time: " + strResponse.time);
	            	}
	            	else if(strResponse===-1){
	            		alert("Response was invalid");
	            	}
	            	else{
	            		alert("Status: UNKNOWN");
	            	}
		         }
	        });
		}
	}

	function downloadCert(){
		$.ajax({
            type: "POST",
            url: "downloadCert/",
            complete: function(response){
           	 	alert("Certificate downloaded");
	         }
        });
	}
	</script>
</body>
</html>