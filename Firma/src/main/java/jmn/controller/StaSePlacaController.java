package jmn.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;

import jmn.annotation.PermissionType;
import jmn.configuration.AES;
import jmn.models.DEK;
import jmn.models.Faktura;
import jmn.models.PredlogPlacanja;
import jmn.models.Racun;
import jmn.models.StaSePlaca;
import jmn.pojo.UserData;
import jmn.services.DEKService;
import jmn.services.FakturaService;
import jmn.services.PredlogPlacanjaService;
import jmn.services.RacunService;
import jmn.services.StaSePlacaService;

@Controller
public class StaSePlacaController {

	@Autowired
	private RacunService rs;
	
	@Autowired
	private FakturaService fs;
	
	@Autowired
	private PredlogPlacanjaService pps;
	
	@Autowired
	private StaSePlacaService ssps;
	
	@Autowired
	private DEKService dks;
	
	@PermissionType("StaSePlaca:view")
	@RequestMapping(value="/staSePlaca/", method=RequestMethod.GET)
	public ResponseEntity<HashMap<String, Object>> listAllPlacanja(HttpSession session){
		UserData u = (UserData)session.getAttribute("user");
		int pb = u.getPreduzece().getPib();
		DEK kk = dks.findByPreduzece(u.getPreduzece());
		kk.setValue(AES.decryptDEK(kk.getValue(), Integer.toString(pb)));
		HashMap<String, Object> resp = new HashMap<String, Object>();
		List<Faktura> fakture = fs.findByVrstaFaktureAndPreduzece("ulazne", u.getPreduzece());
		
		List<PredlogPlacanja> predlozi = pps.findByPreduzece(u.getPreduzece());
		List<Racun> racuni = rs.findByPreduzece(u.getPreduzece());
		for(Racun r : racuni){
			r.setBrRacuna(AES.decryptCBC(r.getBrRacuna(), kk.getValue(), kk.getIv()));
		}
		resp.put("fakture", fakture);
		resp.put("predlozi", predlozi);
		resp.put("racuni", racuni);
		List<StaSePlaca> placanje = new ArrayList<StaSePlaca>();
		for(Faktura f : fakture){
			placanje.addAll(ssps.findByFaktura(f));
		}
		for(PredlogPlacanja p : predlozi){
			List<StaSePlaca> temp = ssps.findByPredlogPlacanja(p);
			for(StaSePlaca t : temp){
				if(!placanje.contains(t)){
					placanje.add(t);
				}
			}
		}
		
		for(StaSePlaca t : placanje){
			t.setSaKogRacuna(AES.decryptCBC(t.getSaKogRacuna(),kk.getValue(),kk.getIv()));
		}
		
		if(placanje.isEmpty())
			return new ResponseEntity<HashMap<String, Object>>(resp, HttpStatus.OK);
		resp.put("placanje", placanje);
		return new ResponseEntity<HashMap<String, Object>>(resp, HttpStatus.OK);
	}
	@PermissionType("StaSePlaca:add")
	@RequestMapping(value = "/staSePlaca/", method = RequestMethod.POST)
	    public ResponseEntity<Void> createPlacanje(HttpSession session, @RequestBody StaSePlaca placanje, UriComponentsBuilder ucBuilder) {
			UserData u = (UserData)session.getAttribute("user");
			int pb = u.getPreduzece().getPib();
			DEK kk = dks.findByPreduzece(u.getPreduzece());
			kk.setValue(AES.decryptDEK(kk.getValue(), Integer.toString(pb)));

			Faktura f = fs.findOne(placanje.getFaktura().getId());
			PredlogPlacanja p = pps.findOne(placanje.getPredlogPlacanja().getId());
			placanje.setPredlogPlacanja(p);
			placanje.setUlaznaFaktura(f);
			placanje.setSaKogRacuna(AES.encryptCBC(placanje.getSaKogRacuna(), kk.getValue(), kk.getIv()));
	        placanje = ssps.saveStaSePlaca(placanje);
	        try {
		        HttpHeaders headers = new HttpHeaders();
		        headers.setLocation(ucBuilder.path("/staSePlaca/{id}").buildAndExpand(placanje.getId()).toUri());
		        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	        }
	        catch(Exception e){
	        	return new ResponseEntity<Void>(HttpStatus.CONFLICT);
	        }
	    }
	@PermissionType("StaSePlaca:edit")
	@RequestMapping(value = "/staSePlaca/{id}", method = RequestMethod.POST)
	    public ResponseEntity<?> updatePlacanje(HttpSession session, @PathVariable("id") long id, @RequestBody StaSePlaca placanje) {
			UserData u = (UserData)session.getAttribute("user");
			int pb = u.getPreduzece().getPib();
			DEK kk = dks.findByPreduzece(u.getPreduzece());
			kk.setValue(AES.decryptDEK(kk.getValue(), Integer.toString(pb)));

			StaSePlaca exists = ssps.findOne(placanje.getId());
		 	if(exists == null){
		 		return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		 	}
		 	exists.setIznos(placanje.getIznos());
		 	exists.setPredlogPlacanja(placanje.getPredlogPlacanja());
		 	exists.setSaKogRacuna(AES.encryptCBC(placanje.getSaKogRacuna(),kk.getValue(), kk.getIv()));
		 	exists.setUlaznaFaktura(placanje.getFaktura());
		 	exists = ssps.saveStaSePlaca(exists);
		 	return new ResponseEntity<StaSePlaca>(exists, HttpStatus.OK);
	    }
	@PermissionType("StaSePlaca:delete")
	 @RequestMapping(value = "/staSePlaca/{id}", method = RequestMethod.DELETE)
	    public ResponseEntity<?> deletePlacanje(HttpSession session, @PathVariable("id") long id) {
		 StaSePlaca exists = ssps.findOne(id);
		 if(exists == null)
			 return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		 
		 ssps.delete(exists);
	     return new ResponseEntity<StaSePlaca>(HttpStatus.NO_CONTENT);
	    }
	 
	@PermissionType("StaSePlaca:search")
	@RequestMapping(value = "/staSePlaca/search/", method = RequestMethod.POST)
	    public ResponseEntity<?> searchPlacanje(HttpSession session, @RequestBody StaSePlaca placanje, UriComponentsBuilder ucBuilder) {
		List<StaSePlaca> lista = null;
		if(placanje.getFaktura() != null)
			lista = ssps.findByFaktura(placanje.getFaktura());
		else if (placanje.getPredlogPlacanja() != null)
			lista = ssps.findByPredlogPlacanja(placanje.getPredlogPlacanja());
		
        if(lista == null)
			 return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		 
	     return new ResponseEntity<List<StaSePlaca>>(lista, HttpStatus.OK);
    }

}
