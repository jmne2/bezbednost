package jmn.services;

import java.util.List;

import jmn.models.NalogZaPlacanje;
import jmn.models.PredlogPlacanja;

public interface NalogZaPlacanjeService {

	List<NalogZaPlacanje> findAll();

	NalogZaPlacanje saveNalogZaPlacanje(NalogZaPlacanje nzp);

	NalogZaPlacanje findOne(Long id);

	void delete(Long id);

	void delete(NalogZaPlacanje nzp);

	List<NalogZaPlacanje> findByUplatilacAndPrimalac(String uplatilac, String primalac);

	List<NalogZaPlacanje> findByUplatilacContainingAndSvrhaUplateContainingAndPrimalacContainingAndSifraPlacanjaContainingAndValutaContainingAndIznosAndBrModelaAndPozivNaBrojContaining(
			String uplatilac, String svrhaUplate, String primalac, String sifraPlacanja, String valuta, double iznos,
			int brModela, String pozivNaBroj);

	List<NalogZaPlacanje> findByUplatilacContainingAndSvrhaUplateContainingAndPrimalacContainingAndSifraPlacanjaContainingAndValutaContainingAndIznosAndBrModelaAndPozivNaBrojContainingAndZaPredlog(
			String uplatilac, String svrhaUplate, String primalac, String sifraPlacanja, String valuta, double iznos,
			int brModela, String pozivNaBroj, PredlogPlacanja zaPredlog);
}
