package izvod.data;

import java.sql.Date;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import annotations.SqlDateAdapter;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "nalogBanka", 
		namespace="http://izvod/data", 
		propOrder = {"idPoruke",
					"uplatilac",
					 "svrhaUplate",
					 "primalac",
					 "datumNaloga", 
					 "datumValute",
					 "racunUplatioca",
					 "modelZaduzenja",
					 "pozivNaBrojZaduzenja",
					 "racunPrimaoca",
					 "modelOdobrenja",
					 "pozivNaBrojOdobrenja",
					 "iznos", 
					 "oznakaValute", 
					 "hitno"})

public class NalogBanka implements java.io.Serializable{

	@XmlElement(name="idPoruke", required = true)
	@Size(min = 1, max = 50)
	private String idPoruke;
	
	@XmlElement(name="uplatilac", required = true)
	@Size(min = 1, max = 255)
	private String uplatilac;
	
	@XmlElement(name="svrhaUplate", required = true)
	@Size(min = 1, max = 255)
	private  String svrhaUplate;
	
	@XmlElement(name="primalac", required = true)
	@Size(min = 1, max = 255)
	private String primalac;
	
	@XmlElement(name="datumNaloga", required = true)
	@XmlJavaTypeAdapter(SqlDateAdapter.class)
	private Date datumNaloga;
	
	@XmlElement(name="datumValute", required = true)
	@XmlJavaTypeAdapter(SqlDateAdapter.class)
	private Date datumValute;
	
	@XmlElement(name="racunUplatioca", required = true)
	@Size(min = 18, max = 18)
	private String racunUplatioca;
	
	@XmlElement(name="modelZaduzenja", required = true)
	@Digits(integer = 2, fraction = 0)									
	private int modelZaduzenja;
	
	@XmlElement(name="pozivNaBrojZaduzenja", required = true)
	@Size(min = 1, max = 20)
	private String pozivNaBrojZaduzenja;
	
	@XmlElement(name="racunPrimaoca", required = true)
	@Size(min = 18, max = 18)
	private String racunPrimaoca;

	@XmlElement(name="modelOdobrenja", required = true)
	@Digits(integer = 2, fraction = 0)								
	private int modelOdobrenja;
	
	@XmlElement(name="pozivNaBrojOdobrenja", required = true)
	@Size(min = 1, max = 20)
	private String pozivNaBrojOdobrenja;

	@XmlElement(name="iznos", required = true)
	@Digits(integer = 15, fraction = 2)
	private double iznos;

	@XmlElement(name="oznakaValute", required = true)
	@Size(min = 3, max = 3)
	private String oznakaValute;

	@XmlElement(name="hitno", required = true)
	private boolean hitno;

	public NalogBanka() {
	}

	
	public NalogBanka(String idPoruke, String uplatilac, String svrhaUplate, String primalac, Date datumNaloga,
			Date datumValute, String racunUplatioca, int modelZaduzenja, String pozivNaBrojZaduzenja,
			String racunPrimaoca, int modelOdobrenja, String pozivNaBrojOdobrenja, 
			double iznos, String oznakaValute, boolean hitno) {
		super();
		this.idPoruke = idPoruke;
		this.uplatilac = uplatilac;
		this.svrhaUplate = svrhaUplate;
		this.primalac = primalac;
		this.datumNaloga = datumNaloga;
		this.datumValute = datumValute;
		this.racunUplatioca = racunUplatioca;
		this.modelZaduzenja = modelZaduzenja;
		this.pozivNaBrojZaduzenja = pozivNaBrojZaduzenja;
		this.racunPrimaoca = racunPrimaoca;
		this.modelOdobrenja = modelOdobrenja;
		this.pozivNaBrojOdobrenja = pozivNaBrojOdobrenja;
		this.iznos = iznos;
		this.oznakaValute = oznakaValute;
		this.hitno = hitno;
	}


	public String getUplatilac() {
		return uplatilac;
	}

	public void setUplatilac(String uplatilac) {
		this.uplatilac = uplatilac;
	}

	public String getSvrhaUplate() {
		return svrhaUplate;
	}

	public void setSvrhaUplate(String svrhaUplate) {
		this.svrhaUplate = svrhaUplate;
	}

	public String getPrimalac() {
		return primalac;
	}

	public void setPrimalac(String primalac) {
		this.primalac = primalac;
	}

	public double getIznos() {
		return iznos;
	}

	public void setIznos(double iznos) {
		this.iznos = iznos;
	}

	public String getRacunPrimaoca() {
		return racunPrimaoca;
	}

	public void setRacunPrimaoca(String racunPrimaoca) {
		this.racunPrimaoca = racunPrimaoca;
	}



	public String getIdPoruke() {
		return idPoruke;
	}


	public void setIdPoruke(String idPoruke) {
		this.idPoruke = idPoruke;
	}


	public Date getDatumNaloga() {
		return datumNaloga;
	}


	public void setDatumNaloga(Date datumNaloga) {
		this.datumNaloga = datumNaloga;
	}


	public Date getDatumValute() {
		return datumValute;
	}


	public void setDatumValute(Date datumValute) {
		this.datumValute = datumValute;
	}


	public String getRacunUplatioca() {
		return racunUplatioca;
	}


	public void setRacunUplatioca(String racunUplatioca) {
		this.racunUplatioca = racunUplatioca;
	}


	public int getModelZaduzenja() {
		return modelZaduzenja;
	}


	public void setModelZaduzenja(int modelZaduzenja) {
		this.modelZaduzenja = modelZaduzenja;
	}


	public String getPozivNaBrojZaduzenja() {
		return pozivNaBrojZaduzenja;
	}


	public void setPozivNaBrojZaduzenja(String pozivNaBrojZaduzenja) {
		this.pozivNaBrojZaduzenja = pozivNaBrojZaduzenja;
	}


	public int getModelOdobrenja() {
		return modelOdobrenja;
	}


	public void setModelOdobrenja(int modelOdobrenja) {
		this.modelOdobrenja = modelOdobrenja;
	}


	public String getPozivNaBrojOdobrenja() {
		return pozivNaBrojOdobrenja;
	}


	public void setPozivNaBrojOdobrenja(String pozivNaBrojOdobrenja) {
		this.pozivNaBrojOdobrenja = pozivNaBrojOdobrenja;
	}


	public String getOznakaValute() {
		return oznakaValute;
	}


	public void setOznakaValute(String oznakaValute) {
		this.oznakaValute = oznakaValute;
	}


	public boolean isHitno() {
		return hitno;
	}


	public void setHitno(boolean hitno) {
		this.hitno = hitno;
	}


	public String toString() {
    /*    StringBuffer buffer = new StringBuffer();
        buffer.append(uplatilac);
        buffer.append(" ");
        buffer.append(svrhaUplate);
        buffer.append(" ");
        buffer.append(primalac);
        buffer.append(" ");
        buffer.append(sifraPlacanja);
        buffer.append(" ");
        buffer.append(valuta);
        buffer.append(" ");
        buffer.append(iznos);
        buffer.append(" ");
        buffer.append(racunPrimaoca);
        buffer.append(" ");

        return buffer.toString();*/
		return "blaaa toString() ";
    }

}
