package certificateApp.util;

import java.lang.reflect.Method;
import java.util.Set;

import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response.Status;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import certificateApp.annotations.Authenticate;
import certificateApp.annotations.PermissionType;
import certificateApp.models.Permission;
import certificateApp.models.Role;
import certificateApp.models.User;
import certificateApp.service.RoleService;

@Interceptor
@Authenticate
public class AuthenticationInterceptor implements HandlerInterceptor{

	public AuthenticationInterceptor() {
		super();
		System.out.println("=====================");
		System.out.println("Created Interceptor");
	}

	private static Logger log = Logger.getLogger(AuthenticationInterceptor.class);

	@Context
	private HttpServletRequest request;
	@Autowired
	private RoleService rs;
	
/*	@AroundInvoke
	public Object intercept(InvocationContext context) throws Exception{
		
		User user = (User) request.getSession().getAttribute("user");
		log.info("user: "+user);
		if (user == null) {
			throw new ServiceException("Not logged in", Status.UNAUTHORIZED);
		}	
		System.out.println("INTERCEPTING");
		Method method = context.getMethod();
		if(method.isAnnotationPresent(PermissionType.class)){
			PermissionType annotation = method.getAnnotation(PermissionType.class);
			String permission = annotation.value(); 
			System.out.println("Permisija potrebna : " + permission);
			Set<Role> userRoles = user.getAllowed();
			for(Role role : userRoles){
				System.out.println("Rola " + role.getName());
				Set<Permission> userPerm = role.getPermissions();
				for(Permission perm : userPerm){
					System.out.println("Permisija " + perm.getName());
					if(permission.equals(perm.getName())){
						Object result = context.proceed();
						return result;
					}
						
				}
			}
			throw new ServiceException("Not authorized", Status.UNAUTHORIZED);
		}
		
		Object result = context.proceed();
		return result;
	}
*/
	@Override
	public void afterCompletion(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, Exception arg3)
			throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, ModelAndView arg3)
			throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
 
	/*	if(request.getRequestURI().toString().equals("/login/")){
			System.out.println("IN LOGIN");
			return true;
		}*/
		
		User user = (User) request.getSession().getAttribute("user");
		log.info("user: "+user);
		if (user == null) {
			throw new ServiceException("Not logged in", Status.UNAUTHORIZED);
		}	
		System.out.println("INTERCEPTING");
		//Method method = arg2.getMethod();
		if (handler instanceof HandlerMethod) {
		    HandlerMethod method = (HandlerMethod) handler;
		    if (method.getMethod().isAnnotationPresent(PermissionType.class)) {
		    	PermissionType annotation = method.getMethodAnnotation(PermissionType.class);
				String permission = annotation.value(); 
				System.out.println("Permisija potrebna : " + permission);
				Set<Role> userRoles = user.getAllowed();
				if(userRoles==null){
					System.out.println("Nulllll");
				}
				for(Role role : userRoles){
					System.out.println("Rola " + role.getName());
					Set<Permission> userPerm = role.getPermissions();
					for(Permission perm : userPerm){
						System.out.println("Permisija " + perm.getName());
						if(permission.equals(perm.getName())){
							//Object result = context.proceed();
							return true;
						}
							
					}
				}
				throw new ServiceException("Not authorized", Status.UNAUTHORIZED);
		    }
		}
//		if(method.isAnnotationPresent(PermissionType.class)){
//			PermissionType annotation = method.getAnnotation(PermissionType.class);
//			String permission = annotation.value(); 
//			System.out.println("Permisija potrebna : " + permission);
//			Set<Role> userRoles = user.getAllowed();
//			for(Role role : userRoles){
//				System.out.println("Rola " + role.getName());
//				Set<Permission> userPerm = role.getPermissions();
//				for(Permission perm : userPerm){
//					System.out.println("Permisija " + perm.getName());
//					if(permission.equals(perm.getName())){
//						//Object result = context.proceed();
//						return true;
//					}
//						
//				}
//			}
//			throw new ServiceException("Not authorized", Status.UNAUTHORIZED);
//		}
		
		return false;
	}

	

}