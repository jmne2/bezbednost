package jmn.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;

import jmn.annotation.PermissionType;
import jmn.models.Banka;
import jmn.models.SifrarnikMetoda;
import jmn.pojo.UserData;
import jmn.services.BankaService;

@Controller
public class BankaController {

	final static Logger logger = Logger.getLogger(BankaController.class);	
	
	@Autowired
	private BankaService bs;

	@PermissionType("Banke:view")
	@RequestMapping(value="/banke/", method=RequestMethod.GET)
	public ResponseEntity<List<Banka>> fetchBanke(HttpSession session){
		List<Banka> banke = bs.findAll();
		if(banke.isEmpty())
			return new ResponseEntity<List<Banka>>(HttpStatus.NO_CONTENT);
		return new ResponseEntity<List<Banka>>(banke, HttpStatus.OK);
	}
	
	@PermissionType("Banke:add")
	@RequestMapping(value = "/banke/", method = RequestMethod.POST)
	public ResponseEntity<Void> createBanku(HttpSession session, @RequestBody Banka banka, UriComponentsBuilder ucBuilder) {
			UserData u = (UserData)session.getAttribute("user");
	        Banka b = bs.saveBanka(banka);
	        try {
		        HttpHeaders headers = new HttpHeaders();
		        headers.setLocation(ucBuilder.path("/banke/{id}").buildAndExpand(b.getId()).toUri());
		        logger.warn("Korisnik "+ u.getId() + " uspesno je izvrsio metodu " + SifrarnikMetoda.methods.get(
		 				Thread.currentThread().getStackTrace()[1].getMethodName()));
		        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	        }
	        catch(Exception e){
	        	logger.error("Greska pri izvrsenju " + SifrarnikMetoda.methods.get(Thread.currentThread().getStackTrace()[1].getMethodName())
	        	+ " za korisnika " + u.getId());
	        	return new ResponseEntity<Void>(HttpStatus.CONFLICT);
	        }
	    }
	@PermissionType("Banke:edit")
	 @RequestMapping(value = "/banke/{id}", method = RequestMethod.POST)
	    public ResponseEntity<?> updateBanku(HttpSession session, @PathVariable("id") long id, @RequestBody Banka banka) {
			UserData u = (UserData)session.getAttribute("user");
		 	Banka exists = bs.findOne(id);
		 	if(exists == null){
		 		logger.error("Greska pri izvrsenju " + SifrarnikMetoda.methods.get(Thread.currentThread().getStackTrace()[1].getMethodName())
	        	+ " za korisnika " + u.getId());
		 		return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		 	}
		 	exists.setNaziv(banka.getNaziv());
		 	
		 	exists = bs.saveBanka(exists);
		 	logger.warn("Korisnik "+ u.getId() + " uspesno je izvrsio metodu " + SifrarnikMetoda.methods.get(
	 				Thread.currentThread().getStackTrace()[1].getMethodName()));
	        
		 	return new ResponseEntity<Banka>(exists, HttpStatus.OK);
	    }
	
	@PermissionType("Banke:delete")
	@RequestMapping(value = "/banke/{id}", method = RequestMethod.DELETE)
	    public ResponseEntity<?> deleteBanku(HttpSession session, @PathVariable("id") long id) {
		 	UserData u = (UserData)session.getAttribute("user");
			Banka exists = bs.findOne(id);
			if(exists == null){
				logger.error("Greska pri izvrsenju " + SifrarnikMetoda.methods.get(Thread.currentThread().getStackTrace()[1].getMethodName())
	        	+ " za korisnika " + u.getId());
				return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
			}
			bs.delete(id);
			logger.warn("Korisnik "+ u.getId() + " uspesno je izvrsio metodu " + SifrarnikMetoda.methods.get(
	 				Thread.currentThread().getStackTrace()[1].getMethodName()));
	        
			return new ResponseEntity<Banka>(HttpStatus.NO_CONTENT);
	    }
	
	@PermissionType("Banke:search")
	@RequestMapping(value = "/banke/search/", method = RequestMethod.POST)
	    public ResponseEntity<?> searchBanke(HttpSession session, @RequestBody Banka banka) { 
		if(banka.getNaziv() == null)
			 banka.setNaziv("");

		 List<Banka> banke = bs.findByNazivContaining(banka.getNaziv());
		 if(banke == null)
			 return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);

	     return new ResponseEntity<List<Banka>>(banke, HttpStatus.OK);
	    }

}
