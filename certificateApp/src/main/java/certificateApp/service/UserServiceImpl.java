package certificateApp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import certificateApp.models.User;
import certificateApp.repository.UserRepository;

@Service
@Transactional
public class UserServiceImpl implements UserService{

	@Autowired
	private UserRepository ur ;
	
	@Override
	public List<User> findAll() {
		return (List<User>) ur.findAll();
	}

	@Override
	public User saveUser(User user) {
		return ur.save(user);
	}

	@Override
	public User findOne(long id) {
		return ur.findOne(id);
	}

	@Override
	public User findByUsernameAndPassword(String username, String password) {
		return ur.findByUsernameAndPassword(username, password);
	}

	@Override
	public void delete(User user) {
		ur.delete(user);
	}


	@Override
	public User findByName(String name) {
		return ur.findByName(name);
	}

	@Override
	public User findByUsername(String username) {
		return ur.findByUsername(username);
	}

}
