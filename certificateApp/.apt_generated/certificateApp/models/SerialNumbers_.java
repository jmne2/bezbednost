package certificateApp.models;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(SerialNumbers.class)
public abstract class SerialNumbers_ {

	public static volatile SingularAttribute<SerialNumbers, String> serialNumber;
	public static volatile SingularAttribute<SerialNumbers, String> alias;
	public static volatile SingularAttribute<SerialNumbers, Long> Id;
	public static volatile SingularAttribute<SerialNumbers, String> username;

}

