'use strict';

angular.module('repo3App').controller('NalogController', ['$scope','$uibModal', 'NalogService', function($scope,$uibModal,NalogService) {
    var self = this;
    self.predlog = {};
    self.predlozi = [];
    self.racun = {};
    self.racuni = [];
    self.nalog = {};
    self.nalozi = [];
    self.show = false;
    self.logged = false;
    self.children = [];
    self.valute = ["RSD"];
    self.us = {};
    
    self.submit = submit;
    self.edit = edit;
    self.remove = remove;
    self.reset = reset;
    self.search = search;
    self.currState = "edit";
    self.showPredloge = showPredloge;
    self.showRacune = showRacune;
    self.sendNalog = sendNalog;
    self.indSelected = -1;
    $scope.indSelectedVote = -1;
    $scope.required = true;
    
    $scope.$on('dispAfterLog', function(event, args) {
    	if(self === args.ctrl){
    		self.logged = true;
    		self.show = true;
    		self.us = args.info;
    		if(args.parent != null){
    			self.nalog.zaPredlog = args.parent.predlozi[args.parent.indSelected];
    			search(self.nalog);
    		} else {
    			fetchAllNaloge();
    		}
    	} else {
    		self.logged = false;
    		self.show = false;
    	}
    });
    
    $scope.$on('changeState', function(event, args) {    	
    	if(self === args.ctrl){
    		self.currState = args.state;
    		$scope.required = (self.currState === "search") ? false : true;
    		self.nalog = {};
    	}
    });
    $scope.$on('hide', function(event, args) {
    	self.show = false;
    	
    });
    $scope.$on('resetEvent', function(event, args) {    	
    	if(self === args.ctrl){
    		self.currState = "edit";
    		fetchAllNaloge();
    	}
    });
    $scope.$on('changeSelection', function(event, args) {    	
    	if(self === args.ctrl){
    		switch (args.direction) {
	            case 'prev':
	                prev();
	                break;
	            case 'next':
	                next();
	                break;
	            case 'first':
	            	first();
	            	break;
	            case 'last':
	            	last();
	            	break;
    		}
    	}
    });
    
    $scope.$on('deleteSel', function(event, args) {    	
    	if(self === args.ctrl){
    		if($scope.indSelectedVote != -1){
    			var selected = self.nalozi[$scope.indSelectedVote];
    			remove(selected.id);
    			self.currState = "edit";
    		}
    	}
    });
    
    function first(){
    	$scope.indSelectedVote = 0;
    	self.indSelected = $scope.indSelectedVote;
    }
    function prev(){
    	if(($scope.indSelectedVote != 0) && ($scope.indSelectedVote != -1)){
    		$scope.indSelectedVote = $scope.indSelectedVote - 1;
    		self.indSelected = $scope.indSelectedVote;
    	}
    }
    
    function next(){
    	if(($scope.indSelectedVote != self.nalozi.length) && ($scope.indSelectedVote != -1)){
    		$scope.indSelectedVote = $scope.indSelectedVote + 1;
    		self.indSelected = $scope.indSelectedVote;
    	}
    }
    
    function last(){
    	$scope.indSelectedVote = self.nalozi.length - 1;
    	self.indSelected = $scope.indSelectedVote;
    }
 
    $scope.indSelectedVote = null;
    $scope.setSelected = function (indSelectedVote) {
       $scope.indSelectedVote = indSelectedVote;
       self.indSelected = $scope.indSelectedVote;
       if(self.currState === "edit"){
    	   var id = self.nalozi[indSelectedVote].id;
    	   edit(id);
   		}
    };
   
    function showPredloge() {

        var modalInstance = $uibModal.open({
          templateUrl: 'myModalContent.html',
          controller: ModalInstanceCtrl,
          resolve: {
            items: function () {
              return self.predlozi;
            },
            type: function(){
            	  return "Predloge za placanje";
              }
          }
        });

        modalInstance.result.then(function (selectedItem) {
          self.predlog = self.predlozi[selectedItem];
        }, function () {
        });
      };
      
      function showRacune() {

          var modalInstance = $uibModal.open({
            templateUrl: 'myModalContent.html',
            controller: ModalInstanceCtrl,
            resolve: {
              items: function () {
                return self.racuni;
              },
              type: function(){
              	  return "Racune Partnera";
                }
            }
          });

          modalInstance.result.then(function (selectedItem) {
            self.racun = self.racuni[selectedItem];
          }, function () {
          });
        };
    
    function sendNalog(id){
    	NalogService.sendNalog(id)
    	.then(function(d) {
            },
            function(errResponse){
                console.error('Error while fetching Receits');
            }
        );
    }    
        
    function fetchAllNaloge(){
    	NalogService.fetchAllNaloge()
            .then(
            function(d) {
            	self.nalozi = [];
                self.predlozi = d["predlozi"];
                self.nalozi = d["nalozi"];
                self.racuni = d["racuni"];
            },
            function(errResponse){
                console.error('Error while fetching Receits');
            }
        );
    }
 
    function createNalog(nalog){
    	NalogService.createNalog(nalog)
            .then(
            fetchAllNaloge,
            function(errResponse){
                console.error('Error while creating User');
            }
        );
    }
 
    function updateNalog(nalog, id){
    	NalogService.updateNalog(nalog, id)
            .then(
            fetchAllNaloge,
            function(errResponse){
                console.error('Error while updating User');
            }
        );
    }
 
    function deleteNalog(id){
    	NalogService.deleteNalog(id)
            .then(
            fetchAllNaloge,
            function(errResponse){
                console.error('Error while deleting User');
            }
        );
    }
 
    function submit() {
    	if (self.currState === "add"){
    		if(self.predlog.id != null && !angular.isUndefined(self.predlog.id) && self.racun.id != null && 
    				!angular.isUndefined(self.racun.id) ){
    			if (angular.isUndefined(self.nalog.valuta)){ self.nalog.valuta = self.valute[0];}
    			if (angular.isUndefined(self.nalog.hitno)){ self.nalog.hitno = false;}
    			self.nalog.uplatilac = self.us.preduzece.naziv;
	            self.nalog.zaPredlog = self.predlog;
	            self.nalog.racunPrimaoca = self.racun;
	            createNalog(self.nalog);
    		}
        }else if (self.currState === "edit"){
        	if(self.predlog.id != null && !angular.isUndefined(self.predlog.id) && self.racun.id != null && 
    				!angular.isUndefined(self.racun.id)){
        		if (angular.isUndefined(self.nalog.valuta)){ self.nalog.valuta = self.valute[0];}
        		if (angular.isUndefined(self.nalog.hitno)){ self.nalog.hitno = false;}
        		self.nalog.uplatilac = self.us.preduzece.naziv;
        		self.nalog.zaPredlog = self.predlog;
	            self.nalog.racunPrimaoca = self.racun;
	            updateNalog(self.nalog, self.nalog.id);
    		}
        } else if (self.currState === "search"){
        	self.nalog.uplatilac = self.us.preduzece.naziv;
            self.nalog.zaPredlog = self.predlog;
            self.nalog.racunPrimaoca = self.racun;
        	search(self.nalog);
        }
	        reset();
    }
 
    function edit(id){
        for(var i = 0; i < self.nalozi.length; i++){
            if(self.nalozi[i].id === id) {
                self.nalog = angular.copy(self.nalozi[i]);
                self.predlog = self.nalog.zaPredlog;
                self.racun = self.nalog.racunPrimaoca;
                break;
            }
        }
    }
 
    function remove(id){
        if(self.nalog.id === id) {//clean form if the user to be deleted is shown there.
            reset();
        }
        deleteNalog(id);
    }
 
 
    function reset(){
        self.recun={};
        self.predlog= {};
        self.nalog = {};
        $scope.myForm.$setPristine(); //reset Form
    }
    
    function search(nalog){
    	NalogService.searchNaloge(nalog)
        .then(
        function(d) {
        	self.nalozi = d;
            $scope.indSelectedVote = -1;
            self.indSelected = $scope.indSelectedVote;
        },
        function(errResponse){
            console.error('Error while fetching Users');
        }
        );
    }
    
    var ModalInstanceCtrl = function ($scope, $uibModalInstance, items, type) {

    	  $scope.items = items;
    	  $scope.selected = {
    	    item: -1
    	  };

    	  $scope.type = type;
    	  
    	  $scope.ok = function () {
    		  $uibModalInstance.close($scope.selected.item);
    	  };

    	  $scope.cancel = function () {
    		  $uibModalInstance.dismiss('cancel');
    	  };
    	};
}]);