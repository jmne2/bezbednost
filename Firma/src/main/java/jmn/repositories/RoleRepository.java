package jmn.repositories;

import org.springframework.data.repository.CrudRepository;

import jmn.models.Role;

public interface RoleRepository extends CrudRepository<Role, Long>{

	public Role findByNaziv(String naziv);
}
