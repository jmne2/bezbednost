<div class="generic-container" ng-controller="FakturaController as ctrl" id="Fakture">
      
  <!-- DUGMICI ZA FAKTURE -->    
      <div id="dugmici" class="centered" style="display: inline-block; align-self: center;  padding-top: 150px; padding-left: 400px;" ng-show="ctrl.dugmici" >
		 <button type="button" class="btn btn-w3r" ng-click="ctrl.showUlazne()">Ulazne fakture</button> 
		 <button type="button" class="btn btn-w3r" ng-click="ctrl.showIzlazne()">Izlazne fakture</button> 
      </div>
   <!-- ULAZNE FAKTURE -->   
       <div id="ulazneFakture" ng-show="ctrl.ufaktureshow"  >
          <div class="panel panel-default">
                <!-- Default panel contents -->
                
                <div class="panel-heading"><span class="lead">Lista ulaznih faktura </span></div>
              <div class="tablecontainer">
                  <table class="table table-hover">
                      <thead>
                          <tr>
                              <th>ID.</th>
                              <th>Broj fakture</th>
                              <th>Datum fakture</th>
                              <th>Datum valute</th>
                              <th>Vrednost robe</th>
                              <th>Vrednost usluga</th>
                              <th>Vrednost roba i usluga</th>
                              <th>Ukupan rabat</th>
                              <th>Ukupan porez</th>
                              <th>Oznaka valute</th>
                              <th>Ukupan iznos</th>
                              <th>Preostali iznos</th>
                              <th>Poslovna godina</th>
                              <th>Poslovni partner</th>
                              <th>Racun partnera</th>
                          </tr>
                      </thead>
                      <tbody>
                      <!-- ng-click="setSelected(d.id)" ng-class="{selected: d.id === idSelectedVote}" -->
                          <tr ng-repeat="u in ctrl.ufakture" ng-click="setSelected($index)" ng-class="{selected: $index === indSelectedVote}">
                              <td><span ng-bind="u.id"></span></td>
                              <td><span ng-bind="u.brojFakture"></span></td>
                              <td><span ng-bind="u.datumFakture"></span></td>
                              <td><span ng-bind="u.datumValute"></span></td>
                              <td><span ng-bind="u.vrednostRobe"></span></td>
                              <td><span ng-bind="u.vrednostUsluga"></span></td>
                              <td><span ng-bind="u.ukupnoRobaIUsluge"></span></td>
                              <td><span ng-bind="u.ukupanRabat"></span></td>
                              <td><span ng-bind="u.ukupanPorez"></span></td>
                              <td><span ng-bind="u.oznakaValute"></span></td>
                              <td><span ng-bind="u.ukIznos"></span></td>
                              <td><span ng-bind="u.preostaliIznos"></span></td>
                              <td><span ng-bind="u.poslovnaGodina.godina"></span></td>
                              <td><span ng-bind="u.poslovniPartner.naziv"></span></td>
                              <td><span ng-bind="u.racunPartnera.brRacuna"></span></td>
                          </tr>
                      </tbody>
                  </table>
              </div>
          </div>
          
              
          <div class="panel panel-default">
              <div class="formcontainer">
                  <form ng-submit="ctrl.submit()" name="myForm" class="form-horizontal">
                      <input type="hidden" ng-model="ctrl.ufaktura.id" />
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="brojFakture">Broj fakutre</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="ctrl.ufaktura.brojFakture" id="brojFakture" class="oznaka form-control input-sm" ng-disabled="disableForm" >
                              </div>
                          </div>
                      </div>
                                                
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="datumFakture">Datum fakture</label>
                              <div class="col-md-7">
                              <input type="text" class="form-control" uib-datepicker-popup="dd-MMMM-yyyy" ng-model="ctrl.ufaktura.datumFakture" ng-disabled="disableForm" />
                              <span class="input-group-btn">
					            <button type="button" class="btn btn-default" ng-click="open1()" ng-disabled="disableForm" ><i class="glyphicon glyphicon-calendar"></i></button>
					          </span>
					           </div>
                          </div>
                      </div>
                      
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="datumValute">Datum valute</label>
                              <div class="col-md-7">
                              <input type="text" class="form-control" uib-datepicker-popup="dd-MMMM-yyyy" ng-model="ctrl.ufaktura.datumValute" ng-disabled="disableForm"/>
                              <span class="input-group-btn">
					            <button type="button" class="btn btn-default" ng-click="open1()" ng-disabled="disableForm" ><i class="glyphicon glyphicon-calendar"></i></button>
					          </span>
					           </div>
                          </div>
                      </div>
 					<div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="vrednostRobe">Vrednost robe</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="ctrl.ufaktura.vrednostRobe" id="vrednostRobe" class="oznaka form-control input-sm" ng-disabled="disableForm" >
                              </div>
                          </div>
                      </div>
 					
 					
 					 <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="vrednostUsluga">Vrednost usluga</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="ctrl.ufaktura.vrednostUsluga" id="vrednostUsluga" class="oznaka form-control input-sm" ng-disabled="disableForm"/>
                              </div>
                          </div>
                      </div>
                      
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="ukupnoRobaIUsluge">Ukupno roba i usluge</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="ctrl.ufaktura.ukupnoRobaIUsluge" id="ukupnoRobaIUsluge" class="oznaka form-control input-sm" ng-disabled="disableForm"/>
                              </div>
                          </div>
                      </div>
                      
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="ukupanRabat">Ukupan rabat</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="ctrl.ufaktura.ukupanRabat" id="ukupanRabat" class="oznaka form-control input-sm" ng-disabled="disableForm"/>
                              </div>
                          </div>
                      </div>
                      
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="ukupanPorez">Ukupan porez</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="ctrl.ufaktura.ukupanPorez" id="ukupanPorez" class="oznaka form-control input-sm" ng-disabled="disableForm"/>
                              </div>
                          </div>
                      </div>
 					
 					<div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="oznakaValute">Oznaka valute</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="ctrl.ufaktura.oznakaValute" id="oznakaValute" class="oznaka form-control input-sm" ng-disabled="disableForm"/>
                              </div>
                          </div>
                      </div>

 					<div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="ukIznos">Ukupan iznos</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="ctrl.ufaktura.ukIznos" id="ukIznos" class="oznaka form-control input-sm" ng-disabled="disableForm"/>
                              </div>
                          </div>
                      </div>
                      
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="preostaliIznos">Preostali iznos</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="ctrl.ufaktura.preostaliIznos" id="preostaliIznos" class="oznaka form-control input-sm" ng-disabled="disableForm"/>
                              </div>
                          </div>
                      </div>
                      
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="poslovnaGodina">Poslovna godina</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="ctrl.ufaktura.poslovnaGodina.godina" id="poslovnaGodina" class="oznaka form-control input-sm" ng-disabled="disableForm"/>
                              </div>
                          </div>
                      </div>
                      
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="poslovniPartner">Poslovni partner</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="ctrl.ufaktura.poslovniPartner.naziv" id="poslovniPartner" class="oznaka form-control input-sm" ng-disabled="disableForm"/>
                              </div>
                          </div>
                      </div>
                      
                        <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="racunPartnera">Racun partnera</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="ctrl.ufaktura.racunPartnera.brRacuna" id="racunPartnera" class="oznaka form-control input-sm" ng-disabled="disableForm"/>
                              </div>
                          </div>
                      </div>
                      
                      <div class="row">
                          <div class="form-actions floatRight">
                              <input type="submit" id ="subId" value="Commit" class="btn btn-primary btn-sm" ng-disabled="myForm.$invalid">
                              <button type="button" ng-click="ctrl.reset()" class="btn btn-warning btn-sm" ng-disabled="myForm.$pristine">Rollback</button>
                              <button type="button" ng-click="ctrl.back()" class="btn btn-primary btn-sm">Back</button>
                              <label>{{ctrl.currState}}</label>
                          </div>
                      </div>
                  </form>
              </div>
          </div>
      </div>
      
      <!-- IZLAZNE FAKTURE -->   
       <div id="izlazneFakture" ng-show="ctrl.ifaktureshow" >
          <div class="panel panel-default">
                <!-- Default panel contents -->
                
                <div class="panel-heading"><span class="lead">Lista izlaznih faktura </span></div>
              <div class="tablecontainer">
                  <table class="table table-hover">
                      <thead>
                          <tr>
                              <th>ID.</th>
                              <th>Broj fakture</th>
                              <th>Datum fakture</th>
                              <th>Datum valute</th>
                              <th>Vrednost robe</th>
                              <th>Vrednost usluga</th>
                              <th>Vrednost roba i usluga</th>
                              <th>Ukupan rabat</th>
                              <th>Ukupan porez</th>
                              <th>Oznaka valute</th>
                              <th>Ukupan iznos</th>
                              <th>Preostali iznos</th>
                              <th>Poslovna godina</th>
                              <th>Poslovni partner</th>
                              <th>Racun partnera</th>
                          </tr>
                      </thead>
                      <tbody>
                      <!-- ng-click="setSelected(d.id)" ng-class="{selected: d.id === idSelectedVote}" -->
                          <tr ng-repeat="i in ctrl.ifakture" ng-click="setSelected($index)" ng-class="{selected: $index === indSelectedVote}">
                              <td><span ng-bind="i.id"></span></td>
                              <td><span ng-bind="i.brojFakture"></span></td>
                              <td><span ng-bind="i.datumFakture"></span></td>
                              <td><span ng-bind="i.datumValute"></span></td>
                              <td><span ng-bind="i.vrednostRobe"></span></td>
                              <td><span ng-bind="i.vrednostUsluga"></span></td>
                              <td><span ng-bind="i.ukupnoRobaIUsluge"></span></td>
                              <td><span ng-bind="i.ukupanRabat"></span></td>
                              <td><span ng-bind="i.ukupanPorez"></span></td>
                              <td><span ng-bind="i.oznakaValute"></span></td>
                              <td><span ng-bind="i.ukIznos"></span></td>
                              <td><span ng-bind="i.preostaliIznos"></span></td>
                              <td><span ng-bind="i.poslovnaGodina.godina"></span></td>
                              <td><span ng-bind="i.poslovniPartner.naziv"></span></td>
                              <td><span ng-bind="i.racunPartnera.brRacuna"></span></td>
                          </tr>
                      </tbody>
                  </table>
              </div>
          </div>
          
              
          <div class="panel panel-default">
              <div class="formcontainer">
                  <form ng-submit="ctrl.submit()" name="myForm" class="form-horizontal">
                      <input type="hidden" ng-model="ctrl.ufaktura.id" />
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="brojFakture">Broj fakutre</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="ctrl.ifaktura.brojFakture" id="brojFakture" class="oznaka form-control input-sm" ng-disabled="ctrl.disableForm" >
                              </div>
                          </div>
                      </div>
                                                
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="datumFakture">Datum fakture</label>
                              <div class="col-md-7">
                              <input type="text" class="form-control" uib-datepicker-popup="dd-MMMM-yyyy" ng-model="ctrl.ifaktura.datumFakture" ng-disabled="ctrl.disableForm" />
					          <span class="input-group-btn">
					            <button type="button" class="btn btn-default" ng-click="open1()" ng-disabled="disableForm" ><i class="glyphicon glyphicon-calendar"></i></button>
					          </span>
                              </div>
                          </div>
                      </div>
                      
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="datumValute">Datum valute</label>
                              <div class="col-md-7">
                              <input type="text" class="form-control" uib-datepicker-popup="dd-MMMM-yyyy" ng-model="ctrl.ifaktura.datumValute" ng-disabled="ctrl.disableForm"/>
					          <span class="input-group-btn">
					            <button type="button" class="btn btn-default" ng-click="open1()" ng-disabled="disableForm" ><i class="glyphicon glyphicon-calendar"></i></button>
					          </span>
                              </div>
                          </div>
                      </div>
 					<div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="vrednostRobe">Vrednost robe</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="ctrl.ifaktura.vrednostRobe" id="vrednostRobe" class="oznaka form-control input-sm" >
                              </div>
                          </div>
                      </div>
 					
 					
 					 <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="vrednostUsluga">Vrednost usluga</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="ctrl.ifaktura.vrednostUsluga" id="vrednostUsluga" class="oznaka form-control input-sm" ng-disabled="ctrl.disableForm"/>
                              </div>
                          </div>
                      </div>
                      
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="ukupnoRobaIUsluge">Ukupno roba i usluge</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="ctrl.ifaktura.ukupnoRobaIUsluge" id="ukupnoRobaIUsluge" class="oznaka form-control input-sm" ng-disabled="ctrl.disableForm"/>
                              </div>
                          </div>
                      </div>
                      
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="ukupanRabat">Ukupan rabat</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="ctrl.ifaktura.ukupanRabat" id="ukupanRabat" class="oznaka form-control input-sm" ng-disabled="ctrl.disableForm"/>
                              </div>
                          </div>
                      </div>
                      
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="ukupanPorez">Ukupan porez</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="ctrl.ifaktura.ukupanPorez" id="ukupanPorez" class="oznaka form-control input-sm" ng-disabled="ctrl.disableForm"/>
                              </div>
                          </div>
                      </div>
 					
 					<div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="oznakaValute">Oznaka valute</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="ctrl.ifaktura.oznakaValute" id="oznakaValute" class="oznaka form-control input-sm" ng-disabled="ctrl.disableForm"/>
                              </div>
                          </div>
                      </div>

 					<div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="ukIznos">Ukupan iznos</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="ctrl.ifaktura.ukIznos" id="ukIznos" class="oznaka form-control input-sm" ng-disabled="ctrl.disableForm"/>
                              </div>
                          </div>
                      </div>
                      
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="preostaliIznos">Preostali iznos</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="ctrl.ifaktura.preostaliIznos" id="preostaliIznos" class="oznaka form-control input-sm" ng-disabled="ctrl.disableForm"/>
                              </div>
                          </div>
                      </div>
                      
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="poslovnaGodina">Poslovna godina</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="ctrl.ifaktura.poslovnaGodina.godina" id="poslovnaGodina" class="oznaka form-control input-sm" ng-disabled="ctrl.disableForm"/>
                              </div>
                          </div>
                      </div>
                      
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="poslovniPartner">Poslovni partner</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="ctrl.ifaktura.poslovniPartner.naziv" id="poslovniPartner" class="oznaka form-control input-sm" ng-disabled="ctrl.disableForm"/>
                              </div>
                          </div>
                      </div>
                      
                        <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="racunPartnera">Racun partnera</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="ctrl.ifaktura.racunPartnera.brRacuna" id="racunPartnera" class="oznaka form-control input-sm" ng-disabled="ctrl.disableForm"/>
                              </div>
                          </div>
                      </div>
                      
                      <div class="row">
                          <div class="form-actions floatRight">
                              <input type="submit" id ="subId" value="Commit" class="btn btn-primary btn-sm" ng-disabled="myForm.$invalid">
                              <button type="button" ng-click="ctrl.reset()" class="btn btn-warning btn-sm" ng-disabled="myForm.$pristine">Rollback</button>
                              <button type="button" ng-click="ctrl.back()" class="btn btn-primary btn-sm">Back</button>
                              <label>{{ctrl.currState}}</label>
                          </div>
                      </div>
                      
                  </form>
              </div>
          </div>
      </div>
      
</div>
