package jmn.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import jmn.models.Faktura;
import jmn.models.StavkaFakture;


public interface StavkaFaktureRepository extends CrudRepository<StavkaFakture, Long>{

	public List<StavkaFakture>  findByStavkaFakture(Faktura stavkaFaktura);
	public List<StavkaFakture> findByJedinicaMereAndJedinicnaCenaAndKolicinaAndNazivRobeUslugeContainingAndProcenatRabataAndUkupanPorezAndVrednostAndStavkaFakture(double jedinicaMere, double jedinicnaCena, double kolicina,String nazivRobeIUsluga,double procenatRabata,double ukupanPorez,double vrednost, Faktura stavkaFakture);
}


