package jmn.services;

import java.util.List;

import jmn.models.Faktura;
import jmn.models.StavkaIzvoda;
import jmn.models.ZatvaranjeIF;

public interface ZatvaranjeIFService {

	List<ZatvaranjeIF> findAll();

	ZatvaranjeIF saveZatvaranjeIF(ZatvaranjeIF z);

	ZatvaranjeIF findOne(Long id);

	void delete(Long id);

	void delete(ZatvaranjeIF z);
	
	List<ZatvaranjeIF> findByIzlaznaFaktura(Faktura izlaznaFaktura);
	
	List<ZatvaranjeIF> findByStavkaIzvoda(StavkaIzvoda stavkaIzvoda);
}
