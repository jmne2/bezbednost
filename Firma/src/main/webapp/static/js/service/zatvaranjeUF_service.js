'use strict';
 
angular.module('repo3App').factory('ZatvaranjeUFService', ['$http', '$q', 'urls', function($http, $q, urls){
 
   
 
    var factory = {
    	fetchAllStavkeIzvoda: fetchAllStavkeIzvoda,
    	fetchAllUlazneFakture: fetchAllUlazneFakture,
        searchStavkeIzvoda: searchStavkeIzvoda,
        searchUlazneFakture: searchUlazneFakture,
        closeUlazneFakture: closeUlazneFakture 
    };
 
    return factory;
 
    function fetchAllStavkeIzvoda() {
        var deferred = $q.defer();
        $http.post(urls.ZATVARANJE_UF_SERVICE+"stavkeIzvoda/")
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while fetching stavke izvoda za zatvaranje');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
    function fetchAllUlazneFakture() {
        var deferred = $q.defer();
        $http.post(urls.ZATVARANJE_UF_SERVICE+"fakture/")
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while fetching fakture za zatvaranje');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
    
    function searchStavkeIzvoda(stavka){
    	var deferred = $q.defer();
        $http.post(urls.ZATVARANJE_UF_SERVICE+"search/", stavka)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
    
    
    function searchUlazneFakture(ulaznaFaktura){
    	var deferred = $q.defer();
        $http.post(urls.ZATVARANJE_UF_SERVICE+"search/", ulaznaFaktura)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
    function closeUlazneFakture(ulazneFaktura,stavkeIzvoda){
    	var deferred = $q.defer();
        $http.post(urls.ZATVARANJE_UF_SERVICE+"close/", ulazneFaktura, stavkeIzvoda)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
}]);