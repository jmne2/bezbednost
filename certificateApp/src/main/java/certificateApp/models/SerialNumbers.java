package certificateApp.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class SerialNumbers implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long Id;	
	
	@Column(nullable = false, unique = true)
	private String serialNumber;
	
	@Column(nullable = false, unique = true)
	private String alias;

	@Column(nullable = false)
	private String username;
	
	@Column(nullable = true)
	private String issuerSerNum;
	
	public SerialNumbers(String serialNumber, String alias, String username, String issuerSerNum) {
		super();
		this.serialNumber = serialNumber;
		this.alias = alias;
		this.username = username;
		this.issuerSerNum = issuerSerNum;
	}

	public SerialNumbers() {
		super();
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public Long getId() {
		return Id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getIssuerSerNum() {
		return issuerSerNum;
	}

	public void setIssuerSerNum(String issuerSerNum) {
		this.issuerSerNum = issuerSerNum;
	}
	
	
	
}
