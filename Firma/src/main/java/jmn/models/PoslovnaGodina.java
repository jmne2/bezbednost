package jmn.models;

import static javax.persistence.CascadeType.ALL;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class PoslovnaGodina implements Serializable {

	@Id
	@GeneratedValue
	private Long id;
	
	@Column (nullable=false)
	private int godina;
	
	@Column (nullable=false)
	private Boolean zakljucena;

	@OneToMany(cascade={ALL}, fetch=FetchType.LAZY, mappedBy="poslovnaGodina")
	@JsonIgnore
	private Set<Faktura> fakture = new HashSet<Faktura>();

	@ManyToOne()
	@JoinColumn(name="preduzece")
	private Preduzece preduzece;
	
	public PoslovnaGodina() {
	}

	public PoslovnaGodina(int godina, Boolean zakljucena, 
			Set<Faktura> fakture, Preduzece preduzece) {
		super();
		this.godina = godina;
		this.zakljucena = zakljucena;
		this.fakture = fakture;
		this.preduzece = preduzece;
	}

	public int getGodina() {
		return godina;
	}

	public void setGodina(int godina) {
		this.godina = godina;
	}

	
	public Long getId() {
		return id;
	}

	public Set<Faktura> getFakture() {
		return fakture;
	}

	public void setFakture(Set<Faktura> fakture) {
		this.fakture = fakture;
	}

	public void addFaktura(Faktura izf){
		this.fakture.add(izf);
		if(!izf.getPoslovnaGodina().equals(this))
			izf.setPoslovnaGodina(this);
	}

	public Boolean getZakljucena() {
		return zakljucena;
	}

	public void setZakljucena(Boolean zakljucena) {
		this.zakljucena = zakljucena;
	}

	public Preduzece getPreduzece() {
		return preduzece;
	}

	public void setPreduzece(Preduzece preduzece) {
		this.preduzece = preduzece;
	}
	
}
