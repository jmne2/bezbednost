package jmn.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

import jmn.models.DnevnoStanje;
import jmn.models.Racun;

public interface DnevnoStanjeRepository extends CrudRepository<DnevnoStanje, Long> {

	public DnevnoStanje findByDatumIzvodaAndRacun(Date datumIzvoda, Racun racun);

	public List<DnevnoStanje> findByDatumIzvoda(Date datumIzvoda);

	public List<DnevnoStanje> findByRacun(Racun racun);
}
