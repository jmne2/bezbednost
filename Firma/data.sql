-- DRZAVE--

INSERT INTO `jdbc2`.`drzava` (`id`, `naziv`, `oznaka`) VALUES ('1', 'Srbija', 'SRB');
INSERT INTO `jdbc2`.`drzava` (`id`, `naziv`, `oznaka`) VALUES ('2', 'Crna Gora', 'MNE');
INSERT INTO `jdbc2`.`drzava` (`id`, `naziv`, `oznaka`) VALUES ('3', 'Bosna i Hercegovina', 'BIH');
INSERT INTO `jdbc2`.`drzava` (`id`, `naziv`, `oznaka`) VALUES ('4', 'Hrvatska', 'CRO');
INSERT INTO `jdbc2`.`drzava` (`id`, `naziv`, `oznaka`) VALUES ('5', 'Slovenija', 'SLO');

-- NASELJENA MESTA--

INSERT INTO `jdbc2`.`naseljeno_mesto` (`id`, `naziv`, `post_broj`, `drzava`) VALUES ('1', 'Novi Sad', '21000', '1');
INSERT INTO `jdbc2`.`naseljeno_mesto` (`id`, `naziv`, `post_broj`, `drzava`) VALUES ('2', 'Beograd', '110000', '1');
INSERT INTO `jdbc2`.`naseljeno_mesto` (`id`, `naziv`, `post_broj`, `drzava`) VALUES ('3', 'Asanja', '22418', '1');
INSERT INTO `jdbc2`.`naseljeno_mesto` (`id`, `naziv`, `post_broj`, `drzava`) VALUES ('4', 'Babusnica', '18330', '1');
INSERT INTO `jdbc2`.`naseljeno_mesto` (`id`, `naziv`, `post_broj`, `drzava`) VALUES ('5', 'Vilovo', '21246', '1');
INSERT INTO `jdbc2`.`naseljeno_mesto` (`id`, `naziv`, `post_broj`, `drzava`) VALUES ('6', 'Zajecar', '19000', '1');
INSERT INTO `jdbc2`.`naseljeno_mesto` (`id`, `naziv`, `post_broj`, `drzava`) VALUES ('7', 'Kragujevac', '34000', '1');
INSERT INTO `jdbc2`.`naseljeno_mesto` (`id`, `naziv`, `post_broj`, `drzava`) VALUES ('8', 'Kraljevo', '36000', '1');


-- ADRESE--

INSERT INTO `jdbc2`.`adresa` (`id`, `broj`, `ulica`, `nas_mesto`) VALUES ('1', '18', 'Partizanskih baza', '1');
INSERT INTO `jdbc2`.`adresa` (`id`, `broj`, `ulica`, `nas_mesto`) VALUES ('2', '25', 'Vladike Cirica', '1');
INSERT INTO `jdbc2`.`adresa` (`id`, `broj`, `ulica`, `nas_mesto`) VALUES ('3', '9', 'Bulevar Jovana Ducica', '1');
INSERT INTO `jdbc2`.`adresa` (`id`, `broj`, `ulica`, `nas_mesto`) VALUES ('4', '65', 'Cara Lazara', '1');
INSERT INTO `jdbc2`.`adresa` (`id`, `broj`, `ulica`, `nas_mesto`) VALUES ('5', '14', 'Gajeva', '1');


-- sifrarnik delatnosti-- 

INSERT INTO `jdbc2`.`sifrarnik_delatnosti` (`id`, `naziv_delatnosti`, `sifra`) VALUES ('1', 'Uzgoj muznih krava', '0141');
INSERT INTO `jdbc2`.`sifrarnik_delatnosti` (`id`, `naziv_delatnosti`, `sifra`) VALUES ('2', 'Uzgoj drugih goveda i bivola', '0142');
INSERT INTO `jdbc2`.`sifrarnik_delatnosti` (`id`, `naziv_delatnosti`, `sifra`) VALUES ('3', 'Uzgoj konja i drugih kopitara', '0143');


-- preduzece--

INSERT INTO `jdbc2`.`preduzece` (`id`, `br_telefona`, `email`, `maticni_broj`, `naziv`, `pib`, `adresa`, `sifra_delatnosti`) VALUES ('1', '021654798', 'preduzece@gmail.com', '21458976', 'preduzece1', '123654789', '1', '1');


-- poslovni partneri--

INSERT INTO `jdbc2`.`poslovni_partner` (`id`, `br_telefona`, `email`, `maticni_broj`, `naziv`, `pib`, `vrsta`, `adresa`, `preduzece`, `sifra_delatnosti`) VALUES ('1', '021789954', 'pp1', '18745632', 'pp1', '999456320', 'dobavljac', '2', '1', '2');
INSERT INTO `jdbc2`.`poslovni_partner` (`id`, `br_telefona`, `email`, `maticni_broj`, `naziv`, `pib`, `vrsta`, `adresa`, `preduzece`, `sifra_delatnosti`) VALUES ('2', '021789654', 'pp2', '98745632', 'pp2', '987456320', 'dobavljac', '3', '1', '2');
INSERT INTO `jdbc2`.`poslovni_partner` (`id`, `br_telefona`, `email`, `maticni_broj`, `naziv`, `pib`, `vrsta`, `adresa`, `preduzece`, `sifra_delatnosti`) VALUES ('3', '021654789', 'pp3', '99999999', 'pp3', '888888888', 'kupac', '1', '1', '3');

-- poslovna godina 

INSERT INTO `jdbc2`.`poslovna_godina` (`id`, `godina`, `zakljucena`, `preduzece`) VALUES ('1', '2017', false, '1');


