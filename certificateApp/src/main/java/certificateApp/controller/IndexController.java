package certificateApp.controller;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.SignatureException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Random;

import javax.servlet.http.HttpSession;

import org.bouncycastle.openssl.jcajce.JcaPEMWriter;
import org.bouncycastle.operator.OperatorCreationException;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;

import certificateApp.annotations.PermissionType;
import certificateApp.generator.KeyStoreReader;
import certificateApp.generator.KeyStoreWriter;
import certificateApp.generator.NewCertificate;
import certificateApp.models.Certificate;
import certificateApp.models.Role;
import certificateApp.models.SerialNumbers;
import certificateApp.models.User;
import certificateApp.service.RevokedService;
import certificateApp.service.RoleService;
import certificateApp.service.SerialNumbersService;
import certificateApp.service.UserService;

@Controller
public class IndexController {
	
	@Autowired
	private UserService us;
	@Autowired
	private SerialNumbersService snser;
	@Autowired
	private RevokedService rs;
	@Autowired
	private RoleService roleSer;
	
	private Certificate cer;
	private NewCertificate nc;
	private String keyStoreName;
	private String keyStorePassword;
	private OCSPController ocont;
	
	@RequestMapping(value="/", method = RequestMethod.GET)
    public ModelAndView getIndexPage(HttpSession session) {
	/*	Role r1 = new Role("admin");
		Role r2 = new Role("banka");
		Role r3 = new Role("endUser");
		Permission p1 = new Permission("findAllBanks"); //admin, endus
		Permission p2 = new Permission("findAllBankAliases");	//bank
		Permission p3 = new Permission("setSigned"); //admin
		Permission p4 = new Permission("openKeyStoreNBS"); //admin, bank
		Permission p5 = new Permission("openKeyStoreBank"); //admin, bank, end user
		Permission p6 = new Permission("openKeyStoreEndUs"); //bank
		Permission p7 = new Permission("cert:create");	//admin
		Permission p8 = new Permission("cert2:create");	//end user
		Permission p9 = new Permission("chooseCert");	//admin
		Permission p10 = new Permission("page:admin");
		Permission p11 = new Permission("page:bank");
		Permission p12 = new Permission("page:company");
		Permission p13 = new Permission("findCert");
		Permission p14 = new Permission("downloadCert");
		Permission p15 = new Permission("showCsr");			//bank
		Permission p16 = new Permission("showCsrList");		//bank
		Permission p17 = new Permission("approveCsr");		//bank
		Permission p18 = new Permission("saveApprovedCert");	//end user
		Permission p19 = new Permission("withdrawCert");		//all
		Permission p20 = new Permission("findWithdrawCert");	//all
		
		r1.addPermission(p1);
		r1.addPermission(p3);
		r1.addPermission(p4);
		r1.addPermission(p5);
		r1.addPermission(p7);
		r1.addPermission(p9);
		r1.addPermission(p10);
		r1.addPermission(p13);
		r1.addPermission(p14);
		
		r2.addPermission(p2);
		r2.addPermission(p4);
		r2.addPermission(p5);
		r2.addPermission(p6);
		r2.addPermission(p11);
		r2.addPermission(p13);
		r2.addPermission(p14);
		r2.addPermission(p15);
		r2.addPermission(p16);
		r2.addPermission(p17);
		r2.addPermission(p19);
		r2.addPermission(p20);
		
		
		r3.addPermission(p1);
		r3.addPermission(p5); 
		r3.addPermission(p8);
		r3.addPermission(p12);
		r3.addPermission(p13);
		r3.addPermission(p14);
		r3.addPermission(p18);
		
		User u = new User("admin", "admin", "NBS");
		u.addRole(r1);
		us.saveUser(u);
		User u2 = new User("banka1", "pass", "Banka1");
		u2.addRole(r2);
		us.saveUser(u2);
		User u3 = new User("banka2", "pass", "Banka2");
		u3.addRole(r2);
		us.saveUser(u3);
		User u4 = new User("firma1", "pass", "Kor1");
		u4.addRole(r3);
		us.saveUser(u4);
		User u5 = new User("firma2", "pass", "Kor2");
		u5.addRole(r3);
		us.saveUser(u5);*/
		return new ModelAndView("index");
    }
	
	@RequestMapping(value="/login/", method = RequestMethod.POST, produces="application/json") //@ResponseBody
	public ModelAndView login(@RequestBody User user, HttpSession session){
		User u = us.findByUsernameAndPassword(user.getUsername(), user.getPassword());
		System.out.println("loged in");
		if(u!=null){
			session.setAttribute("user", u);
			Role r = u.getAllowed().iterator().next();
			session.setAttribute("role", r);
			findAllBanks(session);
			findAllBankAliases(session);
			return new ModelAndView("redirect:/admin/");
		}
		return null;						//testirati unauthorized
	}
	
	@PermissionType("page:admin")
	@RequestMapping(value="/admin/", method = RequestMethod.GET)
	public @ResponseBody String loginAdmin(HttpSession session){
		return "admin.jsp";
	}
	
	@PermissionType("page:bank")
	@RequestMapping(value="/bank/", method = RequestMethod.GET, produces="application/json")
	public ModelAndView loginBank(HttpSession session){
		return new ModelAndView("bank");
	}
	
	@PermissionType("page:company")
	@RequestMapping(value="/company/", method = RequestMethod.GET, produces="application/json")
	public ModelAndView loginCompany(HttpSession session){
		return new ModelAndView("company");
	}
	
	
	@RequestMapping(value="/logOut/", method = RequestMethod.POST)
	public ResponseEntity<Void>  logout(HttpSession session){
		
		session.setAttribute("user", null);
		return new ResponseEntity<Void>(HttpStatus.OK);	
	}
	
	@PermissionType("cert:create")
	@RequestMapping(value="cert/", method = RequestMethod.POST, produces="application/json")
	public @ResponseBody String cert(@RequestBody Certificate cer, HttpSession session){
		this.cer = cer;
		session.setAttribute("selfSigned", false);
		if(cer.getSelfSigned()!=null ){	//admin
			session.setAttribute("selfSigned", true);
			cer.setIsCa(true);

			User u = (User)session.getAttribute("user");
			try {
				String serNum = countSerialNumber();
				this.nc = new NewCertificate(cer, serNum);
				saveCA(u);
				
				ArrayList<String> aliases = (ArrayList<String>) session.getAttribute("aliases");
				if(aliases==null){
					aliases = new ArrayList<String>();
				}
				aliases.add(cer.getAlias());
				session.setAttribute("aliases", aliases);
				
				
				SerialNumbers sn = new SerialNumbers(serNum, cer.getAlias(), u.getUsername(), null);
				snser.saveSerialNumbers(sn);
				return "selfSigned";
				
			} catch (KeyStoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NoSuchProviderException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (CertificateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		else {
			User u = (User)session.getAttribute("user");
			
			return setSigned(session);
			
		}
		return "Nothing";
	}
	
	@PermissionType("cert2:create")
	@RequestMapping(value="cert2/", method = RequestMethod.POST)
	public @ResponseBody String cert2(@RequestBody Certificate cer, HttpSession session){
		this.cer = cer;
		System.out.println(cer.getAlias());
			try {
					User u = (User)session.getAttribute("user");
					cer.setSelfSigned(false);
					String serNum = countSerialNumber();
					this.nc = new NewCertificate(cer, serNum);
					nc.setSnser(snser);
					nc.generateCSR(cer.getBankName(), u.getUsername());
					return "saved";
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (KeyStoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NoSuchProviderException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (CertificateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvalidKeyException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SignatureException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (OperatorCreationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return "nothing";
	}

	private void saveCA(User u){
		File folder = new File("keyStores");
		File[] listOfFiles = folder.listFiles();
		KeyStoreWriter keyStoreWriter = new KeyStoreWriter();
		boolean exists = false;
		
		String ksName = "";
	
		if(cer.getSelfSigned()!=null && cer.getSelfSigned()==true){
			ksName = openKeyStoreNBS();
		}
		else{
			String num = cer.getBankName().substring(5, cer.getBankName().length());
			ksName = openKeyStoreBank(num);
		}
		if(ksName == ""){
			ksName = openKeyStoreEndUs();
		}
		if(cer.getIsCa()){
			for(File f : listOfFiles){
				if(f.getName().equals(ksName))
					exists = true;
				System.out.println("FILE " + f.getName());
			}
			if(!exists){
				keyStoreWriter.loadKeyStore(null, cer.getPasswordKs().toCharArray());
			}
			else{
				keyStoreWriter.loadKeyStore("keyStores/"+ksName,cer.getPasswordKs().toCharArray());
			}
		}
		
		X509Certificate cert = nc.getCert();
		
		keyStoreWriter.write(cer.getAlias(), nc.getPrivateKey(), cer.getPasswordPk().toCharArray(), cert);
		
			keyStoreWriter.saveKeyStore("keyStores/"+ksName,cer.getPasswordKs().toCharArray());
		
		
		System.out.println("Upisan u keyStore");
		
	}
	
	@PermissionType("chooseCert")
	@RequestMapping(value="chooseCert/", method = RequestMethod.POST)
	public ResponseEntity<Void> chooseCert(@RequestBody String data, HttpSession session) throws IOException{
		System.out.println(data);	
		String[] datas = data.split("&");
		String bank = datas[0].split("=")[1];
		String alias =datas[1].split("=")[1];
		String password=datas[2].split("=")[1];
		String passwordKs=datas[3].split("=")[1];
		
		User u = (User)session.getAttribute("user");
		String ksName = "";
		
		ksName = openKeyStoreNBS();
	
		KeyStoreReader keyStoreReader = new KeyStoreReader();
		System.out.println("alias " + alias);
		System.out.println("password ks " + passwordKs);
		System.out.println("file "+ "keyStores/"+ksName);
		X509Certificate parentCert = (X509Certificate) keyStoreReader.readCertificate("keyStores/"+ksName, passwordKs, alias);
		PrivateKey parentPrivateKey = keyStoreReader.readPrivateKey("keyStores/"+ksName, passwordKs, alias, password);
		
		try {
			String retVal = nc.signed(parentPrivateKey, parentCert);
			if(retVal.equals("revoked")){
				return new ResponseEntity<Void>( HttpStatus.UNAVAILABLE_FOR_LEGAL_REASONS);
			}
			
			bank=bank.substring(3, bank.length()-3);
			cer.setBankName(bank);
			
			saveCA(u);
			
			ArrayList<String> aliases = (ArrayList<String>) session.getAttribute("aliases");
			if(aliases==null){
				aliases = new ArrayList<String>();
			}
			aliases.add(cer.getAlias());
			session.setAttribute("aliases", aliases);
			User user = us.findByName(bank);
			String parentSerNum = parentCert.getSerialNumber().toString(16);
			SerialNumbers serNum = new SerialNumbers(nc.getSerNum(), cer.getAlias(), user.getUsername(), parentSerNum);
			snser.saveSerialNumbers(serNum);
		} catch (CertificateEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new ResponseEntity<Void>( HttpStatus.CREATED);
	}
	
	@PermissionType("findCert")
	@RequestMapping(value="findCert/", method = RequestMethod.POST, produces="application/json")
	public @ResponseBody String  findCert(@RequestBody String data, HttpSession session) throws NoSuchProviderException, SignatureException,  ParseException, InvalidKeyException, CertificateException, NoSuchAlgorithmException, IOException, JSONException{
		X509Certificate crt;
		String[] datas = data.split("&");
		
		String keyStoreName = datas[0].split("=")[1];
		String passwordKs = datas[1].split("=")[1];
		String numStr = datas[2].split("=")[1];
		
		BigInteger bi = new BigInteger(numStr);
		String num = bi.toString(16);	
		
		SerialNumbers sNum = snser.findBySerialNumber(num);
		if(sNum==null){
			return "NoExists";
		}
		
		if(ocont==null)
			ocont = new OCSPController(snser, rs, us);
		String status = ocont.getWithdrawnStatus(numStr);		
		JSONObject body = new JSONObject(status);
		System.out.println("Status : " + body.get("tagNo"));
		
		if(body.get("tagNo")==(Object)1){
			String stat = "revoked";
			Gson gson = new Gson();
        	String json = gson.toJson(stat);
        	return json;
		}
		KeyStore ks;
		System.out.println(num + "  " + passwordKs + "  " + keyStoreName);
		//String passwordKs = "pass";
		try {
			ks = KeyStore.getInstance("JKS", "SUN");
			BufferedInputStream in = new BufferedInputStream(new FileInputStream("keyStores/"+keyStoreName));
			ks.load(in, passwordKs.toCharArray());
			
			String alias = sNum.getAlias();
			System.out.println(alias);
			if(ks.containsAlias(alias)){
				crt=(X509Certificate)ks.getCertificate(alias);
				session.setAttribute("foundCert", crt);
				System.out.println(crt);
				Gson gson = new Gson();
	        	String json = gson.toJson(crt);
	        	return json;
			}
			
		} catch (KeyStoreException e) {
			System.out.println("KEY STORE EXC");
			e.printStackTrace();
		} catch (NoSuchProviderException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CertificateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			String retVal = "WrongPassword";
			Gson gson = new Gson();
        	String json = gson.toJson(retVal);
        	return json;
		}
		
		
		
		return null;
	}
	
	private String countSerialNumber() throws KeyStoreException, NoSuchProviderException, NoSuchAlgorithmException, CertificateException, IOException{
		BigInteger bi = new BigInteger(5, new Random());
		String retVal = bi.toString(16);
		if(snser.findBySerialNumber(retVal)!=null){
			retVal = countSerialNumber();
		}
		
		return retVal;
	}
	
	private ArrayList<String> findAllAliases(HttpSession session){
		KeyStore ks;
		try {
			ks = KeyStore.getInstance("JKS", "SUN");
			User user = (User) session.getAttribute("user");
			String ksName = "";
			ksName = openKeyStoreNBS();

			BufferedInputStream in = new BufferedInputStream(new FileInputStream("keyStores/"+ksName));
			String pass = "pass";
			ks.load(in, pass.toCharArray());
			ArrayList<String> aliases = new ArrayList<String>();
			 Enumeration en = ks.aliases();
			    for (; en.hasMoreElements(); ) {
			        String alias = (String)en.nextElement();
			        aliases.add(alias);
			    }

			session.setAttribute("aliases", aliases);
			return aliases;
		} catch (KeyStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchProviderException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			session.setAttribute("aliases", null);
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CertificateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new ArrayList<String>();
	}
	
	private String setSigned(HttpSession session){
		cer.setIsCa(true);
		cer.setSelfSigned(false);
		String serNum;
		try {
			serNum = countSerialNumber();
			System.out.println("SERNUM1 " + serNum);
			this.nc = new NewCertificate(cer, serNum);
			nc.setRs(rs);
			//Enumeration<String> aliases = ks.aliases();
			//System.out.println(aliases.toString() );
			session.setAttribute("selfSigned", false);
			ArrayList<String> aliases = findAllAliases(session);
			 Gson gson = new Gson();
	     	 String json = gson.toJson(aliases); 
	     	 return json;
		} catch (KeyStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchProviderException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CertificateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	@PermissionType("openKeyStoreNBS")
	private String openKeyStoreNBS(){
		System.out.println("OPENING KEYSTORE NBS");
		return "keyStoreNBS";
	}
	
	@PermissionType("openKeyStoreBank")
	private String openKeyStoreBank(String num){
		System.out.println("OPENING KEYSTORE BANK");
		String ks ="keyStoreBanka"+num;
		return ks;
	}
	
	@PermissionType("openKeyStoreEndUs")
	private String openKeyStoreEndUs(){
		System.out.println("OPENING KEYSTORE ENDUS");
		return "keyStoreEndUs";
	}
	
	@PermissionType("findAllBanks")
	private void findAllBanks(HttpSession session){
		ArrayList<Role> roles = (ArrayList<Role>)roleSer.findByName("banka");		
		ArrayList<User> banks = new ArrayList<User>();
		for(Role r : roles){
			for(User u : r.getUsers()){
				banks.add(u);
			}
		}
		session.setAttribute("banks", banks);
	}

	@PermissionType("findAllBankAliases")
	private void findAllBankAliases(HttpSession session){
		User u = (User)session.getAttribute("user");
		ArrayList<SerialNumbers> sernums = (ArrayList<SerialNumbers>) snser.findByUsername(u.getUsername());
		ArrayList<String> aliases = new ArrayList<String>();
		for(SerialNumbers sn : sernums){
			aliases.add(sn.getAlias());
		}
		session.setAttribute("bankAliases", aliases);
	}
	

	@PermissionType("downloadCert")
	@RequestMapping(value="downloadCert/", method = RequestMethod.POST)
	public ResponseEntity<Void> downloadCert( HttpSession session) {
		User u = (User)session.getAttribute("user");
		File file = new File("Downloads/");
		File[] listOfFiles = file.listFiles();
		int num = listOfFiles.length+1;
		File f = new File("Downloads/Certificate"+num+".cer");
		X509Certificate crt = (X509Certificate)session.getAttribute("foundCert");
		
		if(crt!=null){
			BufferedWriter w;
			try {
				w = new BufferedWriter(new FileWriter(f.getPath()));
				StringWriter sw = new StringWriter();
				JcaPEMWriter pemWriter = new JcaPEMWriter(sw);
				pemWriter.writeObject(crt);
				pemWriter.close();
				sw.close();
				w.write(sw.toString());
				w.flush();
				w.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return new ResponseEntity<Void>(HttpStatus.OK);
		}
		return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	}

}
