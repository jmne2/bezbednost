package certificateApp.service;

import java.util.List;

import certificateApp.models.Role;

public interface RoleService {
	List<Role> findByName(String name);
}
