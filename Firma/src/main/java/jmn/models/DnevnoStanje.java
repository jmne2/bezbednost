package jmn.models;

import static javax.persistence.CascadeType.ALL;

import java.io.Serializable;
import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class DnevnoStanje implements Serializable {

	@Id
	@GeneratedValue
	private Long id;
	
	@Column(nullable = false)
	private Date datumIzvoda;
	
	@Column(nullable = false)			//stanje od juce
	private double prethodnoStanje;

	@Column(nullable = false)			//ukupno koliko je primio taj dan
	private double stanjeUKorist;
	
	@Column(nullable = false)
	private double stanjeNaTeret;
	
	@Column(nullable = false)			//trenutno
	private double novoStanje;
	
	@Column(nullable = false)			//no
	private double rezervisano;
	
	@OneToMany(cascade={ALL}, fetch=FetchType.LAZY, mappedBy="dnevnoStanje")
	@JsonIgnore
	private Set<StavkaIzvoda> stavkeIzvoda = new HashSet<StavkaIzvoda>();

	@ManyToOne()
	@JoinColumn(name="racun")
	private Racun racun;	
	
	public DnevnoStanje() {
	}

	public DnevnoStanje(Date datumIzvoda, double prethodnoStanje, double stanjeUKorist, double stanjeNaTeret,
			double novoStanje, double rezervisano, Set<StavkaIzvoda> stavkeIzvoda, Racun racun) {
		super();
		this.datumIzvoda = datumIzvoda;
		this.prethodnoStanje = prethodnoStanje;
		this.stanjeUKorist = stanjeUKorist;
		this.stanjeNaTeret = stanjeNaTeret;
		this.novoStanje = novoStanje;
		this.rezervisano = rezervisano;
		this.stavkeIzvoda = stavkeIzvoda;
		this.racun = racun;
	}

	public Date getDatumIzvoda() {
		return datumIzvoda;
	}

	public void setDatumIzvoda(Date datumIzvoda) {
		this.datumIzvoda = datumIzvoda;
	}

	public double getPrethodnoStanje() {
		return prethodnoStanje;
	}

	public void setPrethodnoStanje(double prethodnoStanje) {
		this.prethodnoStanje = prethodnoStanje;
	}

	public double getStanjeUKorist() {
		return stanjeUKorist;
	}

	public void setStanjeUKorist(double stanjeUKorist) {
		this.stanjeUKorist = stanjeUKorist;
	}

	public double getStanjeNaTeret() {
		return stanjeNaTeret;
	}

	public void setStanjeNaTeret(double stanjeNaTeret) {
		this.stanjeNaTeret = stanjeNaTeret;
	}

	public double getNovoStanje() {
		return novoStanje;
	}

	public void setNovoStanje(double novoStanje) {
		this.novoStanje = novoStanje;
	}

	public double getRezervisano() {
		return rezervisano;
	}

	public void setRezervisano(double rezervisano) {
		this.rezervisano = rezervisano;
	}

	public Set<StavkaIzvoda> getStavkeIzvoda() {
		return stavkeIzvoda;
	}

	public void setStavkeIzvoda(Set<StavkaIzvoda> stavkeIzvoda) {
		this.stavkeIzvoda = stavkeIzvoda;
	}

	public void addStavkaIzvoda(StavkaIzvoda sti){
		this.stavkeIzvoda.add(sti);
		if(!sti.getDnevnoStanje().equals(this))
			sti.setDnevnoStanje(this);
	}
	
	public Racun getRacun() {
		return racun;
	}

	public void setRacun(Racun racun) {
		this.racun = racun;
	}

	public Long getId() {
		return id;
	}
}
