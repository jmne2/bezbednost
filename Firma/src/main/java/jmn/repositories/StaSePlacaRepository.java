package jmn.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import jmn.models.Faktura;
import jmn.models.PredlogPlacanja;
import jmn.models.StaSePlaca;

public interface StaSePlacaRepository extends CrudRepository<StaSePlaca, Long> {

	public List<StaSePlaca> findByFaktura(Faktura ulaznaFaktura);

	public List<StaSePlaca> findByPredlogPlacanja(PredlogPlacanja predlogPlacanja);
}
