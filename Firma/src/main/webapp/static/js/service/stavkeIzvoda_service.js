
'use strict';
 
angular.module('repo3App').factory('StavkeIzvodaService', ['$http', '$q', 'urls', function($http, $q, urls){
 
    var factory = {
        fetchAllStavke: fetchAllStavke,
        searchStavke: searchStavke
    };
 
    return factory;
 
    function fetchAllStavke() {
        var deferred = $q.defer();
        $http.get(urls.STIZVODA_SERVICE)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while fetching Reciepts');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 

    function searchStavke(stanje){
    	var deferred = $q.defer();
        $http.post(urls.STIZVODA_SERVICE+"search/", stanje)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
}]);