CREATE DATABASE  IF NOT EXISTS `jdbcbank456`;

CREATE TABLE `racuni_klijenata` (
  `br_racuna` varchar(18) NOT NULL,
  `id_racuni_klijenata` int(11) NOT NULL AUTO_INCREMENT,
  `rezervisano` decimal(15,2) not null,
  PRIMARY KEY (`id_racuni_klijenata`),
  UNIQUE KEY `br_racuna_UNIQUE` (`br_racuna`),
  UNIQUE KEY `id_racuni_klijenata_UNIQUE` (`id_racuni_klijenata`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

CREATE TABLE `stavkaizvodabanka` (
  `idstavkaizvodabanka` int(11) NOT NULL AUTO_INCREMENT,
  `uplatilac` varchar(255) NOT NULL,
  `svrhaPlacanja` varchar(255) NOT NULL,
  `primalac` varchar(255) NOT NULL,
  `datumNaloga` date NOT NULL,
  `datumValute` date NOT NULL,
  `racunUplatioca` varchar(18) NOT NULL,
  `modelZaduzenja` int(2) NOT NULL,
  `pozivNaBrojZaduzenja` varchar(20) NOT NULL,
  `racunPrimaoca` varchar(18) NOT NULL,
  `modelOdobrenja` int(2) NOT NULL,
  `pozivNaBrojOdobrenja` varchar(20) NOT NULL,
  `iznos` decimal(15,2) NOT NULL,
  `smer` varchar(1) NOT NULL,
  `id_izvoda` int(11) NOT NULL,
  PRIMARY KEY (`idstavkaizvodabanka`),
  UNIQUE KEY `idstavkaizvodabanka_UNIQUE` (`idstavkaizvodabanka`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;


CREATE TABLE `izvod` (
  `idizvod` int(11) NOT NULL AUTO_INCREMENT,
  `brRacuna` varchar(18) NOT NULL,
  `datumNaloga` date NOT NULL,
  `brPreseka` int(2) NOT NULL,
  `prethodnoStanje` decimal(15,2) NOT NULL,
  `brPromenaUKorist` int(6) NOT NULL,
  `ukupnoUKorist` decimal(15,2) NOT NULL,
  `brPromenaNaTeret` int(6) NOT NULL,
  `ukupnoNaTeret` decimal(15,2) NOT NULL,
  `novoStanje` decimal(15,2) NOT NULL,
  PRIMARY KEY (`idizvod`),
  UNIQUE KEY `idizvod_UNIQUE` (`idizvod`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8

CREATE TABLE `nalogbanka` (
  `idPoruke` varchar(50) NOT NULL,
  `uplatilac` varchar(255) NOT NULL,
  `svrhaUplate` varchar(255) NOT NULL,
  `primalac` varchar(255) NOT NULL,
  `datumNaloga` date NOT NULL,
  `datumValute` date NOT NULL,
  `racunUplatioca` varchar(18) NOT NULL,
  `modelZaduzenja` int(2) NOT NULL,
  `pozivNaBrojZaduzenja` varchar(20) NOT NULL,
  `racunPrimaoca` varchar(18) NOT NULL,
  `modelOdobrenja` int(2) NOT NULL,
  `pozivNaBrojOdobrenja` varchar(20) NOT NULL,
  `iznos` decimal(15,2) NOT NULL,
  `oznakaValute` varchar(3) NOT NULL,
  `idNalogBanka` int(11) NOT NULL,
  `bankCodePrimaoca` varchar(3),
  PRIMARY KEY (`idNalogBanka`),
  UNIQUE KEY `idNalogBanka_UNIQUE` (`idNalogBanka`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
