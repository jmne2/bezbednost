package jmn.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jmn.annotation.PermissionType;
import jmn.models.Faktura;
import jmn.models.Preduzece;
import jmn.models.SifrarnikMetoda;
import jmn.pojo.UserData;
import jmn.services.FakturaService;
import jmn.services.PreduzeceService;;

@Controller
public class FaktureController {

	final static Logger logger = Logger.getLogger(FaktureController.class);
	
	@Autowired
	private FakturaService ufs;
	
	@Autowired
	private PreduzeceService ps;
	
	@PermissionType("Fakture:view")
	@RequestMapping(value="/fakture/ulazne/", method=RequestMethod.GET)
	public ResponseEntity<List<Faktura>> fetchUlazneFakture(HttpSession session){

		UserData u = (UserData)session.getAttribute("user");
		Preduzece preduzece = u.getPreduzece();
		System.out.println("Preduzece je" + preduzece.getId());
		List<Faktura> ulazneFakture = ufs.findByVrstaFaktureAndPreduzece("ulazne", preduzece);
		
		if(ulazneFakture == null)
			return new ResponseEntity<List<Faktura>>(HttpStatus.NO_CONTENT);
		return new ResponseEntity<List<Faktura>>(ulazneFakture, HttpStatus.OK);
	}
	
	@PermissionType("Fakture:view")
	@RequestMapping(value="/fakture/izlazne/", method=RequestMethod.GET)
	public ResponseEntity<List<Faktura>> fetchIzlazneFakture(HttpSession session){
		UserData u = (UserData)session.getAttribute("user");

		Preduzece preduzece = u.getPreduzece(); 
		
		List<Faktura> izlazneFakture = ufs.findByVrstaFaktureAndPreduzece("izlazne", preduzece);
		
		if(izlazneFakture == null)
			return new ResponseEntity<List<Faktura>>(HttpStatus.NO_CONTENT);
		return new ResponseEntity<List<Faktura>>(izlazneFakture, HttpStatus.OK);
	}
	
	
	@PermissionType("Fakture:edit")
	 @RequestMapping(value = "/fakture/{id}", method = RequestMethod.POST)
	    public ResponseEntity<?> updateUlaznaFaktura(HttpSession session,@PathVariable("id") long id, @RequestBody Faktura ulaznaFaktura) {
			UserData u = (UserData)session.getAttribute("user");
			Faktura exists = ufs.findOne(id);
		 	if(exists == null){
		 		logger.error("Greska pri izvrsenju " + SifrarnikMetoda.methods.get(Thread.currentThread().getStackTrace()[1].getMethodName())
	        	+ " za korisnika " + u.getId());
		 		return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		 	}
	        logger.warn("Korisnik "+ u.getId() + " uspesno je izvrsio metodu " + SifrarnikMetoda.methods.get(
		 				Thread.currentThread().getStackTrace()[1].getMethodName()));

		 	return new ResponseEntity<Faktura>(exists, HttpStatus.OK);
	    }
	
	
	@PermissionType("Fakture:search")
	@RequestMapping(value = "/fakture/search/", method = RequestMethod.POST)
	    public ResponseEntity<?> searchUlaznaFaktura(HttpSession session,@RequestBody Faktura ulaznaFaktura) {
			List<Faktura> fakture = null;
			UserData u = (UserData)session.getAttribute("user");
			Preduzece preduzece = u.getPreduzece(); 
			fakture = ufs.findByPreduzeceAndBrojFaktureAndVrstaFakture(preduzece, ulaznaFaktura.getBrojFakture().toString(), ulaznaFaktura.getVrstaFakture());
			if(fakture == null)
			 return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	     
			return new ResponseEntity<List<Faktura>>(fakture, HttpStatus.OK);
	    }	
}
