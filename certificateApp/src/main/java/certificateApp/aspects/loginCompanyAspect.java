package certificateApp.aspects;

import java.lang.reflect.Method;

import javax.servlet.http.HttpSession;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import certificateApp.annotations.PermissionType;
import certificateApp.models.Permission;
import certificateApp.models.Role;
import certificateApp.models.User;
import certificateApp.service.UserService;

@Aspect
@Component
public class loginCompanyAspect {
	@Autowired
	private UserService us;
	@Around("execution(* certificateApp.controller.IndexController.loginCompany(..)) && args(session) && @annotation(certificateApp.annotations.PermissionType)")
	public Object test(ProceedingJoinPoint jp, HttpSession session){
		System.out.println("LOGIN COMPANY");
		User u = (User)session.getAttribute("user");
		u = us.findOne(u.getId());
		if(u!=null){
			System.out.println("TEst2");
			MethodSignature signature = (MethodSignature) jp.getSignature();
			Method method = signature.getMethod();
			PermissionType myAnnotation = method.getAnnotation(PermissionType.class);
			boolean good = false;
			
			for(Role r : u.getAllowed()){
				for(Permission p : r.getPermissions()){
					if(myAnnotation.value().equals(p.getName())){
						good = true;
						break;
					}
				
				}
			}
			
			Object retVal = null;
			if(good){
				try {
					retVal = jp.proceed();
				} catch (Throwable e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			return retVal;
		}
		return null;
	}
}
