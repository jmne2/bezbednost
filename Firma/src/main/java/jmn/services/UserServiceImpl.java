package jmn.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jmn.models.User;
import jmn.repositories.UserRepository;

@Service
public class UserServiceImpl implements UserService{

	@Autowired
	private UserRepository ur;
	
	@Override
	public List<User> findAll() {
		return (List<User>)ur.findAll();
	}

	@Override
	public User saveUser(User us) {
		return ur.save(us);
	}

	@Override
	public User findOne(Long id) {
		return ur.findOne(id);
	}

	@Override
	public void delete(Long id) {
		ur.delete(id);
	}

	@Override
	public void delete(User us) {
		ur.delete(us);
	}

	@Override
	public User findByUsernameAndPassword(String username, String password) {
		return ur.findByUsernameAndPassword(username, password);
	}

	@Override
	public User findByUsername(String username) {
		return ur.findByUsername(username);
	}

}
