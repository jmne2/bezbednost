package nbs;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Date;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import javax.ejb.Stateless;
import javax.jws.WebService;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import db.Database;
import izvod.IzvodWS;
import nbs.data.Mt103;
import nbs.data.Mt900;
import nbs.data.Mt910;
import nbs.data.Nalog;

@Stateless
@WebService(portName = "CBWSPort", 
			serviceName = "CBWSService", 
			targetNamespace = "http://nbs", 
			endpointInterface = "nbs.CBWS", 
			wsdlLocation = "WEB-INF/wsdl/CBWS.wsdl",
			name = "CBWS")
public class CBWSImpl implements CBWS{

	public CBWSImpl(){
		System.out.println("Created CBWSImpl");
	}

	// centralna banka prima nalog mt103, odradi medjubankarski obracun i pozove sendNalog da se posalje drugoj banci mt103
	// vraca mt900 prvoj banci
	@Override
	public Mt900 getNalog(Mt103 mt103) {
		System.out.println("*******************ENTERED CBWS IMPL *********************");
		Mt900 mt900 = new Mt900("idPoruke1234", mt103.getSwiftBankeD(), mt103.getRacunBankeD(), mt103.getIdPoruke(), mt103.getDatumValute(), mt103.getIznos(), mt103.getSifraValute());
		Database db = new Database();
		try {
			db.doTransaction(mt103.getRacunBankeD(), mt103.getRacunBankeP(), mt103.getIznos());
			sendNalog(mt103);
			return mt900;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	// salje mt103 nalog banci primaoca (receiveNalogNbs())
	@Override
	public void sendNalog(Mt103 mt103) {
		GregorianCalendar c = new GregorianCalendar();
		c.setTime(mt103.getDatumValute());
		XMLGregorianCalendar date;
		try {
			date = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
			c.setTime(mt103.getDatumNaloga());
			XMLGregorianCalendar date2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);

			Mt910 mt910 = new Mt910("idPoruke1234", mt103.getSwiftBankeP(), mt103.getRacunBankeP(), mt103.getIdPoruke(), mt103.getDatumNaloga(), mt103.getIznos(), mt103.getSifraValute());
			//TODO: OTVORITI KONEKCIJU KA DRUGOJ BANCI I POSLATI JOJ I MT103 I MT910
			URL wsdlLocation;
			wsdlLocation = new URL("http://localhost:8083/banka-ws/services/IzvodWS?wsdl");
			QName serviceName = new QName("http://izvod", "IzvodWSService");
			QName portName = new QName("http://izvod", "IzvodWSPort");

			Service service = Service.create(wsdlLocation, serviceName);
//			
			IzvodWS izvod = service.getPort(portName, IzvodWS.class);
			izvod.setBankCode("456");
			
			Mt103 mt1032 = new Mt103(mt103.getIdPoruke(), mt103.getSwiftBankeD(), mt103.getRacunBankeD(), mt103.getSwiftBankeP(), mt103.getRacunBankeP(), mt103.getDuznik(), mt103.getSvrhaPlacanja(), mt103.getPrimalac(), mt103.getDatumValute(), mt103.getDatumNaloga(), mt103.getRacunDuznika(), mt103.getModelZaduzenja(), mt103.getPozivNaBrojZaduzenja(), mt103.getRacunPrimaoca(), mt103.getModelOdobrenja(), mt103.getPozivNaBrojOdobrenja(), mt103.getIznos(), mt103.getSifraValute());
			izvod.receiveNalogNbs(mt1032, mt910);
//	
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DatatypeConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	// od banke 1 stizu nalozi za clearing, poziva se metoda clearing2 u banci 2 da se dobiju njeni nalozi
	// izvrsi se medjubankarski obracun (bilateralni neto obracun)
	// salje se banci 1 poruka o zaduzenju mt900 
	// salje se banci 2 poruka o odobrenju mt910 i mt102
	@Override
	public Mt900 settle(ArrayList<Nalog> nalozi, XMLGregorianCalendar datumValute) throws Exception {
		System.out.println("************STARTING SETTLE IN CENTRAL BANK *************");
		String bankCode2 = nalozi.get(0).getRacunPrimaoca().substring(0, 3);
		String bankCode1 = nalozi.get(0).getRacunUplatioca().substring(0, 3);
		System.out.println("********SETTLING BETWEEN " + bankCode2 + " and " + bankCode1 +" ****** ");
		//trazimo od druge banke listu naloga za prvu banku
		URL wsdlLocation;
		try {
			wsdlLocation = new URL("http://localhost:8083/banka-ws/services/IzvodWS?wsdl");
			QName serviceName = new QName("http://izvod", "IzvodWSService");
			QName portName = new QName("http://izvod", "IzvodWSPort");

			Service service = Service.create(wsdlLocation, serviceName);
			
			IzvodWS izvod = service.getPort(portName, IzvodWS.class);
			izvod.setBankCode(bankCode2);
			System.out.println("*****before clearing*******");
			
			List<izvod.Nalog> nalozi2 =izvod.clearing2(bankCode1);
			
			System.out.println("****clearing****");
			ArrayList<Nalog> nalozi22 = new ArrayList<Nalog>();
			
			if(nalozi2!=null){
				for(izvod.Nalog n : nalozi2){
					java.util.Date date = n.getDatumNaloga().toGregorianCalendar().getTime();
					Date date1 = new Date(date.getTime());
					
					Nalog nn = new Nalog(n.getIdNaloga(), n.getUplatilac(), n.getSvrhaUplate(), n.getPrimalac(), date1, n.getRacunUplatioca(), n.getModelZaduzenja(), n.getPozivNaBrojZaduzenja(), n.getRacunPrimaoca(), n.getModelOdobrenja(), n.getPozivNaBrojOdobrenja(), n.getIznos(), n.getOznakaValute());
					nalozi22.add(nn);
				}
			}
			double suma1 = 0;
			double suma2 = 0;
			for(Nalog n : nalozi){
				suma2 += n.getIznos();
			}
			if(nalozi2!=null){
				for(izvod.Nalog n : nalozi2){
					suma1 += n.getIznos();
				}
			}
			System.out.println(" SUMA 1  JE " + suma1 + " SUMA 2 JE " + suma2 );
			Database db = new Database();
			if(suma1 <= suma2){ // banci 2 treba vise da se plati nego banci 1
				// duznik je 1, primalac 2
				System.out.println(" SUMA 1 JE MANJA OD SUME 2 ");
				db.setTransaction(bankCode1, bankCode2, suma1-suma2);
				java.util.Date date =datumValute.toGregorianCalendar().getTime();
				Date date1 = new Date(date.getTime());
				
				Mt900 mt900 = db.createMT900(bankCode1, "12345", date1, suma2, nalozi.get(0).getOznakaValute());
				System.out.println("pre datuma");
				java.util.Date datum = new java.util.Date();
				Date datt = new Date(datum.getTime());
				System.out.println("posle datuma");
				db.createMT102_MT910(bankCode2, bankCode1, suma1+suma2,  nalozi.get(0).getOznakaValute(), date1, datt, nalozi, izvod);
				return mt900;
			} 
			else{
				System.out.println(" SUMA 2 JE MANJA OD SUME 1 ");
				db.setTransaction(bankCode2, bankCode1, suma2-suma1);
				java.util.Date date = datumValute.toGregorianCalendar().getTime();
				Date date1 = new Date(date.getTime());
				
				Mt900 mt900 = db.createMT900(bankCode2, "12345", date1, suma1, nalozi2.get(0).getOznakaValute());
				System.out.println("pre datuma");
				java.util.Date datum = new java.util.Date();
				Date datt = new Date(datum.getTime());
				System.out.println("posle datuma");
				db.createMT102_MT910(bankCode2, bankCode1, suma1+suma2,  nalozi2.get(0).getOznakaValute(), date1, datt, nalozi22, izvod);
				return mt900;
			}
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	
}
