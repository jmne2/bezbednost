package jmn.controller;


import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;

import jmn.annotation.PermissionType;
import jmn.models.Drzava;
import jmn.models.PoslovnaGodina;
import jmn.models.Preduzece;
import jmn.pojo.UserData;
import jmn.services.PoslovnaGodinaService;
import jmn.services.PreduzeceService;

@Controller
public class PoslovnaGodinaController {

		@Autowired
		private PoslovnaGodinaService psr;
		
		@Autowired
		private PreduzeceService ps;
		
		@PermissionType("PoslovneGodine:view")
		@RequestMapping(value="/godine/", method=RequestMethod.GET)
		public ResponseEntity<List<PoslovnaGodina>> fetchPoslovneGodine(HttpSession session){
			UserData u = (UserData) session.getAttribute("user");
			List<PoslovnaGodina> godine = psr.findByPreduzece(u.getPreduzece());
			if(godine.isEmpty())
				return new ResponseEntity<List<PoslovnaGodina>>(HttpStatus.NO_CONTENT);
			return new ResponseEntity<List<PoslovnaGodina>>(godine, HttpStatus.OK);
		}
		
		@PermissionType("PoslovneGodine:add")
		@RequestMapping(value = "/godine/", method = RequestMethod.POST)
		public ResponseEntity<Void> createPoslovnaGodina(HttpSession session, @RequestBody PoslovnaGodina pg, UriComponentsBuilder ucBuilder) {
		        System.out.println("Creating Poslovna Godina " + pg.getGodina());
		  
		        PoslovnaGodina d = psr.savePoslovnaGodina(pg);
		        try {
			        HttpHeaders headers = new HttpHeaders();
			        headers.setLocation(ucBuilder.path("/godine/{id}").buildAndExpand(d.getId()).toUri());
			        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
		        }
		        catch(Exception e){
		        	return new ResponseEntity<Void>(HttpStatus.CONFLICT);
		        }
		    }
		@PermissionType("PoslovneGodine:edit")
		 @RequestMapping(value = "/godine/{id}", method = RequestMethod.POST)
		    public ResponseEntity<?> updatePoslovnaGodina(HttpSession session, @PathVariable("id") long id, @RequestBody PoslovnaGodina pg) {
		 
			PoslovnaGodina exists = psr.findOne(id);
			 	if(exists == null){
			 		return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
			 	}
			 	exists.setGodina(pg.getGodina());
			 	exists.setPreduzece(pg.getPreduzece());
			 	exists.setZakljucena(pg.getZakljucena());
			 	exists = psr.savePoslovnaGodina(exists);
			 	return new ResponseEntity<PoslovnaGodina>(exists, HttpStatus.OK);
		    }
		
		@PermissionType("PoslovneGodine:delete")
		@RequestMapping(value = "/godine/{id}", method = RequestMethod.DELETE)
		    public ResponseEntity<?> deletePoslovnaGodina(HttpSession session, @PathVariable("id") long id) {
			PoslovnaGodina exists = psr.findOne(id);
			 if(exists == null)
				 return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
			 
			 psr.delete(id);
		     return new ResponseEntity<Drzava>(HttpStatus.NO_CONTENT);
		    }
		
		@PermissionType("PoslovneGodine:search")
		@RequestMapping(value = "/godine/search/", method = RequestMethod.POST)
		public ResponseEntity<?> searchPoslovnaGodina(HttpSession session, @RequestBody PoslovnaGodina sd) {

			/*
			 * TO DO: PROVERITI SEARCH AKO NEMA GODINE UNETE 
			 * 
			 */
			Preduzece p = ps.findOne(sd.getPreduzece().getId());
			List<PoslovnaGodina> godine = null;
			if(sd.getGodina() != 0)
			  godine = psr.findByGodinaAndPreduzece(sd.getGodina(), p);
			else
				godine = psr.findByPreduzece(p);
			 if(godine == null)
				 return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);

		     return new ResponseEntity<List<PoslovnaGodina>>(godine, HttpStatus.OK);
	    }
	

}
