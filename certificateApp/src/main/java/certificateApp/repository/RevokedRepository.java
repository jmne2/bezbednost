package certificateApp.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import certificateApp.models.Revoked;;

@Repository
public interface RevokedRepository extends CrudRepository<Revoked, Long>{
	public Revoked findBySerialNumber(String serialNumber);
}
