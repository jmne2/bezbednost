package jmn.services;

import java.util.List;

import jmn.models.Banka;
import jmn.models.PoslovniPartner;
import jmn.models.Racun;
import jmn.models.RacunPartnera;

public interface RacunPartneraService {

	List<RacunPartnera> findAll();

	RacunPartnera saveRacunPartnera(RacunPartnera rp);

	RacunPartnera findOne(Long id);

	void delete(Long id);

	void delete(RacunPartnera rp);
	
	List<RacunPartnera> findByPoslovniPartnerAndBanka(PoslovniPartner poslovniPartner, Banka banka);

	List<RacunPartnera> findByPoslovniPartner(PoslovniPartner poslovniPartner);
}
