package certificateApp.generator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.rmi.server.UID;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.SignatureException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.Calendar;
import java.util.Date;

import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.X500NameBuilder;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.cert.jcajce.JcaX509CertificateHolder;
import org.bouncycastle.openssl.jcajce.JcaPEMWriter;
import org.bouncycastle.operator.OperatorCreationException;
import org.springframework.beans.factory.annotation.Autowired;

import certificateApp.models.Certificate;
import certificateApp.models.IssuerData;
import certificateApp.models.Revoked;
import certificateApp.models.SubjectData;
import certificateApp.service.RevokedService;
import certificateApp.service.SerialNumbersService;

public class NewCertificate {

	private Certificate cer;
	private X509Certificate cert;
	private PrivateKey parentPrivateKey;
	private PrivateKey privateKey;
	private PublicKey publicKey;
	private String serNum;
	@Autowired
	private SerialNumbersService snser;
	@Autowired
	private RevokedService rs;
	

	public NewCertificate(){
		
	}
	
	public NewCertificate(Certificate cer, String serNum) throws IOException{
		if(serNum!=null){
			this.serNum = serNum;
		}
		
		if(cer!=null){
			this.cer = cer;
			
			if(cer.getSelfSigned()){
				selfSigned();
			}
		}
	}
	
	// samopotpisujuci sertifikat -> subjectData dobija javni a issuerData dobija privatni kljuc iz istog keyPair-a
	private void selfSigned() throws IOException{
		KeyPair keyPair = generateKeyPair();
		parentPrivateKey = keyPair.getPrivate();
		privateKey = keyPair.getPrivate();
		publicKey = keyPair.getPublic();
		
		SubjectData subjectData = generateSubjectData(keyPair.getPublic(), cer, null, null);
		IssuerData issuerData = generateIssuerData(keyPair.getPrivate(), cer, null);
		
		CertificateGenerator cg = new CertificateGenerator();
		
		this.cert = cg.generateCertificate(subjectData, issuerData, cer.getIsCa(), false, null);
		
		File f = new File("cerNBS/");
		saveCert(f);
		
		System.out.println("\n===== Podaci o izdavacu sertifikata =====");
		System.out.println(cert.getIssuerX500Principal().getName());
		System.out.println("\n===== Podaci o vlasniku sertifikata =====");
		System.out.println(cert.getSubjectX500Principal().getName());
		System.out.println("\n===== Sertifikat =====");
		System.out.println("-------------------------------------------------------");
		System.out.println(cert);
		System.out.println("-------------------------------------------------------");
		
	}

	public String signed(PrivateKey pk, X509Certificate parentCertificate) throws CertificateEncodingException, IOException{
		X500Name name = new JcaX509CertificateHolder(parentCertificate).getSubject();
		
		KeyPair keyPair = generateKeyPair();
		parentPrivateKey = pk;
		privateKey = keyPair.getPrivate();
		publicKey = keyPair.getPublic();
		
		SubjectData subjectData = generateSubjectData(keyPair.getPublic(), cer, null, null);
		IssuerData issuerData = generateIssuerData(pk, null, name);
		CertificateGenerator cg = new CertificateGenerator();

		Revoked r = rs.findBySerialNumber(parentCertificate.getSerialNumber().toString());
		if(r!=null){
			return "revoked";
		}
		
		this.cert = cg.generateCertificate(subjectData, issuerData, cer.getIsCa(), true, parentCertificate);
		File f = new File("cerBanks/");
		saveCert(f);
		
		System.out.println("\n===== Podaci o izdavacu sertifikata =====");
		System.out.println(cert.getIssuerX500Principal().getName());
		System.out.println("\n===== Podaci o vlasniku sertifikata =====");
		System.out.println(cert.getSubjectX500Principal().getName());
		System.out.println("\n===== Sertifikat =====");
		System.out.println("-------------------------------------------------------");
		System.out.println(cert);
		System.out.println("-------------------------------------------------------");
		return "ok";
	}
	
	private SubjectData generateSubjectData(PublicKey key, Certificate cer, X500Name name, PublicKey publicKey2) {
		
			//Datumi od kad do kad vazi sertifikat
			Date startDate = new Date();
			Date endDate = new Date();
			Calendar c = Calendar.getInstance();
			c.setTime(startDate);
			c.add(Calendar.DATE, 365);  // number of days to add
			endDate = c.getTime();
		if(name==null){	
			//klasa X500NameBuilder pravi X500Name objekat koji predstavlja podatke o vlasniku
			X500NameBuilder builder = new X500NameBuilder(BCStyle.INSTANCE);
		    builder.addRDN(BCStyle.CN, cer.getCommonName());
		    builder.addRDN(BCStyle.SURNAME, cer.getSurname());
		    builder.addRDN(BCStyle.GIVENNAME, cer.getGivenName());
		    builder.addRDN(BCStyle.O, cer.getOrganization());
		    builder.addRDN(BCStyle.OU, cer.getOrganizationUnit());
		    builder.addRDN(BCStyle.C, cer.getCountry());
		    builder.addRDN(BCStyle.E, cer.getEmail());
		    //UID (USER ID) je ID korisnika
		   // builder.addRDN(BCStyle.UID, "123456");
		    UID uid = new UID();
		    builder.addRDN(BCStyle.UID, uid.toString());
		    System.out.println("MY UID IS : " + uid);
		    
		    //Kreiraju se podaci za sertifikat, sto ukljucuje:
		    // - javni kljuc koji se vezuje za sertifikat
		    // - podatke o vlasniku
		    // - serijski broj sertifikata
		    // - od kada do kada vazi sertifikat
		    return new SubjectData(key, builder.build(), serNum, startDate, endDate);
		}
		else{
			return new SubjectData(publicKey2, name, serNum, startDate, endDate);
		}
	}
	
	private IssuerData generateIssuerData(PrivateKey issuerKey, Certificate cer, X500Name name) {
		if(cer!=null && name==null){
			X500NameBuilder builder = new X500NameBuilder(BCStyle.INSTANCE);
			builder.addRDN(BCStyle.CN, cer.getCommonName());
		    builder.addRDN(BCStyle.SURNAME, cer.getSurname());
		    builder.addRDN(BCStyle.GIVENNAME, cer.getGivenName());
		    builder.addRDN(BCStyle.O, cer.getOrganization());
		    builder.addRDN(BCStyle.OU, cer.getOrganizationUnit());
		    builder.addRDN(BCStyle.C, cer.getCountry());
		    builder.addRDN(BCStyle.E, cer.getEmail());
		    //UID (USER ID) je ID korisnika
		    UID uid = new UID();
		    builder.addRDN(BCStyle.UID, uid.toString());
	
		    System.out.println("UID ZA ISSUERA JE : " + uid.toString());
			//Kreiraju se podaci za issuer-a, sto u ovom slucaju ukljucuje:
		    // - privatni kljuc koji ce se koristiti da potpise sertifikat koji se izdaje
		    // - podatke o vlasniku sertifikata koji izdaje nov sertifikat
			return new IssuerData(issuerKey, builder.build());
		}
		else{
			return new IssuerData(issuerKey, name);
		}
	
	}
	
/*	
	private BigInteger countSerialNumber() throws KeyStoreException, NoSuchProviderException, NoSuchAlgorithmException, CertificateException, IOException{
		BigInteger bi = new BigInteger(5, new Random());
		System.out.println("SERIJSKI BROJ " + bi);
		return bi;
	}
	*/
	private KeyPair generateKeyPair() {
        try {
			KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA"); 
			SecureRandom random = SecureRandom.getInstance("SHA1PRNG", "SUN");
			keyGen.initialize(2048, random);
			return keyGen.generateKeyPair();
        } catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (NoSuchProviderException e) {
			e.printStackTrace();
		}
        return null;
	}

	public void generateCSR(String bankName, String username) throws InvalidKeyException, NoSuchAlgorithmException, SignatureException, OperatorCreationException, IOException{
		KeyPair keyPair = generateKeyPair();
		publicKey = keyPair.getPublic();
		privateKey = keyPair.getPrivate();
		
		SubjectData subjectData = generateSubjectData(keyPair.getPublic(), cer, null, null);
		WriteCSR writecsr = new WriteCSR();
		writecsr.setSnser(snser);
		writecsr.write(keyPair, subjectData.getX500name(), bankName, username, cer.getPasswordKs(), cer.getPasswordPk(), cer.getAlias(), serNum);
	}
	
	public String approveCsr(X500Name x500name, PublicKey publicKey, X509Certificate parentCert, PrivateKey parentPrivateKey) throws CertificateEncodingException, IOException{
		SubjectData subjectData = generateSubjectData(null, null, x500name, publicKey);
		this.parentPrivateKey = parentPrivateKey;
		X500Name name = new JcaX509CertificateHolder(parentCert).getSubject();
		IssuerData issuerData = generateIssuerData(parentPrivateKey , null, name);
		
		CertificateGenerator cg = new CertificateGenerator();
		Revoked r = rs.findBySerialNumber(parentCert.getSerialNumber().toString());
		if(r!=null){
			return "revoked";
		}
		
		this.cert = cg.generateCertificate(subjectData, issuerData, false, false, parentCert);
		
		System.out.println("\n===== Podaci o izdavacu sertifikata =====");
		System.out.println(cert.getIssuerX500Principal().getName());
		System.out.println("\n===== Podaci o vlasniku sertifikata =====");
		System.out.println(cert.getSubjectX500Principal().getName());
		System.out.println("\n===== Sertifikat =====");
		System.out.println("-------------------------------------------------------");
		System.out.println(cert);
		System.out.println("-------------------------------------------------------");
		return "ok";
	}
	
	private void saveCert(File file){
		//File[] listOfFiles = file.listFiles();
		//int num = listOfFiles.length+1;
		File f = new File(file+"/Certificate"+cert.getSerialNumber()+".cer");
		BufferedWriter w;
		try {
			w = new BufferedWriter(new FileWriter(f.getPath()));
			StringWriter sw = new StringWriter();
			JcaPEMWriter pemWriter = new JcaPEMWriter(sw);
			pemWriter.writeObject(this.cert);
			pemWriter.close();
			sw.close();
			w.write(sw.toString());
			w.flush();
			w.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public Certificate getCer() {
		return cer;
	}

	public void setCer(Certificate cer) {
		this.cer = cer;
	}

	public X509Certificate getCert() {
		return cert;
	}

	public void setCert(X509Certificate cert) {
		this.cert = cert;
	}

	public PrivateKey getPrivateKey() {
		return privateKey;
	}

	public void setPrivateKey(PrivateKey privateKey) {
		this.privateKey = privateKey;
	}

	public PublicKey getPublicKey() {
		return publicKey;
	}

	public void setPublicKey(PublicKey publicKey) {
		this.publicKey = publicKey;
	}

	public String getSerNum() {
		return serNum;
	}

	public void setSerNum(String serNum) {
		this.serNum = serNum;
	}

	public PrivateKey getParentPrivateKey() {
		return parentPrivateKey;
	}

	public void setParentPrivateKey(PrivateKey parentPrivateKey) {
		this.parentPrivateKey = parentPrivateKey;
	}

	public SerialNumbersService getSnser() {
		return snser;
	}

	public void setSnser(SerialNumbersService snser) {
		this.snser = snser;
	}

	public RevokedService getRs() {
		return rs;
	}

	public void setRs(RevokedService rs) {
		this.rs = rs;
	}

	
}
