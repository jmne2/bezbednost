'use strict';
 
angular.module('repo3App').factory('ZatvaranjeIFService', ['$http', '$q', 'urls', function($http, $q, urls){
 
   
 
    var factory = {
    	fetchAllStavkeIzvoda: fetchAllStavkeIzvoda,
    	fetchAllIlazneFakture: fetchAllIlazneFakture,
    	fetchAllZatvoreneFakture : fetchAllZatvoreneFakture,
    	fetchAllPoslovniPartneri: fetchAllPoslovniPartneri,
        searchStavkeIzvoda: searchStavkeIzvoda,
        searchIzlazneFakture: searchIzlazneFakture,
        closeIzlazneFakture: closeIzlazneFakture,
        razveziIzlazne : razveziIzlazne,
        AutomatskoZatvaranje :AutomatskoZatvaranje
    };
 
    return factory;
 
    function fetchAllStavkeIzvoda(id) {
        var deferred = $q.defer();
        $http.post(urls.ZATVARANJE_IF_SERVICE+"stavkeIzvoda/"+id)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while fetching stavke izvoda za zatvaranje');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
    function fetchAllIlazneFakture(id) {
        var deferred = $q.defer();
        $http.post(urls.ZATVARANJE_IF_SERVICE+"fakture/"+id)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while fetching fakture za zatvaranje');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
    
    function fetchAllZatvoreneFakture() {
        var deferred = $q.defer();
        $http.post(urls.ZATVARANJE_IF_SERVICE+"zatvoreneIF/")
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while fetching zatvorene fakture');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
    function searchStavkeIzvoda(stavka){
    	var deferred = $q.defer();
        $http.post(urls.ZATVARANJE_IF_SERVICE+"search/", stavka)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
    
    
    function searchIzlazneFakture(izlaznaFaktura){
    	var deferred = $q.defer();
        $http.post(urls.ZATVARANJE_IF_SERVICE+"search/", izlaznaFaktura)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
 //   function closeIzlazneFakture(izlazneFaktura,stavkeIzvoda, iznos){
    function closeIzlazneFakture(zatvaranjeIF){
    	var deferred = $q.defer();
   //    
    	 $http.post(urls.ZATVARANJE_IF_SERVICE+"zatvoriIF/", zatvaranjeIF)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
    function razveziIzlazne(zatvorenaStavka){
    	var deferred = $q.defer();
        $http.post(urls.ZATVARANJE_IF_SERVICE+"razveziStavku/", zatvorenaStavka)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
    
    function fetchAllPoslovniPartneri(){
    	var deferred = $q.defer();
        $http.post(urls.ZATVARANJE_IF_SERVICE+"poslovniPartneri/")
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
    
    function AutomatskoZatvaranje(){
    	var deferred = $q.defer();
        $http.post(urls.ZATVARANJE_IF_SERVICE+"AutomatskizatvoreneIF/")
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
}]);