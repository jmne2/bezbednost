package jmn.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;

import jmn.annotation.PermissionType;
import jmn.models.PredlogPlacanja;
import jmn.models.Preduzece;
import jmn.models.Racun;
import jmn.models.SifrarnikMetoda;
import jmn.models.User;
import jmn.pojo.UserData;
import jmn.services.PredlogPlacanjaService;
import jmn.services.PreduzeceService;

@Controller
public class PredlogZaPlacanjeController {

	final static Logger logger = Logger.getLogger(PredlogZaPlacanjeController.class);
	
	@Autowired
	private PredlogPlacanjaService pps;
	
	@Autowired
	private PreduzeceService ps;
	
	@PermissionType("PredloziZaPlacanje:view")
	@RequestMapping(value="/predlozi/", method=RequestMethod.GET)
	public ResponseEntity<List<PredlogPlacanja>> fetchPredloge(HttpSession session){
	    UserData u = (UserData) session.getAttribute("user");
        Preduzece p = ps.findOne(u.getPreduzece().getId());
		List<PredlogPlacanja> predlozi = pps.findByPreduzece(p);
		if(predlozi == null)
			return new ResponseEntity<List<PredlogPlacanja>>(HttpStatus.NO_CONTENT);
		return new ResponseEntity<List<PredlogPlacanja>>(predlozi, HttpStatus.OK);
	}
	
	@PermissionType("PredloziZaPlacanje:add")
	@RequestMapping(value = "/predlozi/", method = RequestMethod.POST)
	public ResponseEntity<Void> createPredlog(HttpSession session, @RequestBody PredlogPlacanja predlog, UriComponentsBuilder ucBuilder) {
	        UserData u = (UserData) session.getAttribute("user");
	        Preduzece p = ps.findOne(u.getPreduzece().getId());
	        predlog.setPreduzece(p);
			PredlogPlacanja d = pps.savePredlogPlacanja(predlog); 
	        try {
		        HttpHeaders headers = new HttpHeaders();
		        headers.setLocation(ucBuilder.path("/predlozi/{id}").buildAndExpand(d.getId()).toUri());
		        logger.warn("Korisnik "+ u.getId() + " uspesno je izvrsio metodu " + SifrarnikMetoda.methods.get(
		 				Thread.currentThread().getStackTrace()[1].getMethodName()));
		
		        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	        }
	        catch(Exception e){
	        	logger.error("Greska pri izvrsenju " + SifrarnikMetoda.methods.get(Thread.currentThread().getStackTrace()[1].getMethodName())
	        	+ " za korisnika " + u.getId());
	        	return new ResponseEntity<Void>(HttpStatus.CONFLICT);
	        }
	    }
	@PermissionType("PredloziZaPlacanje:edit")
	 @RequestMapping(value = "/predlozi/{id}", method = RequestMethod.POST)
	    public ResponseEntity<?> updatePredlog(HttpSession session, @PathVariable("id") long id, @RequestBody PredlogPlacanja predlog) {
			UserData u = (UserData) session.getAttribute("user");
			Preduzece p = ps.findOne(u.getPreduzece().getId());
			PredlogPlacanja exists = pps.findOne(id);
		 	if(exists == null){
		 		logger.error("Greska pri izvrsenju " + SifrarnikMetoda.methods.get(Thread.currentThread().getStackTrace()[1].getMethodName())
	        	+ " za korisnika " + u.getId());
		 		return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		 	}
		 	exists.setBroj(predlog.getBroj());
		 	exists.setDatum(predlog.getDatum());
		 	exists.setStatus(predlog.getStatus());
		 	exists.setPreduzece(p);
		 	exists = pps.savePredlogPlacanja(exists);
	        logger.warn("Korisnik "+ u.getId() + " uspesno je izvrsio metodu " + SifrarnikMetoda.methods.get(
	 				Thread.currentThread().getStackTrace()[1].getMethodName()));
		 	return new ResponseEntity<PredlogPlacanja>(exists, HttpStatus.OK);
	    }
	
	@PermissionType("PredloziZaPlacanje:delete")
	@RequestMapping(value = "/predlozi/{id}", method = RequestMethod.DELETE)
	    public ResponseEntity<?> deletePredlog(HttpSession session, @PathVariable("id") long id) {
			UserData u = (UserData)session.getAttribute("user");
		PredlogPlacanja exists = pps.findOne(id); 
		 if(exists == null){
			 logger.error("Greska pri izvrsenju " + SifrarnikMetoda.methods.get(Thread.currentThread().getStackTrace()[1].getMethodName())
	        	+ " za korisnika " + u.getId());
			 return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		 }
		 
		 pps.delete(id);
	     logger.warn("Korisnik "+ u.getId() + " uspesno je izvrsio metodu " + SifrarnikMetoda.methods.get(
				Thread.currentThread().getStackTrace()[1].getMethodName()));
	
	     return new ResponseEntity<Racun>(HttpStatus.NO_CONTENT);
	    }
	
	@PermissionType("PredloziZaPlacanje:search")
	@RequestMapping(value = "/predlozi/search/", method = RequestMethod.POST)
	    public ResponseEntity<?> searchPredloge(HttpSession session, @RequestBody PredlogPlacanja predlog) {
			List<PredlogPlacanja> predlozi = null;	
			if(predlog.getBroj() == null)
				predlog.setBroj("");
			if(predlog.getStatus() == null)
				predlog.setStatus("");
			if(predlog.getDatum() == null)
				predlozi = pps.findByStatusAndBroj(predlog.getStatus(),predlog.getBroj());
			else
				predlozi = pps.findByDatumAndStatusContainingAndBrojContaining(predlog.getDatum(), predlog.getStatus(), 
						predlog.getBroj());
			if(predlozi == null)
			 return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	     
			return new ResponseEntity<List<PredlogPlacanja>>(predlozi, HttpStatus.OK);
	    }
}
