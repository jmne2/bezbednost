<div class="generic-container" ng-controller="SifrarnikDelatnostiController as ctrl" id="SifrarnikDelatnosti">
       <div>
          <div class="panel panel-default">
                <!-- Default panel contents -->
              <div class="panel-heading"><span class="lead">Lista Delatnosti </span></div>
              <div class="tablecontainer">
                  <table class="table table-hover">
                      <thead>
                          <tr>

                              <th>ID.</th>
                              <th>Naziv</th>
                              <th>Sifra</th>
                              <th width="20%"></th>
                          </tr>
                      </thead>
                      <tbody>
                      <!-- ng-click="setSelected(d.id)" ng-class="{selected: d.id === idSelectedVote}" -->
                          <tr ng-repeat="d in ctrl.delatnosti" ng-click="setSelected($index)" ng-class="{selected: $index === indSelectedVote}">
                              <td><span ng-bind="d.id"></span></td>
                              <td><span ng-bind="d.nazivDelatnosti"></span></td>
                              <td><span ng-bind="d.sifra"></span></td>
                              
                          </tr>
                      </tbody>
                  </table>
              </div>
          </div>
          <div class="panel panel-default">
              <div class="formcontainer">
                  <form ng-submit="ctrl.submit()" name="myForm" class="form-horizontal">
                      <input type="hidden" ng-model="ctrl.delatnost.id" />
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="sifra">Sifra </label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="ctrl.delatnost.sifra" id="sifra" class="sifra form-control input-sm" placeholder="Unesite sifru delatnosti" ng-required="required" ng-minlength="{{ctrl.currState === 'search' ? 0 : 3}}" ng-maxlength="5"/>
                                  <div class="has-error" ng-show="myForm.$dirty">
                                      <span ng-show="myForm.oznaka.$error.required">This is a required field</span>
                                      <span ng-show="myForm.oznaka.$error.minlength">Minimum length required is 3</span>
                                      <span ng-show="myForm.oznaka.$error.maxlength">Maximum length required is 5</span>
                                      <span ng-show="myForm.oznaka.$invalid">This field is invalid </span>
                                  </div>
                              </div>
                          </div>
                      </div>
                                                
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="nazivDelatnosti">Naziv</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="ctrl.delatnost.nazivDelatnosti" id="nazivDelatnosti" class="form-control input-sm" ng-required="required" placeholder="Unesite Naziv. [This field is validation free]"/>
                              </div>
                          </div>
                      </div>
 
                      <div class="row">
                          <div class="form-actions floatRight">
                              <input type="submit" id ="subId" value="Commit" class="btn btn-primary btn-sm" ng-disabled="myForm.$invalid">
                              <button type="button" ng-click="ctrl.reset()" class="btn btn-warning btn-sm" ng-disabled="myForm.$pristine">Rollback</button>
                              <label>{{ctrl.currState}}</label>
                           </div>
                      </div>
                  </form>
              </div>
          </div>
          </div>
      </div>
