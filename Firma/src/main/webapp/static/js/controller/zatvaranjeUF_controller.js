'use strict';

angular.module('repo3App').controller('ZatvaranjeUFController', ['$scope', 'ZatvaranjeUFService', '$filter', function($scope,ZatvaranjeUFService, $filter) {
	var self = this;
    self.stavke = [];
    self.stavkeIzvodaZaZatvaranje = []
    self.ufakture = [];
    self.ufaktureZaZatvaranje = []
    self.show = false;
    self.logged = false;
    self.children = [""];
    
    self.submit = submit;
    self.reset = reset;
//    self.search = search;
    self.close = close;
    self.currState = "edit";
    $scope.required = true;
    

    
    $scope.$on('dispAfterLog', function(event, args) {
    	if(self === args.ctrl){
    		self.logged = true;
    		self.show = true;
    		fetchAllStavkeIzvoda();
    		fetchAllUlazneFakture();
    	} else {
    		self.logged = false;
    		self.show = false;
    	}
    });
    
    $scope.$on('changeState', function(event, args) {    	
    	if(self === args.ctrl){
    		self.currState = args.state;
    		if(self.currState === "add"){
    		self.currState = "edit";
    		}
    		$scope.required = (self.currState === "search") ? false : true;
    	}
    });
    $scope.$on('hide', function(event, args) {
    	self.show = false;
    	
    });
    $scope.$on('resetEvent', function(event, args) {    	
    	if(self === args.ctrl){
    		self.currState = "edit";
    		fetchAllStavkeIzvoda();
    		fetchAllUlazneFakture();
    	}
    });
     
 
    $scope.select = function(item) {
        item.selected ? item.selected = false : item.selected = true;
      }
    
    $scope.selectOther = function(item) {
        item.selectedOther ? item.selectedOther = false : item.selectedOther = true;
      }
    
    $scope.getAllSelectedRows = function() {
    	self.ufaktureZaZatvaranje = $filter("filter")(self.ufakture, {
          selected: true
        }, true);
        
    	$filter("filter")(self.ifakture, {
            selected: false
          }, false);
    	
        self.stavkeIzvodaZaZatvaranje = $filter("filter")(self.stavke,{
        	selectedOther: true}, true);
        console.log(self.ufaktureZaZatvaranje);
        console.log(self.stavkeIzvodaZaZatvaranje);
        
      }
    
    
 
    
    function fetchAllStavkeIzvoda(){
    	ZatvaranjeUFService.fetchAllStavkeIzvoda()
            .then(
            function(d) {
            	self.stavke = d;
            },
            function(errResponse){
                console.error('Error while fetching stavke izvoda');
            }
        );
    }
 
    
    function fetchAllUlazneFakture(){
    	ZatvaranjeUFService.fetchAllUlazneFakture()
            .then(
            function(d) {
            	self.ufakture = d;
            },
            function(errResponse){
                console.error('Error while fetching izlazne fakture');
            }
        );
    }
 
    function close(ulazneFakture,stavkeFakture){
    	ZatvaranjeUFService.closeUlazneFakture(ulazneFaktkure,stavkeFakture)
        .then(
        function(d) {
        },
        function(errResponse){
            console.error('Error while closing invoices');
        }
        );
    }
    
    function submit() {
    	if (self.currState === "add"){
    		close(self.ufaktureZaZatvaranje,self.stavkeIzvodaZaZatvaranje)
        }else if (self.currState === "edit"){
        } else if (self.currState === "search"){
    //    	search(self.stavkaFakture);
        }
	        reset();
    }
 
    function edit(id){
        console.log('id to be edited', id);
        for(var i = 0; i < self.stavkeFakture.length; i++){
            if(self.stavkeFakture[i].id === id) {
                self.stavkaFakture = angular.copy(self.stavkeFakture[i]);
                break;
            }
        }
    }
 

 
 
    function reset(){
    	
    }
 /*   
    function search(stavkaFakture){
    	StavkeFaktureService.searchStavkeFakture(stavkaFakture)
        .then(
        function(d) {
        	self.stavkeFakture = d;
            $scope.indSelectedVote = -1;
            self.indSelected = $scope.indSelectedVote;
        },
        function(errResponse){
            console.error('Error while fetching stavke fakture');
        }
        );
    }
    */
}]);