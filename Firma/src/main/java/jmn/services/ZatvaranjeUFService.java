package jmn.services;

import java.util.List;

import jmn.models.Faktura;
import jmn.models.ZatvaranjeUF;

public interface ZatvaranjeUFService {

	List<ZatvaranjeUF> findAll();

	ZatvaranjeUF saveZatvaranjeUF(ZatvaranjeUF z);

	ZatvaranjeUF findOne(Long id);

	void delete(Long id);

	void delete(ZatvaranjeUF z);
	
	List<ZatvaranjeUF> findByUlaznaFaktura(Faktura ulaznaFaktura);
}
