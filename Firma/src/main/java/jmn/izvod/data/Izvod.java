
package jmn.izvod.data;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for izvod complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="izvod">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="brRacuna" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="datumNaloga" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="brPreseka" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="prethodnoStanje" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="brPromenaUKorist" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ukupnoUKorist" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="brPromenaNaTeret" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ukupnoNaTeret" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="novoStanje" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="stavke" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="stavke" type="{http://izvod/data}stavkaIzvodabanka" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "izvod", propOrder = {
    "brRacuna",
    "datumNaloga",
    "brPreseka",
    "prethodnoStanje",
    "brPromenaUKorist",
    "ukupnoUKorist",
    "brPromenaNaTeret",
    "ukupnoNaTeret",
    "novoStanje",
    "stavke"
})
public class Izvod {

    @XmlElement(required = true)
    protected String brRacuna;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar datumNaloga;
    protected int brPreseka;
    protected double prethodnoStanje;
    protected int brPromenaUKorist;
    protected double ukupnoUKorist;
    protected int brPromenaNaTeret;
    protected double ukupnoNaTeret;
    protected double novoStanje;
    protected Izvod.Stavke stavke;

    /**
     * Gets the value of the brRacuna property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBrRacuna() {
        return brRacuna;
    }

    /**
     * Sets the value of the brRacuna property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBrRacuna(String value) {
        this.brRacuna = value;
    }

    /**
     * Gets the value of the datumNaloga property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDatumNaloga() {
        return datumNaloga;
    }

    /**
     * Sets the value of the datumNaloga property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDatumNaloga(XMLGregorianCalendar value) {
        this.datumNaloga = value;
    }

    /**
     * Gets the value of the brPreseka property.
     * 
     */
    public int getBrPreseka() {
        return brPreseka;
    }

    /**
     * Sets the value of the brPreseka property.
     * 
     */
    public void setBrPreseka(int value) {
        this.brPreseka = value;
    }

    /**
     * Gets the value of the prethodnoStanje property.
     * 
     */
    public double getPrethodnoStanje() {
        return prethodnoStanje;
    }

    /**
     * Sets the value of the prethodnoStanje property.
     * 
     */
    public void setPrethodnoStanje(double value) {
        this.prethodnoStanje = value;
    }

    /**
     * Gets the value of the brPromenaUKorist property.
     * 
     */
    public int getBrPromenaUKorist() {
        return brPromenaUKorist;
    }

    /**
     * Sets the value of the brPromenaUKorist property.
     * 
     */
    public void setBrPromenaUKorist(int value) {
        this.brPromenaUKorist = value;
    }

    /**
     * Gets the value of the ukupnoUKorist property.
     * 
     */
    public double getUkupnoUKorist() {
        return ukupnoUKorist;
    }

    /**
     * Sets the value of the ukupnoUKorist property.
     * 
     */
    public void setUkupnoUKorist(double value) {
        this.ukupnoUKorist = value;
    }

    /**
     * Gets the value of the brPromenaNaTeret property.
     * 
     */
    public int getBrPromenaNaTeret() {
        return brPromenaNaTeret;
    }

    /**
     * Sets the value of the brPromenaNaTeret property.
     * 
     */
    public void setBrPromenaNaTeret(int value) {
        this.brPromenaNaTeret = value;
    }

    /**
     * Gets the value of the ukupnoNaTeret property.
     * 
     */
    public double getUkupnoNaTeret() {
        return ukupnoNaTeret;
    }

    /**
     * Sets the value of the ukupnoNaTeret property.
     * 
     */
    public void setUkupnoNaTeret(double value) {
        this.ukupnoNaTeret = value;
    }

    /**
     * Gets the value of the novoStanje property.
     * 
     */
    public double getNovoStanje() {
        return novoStanje;
    }

    /**
     * Sets the value of the novoStanje property.
     * 
     */
    public void setNovoStanje(double value) {
        this.novoStanje = value;
    }

    /**
     * Gets the value of the stavke property.
     * 
     * @return
     *     possible object is
     *     {@link Izvod.Stavke }
     *     
     */
    public Izvod.Stavke getStavke() {
        return stavke;
    }

    /**
     * Sets the value of the stavke property.
     * 
     * @param value
     *     allowed object is
     *     {@link Izvod.Stavke }
     *     
     */
    public void setStavke(Izvod.Stavke value) {
        this.stavke = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="stavke" type="{http://izvod/data}stavkaIzvodabanka" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "stavke"
    })
    public static class Stavke {

        protected List<StavkaIzvodabanka> stavke;

        /**
         * Gets the value of the stavke property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the stavke property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getStavke().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link StavkaIzvodabanka }
         * 
         * 
         */
        public List<StavkaIzvodabanka> getStavke() {
            if (stavke == null) {
                stavke = new ArrayList<StavkaIzvodabanka>();
            }
            return this.stavke;
        }

    }

}
