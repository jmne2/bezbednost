package izvod;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.ejb.Stateless;
import javax.jws.WebService;
import javax.xml.datatype.DatatypeConfigurationException;

import db.Database;
import izvod.data.Izvod;
import izvod.data.Mt102;
import izvod.data.Mt103;
import izvod.data.Mt910;
import izvod.data.Nalog;
import izvod.data.NalogBanka;
import izvod.data.ZahtevZaIzvod;

@Stateless
@WebService(portName = "IzvodWSPort", 
			serviceName = "IzvodWSService", 
			targetNamespace = "http://izvod", 
			endpointInterface = "izvod.IzvodWS", 
			wsdlLocation = "WEB-INF/wsdl/IzvodWS.wsdl",
			name = "IzvodWS")
public class IzvodWSImpl implements IzvodWS{

	private String bankName;
	private String BankCode ="";
	
	public IzvodWSImpl(){
		System.out.println("Created IzvodWSImpl");
		
	}
	
	public Izvod getIzvod(ZahtevZaIzvod zahtev) {
		Database db = new Database(BankCode);
		try {
			Izvod izvod = db.readDataBase(zahtev);
			db.close();
			if (izvod == null){
				System.out.println("NIJE NADJENO");
			}
			return izvod;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public void sendNalog(NalogBanka nalogZaPlacanje) {
		System.out.println("Code je " + BankCode);
		Database db = new Database(BankCode);
		try {
			db.readNalogBanka(nalogZaPlacanje);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		File f = new File("../webapps/banka-ws/WEB-INF/BankData/" + bankName);
		System.out.println("File f je " + f.getAbsolutePath());
		String code= "";
		try {
			BufferedReader bf = new BufferedReader(new FileReader(f));
			while((code = bf.readLine())!=null){

				System.out.println("CODE JE " + code); 
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		BankCode = code;
	}

	public String getBankCode() {
		return BankCode;
	}

    public void receiveMT102AndMT910(Mt102 mt102, Mt910 mt910) {  
    	System.out.println("*******************RECEIVING MT102 AND MT910 *************");
    	System.out.println("********** ABOUT TO SEND " + mt910.getIznos() + " TO " + mt910.getRacunBankeP() +" bank" );
       Database db = new Database(BankCode);
       for(Nalog n : mt102.getNalozi()){
    	   java.util.Date date = n.getDatumNaloga().toGregorianCalendar().getTime();
    	   Date datt = new Date(date.getTime());
    	   java.util.Date date2 = new java.util.Date();
    	   if(mt102.getDatumValute()!=null){
    		    date2 = mt102.getDatumValute().toGregorianCalendar().getTime();
    	   }
    		Date date22 = new Date(date2.getTime());
			
    	   izvod.data.Mt103 mt103 = new izvod.data.Mt103(mt102.getIdPoruke(), mt102.getSwiftBankeD(), mt102.getRacunBankeD(), mt102.getSwiftBankeP(), mt102.getRacunBankeP(), n.getUplatilac(), n.getSvrhaUplate(), n.getPrimalac(), datt, date22, n.getRacunUplatioca(), n.getModelZaduzenja(), n.getPozivNaBrojZaduzenja(), n.getRacunPrimaoca(), n.getModelOdobrenja(), n.getPozivNaBrojOdobrenja(), n.getIznos(), n.getOznakaValute());
    		try {
				db.finishTransaction(mt103, mt910);
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
       }
       db.emptyNalogBanka();
       
    }

    @Override
    public void receiveNalogNbs(Mt103 mt103,Mt910 mt910) {  
        Database db = new Database(BankCode);
//        java.util.Date date1 = mt103.getDatumNaloga().toGregorianCalendar().getTime();
// 	   Date datt1 = new Date(date1.getTime());
// 	  java.util.Date date2 = mt103.getDatumNaloga().toGregorianCalendar().getTime();
//	   Date datt2 = new Date(date2.getTime());
//        izvod.data.Mt103 mt1032 = new izvod.data.Mt103(mt103.getIdPoruke(), mt103.getSwiftBankeD(), mt103.getRacunBankeD(), mt103.getSwiftBankeP(), mt103.getRacunBankeP(), mt103.getDuznik(), mt103.getSvrhaPlacanja(), mt103.getPrimalac(), datt2, datt1, mt103.getRacunDuznika(), mt103.getModelZaduzenja(), mt103.getPozivNaBrojZaduzenja(), mt103.getRacunPrimaoca(), mt103.getModelOdobrenja(), mt103.getPozivNaBrojOdobrenja(), mt103.getIznos(), mt103.getSifraValute());
        try {
			db.finishTransaction(mt103, mt910);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }


    public void clearing(String bankCode2) {  
    	System.out.println("***********STARTING CLEARING*************");
       Database db = new Database(BankCode);
       try {
		db.clear(bankCode2);
	} catch (MalformedURLException | SQLException | DatatypeConfigurationException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
        
    }


    public void setBankCode(String bankCode) {  
       this.BankCode = bankCode;  
    }


    public ArrayList<Nalog> clearing2(String bankCode) {  
        Database db = new Database(BankCode);
        try {
			return db.clear2(bankCode);
		} catch (MalformedURLException | SQLException | DatatypeConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return null;
    }


/*	public Izvod[] getIzvodi(){
		System.out.println("Invoked getIzvodi");
		if(izvodi==null){
			System.out.println("NULL");
			if(izvodi.size()==0){
				System.out.println("SIZE JE 0000000");
			}
		}
		System.out.println("SIze : " + izvodi.size());
		return toArray();
	}
	
	private Izvod[] toArray() {
        if(izvodi.size() == 0)
            return null;
        
        Izvod[] retVal = new Izvod[izvodi.size()];
        Enumeration<Izvod> enumeration = izvodi.elements();
        
        int i = 0;
        while(enumeration.hasMoreElements()) {
            Izvod item = enumeration.nextElement();
            retVal[i] = item;
            i++;
        }
        
        return retVal;
    }
    */
}
