<div class="generic-container" ng-controller="ZatvaranjeUFController as ctrl" id="ZatvaranjeUF">
      <style>
      .ScrollStyle
	{
    max-height: 250px;
    overflow-y: scroll;
	}

  	tr.selected td {
      background: #96a6ff;
    }
    
    tr.selectedOther td {
      background: #96a6ff;
    }
    
      </style>
       <!-- DUGMICI ZA BIRANJE VRSTE ZATVARANJA -->    
      <div id="dugmici" class="centered" style="display: inline-block; align-self: center;  padding-top: 150px; padding-left: 400px;" ng-show="ctrl.dugmici" >
		 <button type="button" class="btn btn-w3r" ng-click="ctrl.showRucnoZatvaranje()">Automatsko zatvaranje</button> 
		 <button type="button" class="btn btn-w3r" ng-click="ctrl.showReport()">Rucno zatvaranje</button> 
      </div>
      <!-- STAVKE IZVODAAA -->
      <div >
          <div class="panel panel-default">
                <!-- Default panel contents -->
              <div class="panel-heading"><span class="lead">Lista stavki izvoda </span></div>
              <div class="tablecontainer ScrollStyle"  >
                  <table class="table table-hover ">
                      <thead>
                          <tr>
                              <th>ID</th>
                              <th>Duznik</th>
                              <th>Svrha placanja</th>
                              <th>Iznos</th>
                              <th>Primalac</th>
                              <th>Datum naloga</th>
                              <th>Datum valute</th>
                              <th>Racun primaoca</th>
                              <th>Racun duznika</th>
                              <th>Model zaduzenja</th>
                              <th>Poziv na proj zaduzenja</th>
                              <th>Model odobrenja</th>
                              <th>Poziv na broj odobrenja</th>
                              <th>Smer</th>
                              <th>Za Izvod</th>
                          </tr>
                      </thead>
                      <tbody>
                          <tr ng-repeat="s in ctrl.stavke" ng-click="selectOther(s)" ng-class="{'selectedOther': s.selectedOther}">
                              <td><span ng-bind="s.id"></span></td>
                              <td><span ng-bind="s.duznik"></span></td>
                              <td><span ng-bind="s.svrhaPlacanja"></span></td>
                              <td><span ng-bind="s.iznos"></span></td>
                              <td><span ng-bind="s.primalac"></span></td>
                              <td><span ng-bind="s.datumNaloga | date: 'dd.MM.yyyy'"></span></td>
                              <td><span ng-bind="s.datumValute | date: 'dd.MM.yyyy'"></span></td>
                              <td><span ng-bind="s.racunPrimaoca"></span></td>
                              <td><span ng-bind="s.racunDuznika"></span></td>
                              <td><span ng-bind="s.modelZaduzenja"></span></td>
                              <td><span ng-bind="s.pozivNaBrojZaduzenja"></span></td>
                              <td><span ng-bind="s.modelOdobrenja"></span></td>
                              <td><span ng-bind="s.pozivNaBrojOdobrenja"></span></td>
                              <td><span>{{s.smer === 'P' ? 'U korist' : 'Na teret'}}</span></td>
                              <td><span ng-bind="s.dnevnoStanje.datumIzvoda.substr(8,2)"></span>.<span ng-bind="s.dnevnoStanje.datumIzvoda.substr(5,2)"></span>.<span ng-bind="s.dnevnoStanje.datumIzvoda.substr(0,4)"></span></td>
                          </tr>
                      </tbody>
                  </table>
              </div>
          </div>
          </div>
          
          <!--  ng-show="ctrl.ufaktureshow"  -->
       <!-- ULAZNE FAKTUREE -->
       
       <!-- ULAZNE FAKTURE -->   
       <div id="ulazneFakture" >
          <div class="panel panel-default">
                <!-- Default panel contents -->
                
                <div class="panel-heading"><span class="lead">Lista ulaznih faktura </span></div>
              <div class="tablecontainer ScrollStyle">
                  <table class="table table-hover">
                      <thead>
                          <tr>
                              <th>ID.</th>
                              <th>Broj fakture</th>
                              <th>Datum fakture</th>
                              <th>Datum valute</th>
                              <th>Vrednost robe</th>
                              <th>Vrednost usluga</th>
                              <th>Vrednost roba i usluga</th>
                              <th>Ukupan rabat</th>
                              <th>Ukupan porez</th>
                              <th>Oznaka valute</th>
                              <th>Ukupan iznos</th>
                              <th>Preostali iznos</th>
                              <th>Poslovna godina</th>
                              <th>Poslovni partner</th>
                              <th>Racun partnera</th>
                          </tr>
                      </thead>
                      <tbody>
                      <!-- ng-click="setSelected(d.id)" ng-class="{selected: d.id === idSelectedVote}" -->
                          <tr ng-repeat="u in ctrl.ufakture"  ng-click="select(i)" ng-class="{'selected': i.selected}">
                              <td><span ng-bind="u.id"></span></td>
                              <td><span ng-bind="u.brojFakture"></span></td>
                              <td><span ng-bind="u.datumFakture"></span></td>
                              <td><span ng-bind="u.datumValute"></span></td>
                              <td><span ng-bind="u.vrednostRobe"></span></td>
                              <td><span ng-bind="u.vrednostUsluga"></span></td>
                              <td><span ng-bind="u.ukupnoRobaIUsluge"></span></td>
                              <td><span ng-bind="u.ukupanRabat"></span></td>
                              <td><span ng-bind="u.ukupanPorez"></span></td>
                              <td><span ng-bind="u.oznakaValute"></span></td>
                              <td><span ng-bind="u.ukIznos"></span></td>
                              <td><span ng-bind="u.preostaliIznos"></span></td>
                              <td><span ng-bind="u.poslovnaGodina.godina"></span></td>
                              <td><span ng-bind="u.poslovniPartner.naziv"></span></td>
                              <td><span ng-bind="u.racunPartnera.brRacuna"></span></td>
                          </tr>
                      </tbody>
                  </table>
              </div>
          </div>
          </div>
          
          
          <div class="panel panel-default">
              <div class="formcontainer">
                  <form ng-submit="ctrl.submit()" name="myForm" class="form-horizontal">
          
                      <div class="row">
                          <div class="form-actions floatRight">
                           <button ng-click="getAllSelectedRows()">getAllSelectedRows</button>
                              <input type="submit" id ="subId" value="Commit" class="btn btn-primary btn-sm" ng-disabled="myForm.$invalid">
                              <button type="button" ng-click="ctrl.reset()" class="btn btn-warning btn-sm" ng-disabled="myForm.$pristine">Rollback</button>
                              <label>{{ctrl.currState}}</label>
                          </div>
                      </div>
                  </form>
              </div>
          </div>
</div>
