'use strict';
 
//angular.module('repo3App').controller('DrzavaController', ['$scope', 'DrzavaService', SDCtrl]);
angular.module('repo3App').controller('SifrarnikDelatnostiController', SDCtrl);

function SDCtrl($scope, SifrarnikDelatnostiService) {
    var self = this;
    self.drzava={id:null, oznaka:'', naziv:''};
    self.drzave=[];
    self.show = false;
 //   self.children = ['NaseljenoMestoController'];
    self.logged = false;
 
    self.submit = submit;
    self.edit = edit;
    self.remove = remove;
    self.reset = reset;
    self.first = first;
    self.prev = prev;
    self.next = next;
    self.last = last;
    self.search = search;
    self.currState = "edit";
    self.children = [];
    $scope.indSelectedVote = -1;
    $scope.required = true;
    
    $scope.$on('changeState', function(event, args) {    	
    	if(self === args.ctrl){
    		self.currState = args.state;
    		$scope.required = (self.currState === "search") ? false : true;
    		self.delatnost = {};
    	}
    });
    $scope.$on('resetEvent', function(event, args) {    	
    	if(self === args.ctrl){
    		self.currState = "edit";
    		fetchDelatnosti();
    	}
    });
    //TODO: change or scrap!!!
    $scope.$on('hidingYoCountries', function(event, args) {
    	self.show = args.arg[0];
    	
    });
    $scope.$on('hide', function(event, args) {
    	self.show = false;
    	
    });
    $scope.$on('changeSelection', function(event, args) {    	
    	if(self === args.ctrl){
    		switch (args.direction) {
	            case 'prev':
	                prev();
	                break;
	            case 'next':
	                next();
	                break;
	            case 'first':
	            	first();
	            	break;
	            case 'last':
	            	last();
	            	break;
    		}
    	}
    });
    
    $scope.$on('deleteSel', function(event, args) {    	
    	if(self === args.ctrl){
    		if($scope.indSelectedVote != -1){
    			var selectedDelatnost = self.delatnosti[$scope.indSelectedVote];
    			remove(selectedDelatnost.id);
    			self.currState = "edit";
    		}
    	}
    });
    $scope.$on('dispAfterLog', function(event, args) {
    	if(self === args.ctrl){
    		self.logged = true;
    		fetchDelatnosti();
    	} else {
    		self.logged = false;
    	}
    });
    
    function indexOfRole(propertyName,lookingForValue,array) {
        for (var i in array) {
            if (array[i][propertyName] == lookingForValue) {
                return i;
            }
        }
        return -1;
    }
    
    function first(){
    	$scope.indSelectedVote = 0;
    }
    function prev(){
    	if(($scope.indSelectedVote != 0) && ($scope.indSelectedVote != -1)){
    		$scope.indSelectedVote = $scope.indSelectedVote - 1;
    	}
    }
    
    function next(){
    	if(($scope.indSelectedVote != self.drzave.length) && ($scope.indSelectedVote != -1)){
    		$scope.indSelectedVote = $scope.indSelectedVote + 1;
    	}
    }
    
    function last(){
    	$scope.indSelectedVote = self.delatnosti.length - 1;
    }
    
    $scope.setSelected = function (indSelectedVote) {
    	$scope.indSelectedVote = indSelectedVote;
    	if(self.currState === "edit"){
    		var id = self.delatnosti[indSelectedVote].id;
    		edit(id);
    	}
    };
    $scope.changeState = function(state){
    	if(self.currState != state){
    		self.currState = state;
    	}
    }
    
    function fetchDelatnosti(){
        SifrarnikDelatnostiService.fetchDelatnosti()
            .then(
            function(d) {
            	self.delatnosti = d;
                $scope.indSelectedVote = -1;
                self.show = true;
                if (self.currState === "delete"){
                	self.currState = "edit";
                }
            },
            function(errResponse){
                console.error('Error while fetching Users');
            }
        );
    }
 
    function createDelatnost(delatnost){
        SifrarnikDelatnostiService.createDelatnost(delatnost)
            .then(
            fetchDelatnosti,
            function(errResponse){
                console.error('Error while creating User');
            }
        );
    }
 
    function updateDelatnost(delatnost, id){
        SifrarnikDelatnostiService.updateDelatnost(delatnost, id)
            .then(
            fetchDelatnosti,
            function(errResponse){
                console.error('Error while updating User');
            }
        );
    }
 
    function deleteDelatnost(id){
        SifrarnikDelatnostiService.deleteDelatnost(id)
            .then(
            fetchDelatnosti,
            function(errResponse){
                console.error('Error while deleting User');
            }
        );
    }
 
    function submit() {
        
    	if(self.currState === "add"){
            console.log('Saving New THing', self.delatnost);
            createDelatnost(self.delatnost);        	
        } else if(self.currState === "edit"){
            updateDelatnost(self.delatnost, self.delatnost.id);
            console.log('Thing updated with id ', self.delatnost.id);
        } else if (self.currState === "search"){
        	search(self.delatnost);
        }
        reset();
    }
 
    function edit(id){
        console.log('id to be edited', id);
        for(var i = 0; i < self.delatnosti.length; i++){
            if(self.delatnosti[i].id === id) {
                self.delatnost = angular.copy(self.delatnosti[i]);
                break;
            }
        }
    }
 
    function remove(id){
        console.log('id to be deleted', id);
        if(self.delatnost.id === id) {//clean form if the user to be deleted is shown there.
            reset();
        }
        deleteDelatnost(id);
    }
 
    function search(delatnost){
    	SifrarnikDelatnostiService.searchDelatnost(delatnost)
        .then(
        function(d) {
        	self.delatnosti = d;
            $scope.indSelectedVote = -1;
        },
        function(errResponse){
            console.error('Error while fetching Users');
        }
        );
    }
    
    function reset(){
        self.delatnost={};
        $scope.myForm.$setPristine(); //reset Form
    }
};