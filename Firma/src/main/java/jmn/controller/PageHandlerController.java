package jmn.controller;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jmn.annotation.PermissionType;
import jmn.annotation.UserRequired;

@Controller
public class PageHandlerController {

	@UserRequired
	@RequestMapping(value="/startOptions", method = RequestMethod.GET)
    public String serveStartingTemplate(HttpSession session) {
        return "startOptions";
    }
 
	@UserRequired
	@RequestMapping(value="/changePass", method = RequestMethod.GET)
	 public String serveTemplate(HttpSession session) {
	     return "changePass";
	 }

	@PermissionType("NaseljenaMesta:view")
	@RequestMapping(value="/NaseljenaMesta", method = RequestMethod.GET)
    public String serveMestaTemplate(HttpSession session) {
        return "NaseljenaMesta";
    }
	@PermissionType("Drzave:view")
	@RequestMapping(value="/Drzave", method = RequestMethod.GET)
    public String serveDrzaveTemplate(HttpSession session) {
        return "Drzave";
    }
	
	@PermissionType("SifrarnikDelatnosti:view")
	@RequestMapping(value="/SifrarnikDelatnosti", method = RequestMethod.GET)
	public String serveSifrarnikTemplate(HttpSession session) {
		return "SifrarnikDelatnosti";
	}
	
	@PermissionType("Racuni:view")
	@RequestMapping(value="/Racuni", method = RequestMethod.GET)
	public String serveRacuniTemplate(HttpSession session) {
		return "Racuni";
	}

	@PermissionType("RacuniPartnera:view")
	@RequestMapping(value="/RacuniPartnera", method = RequestMethod.GET)
	public String serveRacuniPartneraTemplate(HttpSession session) {
		return "RacuniPartnera";
	}
	
	@PermissionType("PoslovneGodine:view")
	@RequestMapping(value="/PoslovneGodine", method = RequestMethod.GET)
	public String serveGodineTemplate(HttpSession session) {
		return "PoslovneGodine";
	}
	
	@PermissionType("PoslovniPartner:view")
	@RequestMapping(value="/PoslovniPartner", method = RequestMethod.GET)
	public String servePartnerTemplate(HttpSession session) {
		return "PoslovniPartner";
	}
	
	@PermissionType("StaSePlaca:view")
	@RequestMapping(value="/StaSePlaca", method = RequestMethod.GET)
	public String servePlacanjeTemplate(HttpSession session) {
		return "StaSePlaca";
	}
	
	@PermissionType("PredloziZaPlacanje:view")
	@RequestMapping(value="/PredloziZaPlacanje", method = RequestMethod.GET)
	public String servePredlogeTemplate(HttpSession session) {
		return "PredloziZaPlacanje";
	}

	@PermissionType("NaloziZaPlacanje:view")
	@RequestMapping(value="/NaloziZaPlacanje", method = RequestMethod.GET)
	public String serveNalogeTemplate(HttpSession session) {
		return "NaloziZaPlacanje";
	}
	@PermissionType("Fakture:view")
 @RequestMapping(value="/Fakture", method = RequestMethod.GET)
 public String serveFaktureTemplate(HttpSession session) {
     return "Fakture";
 }
 
	@PermissionType("Adrese:view")
	@RequestMapping(value="/Adrese", method = RequestMethod.GET)
	public String serveAdreseTemplate(HttpSession session) {
		return "Adrese";
	}
	
	@PermissionType("Banke:view")
	@RequestMapping(value="/Banke", method = RequestMethod.GET)
	public String serveBankeTemplate(HttpSession session) {
		return "Banke";
	}
	
	@PermissionType("Preduzeca:view")
	@RequestMapping(value="/Preduzeca", method = RequestMethod.GET)
	public String servePreduzeceTemplate(HttpSession session) {
		return "Preduzeca";
	}
	
	@PermissionType("DnevnaStanja:view")
	@RequestMapping(value="/DnevnaStanja", method = RequestMethod.GET)
	public String serveDnevnaTemplate(HttpSession session) {
		return "DnevnaStanja";
	}
	
	@PermissionType("StavkeIzvoda:view")
	@RequestMapping(value="/StavkeIzvoda", method = RequestMethod.GET)
	public String serveStavkeTemplate(HttpSession session) {
		return "StavkeIzvoda";
	}
	
	@RequestMapping(value="/loginBlock", method = RequestMethod.GET)
	public String serveloginTemplate(HttpSession session) {
		return "loginBlock";
	}
 
	@PermissionType("ZatvaranjeUF:view")
	@RequestMapping(value="/ZatvaranjeUF", method = RequestMethod.GET)
	public String servesZatvaranjeUFTemplate(HttpSession session) {
		return "ZatvaranjeUF";
	}
	
	@PermissionType("ZatvaranjeIF:view")
	@RequestMapping(value="/ZatvaranjeIF", method = RequestMethod.GET)
	public String servesZatvaranjeIFTemplate(HttpSession session) {
		return "ZatvaranjeIF";
	}
	
	@PermissionType("StavkeFakture:view")
	@RequestMapping(value="/StavkeFakture", method = RequestMethod.GET)
	public String servestavkeFaktureTemplate(HttpSession session) {
		return "StavkeFakture";
	}
}
