package nbs;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class AES {

	private static SecretKeySpec secretKey;
    private static byte[] key;
    private static IvParameterSpec IV;
    private static int ivSize = 16;
    private static byte[] bIV;
 
    public static void setKey(String myKey) 
    {
        MessageDigest sha = null;
        try {
            key = myKey.getBytes("UTF-8");
            sha = MessageDigest.getInstance("SHA-256");
            key = sha.digest(key);
            key = Arrays.copyOf(key, 16); 
            System.out.println("*****key je " + key);
            secretKey = new SecretKeySpec(key, "AES");
            System.out.println("******secret key je " + secretKey);
        } 
        catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } 
        catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
 
    private static void setIV(){
    	bIV = new byte[ivSize];
        SecureRandom random = new SecureRandom();
        random.nextBytes(bIV);
        IV = new IvParameterSpec(bIV);
    }
    
    public static String encodeIV(){
    	return Base64.getEncoder().encodeToString(bIV);
    }
    
    public static String initDEK(){
    	byte[] kk = new byte[16];
    	SecureRandom random = new SecureRandom();
        random.nextBytes(kk);
    	return Base64.getEncoder().encodeToString(kk);
    }
    
    public static String encryptCBC(String strToEncrypt, String secret, String iv) 
    {
        try
        {
            setKey(secret);
            IV = new IvParameterSpec(Base64.getDecoder().decode(iv));
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey, IV);
            return Base64.getEncoder().encodeToString(cipher.doFinal(strToEncrypt.getBytes("UTF-8")));
        } 
        catch (Exception e) 
        {
            System.out.println("Error while encrypting: " + e.toString());
        }
        return null;
    }

    public static String decryptCBC(String strToDecrypt, String secret, String iv) 
    {
        try
        {
            setKey(secret);
            IV = new IvParameterSpec(Base64.getDecoder().decode(iv));
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.DECRYPT_MODE, secretKey, IV);
            return new String(cipher.doFinal(Base64.getDecoder().decode(strToDecrypt)));
        } 
        catch (Exception e) 
        {
            System.out.println("Error while decrypting: " + e.toString());
        }
        return null;
    }
    
    
    public static String encryptDEK(String strToEncrypt, String secret) 
    {
        try
        {
            setKey(secret);
            setIV();
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            return Base64.getEncoder().encodeToString(cipher.doFinal(strToEncrypt.getBytes("UTF-8")));
        } 
        catch (Exception e) 
        {
            System.out.println("Error while encrypting: " + e.toString());
        }
        return null;
    }
    
    
 
    public static String decryptDEK(String strToDecrypt, String secret) 
    {
        try
        {
            setKey(secret);
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
            cipher.init(Cipher.DECRYPT_MODE, secretKey);
            return new String(cipher.doFinal(Base64.getDecoder().decode(strToDecrypt)));
        } 
        catch (Exception e) 
        {
            System.out.println("Error while decrypting: " + e.toString());
        }
        return null;
    }
}
