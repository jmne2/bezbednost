package jmn.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jmn.models.Permission;
import jmn.repositories.PermissionRepository;

@Service
public class PermissionServiceImpl implements PermissionService{

	@Autowired
	private PermissionRepository pr;
	
	@Override
	public List<Permission> findAll() {
		return (List<Permission>) pr.findAll();
	}

	@Override
	public Permission savePermission(Permission perm) {
		return pr.save(perm);
	}

	@Override
	public Permission findOne(Long id) {
		return pr.findOne(id);
	}

	@Override
	public void delete(Long id) {
		pr.delete(id);
	}

	@Override
	public void delete(Permission perm) {
		pr.delete(perm);
	}

}
