package jmn.models;

import java.util.HashMap;

public class SifrarnikMetoda {
	
	public static HashMap<String, String> methods = new HashMap<String, String>();
	
	static {
		methods.put("login", "M1-1");
		methods.put("logout", "M1-2");
		methods.put("changePass", "M1-3");
		methods.put("updateBanku", "M2-1");
		methods.put("createBanku", "M2-2");
		methods.put("deleteBanku", "M2-3");
		methods.put("createRacun", "M2-4");
		methods.put("updateRacun", "M2-5");
		methods.put("deleteRacun", "M2-6");
		methods.put("createRacunPartnera", "M2-7");
		methods.put("updateRacunPartnera", "M2-8");
		methods.put("deleteRacunPartnera", "M2-9");
		methods.put("createStanja", "M3-1");
		methods.put("listAllStavkeIzvoda", "M3-2");
		methods.put("createPredlog", "M4-1");
		methods.put("updatePredlog", "M4-2");
		methods.put("deletePredlog", "M4-3");
		methods.put("createNalogZaPlacanje", "M4-4");
		methods.put("updateNalogZaPlacanje", "M4-5");
		methods.put("sendNalogZaPlacanje", "M4-6");
		methods.put("deleteNalogZaPlacanje", "M4-7");
		methods.put("updateUlaznaFaktura", "M5-1");
		methods.put("AutomatskoZatvaranje", "M5-2");
		methods.put("razvezi", "M5-3");
		methods.put("serveMestaTemplate", "M6-1");
		methods.put("serveDrzaveTemplate", "M6-2");
		methods.put("serveSifrarnikTemplate", "M6-3");
		methods.put("serveRacuniTemplate", "M6-4");
		methods.put("serveRacuniPartneraTemplate", "M6-5");
		methods.put("serveGodineTemplate", "M6-6");
		methods.put("servePartnerTemplate", "M6-7");
		methods.put("servePlacanjeTemplate", "M6-8");
		methods.put("servePredlogeTemplate", "M6-9");
		methods.put("serveNalogeTemplate", "M6-10");
		methods.put("serveFaktureTemplate", "M6-11");
		methods.put("serveAdreseTemplate", "M6-12");
		methods.put("serveBankeTemplate", "M6-13");
		methods.put("servePreduzeceTemplate", "M6-14");
		methods.put("serveDnevnaTemplate", "M6-15");
		methods.put("serveStavkeTemplate", "M6-16");
		methods.put("servesZatvaranjeUFTemplate", "M6-17");
		methods.put("servesZatvaranjeIFTemplate", "M6-18");
		methods.put("servestavkeFaktureTemplate", "M6-19");
	}

}
