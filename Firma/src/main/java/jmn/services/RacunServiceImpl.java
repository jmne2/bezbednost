package jmn.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jmn.models.Banka;
import jmn.models.Preduzece;
import jmn.models.Racun;
import jmn.repositories.RacunRepository;

@Service
public class RacunServiceImpl implements RacunService{

	@Autowired
	private RacunRepository rr;
	
	@Override
	public List<Racun> findAll() {
		// TODO Auto-generated method stub
		return (List<Racun>)rr.findAll();
	}

	@Override
	public Racun saveRacun(Racun r) {
		// TODO Auto-generated method stub
		return rr.save(r);
	}

	@Override
	public Racun findOne(Long id) {
		// TODO Auto-generated method stub
		return rr.findOne(id);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		rr.delete(id);
	}

	@Override
	public void delete(Racun r) {
		// TODO Auto-generated method stub
		rr.delete(r);
	}

	@Override
	public List<Racun> findByPreduzeceAndBanka(Preduzece preduzece, Banka banka) {
		// TODO Auto-generated method stub
		return rr.findByPreduzeceAndBanka(preduzece, banka);
	}

	@Override
	public List<Racun> findByPreduzece(Preduzece preduzece) {
		// TODO Auto-generated method stub
		return rr.findByPreduzece(preduzece);
	}

	@Override
	public Racun findByBrRacuna(String brRacuna) {
		return rr.findByBrRacuna(brRacuna);
	}

}
