package izvod.data;

import java.sql.Date;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import annotations.SqlDateAdapter;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "stavkaIzvodabanka", 
		namespace="http://izvod/data", 
		propOrder = {"uplatilac",
				 "svrhaPlacanja",
				 "primalac",
				 "datumNaloga", 
				 "datumValute",
				 "racunUplatioca",
				 "modelZaduzenja",
				 "pozivNaBrojZaduzenja",
				 "racunPrimaoca",
				 "modelOdobrenja",
				 "pozivNaBrojOdobrenja",
				 "iznos", 
				 "smer"})
public class StavkaIzvodaBanka implements java.io.Serializable{
//private static final long serialVersionUID = 4575678568872321L;
	
	@XmlTransient
	private int id;
	
	@XmlElement(name="uplatilac", required = true)
	@Size(min = 1, max = 255)
	private String uplatilac;
	
	@XmlElement(name="svrhaPlacanja", required = true)
	@Size(min = 1, max = 255)
	private String svrhaPlacanja;
	
	@XmlElement(name="primalac", required = true)
	@Size(min = 1, max = 255)
	private String primalac;

	@XmlElement(name="datumNaloga", required = true)
	@XmlJavaTypeAdapter(SqlDateAdapter.class)
	private Date datumNaloga;
	
	@XmlElement(name="datumValute", required = true)
	@XmlJavaTypeAdapter(SqlDateAdapter.class)
	private Date datumValute;
	
	@XmlElement(name="racunUplatioca", required = true)
	@Size(min = 18, max = 18)
	private String racunUplatioca;
	
	@XmlElement(name="modelZaduzenja", required = true)
	@Digits(integer = 2, fraction = 0)									
	private int modelZaduzenja;
	
	@XmlElement(name="pozivNaBrojZaduzenja", required = true)
	@Size(min = 1, max = 20)
	private String pozivNaBrojZaduzenja;
	
	@XmlElement(name="racunPrimaoca", required = true)
	@Size(min = 18, max = 18)
	private String racunPrimaoca;

	@XmlElement(name="modelOdobrenja", required = true)
	@Digits(integer = 2, fraction = 0)								
	private int modelOdobrenja;
	
	@XmlElement(name="pozivNaBrojOdobrenja", required = true)
	@Size(min = 1, max = 20)
	private String pozivNaBrojOdobrenja;
	
	@XmlElement(name="iznos", required = true)
	@Digits(integer = 15, fraction = 2)
	private double iznos;

	@XmlElement(name="smer", required = true)
	@Size(min = 1, max = 1)
	private String smer;				 //duguje , potrazuje
	
	public StavkaIzvodaBanka(){}
	
	
	public StavkaIzvodaBanka(String uplatilac, String svrhaPlacanja, String primalac, Date datumNaloga,
			Date datumValute, String racunUplatioca, int modelZaduzenja, String pozivNaBrojZaduzenja,
			String racunPrimaoca, int modelOdobrenja, String pozivNaBrojOdobrenja, double iznos, String smer) {
		super();
		this.uplatilac = uplatilac;
		this.svrhaPlacanja = svrhaPlacanja;
		this.primalac = primalac;
		this.datumNaloga = datumNaloga;
		this.datumValute = datumValute;
		this.racunUplatioca = racunUplatioca;
		this.modelZaduzenja = modelZaduzenja;
		this.pozivNaBrojZaduzenja = pozivNaBrojZaduzenja;
		this.racunPrimaoca = racunPrimaoca;
		this.modelOdobrenja = modelOdobrenja;
		this.pozivNaBrojOdobrenja = pozivNaBrojOdobrenja;
		this.iznos = iznos;
		this.smer = smer;
	}
	
	public String getUplatilac() {
		return uplatilac;
	}


	public void setUplatilac(String uplatilac) {
		this.uplatilac = uplatilac;
	}


	public String getSvrhaPlacanja() {
		return svrhaPlacanja;
	}


	public void setSvrhaPlacanja(String svrhaPlacanja) {
		this.svrhaPlacanja = svrhaPlacanja;
	}


	public String getPrimalac() {
		return primalac;
	}


	public void setPrimalac(String primalac) {
		this.primalac = primalac;
	}


	public Date getDatumNaloga() {
		return datumNaloga;
	}


	public void setDatumNaloga(Date datumNaloga) {
		this.datumNaloga = datumNaloga;
	}


	public Date getDatumValute() {
		return datumValute;
	}


	public void setDatumValute(Date datumValute) {
		this.datumValute = datumValute;
	}


	public String getRacunUplatioca() {
		return racunUplatioca;
	}


	public void setRacunUplatioca(String racunUplatioca) {
		this.racunUplatioca = racunUplatioca;
	}


	public int getModelZaduzenja() {
		return modelZaduzenja;
	}


	public void setModelZaduzenja(int modelZaduzenja) {
		this.modelZaduzenja = modelZaduzenja;
	}


	public String getPozivNaBrojZaduzenja() {
		return pozivNaBrojZaduzenja;
	}


	public void setPozivNaBrojZaduzenja(String pozivNaBrojZaduzenja) {
		this.pozivNaBrojZaduzenja = pozivNaBrojZaduzenja;
	}


	public String getRacunPrimaoca() {
		return racunPrimaoca;
	}


	public void setRacunPrimaoca(String racunPrimaoca) {
		this.racunPrimaoca = racunPrimaoca;
	}


	public int getModelOdobrenja() {
		return modelOdobrenja;
	}


	public void setModelOdobrenja(int modelOdobrenja) {
		this.modelOdobrenja = modelOdobrenja;
	}


	public String getPozivNaBrojOdobrenja() {
		return pozivNaBrojOdobrenja;
	}


	public void setPozivNaBrojOdobrenja(String pozivNaBrojOdobrenja) {
		this.pozivNaBrojOdobrenja = pozivNaBrojOdobrenja;
	}


	public double getIznos() {
		return iznos;
	}


	public void setIznos(double iznos) {
		this.iznos = iznos;
	}


	public String getSmer() {
		return smer;
	}


	public void setSmer(String smer) {
		this.smer = smer;
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	
}
