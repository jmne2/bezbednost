package jmn.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jmn.models.DEK;
import jmn.models.Preduzece;
import jmn.repositories.DEKRepository;

@Service
public class DEKServiceImpl implements DEKService{

	@Autowired
	private DEKRepository dkr;

	@Override
	public DEK findByPreduzece(Preduzece preduzece) {
		return dkr.findByPreduzece(preduzece);
	}

	@Override
	public DEK saveDEK(DEK dk) {
		return dkr.save(dk);
	}

	@Override
	public DEK findOne(Long id) {
		return dkr.findOne(id);
	}

	@Override
	public void delete(Long id) {
		dkr.delete(id);
	}
}
