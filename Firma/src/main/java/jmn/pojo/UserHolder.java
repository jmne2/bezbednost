package jmn.pojo;

public class UserHolder {

	private String uname;
	
	private String pname;

	public UserHolder() {
	}

	public UserHolder(String uname, String pname) {
		super();
		this.uname = uname;
		this.pname = pname;
	}

	public String getUname() {
		return uname;
	}

	public void setUname(String uname) {
		this.uname = uname;
	}

	public String getPname() {
		return pname;
	}

	public void setPname(String pname) {
		this.pname = pname;
	}
	
	
}
